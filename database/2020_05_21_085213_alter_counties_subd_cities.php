<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterCountiesSubdCities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('counties_subdivision_cities', function (Blueprint $table) {
            $table->float('lat',10,6)->nullable()->comment('County latitude')->after('county_id');
            $table->float('lng',10,6)->nullable()->comment('County longitude')->after('lat');
            $table->float('ne_lat',10,6)->nullable()->comment('NorthEast GeoCode used for borderline')->after('lng');
            $table->float('ne_lng',10,6)->nullable()->comment('NorthEast used for borderline')->after('ne_lat');
            $table->float('sw_lat',10,6)->nullable()->comment('SouthWest GeoCode used for borderline')->after('ne_lng');
            $table->float('sw_lng',10,6)->nullable()->comment('SouthWest GeoCode used for borderline')->after('sw_lat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('counties_subdivision_cities', function (Blueprint $table) {
            $table->dropColumn('lat');
            $table->dropColumn('lng');
            $table->dropColumn('ne_lat');
            $table->dropColumn('ne_lng');
            $table->dropColumn('sw_lat');
            $table->dropColumn('sw_lng');
        });
    }
}
