<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesEducationalAttainmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories_educational_attainment', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('census_group_variable');
            $table->string('census_variable_prefix');
            $table->timestamps();
            $table->softDeletes('deleted_at', 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories_educational_attainment', function (Blueprint $table) {
            //
        });
    }
}
