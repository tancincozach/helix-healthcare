<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePopulationBpovLvlLangSpokenStates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('population_bpov_lvl_lang_spkn_states', function (Blueprint $table) {
            $table->id();
            $table->foreignId('state_id')->comment('Foreign Key: states(id) table');
            $table->foreign('state_id')->references('id')->on('states');
            $table->foreignId('lang_category_id')->comment('Foreign Key: categories_language_spoken(id) table');
            $table->foreign('lang_category_id')->references('id')->on('categories_language_spoken'); 
            $table->string('language_spoken')->nullable()->comment('Language Category');                       
            $table->integer('population')->nullable()->comment('Population Below Poverty Level By Language Spoken');
            $table->integer('5_to_17_yr_population')->nullable()->comment('Population Below Poverty Level 5-17 years old By Language Spoken');
            $table->integer('18_above_population')->nullable()->comment('Population Below Poverty Level 18 years or above By Language Spoken');
            $table->string('acs_year',10)->comment('American Survey Year');
            $table->timestamps();
            $table->softDeletes('deleted_at', 0);
            $table->index('state_id');
            $table->index('lang_category_id');
            $table->index('acs_year');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('population_bpov_lvl_lang_spkn_states');
    }
}
