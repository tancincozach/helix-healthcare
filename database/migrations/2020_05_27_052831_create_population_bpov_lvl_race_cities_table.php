<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePopulationBpovLvlRaceCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('population_bpov_lvl_race_cities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('city_id')->comment('Foreign Key: cities(id) table');
            $table->foreign('city_id')->references('id')->on('cities');
            $table->foreignId('race_category_id')->comment('Foreign Key: categories_race(id) table');
            $table->foreign('race_category_id')->references('id')->on('categories_race');
            $table->string('race')->comment('Race Category');
            $table->integer('population')->nullable()->comment('Population Below Poverty Level By Race');
            $table->integer('population_male')->nullable()->comment('Population Below Poverty Level By Race');
            $table->integer('population_female')->nullable()->comment('Population Below Poverty Level By Race');
            $table->string('acs_year',10)->comment('American Survey Year');
            $table->timestamps();
            $table->softDeletes('deleted_at', 0);
            $table->index('city_id');
            $table->index('race_category_id');
            $table->index('acs_year');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('population_bpov_lvl_race_cities', function (Blueprint $table) {
            //
        });
    }
}
