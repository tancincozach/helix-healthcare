<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePopulationStates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('population_states', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('state_id')->nullable()->comment('Foreign Key: states(id) table');
            $table->foreign('state_id')->references('id')->on('states');
            $table->integer('population')->nullable()->comment('Total Population (State,Counties,Cities)');
            $table->string('acs_year')->nullable()->comment('Population Year');
            $table->timestamps();
            $table->softDeletes('deleted_at', 0);
            $table->index('state_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('population_states', function (Blueprint $table) {
            //
        });
    }
}
