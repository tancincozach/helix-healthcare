<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePopulationBpovLvlFamilyCities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('population_bpov_lvl_family_cities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('city_id')->comment('Foreign Key: cities(id) table');
            $table->foreign('city_id')->references('id')->on('cities');
            $table->string('family_category')->nullable()->comment('Family Category');
            $table->foreignId('family_category_id')->comment('Foreign Key: states(id) table');
            $table->foreign('family_category_id')->references('id')->on('categories_family'); 
            $table->integer('population')->nullable()->comment('Population Below Poverty Level By Family');
            $table->string('acs_year',10)->comment('American Survey Year');
            $table->timestamps();
            $table->softDeletes('deleted_at', 0);
            $table->index('city_id');
            $table->index('family_category_id');
            $table->index('acs_year');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('population_bpov_lvl_family_cities', function (Blueprint $table) {

        });
    }
}
