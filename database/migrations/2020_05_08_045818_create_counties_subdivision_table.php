<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountiesSubdivisionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('counties_subdivision', function (Blueprint $table) {
            $table->bigIncrements('id');            
            $table->foreignId('city_id')->nullable()->comment('Foreign Key: cities(id) table');
            $table->foreignId('county_id')->nullable()->comment('Foreign Key: counties(id) table');
            $table->foreign('county_id')->references('id')->on('counties');
            $table->integer('current_population')->comment('Current Population');
            $table->integer('census_cs_id')->comment('Census County Subdvision Reference ID');
            $table->timestamps();
            $table->softDeletes('deleted_at', 0);
            $table->index('city_id');
            $table->index('county_id');
            $table->index('census_cs_id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('counties_subdivision', function (Blueprint $table) {
            //
        });
    }
}
