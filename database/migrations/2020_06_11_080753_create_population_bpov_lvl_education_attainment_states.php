<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePopulationBpovLvlEducationAttainmentStates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('population_bpov_lvl_educ_attain_states', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('state_id')->comment('Foreign Key: states(id) table');
            $table->foreign('state_id')->references('id')->on('states');
            $table->foreignId('ed_category_id')->comment('Foreign Key: categories_educational_attainment(id) table');
            $table->foreign('ed_category_id')->references('id')->on('categories_educational_attainment'); 
            $table->string('educ_attainment')->nullable()->comment('Education Attainment Category');                       
            $table->integer('population')->nullable()->comment('Population Below Poverty Level By Education Attainment');
            $table->integer('population_male')->nullable()->comment('Total Male Population Below Poverty Level By Education Attainment');
            $table->integer('population_female')->nullable()->comment('Total Female Population Below Poverty Level By Education Attainment');
            $table->string('acs_year',10)->comment('American Survey Year');
            $table->timestamps();
            $table->softDeletes('deleted_at', 0);
            $table->index('state_id');
            $table->index('ed_category_id');
            $table->index('acs_year');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('population_bpov_lvl_educ_attain_states', function (Blueprint $table) {
            //
        });
    }
}
