<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePopUsRegions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('population_us_regions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('region_id')->nullable()->comment('Foreign Key: us_regions(id) table');
            $table->foreign('region_id')->references('id')->on('us_regions');
            $table->integer('population')->nullable()->comment('Total Population per Region');
            $table->string('pop_year','25')->nullable()->comment('Population Year');
            $table->timestamps();
            $table->softDeletes('deleted_at', 0);
            $table->index('region_id');
            $table->index('pop_year');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('population_us_regions', function (Blueprint $table) {
            //
        });
    }
}
