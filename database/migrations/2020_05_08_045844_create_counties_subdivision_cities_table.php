<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountiesSubdivisionCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('counties_subdivision_cities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('city_id')->nullable()->comment('Foreign Key: cities(id) table');
            $table->foreignId('cs_id')->nullable()->comment('Foreign Key: counties_subdivision(id) table');
            $table->foreign('cs_id')->references('id')->on('counties_subdivision');
            $table->integer('census_csc_id')->comment('Census Counties Subdivision Cities/Place/Town Reference Id');
            $table->timestamps();
            $table->softDeletes('deleted_at', 0);
            $table->index('cs_id');
            $table->index('census_csc_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('counties_subdivision_cities', function (Blueprint $table) {
            //
        });
    }
}
