<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePopulationByRaceCounties extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('population_by_race_counties', function (Blueprint $table) {
            $table->bigIncrements('id');            
            $table->foreignId('county_id')->nullable()->comment('Foreign Key: counties(id) table');
            $table->foreign('county_id')->references('id')->on('counties');            
            $table->text('race')->nullable()->comment('Race Category');
            $table->foreignId('race_category_id')->comment('Foreign Key: categories_race(id) table');
            $table->foreign('race_category_id')->references('id')->on('categories_race');
            $table->integer('population')->nullable()->comment('Total Population (State,Counties,Cities)');
            $table->timestamps();
            $table->softDeletes('deleted_at', 0);
            $table->index('county_id');
            $table->index('race');
            $table->index('race_category_id');      
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('population_by_race_counties', function (Blueprint $table) {
            //
        });
    }
}
