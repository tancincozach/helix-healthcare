<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePopCountiesSubdCities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('population_counties_subdivision_cities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('csc_id')->nullable()->comment('Foreign Key: counties_subdivision_cities(id) table');
            $table->foreign('csc_id')->references('id')->on('counties_subdivision_cities');
            $table->foreignId('state_id')->nullable()->comment('Foreign Key: states(id) table');
            $table->foreign('state_id')->references('id')->on('states');
            $table->foreignId('county_id')->nullable()->comment('Foreign Key: counties(id) table');
            $table->foreign('county_id')->references('id')->on('counties');
            $table->integer('population')->nullable()->comment('Total Population per County Subdivision Cities/Places/Towns');
            $table->string('pop_year','25')->nullable()->comment('Population Year');
            $table->timestamps();
            $table->softDeletes('deleted_at', 0);
            $table->index('csc_id');
            $table->index('state_id');
            $table->index('county_id');
            $table->index('pop_year');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('population_counties_subdivision_cities', function (Blueprint $table) {
            //
        });
    }
}
