<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePopulationBpovLvlEmploymentCounties extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('population_bpov_lvl_employment_counties', function (Blueprint $table) {
            $table->id();
            $table->foreignId('county_id')->comment('Foreign Key: counties(id) table');
            $table->foreign('county_id')->references('id')->on('counties');
            $table->foreignId('emp_category_id')->comment('Foreign Key: categories_employment(id) table');
            $table->foreign('emp_category_id')->references('id')->on('categories_employment'); 
            $table->string('employment')->nullable()->comment('Employment Category');                       
            $table->integer('population')->nullable()->comment('Population Below Poverty Level By Employment');
            $table->integer('population_male')->nullable()->comment('Total Male Population Below Poverty Level By Employment');
            $table->integer('population_female')->nullable()->comment('Total Female Population Below Poverty Level By Employment');
            $table->string('acs_year',10)->comment('American Survey Year');
            $table->timestamps();
            $table->softDeletes('deleted_at', 0);
            $table->index('county_id');
            $table->index('emp_category_id');
            $table->index('acs_year');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('population_bpov_lvl_employment_counties');
    }
}
