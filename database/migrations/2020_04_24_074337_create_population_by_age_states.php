<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePopulationByAgeStates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('population_by_age_states', function (Blueprint $table) {
            $table->bigIncrements('id');            
            $table->foreignId('state_id')->nullable()->comment('Foreign Key: states(id) table');
            $table->foreign('state_id')->references('id')->on('states');                                  
            $table->string('age_category')->nullable()->comment('Age Category');            
            $table->integer('population')->nullable()->comment('Total Population (State,Counties,Cities)');
            $table->integer('m_population')->nullable()->comment('Total Male Population');
            $table->integer('f_population')->nullable()->comment('Total Female Population');
            $table->string('acs_year',10)->nullable();
            $table->foreignId('age_category_id')->nullable()->comment('Foreign Key: categories_age(id) table');
            $table->foreign('age_category_id')->references('id')->on('categories_age');
            $table->timestamps();
            $table->softDeletes('deleted_at', 0);
            $table->index('state_id');
            $table->index('age_category_id');
            $table->index('age_category');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('population_by_age_states', function (Blueprint $table) {
            //
        });
    }
}
