<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePopulationEducationFieldRaceCounties extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('population_edu_fld_race_counties', function (Blueprint $table) {
                    $table->bigIncrements('id');
                    $table->foreignId('county_id')->comment('Foreign Key: counties(id) table');
                    $table->foreign('county_id')->references('id')->on('counties');
                    $table->foreignId('race_category_id')->comment('Foreign Key: categories_race(id) table');
                    $table->foreign('race_category_id')->references('id')->on('categories_race');
                    $table->foreignId('edu_fld_id')->comment('Foreign Key: categories_educational_attainment(id) table');
                    $table->foreign('edu_fld_id')->references('id')->on('categories_educational_attainment');                    
                    $table->string('race')->comment('Race Category');
                    $table->string('field_name')->comment('Education Bachelors Field Category');
                    $table->integer('population')->nullable()->comment('Population Below Poverty Level By Race');            
                    $table->string('acs_year',10)->comment('American Survey Year');
                    $table->timestamps();
                    $table->softDeletes('deleted_at', 0);
                    $table->index('county_id');
                    $table->index('race_category_id');
                    $table->index('edu_fld_id');
                    $table->index('acs_year');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('population_edu_fld_race_counties', function (Blueprint $table) {
            //
        });
    }
}
