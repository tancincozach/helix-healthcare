<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePopUsDivisions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('population_us_divisions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('division_id')->nullable()->comment('Foreign Key: us_division(id) table');
            $table->foreign('division_id')->references('id')->on('us_divisions');
            $table->integer('population')->nullable()->comment('Total Population per Division');
            $table->string('pop_year','25')->nullable()->comment('Population Year');
            $table->timestamps();
            $table->softDeletes('deleted_at', 0);
            $table->index('division_id');
            $table->index('pop_year');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('population_us_divisions', function (Blueprint $table) {
            //
        });
    }
}
