<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePopulationHouseholdSizeStates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('population_by_house_size_states', function (Blueprint $table) {
            $table->id();
            $table->foreignId('state_id')->comment('Foreign Key: states(id) table');
            $table->foreign('state_id')->references('id')->on('states');
            $table->foreignId('hhold_category_id')->comment('Foreign Key: categories_household_size(id) table');
            $table->foreign('hhold_category_id')->references('id')->on('categories_household_size'); 
            $table->string('household_size')->nullable()->comment('HouseHold Size Category');                       
            $table->integer('population')->nullable()->comment('Population Below Poverty Level By HouseHold Size');
            $table->string('acs_year',10)->comment('American Survey Year');
            $table->timestamps();
            $table->softDeletes('deleted_at', 0);
            $table->index('state_id');
            $table->index('hhold_category_id');
            $table->index('acs_year');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('population_by_house_size_states');
    }
}
