<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;


class CreateStateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('states', function (Blueprint $table) {            
            $table->bigIncrements('id');
            $table->string('census_state_id',10)->nullable()->comment('Generated ID from Census');
            $table->string('name',250);
            $table->float('lat',10,6)->nullable()->comment('State GeoCode');
            $table->float('lng',10,6)->nullable()->comment('State GeoCode');
            $table->float('ne_lat',10,6)->nullable()->comment('NorthEast GeoCode used for borderline');
            $table->float('ne_lng',10,6)->nullable()->comment('NorthEast used for borderline');
            $table->float('sw_lat',10,6)->nullable()->comment('SouthWest GeoCode used for borderline');
            $table->float('sw_lng',10,6)->nullable()->comment('SouthWest GeoCode used for borderline');
            $table->integer('current_acs_population')->nullable()->comment('Population per State');                               
            $table->string('acs_year',10)->nullable();
            $table->string('code',25)->nullable()->comment('Postal Code');                
            $table->index(['census_state_id','name']);
            $table->timestamps();
            $table->softDeletes('deleted_at', 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('states', function (Blueprint $table) {
            //
        });
    }
}
