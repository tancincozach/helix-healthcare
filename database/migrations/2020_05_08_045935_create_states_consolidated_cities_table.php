<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStatesConsolidatedCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('states_consolidated_cities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable()->comment('Consolidated City Name');
            $table->foreignId('state_id')->nullable()->comment('Foreign Key: states(id) table');
            $table->foreign('state_id')->references('id')->on('states');
            $table->integer('census_scc_id')->nullable()->comment('State Consolidated City Reference Id');
            $table->timestamps();
            $table->softDeletes('deleted_at', 0);
            $table->index('name');
            $table->index('state_id');
            $table->index('census_scc_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('states_consolidated_cities', function (Blueprint $table) {
            //
        });
    }
}
