<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePopulationCensusTractBlocks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('population_census_tract_blocks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('censust_block_id')->nullable()->comment('Foreign Key: census_tract_blocks(id) table');
            $table->foreign('censust_block_id')->references('id')->on('census_tract_blocks');
            $table->integer('population')->nullable()->comment('Total Population Census Tracts');
            $table->string('acs_year')->nullable()->comment('Population Year');
            $table->timestamps();
            $table->softDeletes('deleted_at', 0);
            $table->index('censust_block_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('population_census_tract_blocks');
    }
}
