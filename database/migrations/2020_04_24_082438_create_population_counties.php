<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePopulationCounties extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('population_counties', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('county_id')->nullable()->comment('Foreign Key: counties(id) table');
            $table->foreign( 'county_id')->references('id')->on('counties'); 
            $table->integer('population')->nullable()->comment('Total Population (State,Counties,Cities)');
            $table->string('acs_year')->nullable()->comment('Population Year');
            $table->timestamps();
            $table->softDeletes('deleted_at', 0);
            $table->index('county_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('population_counties', function (Blueprint $table) {
            //
        });
    }
}
