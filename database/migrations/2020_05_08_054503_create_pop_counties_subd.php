<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePopCountiesSubd extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('population_counties_subdivision', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('cs_id')->comment('Foreign Key: counties_subdivision(id) table');
            $table->foreign('cs_id')->references('id')->on('counties_subdivision');
            $table->foreignId('state_id')->comment('Foreign Key: states(id) table');
            $table->foreign('state_id')->references('id')->on('states');
            $table->foreignId('county_id')->nullable()->comment('Foreign Key: counties(id) table');
            $table->foreign('county_id')->references('id')->on('counties');
            $table->integer('population')->nullable()->comment('Total Population per County Subdivision');
            $table->string('pop_year','25')->nullable()->comment('Population Year');
            $table->timestamps();
            $table->softDeletes('deleted_at', 0);
            $table->index('cs_id');
            $table->index('state_id');
            $table->index('county_id');
            $table->index('pop_year');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('population_counties_subdivision', function (Blueprint $table) {
            //
        });
    }
}
