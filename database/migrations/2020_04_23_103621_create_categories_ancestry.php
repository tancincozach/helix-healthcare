<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesAncestry extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categrories_ancestry', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->string('group')->nullable()->comment('Group:Race,Tribe,.etc');
            $table->string('blacklist_group')->nullable()->comment('Avoided Group:Race,Tribe,.etc');
            $table->timestamps();
            $table->index(['name','group','blacklist_group']);
            $table->softDeletes('deleted_at', 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categrories_ancestry', function (Blueprint $table) {
            //
        });
    }
}
