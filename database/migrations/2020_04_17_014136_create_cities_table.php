<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('census_place_id')->unsigned();
            $table->string('name',250);
            $table->integer('state_id')->nullable()->comment('state foreign key');                        
            $table->float('lat',10,6)->nullable()->comment('State GeoCode');
            $table->float('lng',10,6)->nullable()->comment('State GeoCode');
            $table->float('ne_lat',10,6)->nullable()->comment('NorthEast GeoCode used for borderline');
            $table->float('ne_lng',10,6)->nullable()->comment('NorthEast used for borderline');
            $table->float('sw_lat',10,6)->nullable()->comment('SouthWest GeoCode used for borderline');
            $table->float('sw_lng',10,6)->nullable()->comment('SouthWest GeoCode used for borderline');
            $table->integer('current_acs_population')->nullable()->comment('Population per City');
            $table->string('acs_year',10)->nullable();
            $table->index(['state_id']);
            $table->timestamps();
            $table->softDeletes('deleted_at', 0);
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cities', function (Blueprint $table) {
            //
        });
    }
}
