<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePopStateConsolCities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('population_states_consolidated_cities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('scc_id')->nullable()->comment('Foreign Key: states_consolidated_cities(id) table');
            $table->foreign('scc_id')->references('id')->on('states_consolidated_cities');
            $table->foreignId('state_id')->nullable()->comment('Foreign Key: states(id) table');
            $table->foreign('state_id')->references('id')->on('states');
            $table->integer('population')->nullable()->comment('Total Population per Consolidated City');
            $table->string('pop_year','25')->nullable()->comment('Population Year');
            $table->timestamps();
            $table->softDeletes('deleted_at', 0);
            $table->index('scc_id');
            $table->index('state_id');
            $table->index('pop_year');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('population_states_consolidated_cities', function (Blueprint $table) {
            //
        });
    }
}
