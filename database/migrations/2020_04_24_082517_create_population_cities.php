<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePopulationCities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('population_cities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('city_id')->nullable()->comment('Foreign Key: cities(id) table');            
            $table->foreign('city_id')->references('id')->on('cities');    
            $table->integer('population')->nullable()->comment('Total Population (State,Counties,Cities)');
            $table->string('acs_year')->nullable()->comment('Population Year');
            $table->timestamps();
            $table->softDeletes('deleted_at', 0);
            $table->index('city_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('population_cities', function (Blueprint $table) {
            //
        });
    }
}
