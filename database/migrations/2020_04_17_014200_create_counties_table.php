<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('counties', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('census_county_id')->comment('Census Generated County ID');
            $table->integer('state_id')->comment('Foreign Key State ID');
            $table->string('name',250);
            $table->float('lat',10,6)->nullable()->comment('County latitude');
            $table->float('lng',10,6)->nullable()->comment('County longitude');
            $table->float('ne_lat',10,6)->nullable()->comment('NorthEast GeoCode used for borderline');
            $table->float('ne_lng',10,6)->nullable()->comment('NorthEast used for borderline');
            $table->float('sw_lat',10,6)->nullable()->comment('SouthWest GeoCode used for borderline');
            $table->float('sw_lng',10,6)->nullable()->comment('SouthWest GeoCode used for borderline');
            $table->integer('current_acs_population')->nullable()->comment('Population per county');
            $table->string('acs_year',10)->nullable();   
            $table->index(['state_id', 'census_county_id']);
            $table->timestamps();
            $table->softDeletes('deleted_at', 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('counties', function (Blueprint $table) {
            //
        });
    }
}
