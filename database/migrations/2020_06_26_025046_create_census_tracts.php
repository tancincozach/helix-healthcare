<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCensusTracts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('census_tracts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name',250);
            $table->string('tract_no')->comment('Census Bureau generated Tract ID');
            $table->string('geo_id')->comment('Census Bureau  generated GEO ID');
            $table->foreignId('county_id')->comment('Foreign Key: counties(id) table'); 
            $table->foreign('county_id')->references('id')->on('counties');
            $table->integer('current_acs_population')->nullable()->comment('Current Population');
            $table->string('acs_year',10)->nullable();
            $table->timestamps();
            $table->softDeletes('deleted_at', 0);
            $table->index('tract_no');
            $table->index('name');
            $table->index('county_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('census_tracts');
    }
}
