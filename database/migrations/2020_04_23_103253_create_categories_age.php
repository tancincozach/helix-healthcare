<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesAge extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories_age', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('census_group_variable')->nullable();
            $table->string('census_variable_prefix')->nullable();
            $table->timestamps();
            $table->softDeletes('deleted_at', 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('categories_age', function (Blueprint $table) {
            //
        });
    }
}
