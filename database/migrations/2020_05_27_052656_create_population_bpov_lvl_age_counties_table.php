<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePopulationBpovLvlAgeCountiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('population_bpov_lvl_age_counties', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('county_id')->comment('Foreign Key: counties(id) table');
            $table->foreign('county_id')->references('id')->on('counties');
            $table->foreignId('age_category_id')->comment('Foreign Key: categories_age(id) table');
            $table->foreign('age_category_id')->references('id')->on('categories_age');
            $table->string('age_category')->nullable()->comment('Age Category');
            $table->enum('gender',['M','F'])->nullable();
            $table->integer('population')->nullable()->comment('Population Below Poverty Level By Age');
            $table->string('acs_year',10)->comment('American Survey Year');            
            $table->timestamps();
            $table->softDeletes('deleted_at', 0);
            $table->index('county_id');
            $table->index('age_category_id');
            $table->index('acs_year');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('population_bpov_lvl_age_counties', function (Blueprint $table) {
            //
        });
    }
}
