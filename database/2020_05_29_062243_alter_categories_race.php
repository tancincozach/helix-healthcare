<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterCategoriesRace extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categories_race', function (Blueprint $table) {
            $table->string('other_census_variable_prefix')->nullable()->comment('Other Census Variable')->after('census_variable_prefix');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categories_race', function (Blueprint $table) {
            $table->dropColumn('other_census_variable_prefix');
        });
    }
}
