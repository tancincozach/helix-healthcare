<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


if (app()->getProvider('\Dingo\Api\Provider\LaravelServiceProvider')) {
    $api = app('Dingo\Api\Routing\Router');
    $api->version('v1', ['namespace' => 'App\Http\Controllers\Api'], function ($api) {

        $api->get('/unauthorized', 'AuthController@unauthorized');
        $api->post('/auth/register', 'AuthController@register');
        $api->post('/auth/token', 'AuthController@getToken');
        $api->post('login', 'AuthController@getToken');
        $api->post('register', 'AuthController@register');
        $api->post('/auth/check-auth', 'AuthController@check')->middleware('CheckClientCredentials','auth:api');
        $api->post('/auth/logout', 'AuthController@logout')->middleware('CheckClientCredentials','auth:api');
        $api->get('cron_geo', 'CensusCronGeoWebScrapeController@index');
        $api->post('track-address', 'GeoCodeController@index');
        
        $api->group(['prefix' => 'fetch_census_data'], function ($api) {
            $api->get('pop-by-age-race', 'WebScrapePopulationByAgeRace@index');
            $api->get('pop-by-age-poverty-status', 'WebScrapePopulationByPovertyStatus@index');
            $api->get('us-geo-data', 'CensusGeoWebScrapeController@index');
            $api->get('us-acs-data', 'CensusACSWebScrapeController@index');
        });

        $api->group(['namespace' => 'Secured', 'middleware' =>['CheckClientCredentials','auth:api']], function ($api) {
            $api->post('details', 'AuthController@details');
            $api->group(['prefix' => 'categories'], function ($api) {
                $api->get('age','AgeCategoriesController@index');
                $api->get('race','RaceCategoriesController@index');
            });
            
            $api->group(['prefix' => 'places'], function ($api) {
                $api->get('states','StatesController@index');
                $api->get('cities','CitiesController@index');                        
                $api->get('counties','CountiesController@index'); 
            });      
        
            $api->group(['prefix' => 'population'], function ($api) {                    
                    $api->get('states','PopulationByStateController@index');
                    $api->get('cities','PopulationByCityController@index');                        
                    $api->get('counties','PopulationByCountyController@index');
                });        
                
            $api->group(['prefix' => 'population-by-age'], function ($api) {
                    $api->get('states','PopulationByAgeStateController@index');
                    $api->get('cities','PopulationByAgeCityController@index');                
                    $api->get('counties','PopulationByAgeCountyController@index');

                    $api->get('/states/{id}', 'PopulationByAgeStateController@show');
                    $api->get('/cities/{id}', 'PopulationByAgeCityController@show');
                    $api->get('/counties/{id}', 'PopulationByAgeCountyController@show');
            });

            $api->group(['prefix' => 'population-by-race'], function ($api) {
                $api->get('states','PopulationByRaceStateController@index');
                $api->get('cities','PopulationByRaceCityController@index');                
                $api->get('counties','PopulationByRaceCountyController@index');

                $api->get('/states/{id}', 'PopulationByRaceStateController@show');
                $api->get('/cities/{id}', 'PopulationByRaceCityController@show');
                $api->get('/counties/{id}', 'PopulationByRaceCountyController@show');
            });

            $api->group(['prefix' => 'population-by-race/education-field'], function ($api) {
                    $api->get('states','PopulationByRaceEdFieldStateController@index');
                    $api->get('cities','PopulationByRaceEdFieldCityController@index');
                    $api->get('counties','PopulationByRaceEdFieldCountyController@index');
                    $api->get('/states/{id}', 'PopulationByRaceEdFieldStateController@show');
                    $api->get('/cities/{id}', 'PopulationByRaceEdFieldCityController@show');
                    $api->get('/counties/{id}', 'PopulationByRaceEdFieldCountyController@show');
            });

            $api->group(['prefix' => 'population-by-race/poverty-lvl/below'], function ($api) {                    
                    $api->get('states','PopulationByRaceBPovStateController@index');
                    $api->get('cities','PopulationByRaceBPovCityController@index');
                    $api->get('counties','PopulationByRaceBPovCountyController@index');
                    $api->get('/states/{id}', 'PopulationByRaceBPovStateController@show');
                    $api->get('/cities/{id}', 'PopulationByRaceBPovCityController@show');
                    $api->get('/counties/{id}', 'PopulationByRaceBPovCountyController@show');
            });
            $api->resource('explore','ExploreController');
            $api->resource('states', 'StatesController');
            $api->resource('counties', 'CountiesController');
            $api->resource('cities', 'CitiesController');
            // POPULATION RESOURCES(CITIES,STATE,COUNTIES)
            $api->resource('population-by-state', 'PopulationByStateController');
            $api->resource('population-by-city', 'PopulationByCityController');
            $api->resource('population-by-county', 'PopulationByCountyController');
    

            // RACE BY POPULATION RESOURCES(CITIES,STATE,COUNTIES)
            $api->resource('population-race-state', 'PopulationByRaceStateController');
            $api->resource('population-race-city', 'PopulationByRaceCityController');
            $api->resource('population-race-county', 'PopulationByRaceCountyController');

            
            // RACE BY POPULATION RESOURCES(CITIES,STATE,COUNTIES)
            $api->resource('population-bpov-race-state', 'PopulationByRaceBPovStateController');
            $api->resource('population-bpov-race-city', 'PopulationByRaceBPovCityController');
            $api->resource('population-bpov-race-county', 'PopulationByRaceBPovCountyController');

            
            
            // RACE EDUCATION FIELD POPULATION RESOURCES(CITIES,STATE,COUNTIES)
            $api->resource('population-race-edu-field-state', 'PopulationByRaceEdFieldStateController');
            $api->resource('population-race-edu-field-city', 'PopulationByRaceEdFieldCityController');
            $api->resource('population-race-edu-field-county', 'PopulationByRaceEdFieldCountyController');
            
            // POPULATION AGE RESOURCES(CITIES,STATE,COUNTIES)
            $api->resource('population-by-age-state', 'PopulationByAgeStateController');
            $api->resource('population-by-age-county', 'PopulationByAgeCountyController');
            $api->resource('population-by-age-city', 'PopulationByAgeCityController');   
            $api->resource('age-categories', 'AgeCategoriesController'); 
        });      



     
    });
}
    