export default {
    data() {
        return {
            query: {
                filter: null,
                sorting: null,
                search: null,
                searchBy: null,
                pagination: {
                    currentPage: 1,
                    limit: 20       //limit is the same as perPage - variable:  this.query.pagination.limit
                }
            },
            config: {
                pagination: {
                    lastPage: '',
                    currentPage: 1,
                    pages: 1,
                    total: 0,
                    perPage: 20,       //perPage is the same as limit - variable:  this.config.pagination.perPage
                    nextPageUrl: null,
                    prevPageUrl: null,
                    from: 0,
                    to: 0
                }
            }
        }
    },
    computed: {
        paginationFrom: function() {
            return (this.config.pagination.currentPage - 1) * this.config.pagination.perPage + 1;
        },
        paginationPages: function() {
            return Math.ceil(this.config.pagination.total / this.config.pagination.perPage);
        },
        paginationTo: function() {
            let to = this.config.pagination.from * this.config.pagination.perPage;

            return to <= this.config.pagination.total
                ? to
                : this.config.pagination.total;
        }
    },
    methods: {
        alternateBgColor: function(index) {
            return index % 2 == 0 ? 'even' : 'odd';
        },
        setPagination(data) {
            if (data) {
                this.config.pagination.lastPage = data.last_page;
                this.config.pagination.currentPage = data.current_page;
                this.config.pagination.total = data.total;
                this.config.pagination.perPage = data.per_page;       //perPage is the same as limit
                this.config.pagination.nextPageUrl = data.links.next;
                this.config.pagination.prevPageUrl = data.links.previous;
                this.config.pagination.from = this.paginationFrom;
                this.config.pagination.to =this.paginationTo;
                this.config.pagination.pages = this.paginationPages;
                this.query.pagination.currentPage = data.current_page;
            }
        },
        setFilter(filters) {
            this.query.filter = filters;
        },
        setSorting(orderBy) {
            this.query.sorting = orderBy;
        },
        setSearch(searches) {
            this.query.search = searches;
        },
        setSearchBy(searchBy) {
            this.query.searchBy = searchBy;
        },
        setPaginationLimit(limit) {        //limit is the same as perPage
            this.query.pagination.limit = limit;
            this.query.pagination.currentPage =
            (Math.ceil(this.config.pagination.total / limit) < this.query.pagination.currentPage) ?
                Math.ceil(this.config.pagination.total / limit):
                this.query.pagination.currentPage;
        },
        getParams() {
            let params = {
                take: this.query.pagination.limit,
                page: this.query.pagination.currentPage
            };

            // see if we have sorting
            if (this.query.sorting) {
                params['orderBy'] = this.query.sorting;
            }

            // see if we have filters
            if (this.query.filter) {
                params['filters'] = this.query.filter;
            }

            // see if we have searches
            if (this.query.search) {
                params['search'] = this.query.search;
            }

            // see if we have search by options
            if (this.query.searchBy) {
                params['searchBy'] = this.query.searchBy;
            }

            return params;
        },

        gotoPage(page) {
            if (page == 'prev') {
                // if previous page
                this.query.pagination.currentPage -= 1;
            } else if (page == 'next') {
                // if next page
                this.query.pagination.currentPage += 1;
            } else {
                // otherwise, go to page as specicifed
                this.query.pagination.currentPage = page;
            }
        },

        /**
         * Insert a div with width=100%, height with specified after the element that being represented by the selector
         * It's like, insert a new div that element (selector) with width=100% and height specified.
         * @param selector
         * @param height
         */
        async insertSpacer(selector,height,className='spacer'){
            let $ = jQuery;
            let $newDiv = $("<div>",{"class" : className});
            $newDiv.css({width: '100%', height: height});
            $(selector).after($newDiv);
        },

        async freezeElement(selector){
            let $ = jQuery;
            $(selector).css({position: 'fixed'});
        },

        async insertStyle(selector,css){
            $(selector).css(css);
        }
    }
}
