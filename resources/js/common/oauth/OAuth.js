import Client from '../Client';
import config from './config';

let instance = null;

class OAuth {
    constructor() {
        if (instance) {
            return instance;
        }

        this.instance = this;
    }

    // get token
    getToken() {
        return localStorage.getItem(config.auth_token);
    }

    // authenticate user
    authenticate(creds) {
        return Client.request({
            method: 'post',
            url: `${config.url}/token`,
            data: _.assign(creds, config.creds)
        }).then((res) => {
            localStorage.setItem(config.auth_token, res.data.access_token);
            return true;
        }).catch((err) => {
            // invalid user
            return false;
        })
    }

    // get headers
    getHeaders() {
        const token = this.getToken();
        let headers = {};

        if (token) {
            headers['Authorization'] = `Bearer ${token}`;
        }
        return headers;
    }

    // check if user is authenticated
    isAuthenticated() {        
        return Client.request({
            method: 'post',
            url: `${config.url}/check-auth`,
            headers: this.getHeaders()
        }).then((res) => {
            return !!res.data.authenticated;
        }, error => {
            if (error.response.status === 401) {
                Promise.reject(error);
            }else{
                return Promise.reject(error);
            }
          });
    }

    // logout user
    // invalidate token
    logout() {
        return Client.request({
            method: 'post',
            url: `${config.url}/logout`,
            headers: this.getHeaders()
        }).then((err) => {
            // remove stored access_token
            localStorage.removeItem(config.auth_token);
            return true;
        }).catch((err) => {
            return false;
        })
    } 
    // logout user
    // invalidate token
    register(creds) {
        return Client.request({
            method: 'post',
            url: `${config.url}/register`,
            data: creds,
            json: true
        });
    }
    

}

export default new OAuth();
