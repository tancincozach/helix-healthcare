import { BaseModel } from '@/common/model/BaseModel';
import PopulationByCityResource from '@/common/resource/PopulationByCityResource';

export class PopulationByCity extends BaseModel {
    constructor(data) {
        let relations = {
            'data': PopulationByCity
        };

        super(data, relations);
    }

    static get resource() {
        return PopulationByCityResource;
    }
}
