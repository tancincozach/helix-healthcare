import { BaseModel } from '@/common/model/BaseModel';
import PopulationByStateResource from '@/common/resource/PopulationByStateResource';

export class PopulationByState extends BaseModel {
    constructor(data) {
        let relations = {
            'data': PopulationByState
        };

        super(data, relations);
    }

    static get resource() {
        return PopulationByStateResource;
    }
}
