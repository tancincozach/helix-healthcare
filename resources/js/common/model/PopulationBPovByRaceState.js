import { BaseModel } from '@/common/model/BaseModel';
import PopulationBPovByRaceStateResource from '@/common/resource/PopulationBPovByRaceStateResource';

export class PopulationBPovByRaceState extends BaseModel {
    constructor(data) {
        let relations = {
            'data': PopulationBPovByRaceState
        };

        super(data, relations);
    }

    static get resource() {
        return PopulationBPovByRaceStateResource;
    }
}
