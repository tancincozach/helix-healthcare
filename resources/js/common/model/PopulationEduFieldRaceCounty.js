import { BaseModel } from '@/common/model/BaseModel';
import PopulationEduFieldRaceCountyResource from '@/common/resource/PopulationEduFieldRaceCountyResource';

export class PopulationEduFieldRaceCounty extends BaseModel {
    constructor(data) {
        let relations = {
            'data': PopulationEduFieldRaceCounty
        };

        super(data, relations);
    }

    static get resource() {
        return PopulationEduFieldRaceCountyResource;
    }
}
