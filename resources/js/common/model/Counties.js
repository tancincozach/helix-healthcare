import { BaseModel } from '@/common/model/BaseModel';
import CountiesResource from '@/common/resource/CountiesResource';

export class Counties extends BaseModel {
    constructor(data) {
        let relations = {
            'data': Counties
        };

        super(data, relations);
    }

    static get resource() {
        return CountiesResource;
    }
}
