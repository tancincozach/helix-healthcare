import { BaseModel } from '@/common/model/BaseModel';
import PopulationByRaceStateResource from '@/common/resource/PopulationByRaceStateResource';

export class PopulationByRaceState extends BaseModel {
    constructor(data) {
        let relations = {
            'data': PopulationByRaceState
        };

        super(data, relations);
    }

    static get resource() {
        return PopulationByRaceStateResource;
    }
}
