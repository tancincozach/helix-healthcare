import { BaseModel } from '@/common/model/BaseModel';
import PopulationBPovByRaceCityResource from '@/common/resource/PopulationBPovByRaceCityResource';

export class PopulationBPovByRaceCity extends BaseModel {
    constructor(data) {
        let relations = {
            'data': PopulationBPovByRaceCity
        };

        super(data, relations);
    }

    static get resource() {
        return PopulationBPovByRaceCityResource;
    }
}
