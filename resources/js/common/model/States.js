import { BaseModel } from '@/common/model/BaseModel';
import StatesResource from '@/common/resource/StatesResource';

export class States extends BaseModel {
    constructor(data) {
        let relations = {
            'data': States
        };

        super(data, relations);
    }

    static get resource() {
        return StatesResource;
    }
}
