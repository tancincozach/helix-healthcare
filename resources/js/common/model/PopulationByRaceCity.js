import { BaseModel } from '@/common/model/BaseModel';
import PopulationByRaceCityResource from '@/common/resource/PopulationByRaceCityResource';

export class PopulationByRaceCity extends BaseModel {
    constructor(data) {
        let relations = {
            'data': PopulationByRaceCity
        };

        super(data, relations);
    }

    static get resource() {
        return PopulationByRaceCityResource;
    }
}
