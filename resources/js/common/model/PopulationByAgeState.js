import { BaseModel } from '@/common/model/BaseModel';
import PopulationByAgeStateResource from '@/common/resource/PopulationByAgeStateResource';

export class PopulationByAgeState extends BaseModel {
    constructor(data) {
        let relations = {
            'data': PopulationByAgeState
        };

        super(data, relations);
    }

    static get resource() {
        return PopulationByAgeStateResource;
    }
}
