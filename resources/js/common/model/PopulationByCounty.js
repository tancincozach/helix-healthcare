import { BaseModel } from '@/common/model/BaseModel';
import PopulationByCountyResource from '@/common/resource/PopulationByCountyResource';

export class PopulationByCounty extends BaseModel {
    constructor(data) {
        let relations = {
            'data': PopulationByCounty
        };

        super(data, relations);
    }

    static get resource() {
        return PopulationByCountyResource;
    }
}
