import { BaseModel } from '@/common/model/BaseModel';
import PopulationEduFieldRaceStateResource from '@/common/resource/PopulationEduFieldRaceStateResource';

export class PopulationEduFieldRaceState extends BaseModel {
    constructor(data) {
        let relations = {
            'data': PopulationEduFieldRaceState
        };

        super(data, relations);
    }

    static get resource() {
        return PopulationEduFieldRaceStateResource;
    }
}
