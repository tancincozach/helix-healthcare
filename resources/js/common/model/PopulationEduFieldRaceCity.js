import { BaseModel } from '@/common/model/BaseModel';
import PopulationEduFieldRaceCityResource from '@/common/resource/PopulationEduFieldRaceCityResource';

export class PopulationEduFieldRaceCity extends BaseModel {
    constructor(data) {
        let relations = {
            'data': PopulationEduFieldRaceCity
        };

        super(data, relations);
    }

    static get resource() {
        return PopulationEduFieldRaceCityResource;
    }
}
