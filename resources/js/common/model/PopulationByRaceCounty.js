import { BaseModel } from '@/common/model/BaseModel';
import PopulationByRaceCountyResource from '@/common/resource/PopulationByRaceCountyResource';

export class PopulationByRaceCounty extends BaseModel {
    constructor(data) {
        let relations = {
            'data': PopulationByRaceCounty
        };

        super(data, relations);
    }

    static get resource() {
        return PopulationByRaceCountyResource;
    }
}
