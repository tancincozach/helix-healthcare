import { BaseModel } from '@/common/model/BaseModel';
import PopulationBPovByRaceCountyResource from '@/common/resource/PopulationBPovByRaceCountyResource';

export class PopulationBPovByRaceCounty extends BaseModel {
    constructor(data) {
        let relations = {
            'data': PopulationBPovByRaceCounty
        };

        super(data, relations);
    }

    static get resource() {
        return PopulationBPovByRaceCountyResource;
    }
}
