import { BaseModel } from '@/common/model/BaseModel';
import PopulationByAgeCountyResource from '@/common/resource/PopulationByAgeCountyResource';

export class PopulationByAgeCounty extends BaseModel {
    constructor(data) {
        let relations = {
            'data': PopulationByAgeCounty
        };

        super(data, relations);
    }

    static get resource() {
        return PopulationByAgeCountyResource;
    }
}
