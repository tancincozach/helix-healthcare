import { BaseModel } from '@/common/model/BaseModel';
import ExploreResultResource from '@/common/resource/ExploreResultResource';

export class ExploreResult extends BaseModel {
    constructor(data) {
        let relations = {
            'data': ExploreResult
        };

        super(data, relations);
    }

    static get resource() {
        return ExploreResultResource;
    }
}
