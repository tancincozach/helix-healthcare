import { BaseModel } from '@/common/model/BaseModel';
import PopulationByAgeCityResource from '@/common/resource/PopulationByAgeCityResource';

export class PopulationByAgeCity extends BaseModel {
    constructor(data) {
        let relations = {
            'data': PopulationByAgeCity
        };

        super(data, relations);
    }

    static get resource() {
        return PopulationByAgeCityResource;
    }
}
