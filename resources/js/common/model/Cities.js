import { BaseModel } from '@/common/model/BaseModel';
import CitiesResource from '@/common/resource/CitiesResource';

export class Cities extends BaseModel {
    constructor(data) {
        let relations = {
            'data': Cities
        };

        super(data, relations);
    }

    static get resource() {
        return CitiesResource;
    }
}
