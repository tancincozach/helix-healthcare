import { BaseResource } from '@/common/resource/BaseResource';

class PopulationEduFieldRaceCountyResource extends BaseResource {
    constructor() {
        let options = {
            url: '/population-race-edu-field-county/:id'
        };

        super(options);
    }
}

export default new PopulationEduFieldRaceCountyResource();
