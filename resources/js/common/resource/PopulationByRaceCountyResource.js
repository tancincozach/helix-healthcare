import { BaseResource } from '@/common/resource/BaseResource';

class PopulationByRaceCountyResource extends BaseResource {
    constructor() {
        let options = {
            url: '/population-race-county/:id'
        };

        super(options);
    }
}

export default new PopulationByRaceCountyResource();
