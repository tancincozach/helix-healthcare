import { BaseResource } from '@/common/resource/BaseResource';

class ExploreResultResource extends BaseResource {
    constructor() {
        let options = {
            url: '/explore/'
        };

        super(options);
    }
}

export default new ExploreResultResource();
