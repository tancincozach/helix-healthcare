import { BaseResource } from '@/common/resource/BaseResource';

class PopulationByAgeStateResource extends BaseResource {
    constructor() {
        let options = {
            url: '/population-by-age-state/:id'
        };

        super(options);
    }
}

export default new PopulationByAgeStateResource();
