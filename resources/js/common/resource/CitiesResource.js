import { BaseResource } from '@/common/resource/BaseResource';

class CitiesResource extends BaseResource {
    constructor() {
        let options = {
            url: '/cities/:id'
        };

        super(options);
    }
}

export default new CitiesResource();
