import { BaseResource } from '@/common/resource/BaseResource';

class StatesResource extends BaseResource {
    constructor() {
        let options = {
            url: '/states/:id'
        };

        super(options);
    }
}

export default new StatesResource();
