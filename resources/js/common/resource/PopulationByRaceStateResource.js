import { BaseResource } from '@/common/resource/BaseResource';

class PopulationByRaceStateResource extends BaseResource {
    constructor() {
        let options = {
            url: '/population-race-state/:id'
        };

        super(options);
    }
}

export default new PopulationByRaceStateResource();
