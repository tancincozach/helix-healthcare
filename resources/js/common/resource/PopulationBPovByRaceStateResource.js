import { BaseResource } from '@/common/resource/BaseResource';

class PopulationBPovByRaceStateResource extends BaseResource {
    constructor() {
        let options = {
            url: '/population-bpov-race-state/:id'
        };

        super(options);
    }
}

export default new PopulationBPovByRaceStateResource();
