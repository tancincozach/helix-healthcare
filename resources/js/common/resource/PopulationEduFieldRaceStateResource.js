import { BaseResource } from '@/common/resource/BaseResource';

class PopulationEduFieldRaceStateResource extends BaseResource {
    constructor() {
        let options = {
            url: '/population-race-edu-field-state/:id'
        };

        super(options);
    }
}

export default new PopulationEduFieldRaceStateResource();
