import { BaseResource } from '@/common/resource/BaseResource';

class PopulationByCountyResource extends BaseResource {
    constructor() {
        let options = {
            url: '/population-by-county/:id'
        };

        super(options);
    }
}

export default new PopulationByCountyResource();
