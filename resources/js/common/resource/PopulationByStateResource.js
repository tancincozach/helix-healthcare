import { BaseResource } from '@/common/resource/BaseResource';

class PopulationByStateResource extends BaseResource {
    constructor() {
        let options = {
            url: '/population-by-state/:id'
        };

        super(options);
    }
}

export default new PopulationByStateResource();
