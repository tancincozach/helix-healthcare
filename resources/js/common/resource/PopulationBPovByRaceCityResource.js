import { BaseResource } from '@/common/resource/BaseResource';

class PopulationBPovByRaceCityResource extends BaseResource {
    constructor() {
        let options = {
            url: '/population-bpov-race-city/:id'
        };

        super(options);
    }
}

export default new PopulationBPovByRaceCityResource();
