import { BaseResource } from '@/common/resource/BaseResource';

class CountiesResource extends BaseResource {
    constructor() {
        let options = {
            url: '/counties/:id'
        };

        super(options);
    }
}

export default new CountiesResource();
