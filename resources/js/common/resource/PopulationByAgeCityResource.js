import { BaseResource } from '@/common/resource/BaseResource';

class PopulationByAgeCityResource extends BaseResource {
    constructor() {
        let options = {
            url: '/population-by-age-city/:id'
        };

        super(options);
    }
}

export default new PopulationByAgeCityResource();
