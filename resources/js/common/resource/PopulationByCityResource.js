import { BaseResource } from '@/common/resource/BaseResource';

class PopulationByCityResource extends BaseResource {
    constructor() {
        let options = {
            url: '/population-by-city/:id'
        };

        super(options);
    }
}

export default new PopulationByCityResource();
