import { BaseResource } from '@/common/resource/BaseResource';

class PopulationBPovByRaceCountyResource extends BaseResource {
    constructor() {
        let options = {
            url: '/population-bpov-race-county/:id'
        };

        super(options);
    }
}

export default new PopulationBPovByRaceCountyResource();
