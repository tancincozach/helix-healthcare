import { BaseResource } from '@/common/resource/BaseResource';

class PopulationByRaceCityResource extends BaseResource {
    constructor() {
        let options = {
            url: '/population-race-city/:id'
        };

        super(options);
    }
}

export default new PopulationByRaceCityResource();
