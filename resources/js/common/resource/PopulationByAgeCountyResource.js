import { BaseResource } from '@/common/resource/BaseResource';

class PopulationByAgeCountyResource extends BaseResource {
    constructor() {
        let options = {
            url: '/population-by-age-county/:id'
        };

        super(options);
    }
}

export default new PopulationByAgeCountyResource();
