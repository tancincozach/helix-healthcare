import { BaseResource } from '@/common/resource/BaseResource';

class PopulationEduFieldRaceCityResource extends BaseResource {
    constructor() {
        let options = {
            url: '/population-race-edu-field-city/:id'
        };

        super(options);
    }
}

export default new PopulationEduFieldRaceCityResource();
