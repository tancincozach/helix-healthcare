import NProgress from 'nprogress';
import Vue from 'vue'
import Router from 'vue-router'
import OAuth from '@/common/oauth/OAuth';
import { ability } from '@/common/oauth/ability.js';
import defineAbilityFor from '@/common/oauth/ability';
//Container

const TheContainer = () => import('@/layouts/TheContainer')

// Views
const Dashboard = () => import('@/views/pages/Dashboard')

const States = () => import('@/views/pages/States')

const Cities = () => import('@/views/pages/Cities')

const Counties = () => import('@/views/pages/Counties')


const PopByState = () => import('@/views/pages/PopulationByState')

const PopByCity = () => import('@/views/pages/PopulationByCity')

const PopByCounty = () => import('@/views/pages/PopulationByCounty')

const PopByAge = () => import('@/views/pages/PopulationByAge')

const PopByRace = () => import('@/views/pages/PopulationByRace')



const Colors = () => import('@/components/theme/Colors')

const Typography = () => import('@/components/theme/Typography')

const Charts = () => import('@/components/charts/Charts')
const Widgets = () => import('@/components/widgets/Widgets')

// Views - Components
const Cards = () => import('@/components/base/Cards')
const Forms = () => import('@/components/base/Forms')
const Switches = () => import('@/components/base/Switches')
// const Tables = () => import('@/components/base/Tables')
const Tabs = () => import('@/components/base/Tabs')
const Breadcrumbs = () => import('@/components/base/Breadcrumbs')
const Carousels = () => import('@/components/base/Carousels')
const Collapses = () => import('@/components/base/Collapses')
const Jumbotrons = () => import('@/components/base/Jumbotrons')
const ListGroups = () => import('@/components/base/ListGroups')
const Navs = () => import('@/components/base/Navs')
const Navbars = () => import('@/components/base/Navbars')
const Paginations = () => import('@/components/base/Paginations')
const Popovers = () => import('@/components/base/Popovers')
const ProgressBars = () => import('@/components/base/ProgressBars')
const Tooltips = () => import('@/components/base/Tooltips')

// Views - Buttons
const StandardButtons = () => import('@/components/buttons/StandardButtons')
const ButtonGroups = () => import('@/components/buttons/ButtonGroups')
const Dropdowns = () => import('@/components/buttons/Dropdowns')
const BrandButtons = () => import('@/components/buttons/BrandButtons')

// Views - Icons
const CoreUIIcons = () => import('@/components/icons/CoreUIIcons')
const Brands = () => import('@/components/icons/Brands')
const Flags = () => import('@/components/icons/Flags')

//Views - Notifications
const Alerts = () => import('@/components/notifications/Alerts')
const Badges = () => import('@/components/notifications/Badges')
const Modals = () => import('@/components/notifications/Modals')

// Views - Pages
const Page404 = () => import('@/views/pages/Page404')
const Page500 = () => import('@/views/pages/Page500')
const Login = () => import('@/views/pages/Login')
const Register = () => import('@/views/pages/Register')

// Users
const Users = () => import('@/components/users/Users')
const User = () => import('@/components/users/User')

Vue.use(Router)
function configRoutes () {
  return [
    {
      path: '/',
      redirect: '/dashboard',
      name: 'Home',
      component: TheContainer,
      meta: {
        requiresAuth: true,
      },
      children: [
        {
          path: 'dashboard',
          name: 'Dashboard',
          component: Dashboard
        },
        {
          path: 'users',
          meta: {
            label: 'Users'
          },
          component: {
            render(c) {
              return c('router-view')
            }
          },
          children: [
            {
              path: '',
              name: 'Users',
              component: Users
            },
            {
              path: ':id',
              meta: {
                label: 'User Details'
              },
              name: 'User',
              component: User
            }
          ]
        },
        {
          path: 'states',
          name: 'States',
          component:States
        },
        {
          path: 'counties',
          name: 'Counties',
          component:Counties
        },
        {
          path: 'cities',
          name: 'Cities',
          component:Cities
        },
        {
          path: 'population-by-county',
          name: 'Population By County',
          component:PopByCounty
        },
        {
          path: 'population-by-state',
          name: 'Population By State',
          component:PopByState
        },
        {
          path: 'population-by-city',
          name: 'Population By City',
          component:PopByCity
        },
        ,
        {
          path: 'population-by-age',
          name: 'Population By Age',
          component:PopByAge,
          children: [
                {
                  path: 'population-by-age/state/',
                  name: 'State',
                  component:PopByAge,
                },
                
          ]
        },
        {
          path: 'population-by-race',
          name: 'Population By Race',
          component:PopByRace
        },
        {
          path: 'icons',
          redirect: '/icons/coreui-icons',
          name: 'CoreUI Icons',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'coreui-icons',
              name: 'Icons library',
              component: CoreUIIcons
            },
            {
              path: 'brands',
              name: 'Brands',
              component: Brands
            },
            {
              path: 'flags',
              name: 'Flags',
              component: Flags
            }
          ]
        }
      ]
    },
    {
      path: '/pages',
      redirect: '/pages/404',
      name: 'Pages',
      component: {
        render (c) { return c('router-view') }
      },
      children: [
        {
          path: '/404',
          name: 'Page404',
          component: Page404
        },
        {
          path: '500',
          name: 'Page500',
          component: Page500
        },
        {
          path: '/login',
          name: 'Login',
          meta: {
            requiresGuest: true,
          },
          component: Login
        },
        {
          path: '/register',
          name: 'Register',
          meta: {
            requiresGuest: true,
          },
          component: Register
        }
      ]
    }
  ]
}

const router = new Router({
  linkActiveClass: 'active',
  mode: 'history',
  scrollBehavior: () => ({ y: 0 }),
  routes: configRoutes()
});

// Auth Guards
router.beforeEach((to, from, next) => {
  // This page requires authentication
//   console.log('before');
  if (to.matched.some(m => m.meta.requiresAuth)) {
      return OAuth.isAuthenticated().then(response => {
          if (!response) {
              return next({ path: '/login' });
          }          
          return next();
      });
  }
 
   if (to.matched.some(m => m.meta.requiresGuest)) {
        let oauthStatus = OAuth.isAuthenticated().then(response => {
          if(response){
            return next({ path: '/' });
          }
        },function(error) {
         
        });
  }
  return next();
});

// Add page loader
router.beforeResolve((to, from, next) => {
  // if this isn't an initial page load
  if (to.name) {
      // start the route progress bar
      NProgress.start();
  }
  next();
});

router.afterEach((to, from) => {
  // complete the animation of the route progress bar.
  NProgress.done();
});

export default router;


