import Vue from "vue";
import Vuex from "vuex";

// modules
import userModule from "@/store/modules/user/index";
import authModule from "@/store/modules/auth/index";
import statesModule from "@/store/modules/states/index";
import countiesModule from "@/store/modules/counties/index";
import citiesModule from "@/store/modules/cities/index";
import popByStateModule from "@/store/modules/population/states/index";
import popByCountiesModule from "@/store/modules/population/counties/index";
import popByCitiesModule from "@/store/modules/population/cities/index";
import popByAgeStatesModule from "@/store/modules/population-by-age/states/index";
import popByAgeCountiesModule from "@/store/modules/population-by-age/counties/index";
import popByAgeCitiesModule from "@/store/modules/population-by-age/cities/index";
import exploreModule from "@/store/modules/explore/index";
import raceStateModule from "@/store/modules/race/states/index";
import raceCountiesModule from "@/store/modules/race/counties/index";
import raceCitiesModule from "@/store/modules/race/cities/index";
import raceBelowPovertyStatesModule from "@/store/modules/race-bpov-lvl/states/index";
import raceBelowPovertyCountiesModule from "@/store/modules/race-bpov-lvl/counties/index";
import raceBelowPovertyCitiesModule from "@/store/modules/race-bpov-lvl/cities/index";
import raceEduFieldStatesModule from "@/store/modules/race-education-field/states/index";
import raceEduFieldCountiesModule from "@/store/modules/race-education-field/counties/index";
import raceEduFieldCitiesModule from "@/store/modules/race-education-field/cities/index";

// -------------------------------------------------------------------------

Vue.use(Vuex);

import createPersistedState from "vuex-persistedstate";
import * as Cookies from "js-cookie";

const state = {
    sidebarShow: 'responsive',
    sidebarMinimize: false
  }
  
const mutations = {
    toggleSidebarDesktop (state) {
      const sidebarOpened = [true, 'responsive'].includes(state.sidebarShow)
      state.sidebarShow = sidebarOpened ? false : 'responsive'
    },
    toggleSidebarMobile (state) {
      const sidebarClosed = [false, 'responsive'].includes(state.sidebarShow)
      state.sidebarShow = sidebarClosed ? true : 'responsive'
    },
    set (state, [variable, value]) {
      state[variable] = value
    }
  }
  

export default new Vuex.Store({
    modules: {
        states: statesModule,
        counties: countiesModule,
        cities: citiesModule,
        pop_by_states:popByStateModule,
        pop_by_counties:popByCountiesModule,
        pop_by_cities:popByCitiesModule,
        pop_age_states:popByAgeStatesModule,
        pop_age_counties:popByAgeCountiesModule,
        pop_age_cities:popByAgeCitiesModule,
        user:userModule,
        auth:authModule,
        explore:exploreModule,
        race_by_state:raceStateModule,
        race_by_county:raceCountiesModule,
        race_by_city:raceCitiesModule,
        race_edu_field_state:raceEduFieldStatesModule,
        race_edu_field_county:raceEduFieldCountiesModule,
        race_edu_field_city:raceEduFieldCitiesModule,
        race_bpov_state:raceBelowPovertyStatesModule,
        race_bpov_county:raceBelowPovertyCountiesModule,
        race_bpov_city:raceBelowPovertyCitiesModule

    },
    plugins: [
        createPersistedState({
            storage: window.localStorage
        })
    ],
    state,
    mutations
});
