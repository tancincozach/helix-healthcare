const data = state => state.data;
const counties = state => state.counties;
const meta = state => state.meta;

export {
    data,
    meta,
    counties
};
