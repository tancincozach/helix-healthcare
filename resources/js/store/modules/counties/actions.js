import { Counties } from '@/common/model/Counties';
import CountiesResource from '@/common/resource/CountiesResource';

const getCounties = (context, payload) => {
    return Counties.get(payload.query).then(res => { 
        context.commit('CLEAR_STATES');
        context.commit('GET_COUNTIES', { data: res.data, extra: payload.extra });
        context.commit('SAVE_META', res.meta);
    });
}

const pullCounties = (context, payload) => {
    return Counties.get(payload.query).then(res => {
        context.commit('PULL_COUNTIES', { data: res.data, extra: payload.extra });
        context.commit('SAVE_META', res.meta);
    });
};


export { getCounties, pullCounties};
