import _ from 'lodash';

const GET_COUNTIES = (state, payload) => {
    state.data = payload.data;
};

const PULL_COUNTIES = (state, payload) => {
    state.search = null;
    state.search = payload.data;
};

const CLEAR_STATES = state => {
    state.data = [];    
};

const SAVE_META = (state, meta) => {
    state.meta = meta;
};

export { GET_COUNTIES, PULL_COUNTIES, CLEAR_STATES, SAVE_META};
