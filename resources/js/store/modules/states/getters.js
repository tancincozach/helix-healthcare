const data = state => state.data;
const states = state => state.states;
const meta = state => state.meta;

export {
    data,
    meta,
    states
};
