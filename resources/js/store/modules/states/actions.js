import { States } from '@/common/model/States';
import StatesResource from '@/common/resource/StatesResource';

const getStates = (context, payload) => {
    return States.get(payload.query).then(res => { 
        context.commit('CLEAR_STATES');
        context.commit('GET_STATES', { data: res.data, extra: payload.extra });
        context.commit('SAVE_META', res.meta);
    });
}

const pullStates = (context, payload) => {
    return States.get(payload.query).then(res => {
        context.commit('PULL_STATES', { data: res.data, extra: payload.extra });
        context.commit('SAVE_META', res.meta);
    });
};


export { getStates, pullStates};
