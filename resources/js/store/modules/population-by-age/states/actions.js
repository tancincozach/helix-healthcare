import { PopulationByAgeState } from '@/common/model/PopulationByAgeState';
import PopulationByAgeStateResource from '@/common/resource/PopulationByAgeStateResource';

const getPopByAgeStates = (context, payload) => {
    return PopulationByAgeState.get(payload.query).then(res => { 
        context.commit('CLEAR_STATES');
        context.commit('GET_POPAGE_STATES', { data: res.data, extra: payload.extra });
        context.commit('SAVE_META', res.meta);
    });
}

const pullPopByAgeStates = (context, payload) => {
    return PopulationByAgeState.get(payload.query).then(res => {
        context.commit('PULL_POPAGE_STATES', { data: res.data, extra: payload.extra });
        context.commit('SAVE_META', res.meta);
    });
};


export { getPopByAgeStates, pullPopByAgeStates};
