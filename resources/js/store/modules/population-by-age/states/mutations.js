import _ from 'lodash';

const GET_POPAGE_STATES = (state, payload) => {
    state.data = payload.data;
};

const PULL_POPAGE_STATES = (state, payload) => {
    state.search = null;
    state.search = payload.data;
};

const CLEAR_STATES = state => {
    state.data = [];    
};

const SAVE_META = (state, meta) => {
    state.meta = meta;
};

export { GET_POPAGE_STATES, PULL_POPAGE_STATES, CLEAR_STATES, SAVE_META};
