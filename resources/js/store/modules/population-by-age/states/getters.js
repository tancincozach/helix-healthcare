const data = state => state.data;
const meta = state => state.meta;
const states = state => state.cities;

export {
    data,
    meta,
    states
};
