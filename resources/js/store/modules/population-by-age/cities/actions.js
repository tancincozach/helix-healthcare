import { PopulationByAgeCity } from '@/common/model/PopulationByAgeCity';
import PopulationByAgeStateResource from '@/common/resource/PopulationByAgeStateResource';

const getPopByAgeCities = (context, payload) => {
    return PopulationByAgeCity.get(payload.query).then(res => { 
        context.commit('CLEAR_STATES');
        context.commit('GET_POPAGE_CITIES', { data: res.data, extra: payload.extra });
        context.commit('SAVE_META', res.meta);
    });
}

const pullPopByAgeCities= (context, payload) => {
    return PopulationByAgeCity.get(payload.query).then(res => {
        context.commit('PULL_POPAGE_CITIES', { data: res.data, extra: payload.extra });
        context.commit('SAVE_META', res.meta);
    });
};


export { getPopByAgeCities, pullPopByAgeCities};
