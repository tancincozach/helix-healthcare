const data = state => state.data;
const states = state => state.cities;
const meta = state => state.meta;

export {
    data,
    meta,
    states
};
