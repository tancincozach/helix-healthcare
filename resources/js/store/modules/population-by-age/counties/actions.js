import { PopulationByAgeCounty } from '@/common/model/PopulationByAgeCounty';
import PopulationByAgeStateResource from '@/common/resource/PopulationByAgeStateResource';

const getPopByAgeCounties = (context, payload) => {
    return PopulationByAgeCounty.get(payload.query).then(res => { 
        context.commit('CLEAR_STATES');
        context.commit('GET_POPAGE_COUNTIES', { data: res.data, extra: payload.extra });
        context.commit('SAVE_META', res.meta);
    });
}

const pullPopByAgeCounties= (context, payload) => {
    return PopulationByAgeCounty.get(payload.query).then(res => {
        context.commit('PULL_POPAGE_COUNTIES', { data: res.data, extra: payload.extra });
        context.commit('SAVE_META', res.meta);
    });
};


export { getPopByAgeCounties, pullPopByAgeCounties};
