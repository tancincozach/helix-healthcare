import { PopulationByRaceState } from '@/common/model/PopulationByRaceState';

const getPopRaceState = (context, payload) => {
    return PopulationByRaceState.get(payload.query).then(res => {
        context.commit('CLEAR_STATES');
        context.commit('GET_POP_RACE_STATES', { data: res.data, extra: payload.extra });
        context.commit('SAVE_META', res.meta);
    });
}

const pullPopRaceState = (context, payload) => {
    return PopulationByRaceState.get(payload.query).then(res => {
        context.commit('PULL_POP_RACE_STATES', { data: res.data, extra: payload.extra });
        context.commit('SAVE_META', res.meta);
    });
};


export { getPopRaceState, pullPopRaceState};
