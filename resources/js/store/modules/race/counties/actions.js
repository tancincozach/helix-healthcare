import { PopulationByRaceCounty } from '@/common/model/PopulationByRaceCounty';

const getPopRaceCounty = (context, payload) => {
    return PopulationByRaceCounty.get(payload.query).then(res => {
        context.commit('CLEAR_COUNTIES');
        context.commit('GET_POP_RACE_COUNTIES', { data: res.data, extra: payload.extra });
        context.commit('SAVE_META', res.meta);
    });
}

const pullPopRaceCounty = (context, payload) => {
    return PopulationByRaceCounty.get(payload.query).then(res => {
        context.commit('PULL_POP_RACE_COUNTIES', { data: res.data, extra: payload.extra });
        context.commit('SAVE_META', res.meta);
    });
};


export { getPopRaceCounty, pullPopRaceCounty};
