const data = state => state.data;
const states = state => state.pop_state;
const pop_state = state => state.data;
const meta = state => state.meta;

export {
    data,
    pop_state,
    meta,
    states
};
