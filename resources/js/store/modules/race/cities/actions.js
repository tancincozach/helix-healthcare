import { PopulationByRaceCity } from '@/common/model/PopulationByRaceCity';

const getPopRaceCity = (context, payload) => {
    return PopulationByRaceCity.get(payload.query).then(res => {
        context.commit('CLEAR_CITIES');
        context.commit('GET_POP_RACE_CITIES', { data: res.data, extra: payload.extra });
        context.commit('SAVE_META', res.meta);
    });
}

const pullPopRaceCity = (context, payload) => {
    return PopulationByRaceCity.get(payload.query).then(res => {
        context.commit('PULL_POP_RACE_CITIES', { data: res.data, extra: payload.extra });
        context.commit('SAVE_META', res.meta);
    });
};


export { getPopRaceCity, pullPopRaceCity};
