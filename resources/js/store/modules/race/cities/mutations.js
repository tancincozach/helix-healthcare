import _ from 'lodash';

const GET_POP_RACE_CITIES = (state, payload) => {
    state.data = payload.data;
};

const PULL_POP_RACE_CITIES = (state, payload) => {
    state.search = null;
    state.search = payload.data;
};

const CLEAR_CITIES = state => {
    state.data = [];    
};

const SAVE_META = (state, meta) => {
    state.meta = meta;
};

export { GET_POP_RACE_CITIES, PULL_POP_RACE_CITIES, CLEAR_CITIES, SAVE_META};
