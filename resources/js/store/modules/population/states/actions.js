import { PopulationByState } from '@/common/model/PopulationByState';
import PopulationByStateResource from '@/common/resource/PopulationByStateResource';

const getPopState = (context, payload) => {
    return PopulationByState.get(payload.query).then(res => {
        context.commit('CLEAR_STATES');
        context.commit('GET_POP_STATES', { data: res.data, extra: payload.extra });
        context.commit('SAVE_META', res.meta);
    });
}

const pullPopState = (context, payload) => {
    return PopulationByState.get(payload.query).then(res => {
        context.commit('PULL_POP_STATES', { data: res.data, extra: payload.extra });
        context.commit('SAVE_META', res.meta);
    });
};


export { getPopState, pullPopState};
