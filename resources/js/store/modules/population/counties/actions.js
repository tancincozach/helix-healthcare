import { PopulationByCounty } from '@/common/model/PopulationByCounty';

const getPopCounties = (context, payload) => {
    return PopulationByCounty.get(payload.query).then(res => { 
        context.commit('CLEAR_STATES');
        context.commit('GET_POP_COUNTIES', { data: res.data, extra: payload.extra });
        context.commit('SAVE_META', res.meta);
    });
}

const pullPopCounties = (context, payload) => {
    return PopulationByCounty.get(payload.query).then(res => {
        context.commit('PULL_POP_COUNTIES', { data: res.data, extra: payload.extra });
        context.commit('SAVE_META', res.meta);
    });
};


export { getPopCounties, pullPopCounties};
