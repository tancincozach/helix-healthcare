const data = state => state.data;
const meta = state => state.meta;
const states = state => state.population_by_counties;

export {
    data,
    meta,
    states
};
