import { PopulationByCity } from '@/common/model/PopulationByCity';

const getPopCities= (context, payload) => {
    return PopulationByCity.get(payload.query).then(res => { 
        context.commit('CLEAR_STATES');
        context.commit('GET_POP_CITIES', { data: res.data, extra: payload.extra });
        context.commit('SAVE_POP_CITIES_META', res.meta);
    });
}

const pullPopCities = (context, payload) => {
    return PopulationByCity.get(payload.query).then(res => {
        context.commit('PULL_POP_CITIES', { data: res.data, extra: payload.extra });
        context.commit('SAVE_POP_CITIES_META', res.meta);
    });
};


export { getPopCities, pullPopCities};
