import * as actions from './actions';
import * as mutations from './mutations';
import * as getters from './getters';

const state = {
    data: [],
    meta: {},
    population_by_cities: {},
    search: []    
};

export default {
    namespaced: true,
    state,
    actions,
    mutations,
    getters
}
