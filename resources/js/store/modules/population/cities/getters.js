const data = state => state.data;
const meta = state => state.meta;
const states = state => state.population_by_cities;

export {
    data,
    meta,
    states
};
