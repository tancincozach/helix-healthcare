import _ from 'lodash';

const GET_POP_CITIES = (state, payload) => {
    state.data = payload.data;
};

const PULL_POP_CITIES = (state, payload) => {
    state.search = null;
    state.search = payload.data;
};

const CLEAR_STATES = state => {
    state.data = [];    
};

const SAVE_POP_CITIES_META = (state, meta) => {
    state.meta = meta;
};

export { GET_POP_CITIES, PULL_POP_CITIES, CLEAR_STATES, SAVE_POP_CITIES_META};
