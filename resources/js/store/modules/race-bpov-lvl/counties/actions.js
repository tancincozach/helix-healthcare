import { PopulationBPovByRaceCounty } from '@/common/model/PopulationBPovByRaceCounty';

const getPopBpovRaceCounties = (context, payload) => {
    return PopulationBPovByRaceCounty.get(payload.query).then(res => {
        context.commit('CLEAR_BPOV_COUNTIES');
        context.commit('GET_POP_BPOV_RACE_COUNTIES', { data: res.data, extra: payload.extra });
        context.commit('SAVE_META', res.meta);
    });
}

const pullPopBpovRaceCounties = (context, payload) => {
    return PopulationBPovByRaceCounty.get(payload.query).then(res => {
        context.commit('PULL_POP_BPOV_RACE_COUNTIES', { data: res.data, extra: payload.extra });
        context.commit('SAVE_META', res.meta);
    });
};


export { getPopBpovRaceCounties, pullPopBpovRaceCounties};
