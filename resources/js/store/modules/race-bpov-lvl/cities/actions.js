import { PopulationBPovByRaceCity } from '@/common/model/PopulationBPovByRaceCity';

const getPopBpovRaceCities = (context, payload) => {
    return PopulationBPovByRaceCity.get(payload.query).then(res => {
        context.commit('CLEAR_BPOV_CITIES');
        context.commit('GET_POP_BPOV_RACE_CITIES', { data: res.data, extra: payload.extra });
        context.commit('SAVE_META', res.meta);
    });
}

const pullPopBpovRaceCities = (context, payload) => {
    return PopulationBPovByRaceCity.get(payload.query).then(res => {
        context.commit('PULL_POP_BPOV_RACE_CITIES', { data: res.data, extra: payload.extra });
        context.commit('SAVE_META', res.meta);
    });
};


export { getPopBpovRaceCities, pullPopBpovRaceCities};
