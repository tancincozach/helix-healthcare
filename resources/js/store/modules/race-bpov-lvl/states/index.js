import * as actions from './actions';
import * as mutations from './mutations';
import * as getters from './getters';

const state = {
    data: [],
    meta: {},
    pop_state: {},
    search: []    
};

export default {
    namespaced: true,
    state,
    actions,
    mutations,
    getters
}
