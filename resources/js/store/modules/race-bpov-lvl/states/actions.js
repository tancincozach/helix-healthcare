import { PopulationBPovByRaceState } from '@/common/model/PopulationBPovByRaceState';

const getPopBpovRaceStates = (context, payload) => {
    return PopulationBPovByRaceState.get(payload.query).then(res => {
        context.commit('CLEAR_BPOV_STATES');
        context.commit('GET_POP_BPOV_RACE_STATES', { data: res.data, extra: payload.extra });
        context.commit('SAVE_META', res.meta);
    });
}

const pullPopBpovRaceStates = (context, payload) => {
    return PopulationBPovByRaceState.get(payload.query).then(res => {
        context.commit('PULL_POP_BPOV_RACE_STATES', { data: res.data, extra: payload.extra });
        context.commit('SAVE_META', res.meta);
    });
};


export { getPopBpovRaceStates, pullPopBpovRaceStates};
