import _ from 'lodash';

const GET_POP_BPOV_RACE_STATES = (state, payload) => {
    state.data = payload.data;
};

const PULL_POP_BPOV_RACE_STATES = (state, payload) => {
    state.search = null;
    state.search = payload.data;
};

const CLEAR_BPOV_STATES = state => {
    state.data = [];    
};

const SAVE_META = (state, meta) => {
    state.meta = meta;
};

export { GET_POP_BPOV_RACE_STATES, PULL_POP_BPOV_RACE_STATES, CLEAR_BPOV_STATES, SAVE_META};
