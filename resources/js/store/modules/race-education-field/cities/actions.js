import { PopulationEduFieldRaceCity } from '@/common/model/PopulationEduFieldRaceCity';

const getPopEduFieldRaceCities = (context, payload) => {
    return PopulationEduFieldRaceCity.get(payload.query).then(res => {
        context.commit('CLEAR_EFIELD_CITIES');
        context.commit('GET_POP_EFIELD_RACE_CITIES', { data: res.data, extra: payload.extra });
        context.commit('SAVE_META', res.meta);
    });
}

const pullPopEduFieldRaceCities = (context, payload) => {
    return PopulationEduFieldRaceCity.get(payload.query).then(res => {
        context.commit('PULL_POP_EFIELD_RACE_CITIES', { data: res.data, extra: payload.extra });
        context.commit('SAVE_META', res.meta);
    });
};


export { getPopEduFieldRaceCities, pullPopEduFieldRaceCities};
