import { PopulationEduFieldRaceCounty } from '@/common/model/PopulationEduFieldRaceCounty';

const getPopEduFieldRaceCounties = (context, payload) => {
    return PopulationEduFieldRaceCounty.get(payload.query).then(res => {
        context.commit('CLEAR_EFIELD_COUNTIES');
        context.commit('GET_POP_EFIELD_RACE_COUNTIES', { data: res.data, extra: payload.extra });
        context.commit('SAVE_META', res.meta);
    });
}

const pullPopEduFieldRaceCounties = (context, payload) => {
    return PopulationEduFieldRaceCounty.get(payload.query).then(res => {
        context.commit('PULL_POP_EFIELD_RACE_COUNTIES', { data: res.data, extra: payload.extra });
        context.commit('SAVE_META', res.meta);
    });
};


export { getPopEduFieldRaceCounties, pullPopEduFieldRaceCounties};
