import _ from 'lodash';

const GET_POP_EFIELD_RACE_COUNTIES = (state, payload) => {
    state.data = payload.data;
};

const PULL_POP_EFIELD_RACE_COUNTIES = (state, payload) => {
    state.search = null;
    state.search = payload.data;
};

const CLEAR_EFIELD_COUNTIES = state => {
    state.data = [];    
};

const SAVE_META = (state, meta) => {
    state.meta = meta;
};

export { GET_POP_EFIELD_RACE_COUNTIES, PULL_POP_EFIELD_RACE_COUNTIES, CLEAR_EFIELD_COUNTIES, SAVE_META};
