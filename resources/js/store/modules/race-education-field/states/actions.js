import { PopulationEduFieldRaceState } from '@/common/model/PopulationEduFieldRaceState';

const getPopEduFieldRaceStates = (context, payload) => {
    return PopulationEduFieldRaceState.get(payload.query).then(res => {
        context.commit('CLEAR_EFIELD_STATES');
        context.commit('GET_POP_EFIELD_RACE_STATES', { data: res.data, extra: payload.extra });
        context.commit('SAVE_META', res.meta);
    });
}

const pullPopEduFieldRaceStates = (context, payload) => {
    return PopulationEduFieldRaceState.get(payload.query).then(res => {
        context.commit('PULL_POP_EFIELD_RACE_STATES', { data: res.data, extra: payload.extra });
        context.commit('SAVE_META', res.meta);
    });
};


export { getPopEduFieldRaceStates, pullPopEduFieldRaceStates};
