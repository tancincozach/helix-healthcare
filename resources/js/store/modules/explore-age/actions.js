import { ExploreResult } from '@/common/model/ExploreResult';

const getExploreResult= (context, payload) => {
    return ExploreResult.get(payload.query).then(res => { 
        context.commit('CLEAR_STATES');
        context.commit('GET_SEARCH_RESULT', { data: res.data, extra: payload.extra });
        context.commit('SAVE_SEARCH_RESULT_META', res.meta);
    });
}

const pullExploreResults = (context, payload) => {
    return ExploreResult.get(payload.query).then(res => {
        context.commit('PULL_SEARCH_RESULT', { data: res.data, extra: payload.extra });
        context.commit('SAVE_SEARCH_RESULT_META', res.meta);
    });
};


export { getExploreResult, pullExploreResults};
