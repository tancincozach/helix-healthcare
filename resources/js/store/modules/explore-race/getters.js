const data = state => state.data;
const meta = state => state.meta;
const states = state => state.search_results;

export {
    data,
    meta,
    states
};
