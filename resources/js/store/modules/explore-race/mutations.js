import _ from 'lodash';

const GET_SEARCH_RESULT = (state, payload) => {
    state.data = payload.data;
};

const PULL_SEARCH_RESULT = (state, payload) => {
    state.search = null;
    state.search = payload.data;
};

const CLEAR_STATES = state => {
    state.data = [];    
};

const SAVE_SEARCH_RESULT_META = (state, meta) => {
    state.meta = meta;
};

export { GET_SEARCH_RESULT, PULL_SEARCH_RESULT, CLEAR_STATES,SAVE_SEARCH_RESULT_META};
