import { Cities } from '@/common/model/Cities';
import CitiesResource from '@/common/resource/CitiesResource';

const getCities = (context, payload) => {
    return Cities.get(payload.query).then(res => { 
        context.commit('CLEAR_STATES');
        context.commit('GET_CITIES', { data: res.data, extra: payload.extra });
        context.commit('SAVE_META', res.meta);
    });
}

const pullCities = (context, payload) => {
    return Cities.get(payload.query).then(res => {
        context.commit('PULL_CITIES', { data: res.data, extra: payload.extra });
        context.commit('SAVE_META', res.meta);
    });
};


export { getCities, pullCities};
