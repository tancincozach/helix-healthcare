const data = state => state.data;
const meta = state => state.meta;
const cities = state => state.counties;

export {
    data,
    meta,
    cities
};
