import _ from 'lodash';

const GET_CITIES = (state, payload) => {
    state.data = payload.data;
};

const PULL_CITIES = (state, payload) => {
    state.search = null;
    state.search = payload.data;
};

const CLEAR_STATES = state => {
    state.data = [];    
};

const SAVE_META = (state, meta) => {
    state.meta = meta;
};

export { GET_CITIES, PULL_CITIES, CLEAR_STATES, SAVE_META};
