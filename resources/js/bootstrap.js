import Vue from 'vue';
import Router from 'vue-router';
import axios from 'axios';
import BootstrapVue from 'bootstrap-vue';
import { abilitiesPlugin, Can } from '@casl/vue';
import { ability } from "@/common/oauth/ability.js";
Vue.use(Router);
Vue.use(BootstrapVue);
Vue.use(abilitiesPlugin, ability);
Vue.component('Can', Can);
window._ = require('lodash');
window.Vue = Vue;
window.axios = axios;
window._ = require('lodash');
window.Popper = require('popper.js').default;

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';



try {
    window.$ = window.jQuery = require('jquery');

    require('bootstrap');
} catch (e) {}


let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}
