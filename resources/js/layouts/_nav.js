
export default [
  {
    _name: 'CSidebarNav',
    _children: [
      {
        _name: 'CSidebarNavItem',
        name: 'Dashboard',
        to: '/dashboard',
        icon: 'cil-speedometer'
        },
        {
          _name: 'CSidebarNavItem',
          name: 'States',
          to: '/states',
          icon: 'cil-grid'        
        },
        {
          _name: 'CSidebarNavItem',
          name: 'Counties',
          to: '/counties',
          icon: 'cil-grid'        
      },
      {
        _name: 'CSidebarNavItem',
        name: 'Cities',
        to: '/cities',
        icon: 'cil-grid'        
      },    
      {
        _name: 'CSidebarNavDropdown',
        name: 'Population By',
        route: '/base',
        icon: 'cil-puzzle',
        items: [
          {
            name: 'State',
            to: '/population-by-state'
          },
          {
            name: 'Counties',
            to: '/population-by-county'
          }          
          ,
          {
            name: 'Cities',
            to: 'population-by-city'
          }       
          ,
          {
            name: 'Age',
            to: 'population-by-age'
          }  
          ,
          {
            name: 'Race',
            to: 'population-by-race'
          }  
        ]
      }
    ]
  }
]