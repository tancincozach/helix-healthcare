
require('./bootstrap');

import 'core-js/stable'
import Vue from 'vue'
import AppHC from './AppHC'
import CoreuiVue from '@coreui/vue'
import { iconsSet as icons } from './assets/icons/icons.js'
import store from './store/index'

import router from './routes';

Vue.config.performance = true
Vue.use(CoreuiVue)
Vue.prototype.$log = console.log.bind(console);

const app = new Vue({
            el: '#app',
            router,
            store,
            icons
            ,
            components: {
              AppHC
            }
  })
  