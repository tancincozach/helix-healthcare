<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller as BaseController;
use \Prettus\Validator\Exceptions\ValidatorException;
use Dingo\Api\Routing\Helpers;
use Dingo\Api\Http\Request;
use \Prettus\Validator\Contracts\ValidatorInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Api\Http\Response;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Spatie\Geocoder\Geocoder; // Load Spatie Geocoder library
use App\Models\States\State;
use App\Models\Cities\City;
use App\Models\Counties\County;
use App\Models\States\PopulationState;
use App\Models\Counties\PopulationCounty;
use App\Models\Cities\PopulationCity;
use App\Models\States\PopulationByAgeState;
use App\Models\Counties\PopulationByAgeCounty;
use App\Models\Cities\PopulationByAgeCity;
use App\Models\States\PopulationByAgeRaceState;
use App\Models\Counties\PopulationByAgeRaceCounty;
use App\Models\Cities\PopulationByAgeRaceCity;
use CountryState;// Load US based States to get State code to be used for method:processState
use App\Traits\CensusVariableTrait;
use DB;

class WebScrapePopulationByPovertyStatus extends BaseController implements WebScraperAPIInterface
{           
    use CensusVariableTrait;

    public $apiKey ='';
    public $stateList = array();
    public $ageList = array();
    public $acsYr = '';
    
    public function __construct(){
        $this->apiKey = env('API_KEY_CENSUS');        
    }
    public function index( Request $request){     
        set_time_limit(0);		
        ini_set('memory_limit','512M');  
        $this->acsYr = $request->acs_year; 
        $this->processCensusDataPopulationByPovertyStatus();
    }
    public function collectData($url){
        $response = '';
        $response = file_get_contents($url);
    	$jsonResponse =json_decode($response);
        return $jsonResponse;
    }
  
    public function processCensusDataPopulationByPovertyStatus(){
        // Automatic generation of Census Varable  from B01001_003E to S1701_C01_049E - S1701_C01_057E
        // SEX BY AGE LABEL
        $processStatus = false;
        $censusVariables = $this->getPovertyStatusAgeAndSexVariable($this->acsYr);
        collect($censusVariables)->each(function($results,$key){
                                    
        });
    } 

}