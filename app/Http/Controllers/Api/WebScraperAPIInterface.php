<?php

namespace App\Http\Controllers\Api;

use Dingo\Api\Http\Request;

interface WebScraperAPIInterface
{
    public function collectData($url);    
}
