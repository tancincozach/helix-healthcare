<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller as BaseController;
use \Prettus\Validator\Exceptions\ValidatorException;
use Dingo\Api\Routing\Helpers;
use Dingo\Api\Http\Request;
use \Prettus\Validator\Contracts\ValidatorInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Api\Http\Response;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\Services\ThirdParty\CensusBureauGeo;
use Spatie\Geocoder\Geocoder; // Load Spatie Geocoder library
use CountryState;// Load US based States to get State code to be used for method:processState
use App\Traits\CensusVariableTrait;
use App\Repositories\StatesRepository;
use App\Repositories\CountiesRepository;
use App\Repositories\UsRegionsRepository;
use App\Repositories\UsDivisionsRepository;
use App\Repositories\CountiesSubdRepository;
use App\Repositories\CountiesSubdCitiesRepository;
use App\Validators\UsRegionsValidator;
use App\Validators\UsDivisionsValidator;
use App\Validators\StatesValidator;
use App\Validators\CountiesValidator;
use App\Validators\CountiesSubdValidator;
use App\Validators\CountiesSubdCitiesValidator;


class CensusGeoWebScrapeController extends BaseController implements WebScraperAPIInterface
{           
    use CensusVariableTrait;

    public $apiKey ='';
    public $stateList = array();
    public $ageList = array();
    public $acsYr = '';
    
    public function __construct(
        UsRegionsRepository $regionRepo,
        UsDivisionsRepository $divisionRepo,
        StatesRepository $stateRepo,
        CountiesRepository $countyRepo,
        CountiesSubdRepository $countySubdRepo,
        CountiesSubdCitiesRepository $countySubdCitiesRepo,

        UsRegionsValidator $regionValidator,        
        UsDivisionsValidator  $divisionValidator,
        StatesValidator $stateValidator,
        CountiesValidator $countyValidator,        
        CountiesSubdValidator $countySubdValidator,
        CountiesSubdCitiesValidator $countySubdCitiesValidator,
        Request         $request,
        CensusBureauGeo $censusBur
    ){
        $this->apiKey = env('API_KEY_CENSUS');
        $this->rRepository = $regionRepo;
        $this->dRepository = $divisionRepo;        
        $this->sRepository = $stateRepo;
        $this->cRepository = $countyRepo;
        $this->csRepository = $countySubdRepo;
        $this->cscRepository = $countySubdCitiesRepo;
        $this->rValidator  = $regionValidator;
        $this->dValidator  = $divisionValidator;
        $this->sValidator  = $stateValidator;
        $this->cValidator  = $countyValidator;
        $this->csValidator = $countySubdValidator;
        $this->cscValidator = $countySubdCitiesValidator;
        $this->acsYr = $request->acs_year;
        $this->census = $censusBur;
    }
    public function index(Request $request){           
        set_time_limit(0);		
        ini_set('memory_limit','512M'); 
        // $this->setStateTest($request);
        // $this->setRegions();
        // $this->setDivisions();
        // $this->setStates();
        // $this->setCounties();    
        $this->setCountySubdivisions();
        $this->setConsolidatedCities();
        echo "Successfully loaded Geo Census Data (Region,Division,State,Counties,Couties Subdivision and Cities) based on Year:". $this->acsYr ;
    }
    public function setStateTest(Request $request){        
        $this->census->retrieveStates();
        //$this->census->getStates();
    }
    public function collectData($url){
        $http = new Client;
        $response = $http->request('GET',$url);
        $result = json_decode((string) $response->getBody(), true);
        return $result;
    }
    public function store(Request $request){

    }
    public function setRegions(){
        $regionResponse = $this->collectData('https://api.census.gov/data/'.$this->acsYr.'/pep/population?get=GEONAME,POP&for=region:*&key='.$this->apiKey); 
        $this->rRepository->importRegions($regionResponse);
    }
    public function setDivisions(){
        $divisionResponse = $this->collectData('https://api.census.gov/data/'.$this->acsYr.'/pep/population?get=GEONAME,POP&for=division:*&key='.$this->apiKey);
        $this->dRepository->importDivisions($divisionResponse);
    }
    public function setStates(){
        $statesResponse = $this->collectData('https://api.census.gov/data/'.$this->acsYr.'/pep/population?get=GEONAME,POP&for=state:*&key='.$this->apiKey);
        $this->sRepository->importStates($statesResponse,['acsYear'=>$this->acsYr,'stateList'=>CountryState::getStates('US')]);
    }
    public function setCounties(){
        $countiesResponse = $this->collectData('https://api.census.gov/data/'.$this->acsYr.'/pep/population?get=GEONAME,POP&for=county:*&key='.$this->apiKey);
        $this->cRepository->importCounties($countiesResponse,['acsYear'=>$this->acsYr]);
    }
    public function setCountySubdivisions(){
        $this->csRepository->importCountiesSubdivision(['acsYear'=>$this->acsYr,'apiKey'=>$this->apiKey]); 
    }
    public function setConsolidatedCities(){
        $this->cscRepository->importCountiesSubdivisionCities(['acsYear'=>$this->acsYr,'apiKey'=>$this->apiKey]); ;
    }
}