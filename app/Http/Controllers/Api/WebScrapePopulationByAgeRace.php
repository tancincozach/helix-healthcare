<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller as BaseController;
use \Prettus\Validator\Exceptions\ValidatorException;
use Dingo\Api\Routing\Helpers;
use Dingo\Api\Http\Request;
use \Prettus\Validator\Contracts\ValidatorInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Api\Http\Response;
use GuzzleHttp\Exception\GuzzleException;
use Spatie\Geocoder\Geocoder; // Load Spatie Geocoder library
use App\Models\States\State;
use App\Models\Cities\City;
use App\Models\Counties\County;
use App\Models\States\PopulationState;
use App\Models\Counties\PopulationCounty;
use App\Models\Cities\PopulationCity;
use App\Models\States\PopulationByAgeState;
use App\Models\Counties\PopulationByAgeCounty;
use App\Models\Cities\PopulationByAgeCity;
use App\Models\States\PopulationByAgeRaceState;
use App\Models\Counties\PopulationByAgeRaceCounty;
use App\Models\Cities\PopulationByAgeRaceCity;
use CountryState;// Load US based States to get State code to be used for method:processState
use App\Traits\CensusVariableTrait;
use GuzzleHttp\Client;
use DB;

class WebScrapePopulationByAgeRace extends BaseController implements WebScraperAPIInterface
{           
    use CensusVariableTrait;

    public $apiKey ='';
    public $stateList = array();
    public $ageList = array();
    public $acsYr = '';
    
    public function __construct(){
        $this->apiKey = env('API_KEY_CENSUS');        
    }
    public function index( Request $request){     
        set_time_limit(0);		
        ini_set('memory_limit','512M');  
        $this->acsYr = $request->acs_year; 
        $this->processCensusDataPopRaceSexByAge();
    }
    public function collectData($url){
        $response = '';
        $response = file_get_contents($url);
    	$jsonResponse =json_decode($response);
        return $jsonResponse;
    }
  
    public function processCensusDataPopRaceSexByAge(){
        // Automatic generation of Census Varable  from B01001_003E to B01001_026E(Male) and B01001_027E to B01001_049E (Female)
        // SEX BY AGE LABEL
        $processStatus = false;
        $censusVariables = $this->getRaceVariableBySexAndAge($this->acsYr);
        $parameters = [];

        foreach($censusVariables as $key=>$censusDataVar){
            foreach($censusDataVar as $key=>$var){
                foreach($var as $censusData){                                        
                  $censusByStateAgeResponse = $this->http_connect('https://api.census.gov/data/'.$this->acsYr.'/acs/acs5?get='.$censusData['censusVariable'].',NAME&for=state:*','GET');
                   PopulationByAgeRaceState::insertOrUpdate($censusByStateAgeResponse,$censusData);

                  $censusByCountyResponse = $this->http_connect('https://api.census.gov/data/'.$this->acsYr.'/acs/acs5?get='.$censusData['censusVariable'].',NAME&for=county','GET');
                  PopulationByAgeRaceCounty::insertOrUpdate($censusByCountyResponse,$censusData);

                  $censusByCityResponse = $this->http_connect('https://api.census.gov/data/'.$this->acsYr.'/acs/acs5?get='.$censusData['censusVariable'].',NAME&for=place','GET');
                  PopulationByAgeRaceCity::insertOrUpdate($censusByCityResponse,$censusData);                                          
                }
            }
        }
        echo 'Successfull Imported Population By Age with Race'; 
    }
    public function http_connect($url,$request_type,$params=array()){
        $client = new Client;
        $response = $client->request($request_type, $url,$params);
        $result = json_decode((string) $response->getBody(), true);
        var_dump($result);
    }

}