<?php

namespace App\Http\Controllers\Api\Secured;

use App\Http\Controllers\Api\APIBaseController as BaseController;
use Dingo\Api\Http\Request;
use App\Criterias\Cities\Select;
use App\Criterias\Cities\WithState;
use App\Validators\CitiesValidator;
use App\Repositories\CitiesRepository;
use App\Transformers\CitiesTransformer;

class CitiesController extends BaseController
{    
    public function __construct(CitiesRepository $repository,CitiesValidator $validator,CitiesTransformer $transformer){
        $this->repository = $repository;
        $this->validator = $validator;
        $this->transformer = $transformer;
    }

    public function index(Request $request){
        $this->repository->pushCriteria(new Select());
        $this->repository->pushCriteria(new WithState());
        return parent::index($request);
    }   
    public function show($id){

    }
    public function destroy($id){

    }
    public function update(Request $request, $id){

    }
    public function store(Request $request){

    }
}

