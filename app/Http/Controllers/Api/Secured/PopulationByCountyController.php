<?php

namespace App\Http\Controllers\Api\Secured;

use App\Http\Controllers\Api\APIBaseController as BaseController;
use Dingo\Api\Http\Request;
use App\Criterias\Population\PopByCountySelect;
use App\Validators\PopulationByCountyValidator;
use App\Repositories\PopulationByCountyRepository;
use App\Transformers\PopulationByCountyTransformer;

class PopulationByCountyController extends BaseController
{    
    public function __construct(PopulationByCountyRepository $repository,PopulationByCountyValidator $validator,PopulationByCountyTransformer $transformer){
        $this->repository = $repository;
        $this->validator = $validator;
        $this->transformer = $transformer;
    }

    public function index(Request $request){
        $this->repository->pushCriteria(new PopByCountySelect());
        return parent::index($request);
    }   
    public function show($id){

    }
    public function destroy($id){

    }
    public function update(Request $request, $id){

    }
    public function store(Request $request){

    }
}

