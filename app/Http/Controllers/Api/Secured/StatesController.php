<?php

namespace App\Http\Controllers\Api\Secured;

use App\Http\Controllers\Api\APIBaseController as BaseController;
use Dingo\Api\Http\Request;
use App\Validators\StatesValidator;
use App\Repositories\StatesRepository;
use App\Transformers\StatesTransformer;

class StatesController extends BaseController
{    
    public function __construct(StatesRepository $repository,StatesValidator $validator,StatesTransformer $transformer){
        $this->repository = $repository;
        $this->validator = $validator;
        $this->transformer = $transformer;
    }

    public function index(Request $request){
        return parent::index($request);
    }   
    public function show($id){

    }
    public function destroy($id){

    }
    public function update(Request $request, $id){

    }
    public function store(Request $request){

    }
}

