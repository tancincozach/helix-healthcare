<?php

namespace App\Http\Controllers\Api\Secured;

use App\Http\Controllers\Api\APIBaseController as BaseController;
use \Prettus\Validator\Exceptions\ValidatorException;
use Dingo\Api\Routing\Helpers;
use Dingo\Api\Http\Request;
use \Prettus\Validator\Contracts\ValidatorInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Api\Http\Response;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Spatie\Geocoder\Geocoder; // Load Spatie Geocoder library
use CountryState;// Load US based States to get State code to be used for method:processState
use App\Traits\CensusVariableTrait;
use App\Criterias\States\SearchStateByName;
use App\Criterias\Counties\SearchCountyByName;
use App\Criterias\Cities\SearchCityByName;
use App\Repositories\StatesRepository;
use App\Repositories\CountiesRepository;
use App\Repositories\CitiesRepository;
use App\Validators\CitiesValidator;
use App\Validators\StatesValidator;
use App\Validators\CountiesValidator;
use App\Transformers\StatesTransformer;
use App\Transformers\CitiesTransformer;
use App\Transformers\CountiesTransformer;

class ExploreController extends BaseController
{
    public $repository,$transformer,$validator;
    public function __construct(){

    }

    public function index(Request $request ){
            
        if($request->get('search')){
                switch($request->get('searchBy')){
                    case "state":
                        $this->repository = new StatesRepository(app());
                        $this->transformer = new StatesTransformer(app());
                        $this->repository->pushCriteria(new SearchStateByName($request->get('search')));
                    break;
                    case "county":
                        $this->repository = new CountiesRepository(app());
                        $this->transformer = new CountiesTransformer();
                        $this->repository->pushCriteria(new SearchCountyByName($request->get('search')));
                    break;
                    case "city":
                        $this->repository =  new CitiesRepository(app());
                        $this->transformer = new CitiesTransformer();
                        $this->repository->pushCriteria(new SearchCityByName($request->get('search')));
                    break; 
                    default:
                    $this->repository = new StatesRepository(app());
                    $this->transformer = new StatesTransformer(app());
                    $this->repository->pushCriteria(new SearchStateByName($request->get('search')));
                }
        }
        $response = parent::index($request);
        return $response;
    }

    public function show($searchVal)
    {
        try {
            $result = $this->getUserByUsername($userName);
            return $this->response->item($result, $this->transformer);
        } catch (ModelNotFoundException $e) {
            return response()->json(
                [
                'status_code' => 404,
                'message' => 'Can\'t find the requested resource.'
                ], 404
            );
        }
    }
    
}
