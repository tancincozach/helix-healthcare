<?php

namespace App\Http\Controllers\Api\Secured;

use App\Http\Controllers\Api\APIBaseController as BaseController;
use Dingo\Api\Http\Request;
use App\Criterias\Counties\Select;
use App\Criterias\Counties\WithState;
use App\Validators\CountiesValidator;
use App\Repositories\CountiesRepository;
use App\Transformers\CountiesTransformer;

class CountiesController extends BaseController
{    
    public function __construct(CountiesRepository $repository,CountiesValidator $validator,CountiesTransformer $transformer){
        $this->repository = $repository;
        $this->validator = $validator;
        $this->transformer = $transformer;
    }

    public function index(Request $request){
        $this->repository->pushCriteria(new Select());
        $this->repository->pushCriteria(new WithState());
        return parent::index($request);
    }   
    public function show($id){

    }
    public function destroy($id){

    }
    public function update(Request $request, $id){

    }
    public function store(Request $request){

    }
}