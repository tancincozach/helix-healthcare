<?php

namespace App\Http\Controllers\Api\Secured;

use App\Http\Controllers\Api\APIBaseController as BaseController;
use Dingo\Api\Http\Request;
use App\Criterias\Population\GroupByAgeAndGenderCounty;
use App\Criterias\Population\ByAgeWithCountyId;
use App\Validators\PopulationByAgeCountyValidator;
use App\Repositories\PopulationByAgeCountyRepository;
use App\Transformers\PopulationByAgeCountyTransformer;

class PopulationByAgeCountyController extends BaseController
{    
    public function __construct(PopulationByAgeCountyRepository $repository,PopulationByAgeCountyValidator $validator,PopulationByAgeCountyTransformer $transformer){
        $this->repository = $repository;
        $this->validator = $validator;
        $this->transformer = $transformer;
    }

    public function index(Request $request){
        $this->repository->pushCriteria(new GroupByAgeAndGenderCounty());
        return parent::index($request);
    }   
    public function show($id){        
        try {
            $result = $this->getPopByCountyId($id);
            return $this->response->collection($result, $this->transformer);
        } catch (ModelNotFoundException $e) {
            return response()->json(
                [
                'status_code' => 404,
                'message' => 'Can\'t find the requested resource.'
                ], 404
            );
        }
    }
    private function getPopByCountyId($id)
    {        
        $this->repository->pushCriteria(new GroupByAgeAndGenderCounty());
        $this->repository->pushCriteria(new ByAgeWithCountyId($id));
        return $this->repository->get();
    }
    public function destroy($id){

    }
    public function update(Request $request, $id){

    }
    public function store(Request $request){

    }
}

