<?php

namespace App\Http\Controllers\Api\Secured;

use App\Http\Controllers\Api\APIBaseController as BaseController;
use Dingo\Api\Http\Request;
use App\Criterias\Population\GroupByAgeAndGenderCity;
use App\Criterias\Population\ByAgeWithCityId;
use App\Validators\PopulationByAgeCityValidator;
use App\Repositories\PopulationByAgeCityRepository;
use App\Transformers\PopulationByAgeCityTransformer;

class PopulationByAgeCityController extends BaseController
{    
    public function __construct(PopulationByAgeCityRepository $repository,PopulationByAgeCityValidator $validator,PopulationByAgeCityTransformer $transformer){
        $this->repository = $repository;
        $this->validator = $validator;
        $this->transformer = $transformer;
    }

    public function index(Request $request){
        $this->repository->pushCriteria(new GroupByAgeAndGenderCity());
        return parent::index($request);
    }   
    public function show($id){
        try {
            $result = $this->getPopByCityId($id);
            return $this->response->collection($result, $this->transformer);
        } catch (ModelNotFoundException $e) {
            return response()->json(
                [
                'status_code' => 404,
                'message' => 'Can\'t find the requested resource.'
                ], 404
            );
        }
    }
    private function getPopByCityId($id)
    {        
        $this->repository->pushCriteria(new GroupByAgeAndGenderCity());
        $this->repository->pushCriteria(new ByAgeWithCityId($id));
        return $this->repository->get();
    }
    public function destroy($id){

    }
    public function update(Request $request, $id){

    }
    public function store(Request $request){

    }
}

