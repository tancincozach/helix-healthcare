<?php

namespace App\Http\Controllers\Api\Secured;

use App\Http\Controllers\Api\APIBaseController as BaseController;
use Dingo\Api\Http\Request;
use App\Criterias\Population\RaceByCity;
use App\Validators\PopulationBPovByEduCityValidator;
use App\Repositories\PopulationBPovLvlByEduCityRepository;
use App\Transformers\PopulationByEduBPovCityTransformer;

class PopulationBPovEdAttainCityController extends BaseController
{    
    public function __construct(PopulationBPovLvlByEduCityRepository $repository,PopulationBPovByEduCityValidator $validator,PopulationByEduBPovCityTransformer $transformer){
        $this->repository = $repository;
        $this->validator = $validator;
        $this->transformer = $transformer;
    }

    public function index(Request $request){
        return parent::index($request);
    }   
    public function show($id){
        try {
            $result = $this->searchById($id);
            return $this->response->collection($result, $this->transformer);
        } catch (ModelNotFoundException $e) {
            return response()->json(
                [
                'status_code' => 404,
                'message' => 'Can\'t find the requested resource.'
                ], 404
            );
        }
    }
    private function searchById($id)
    {        
        $this->repository->pushCriteria(new RaceByCity($id));
        return $this->repository->get();
    }
    public function destroy($id){

    }
    public function update(Request $request, $id){

    }
    public function store(Request $request){

    }
}

