<?php

namespace App\Http\Controllers\Api\Secured;

use App\Http\Controllers\Api\APIBaseController as BaseController;
use Dingo\Api\Http\Request;
use App\Criterias\Population\WithState;
use App\Criterias\Population\SelectState;
use App\Validators\PopulationByStateValidator;
use App\Repositories\PopulationByStateRepository;
use App\Transformers\PopulationByStateTransformer;

class PopulationByStateController extends BaseController
{    
    public function __construct(PopulationByStateRepository $repository,PopulationByStateValidator $validator,PopulationByStateTransformer $transformer){
        $this->repository = $repository;
        $this->validator = $validator;
        $this->transformer = $transformer;
    }

    public function index(Request $request){
        $this->repository->pushCriteria(new SelectState());
        $this->repository->pushCriteria(new WithState());
        return parent::index($request);
    }   
    public function show($id){

    }
    public function destroy($id){

    }
    public function update(Request $request, $id){

    }
    public function store(Request $request){

    }
}

