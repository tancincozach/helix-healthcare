<?php

namespace App\Http\Controllers\Api\Secured;

use App\Http\Controllers\Api\APIBaseController as BaseController;
use Dingo\Api\Http\Request;
use Criterias\Population\WithCity;
use Criterias\Population\WithCountry;
use Criterias\Population\WithState;
use App\Models\State\PopulationByAgeState;
use App\Models\Counties\PopulationByAgeCounty;
use App\Models\Cities\PopulationByAgeCity;
use App\Models\Cities\PopulationCity;
use App\Models\Counties\PopulationCounty;
use App\Models\State\PopulationState;
use App\Validators\PopulationByCityValidator;
use App\Repositories\PopulationByCityRepository;
use App\Repositories\CitiesRepository;
use App\Repositories\StatesRepository;
use App\Repositories\CountiesRepository;
use App\Repositories\PopulationByStateRepository;
use App\Transformers\PopulationByCityTransformer;

class PopulationController extends BaseController
{    
    public function __construct(PopulationByCityRepository $repository,PopulationByCityValidator $validator,PopulationByCityTransformer $transformer){
        $this->repository = $repository;
        $this->validator = $validator;
        $this->transformer = $transformer;
    }

    public function index(Request $request){
        return parent::index($request);
    }   
    public function show($id){

    }
    public function destroy($id){

    }
    public function update(Request $request, $id){

    }
    public function store(Request $request){

    }
}
