<?php

namespace App\Http\Controllers\Api\Secured;

use App\Http\Controllers\Api\APIBaseController as BaseController;
use Dingo\Api\Http\Request;
use App\Criterias\Population\PopByCitySelect;
use App\Validators\PopulationByCityValidator;
use App\Repositories\PopulationByCityRepository;
use App\Transformers\PopulationByCityTransformer;

class PopulationByCityController extends BaseController
{    
    public function __construct(PopulationByCityRepository $repository,PopulationByCityValidator $validator,PopulationByCityTransformer $transformer){
        $this->repository = $repository;
        $this->validator = $validator;
        $this->transformer = $transformer;
    }

    public function index(Request $request){
        $this->repository->pushCriteria(new PopByCitySelect());
        return parent::index($request);
    }   
    public function show($id){

    }
    public function destroy($id){

    }
    public function update(Request $request, $id){

    }
    public function store(Request $request){

    }
}

