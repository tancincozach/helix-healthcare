<?php

namespace App\Http\Controllers\Api\Secured;

use App\Http\Controllers\Api\APIBaseController as BaseController;
use Dingo\Api\Http\Request;
use App\Criterias\Population\GroupByAgeAndGenderState;
use App\Criterias\Population\ByAgeWithStateId;
use App\Validators\PopulationByAgeStateValidator;
use App\Repositories\PopulationByAgeStateRepository;
use App\Transformers\PopulationByAgeStateTransformer;

class PopulationByAgeStateController extends BaseController
{    
    public function __construct(PopulationByAgeStateRepository $repository,PopulationByAgeStateValidator $validator,PopulationByAgeStateTransformer $transformer){
        $this->repository = $repository;
        $this->validator = $validator;
        $this->transformer = $transformer;
    }

    public function index(Request $request){        
        $this->repository->pushCriteria(new GroupByAgeAndGenderState());
        return parent::index($request);
    }   
    public function show($id){        
        try {
            $result = $this->getPopByStateId($id);
            return $this->response->collection($result, $this->transformer);
        } catch (ModelNotFoundException $e) {
            return response()->json(
                [
                'status_code' => 404,
                'message' => 'Can\'t find the requested resource.'
                ], 404
            );
        }
    }
    private function getPopByStateId($id)
    {        
        $this->repository->pushCriteria(new GroupByAgeAndGenderState());
        $this->repository->pushCriteria(new ByAgeWithStateId($id));
        return $this->repository->get();
    }
    public function destroy($id){

    }
    public function update(Request $request, $id){

    }
    public function store(Request $request){

    }
}

