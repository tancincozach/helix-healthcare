<?php

namespace App\Http\Controllers\Api\Secured;

use App\Http\Controllers\Api\APIBaseController as BaseController;
use Dingo\Api\Http\Request;

class UserController extends BaseController
{
    public function index(Request $request)
    {

        return parent::index($request);
    }

    public function show($id)
    {
        if ('me' == strtolower(trim($id))) {
            $id = request()->user()->id;
        }
        return parent::show($id);
    }

    public function store(Request $request)
    {
        if($request->password) {
            $request->merge([                
                'password' => User::getPasswordHashValue($request->password)
            ]);
        }

        return parent::store($request);
    }

    public function update(Request $request, $id)
    {        
        return parent::update($request, $id);
    }
}
