<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller as BaseController;
use Dingo\Api\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Services\ThirdParty\Geocoding;
use Illuminate\Http\File; 

class GeoCodeController extends BaseController
{
    /**
     * @param $request Dingo Request 
     * 
     * Validates request params if successfull, will call method:censusGeocoding
     * 
    */
    public function index(Request $request){
        set_time_limit(0);		
        ini_set('memory_limit','512M');
        switch($request->action){
            case 'batch':
                $validator = Validator::make($request->all(), [            
                    'address' => 'required|mimes:csv,txt', //Remove space from 'picture'            
                ]);        
                if($validator->fails()) {
                    return response()->json(['error'=>$validator->errors()], 401);   
                }
                return $this->censusGeocoding($request);
            break;
            default:            
                $validator = Validator::make($request->all(), [            
                    'address' => 'required', //Remove space from 'picture'            
                ]);        
                if($validator->fails()) {
                    return response()->json(['error'=>$validator->errors()], 401);   
                }
                return $this->censusGeocoding($request);
            break;

        }        
    }
     /**
     * @param $request  Dingo request
     * 
     * A method that will get make single/multiple request to Census Bureau Geocoding Services
     */    
    private function censusGeocoding(Request $request){        
        try{                                  
            $geocode = new Geocoding();  
            switch($request->action){
                case 'batch':          
                    if(empty($request->file('address'))) throw new \Exception('Empty address csv file');
                    $file_address = $request->file('address')->get();
                    $multipleAddress = preg_split('/\n|\r\n?/', $file_address);
                    $multipleAddressResponse = [];              
                    $resultVintageAndBench = $geocode->getVintageAndBenchmarks();   
                    if(is_array($resultVintageAndBench) && isset($resultVintageAndBench['error']) && !empty($resultVintageAndBench['error'])){
                        throw new \Exception($resultVintageAndBench['error'],$resultVintageAndBench['status_code']);
                    }            
                    if(!empty($multipleAddress) && count($multipleAddress) > 0 ){
                        foreach($multipleAddress as  $key=>$address){
                            if($key > 0 && $address!=''){                                                
                                $multipleAddressResponse[$address]['response'][] = $geocode->trackAddress(str_replace('"','',trim($address)),$resultVintageAndBench['benchmark'],$resultVintageAndBench['vintage']);
                                sleep(3); 
                            }
                        }
                    }                
                  return response()->json( json_encode(  $multipleAddressResponse, JSON_PRETTY_PRINT));          
                break;
                default:                    
                    $resultVintageAndBench = $geocode->getVintageAndBenchmarks();
                    if(is_array($resultVintageAndBench) && isset($resultVintageAndBench['error']) && !empty($resultVintageAndBench['error'])){
                        throw new \Exception($resultVintageAndBench['error'],$resultVintageAndBench['status_code']);
                    }                       
                    $response = $geocode->trackAddress(trim($request->address),$resultVintageAndBench['benchmark'],$resultVintageAndBench['vintage']);
                    if(is_array($response) && isset($response['error']) && !empty($response['error'])){
                        throw new \Exception($response['error'],$response['status_code']);
                    }                               
                     return response()->json( json_encode(  $response, JSON_PRETTY_PRINT));
                break;
            }
        } catch (\Exception $e) {                     
            $status_code = ($e->getCode()=='0' ? 404:$e->getCode());
            return response()->json(array('error'=>$e->getMessage()),$status_code);
        }      
    }
}
