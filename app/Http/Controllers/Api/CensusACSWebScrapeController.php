<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller as BaseController;
use \Prettus\Validator\Exceptions\ValidatorException;
use Dingo\Api\Routing\Helpers;
use Dingo\Api\Http\Request;
use \Prettus\Validator\Contracts\ValidatorInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Api\Http\Response;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Spatie\Geocoder\Geocoder; // Load Spatie Geocoder library
use App\Models\States\State;
use App\Models\Cities\City;
use App\Models\Counties\County;
use App\Models\States\PopulationState;
use App\Models\Counties\PopulationCounty;
use App\Models\Cities\PopulationCity;
use App\Models\States\PopulationByAgeState;
use App\Models\Counties\PopulationByAgeCounty;
use App\Models\Cities\PopulationByAgeCity;
use App\Models\States\PopulationByAgeRaceState;
use App\Models\Counties\PopulationByAgeRaceCounty;
use App\Models\Cities\PopulationByAgeRaceCity;
use CountryState;// Load US based States to get State code to be used for method:processState
use App\Traits\CensusVariableTrait;
use DB;

class CensusACSWebScrapeController extends BaseController implements WebScraperAPIInterface
{           
    use CensusVariableTrait;

    public $apiKey ='';
    public $stateList = array();
    public $ageList = array();
    public $acsYr = '';
    
    public function __construct(){
        $this->apiKey = env('API_KEY_CENSUS');        
    }
    public function index( Request $request){       
        $this->acsYr = $request->acs_year; 
        $this->executeWebScrape();
    }
    public function executeWebScrape(){
        set_time_limit(0);		
        ini_set('memory_limit','512M'); 
        echo ($this->processCensusDataPopulation()) ? 'Successfully Inserted Population (States,Counties and Cities)<br/>':'';   
        echo ($this->processCensusDataPopulationByAge()) ? 'Successfully Inserted PopulationByAge(States,Counties and Cities)<br/>':'';
        echo ($this->processCensusDataPopRaceSexByAge()) ? 'Successfully Inserted PopulationByAge with Race(States,Counties and Cities)<br/>':'';
    } 
    public function collectData($url){
        $response = '';
        $response = file_get_contents($url);
    	$jsonResponse =json_decode($response);
        return $jsonResponse;
    }
    /**
     * Method that will gather data from Census Bureau American Community Survey  and store Population Per (State,Counties and Cities)
     * as well as places coordinates(States,Counties and Cities) to db.
     */
    public function processPlacesWithPopulation(){
        $censusStateResponse =  $this->collectData('https://api.census.gov/data/'.$this->acsYr.'/acs/acs5?get=B01001_001E,NAME&for=state:*');
        $censusCountyResponse =  $this->collectData('https://api.census.gov/data/'.$this->acsYr.'/acs/acs5?get=B01001_001E,NAME&for=county'); 
        $censusCityResponse =  $this->collectData('https://api.census.gov/data/'.$this->acsYr.'/acs/acs5?get=B01001_001E,NAME&for=place');      
        return( 
            State::insertOrUpdate($censusStateResponse,['acsYear'=>$this->acsYr,'stateList'=>CountryState::getStates('US')]) &&
            County::insertOrUpdate($censusCountyResponse,['acsYear'=>$this->acsYr]) &&
            Cities::insertOrUpdate($censusCityResponse,['acsYear'=>$this->acsYr])
        ) ? true:false;
    }  
    /**
     * Method that will gather data from Census Bureau American Community Survey  and store Population Per (State,Counties and Cities) to db.
     */
    public function processCensusDataPopulation(){
        $censusStateResponse =  $this->collectData('https://api.census.gov/data/'.$this->acsYr.'/acs/acs5?get=B01001_001E,NAME&for=state:*');  
        $censusCountyResponse = $this->collectData('https://api.census.gov/data/'.$this->acsYr.'/acs/acs5?get=B01001_001E,NAME&for=county'); 
        $censusCityResponse =   $this->collectData('https://api.census.gov/data/'.$this->acsYr.'/acs/acs5?get=B01001_001E,NAME&for=place');
        return( 
                PopulationState::insertOrUpdate($censusStateResponse,$this->acsYr) &&
                PopulationCounty::insertOrUpdate($censusCountyResponse,$this->acsYr) &&
                PopulationCity::insertOrUpdate($censusCityResponse,$this->acsYr)
            ) ? true:false;
    }
    /**
     * Method that will gather Population By Age/Sex from Census Bureau American Community Survey  and store Population Per (State,Counties and Cities) to db.
    * @return [boolean]
    */
    public function processCensusDataPopulationByAge(){
        // Automatic generation of Census Varable  from B01001_003E to B01001_026E(Male) and B01001_027E to B01001_049E (Female)
        // SEX BY AGE LABEL
        $processStatus = false;
        $censusVariables = $this->getAgeAndSexVariables($this->acsYr);             
        $parameters = [];
        foreach($censusVariables as $key=>$censusData){        
            
            $censusByStateAgeResponse =  $this->collectData('https://api.census.gov/data/'.$this->acsYr.'/acs/acs5?get='.$censusData['censusVariable'].',NAME&for=state:*');
            $censusByCountyResponse =  $this->collectData('https://api.census.gov/data/'.$this->acsYr.'/acs/acs5?get='.$censusData['censusVariable'].',NAME&for=county');
            $censusByCityResponse =  $this->collectData('https://api.census.gov/data/'.$this->acsYr.'/acs/acs5?get='.$censusData['censusVariable'].',NAME&for=place');         
            $processStatus = (
                                 PopulationByAgeState::insertOrUpdate($censusByStateAgeResponse,$censusData) &&
                                 PopulationByAgeCounty::insertOrUpdate($censusByCountyResponse,$censusData)  &&
                                 PopulationByAgeCity::insertOrUpdate($censusByCityResponse,$censusData)
                            ) ? true:false;
        }
        return $processStatus;
    }

 /**
     * Method that will gather Population By Age/Sex from Census Bureau American Community Survey  and store Population Per (State,Counties and Cities) to db.
    * @return [boolean]
    */
    public function processCensusDataPopRaceSexByAge(){
        // Automatic generation of Census Varable  from B01001_003E to B01001_026E(Male) and B01001_027E to B01001_049E (Female)
        // SEX BY AGE LABEL
        $processStatus = false;
        $censusVariables = $this->getRaceVariableBySexAndAge($this->acsYr);
        $parameters = [];
        foreach($censusVariables as $key=>$censusDataVar){
            foreach($censusDataVar as $key=>$var){
                foreach($var as $censusData){
                    $censusByStateAgeResponse =  $this->collectData('https://api.census.gov/data/'.$this->acsYr.'/acs/acs5?get='.$censusData['censusVariable'].',NAME&for=state:*');               
                    $censusByCountyResponse =  $this->collectData('https://api.census.gov/data/'.$this->acsYr.'/acs/acs5?get='.$var['censusVariable'].',NAME&for=county');
                    $censusByCityResponse =  $this->collectData('https://api.census.gov/data/'.$this->acsYr.'/acs/acs5?get='.$var['censusVariable'].',NAME&for=place');
                    $processStatus = (
                        PopulationByAgeRaceState::insertOrUpdate($censusByStateAgeResponse,$censusData)  && 
                        PopulationByAgeRaceCounty::insertOrUpdate($censusByCountyResponse,$censusData)  &&
                        PopulationByAgeRaceCity::insertOrUpdate($censusByCityResponse,$censusData)
                    ) ? true:false;
                }
            }
        }        
    }

     /**
     * Method that will gather Population Poverty Status By Age from Census Bureau American Community Survey  and store Population Per (State,Counties and Cities) to db.
    * @return [boolean]
    */
    public function processCensusDataPopulationByPovertyStatus(){
        // Automatic generation of Census Varable  from B01001_003E to S1701_C01_049E - S1701_C01_057E
        // SEX BY AGE LABEL
        $processStatus = false;
        $censusVariables = $this->getPovertyStatusAgeAndSexVariable();
        
    }   
}