<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller as BaseController;
use \Prettus\Validator\Exceptions\ValidatorException;
use Dingo\Api\Routing\Helpers;
use Dingo\Api\Http\Request;
use App\Jobs\CensusBureauGeoDataToDb;

class CensusCronGeoWebScrapeController extends BaseController 
{           
    
    public function __construct(){
    }
    public function index(Request $request){           
        set_time_limit(0);		
        ini_set('memory_limit','512M'); 
        dispatch(new CensusBureauGeoDataToDb());
    }
}