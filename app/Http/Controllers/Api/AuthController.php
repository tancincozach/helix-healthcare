<?php

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller as BaseController;
use \Prettus\Validator\Exceptions\ValidatorException;
use Dingo\Api\Routing\Helpers;
use Dingo\Api\Http\Request;
use Illuminate\Api\Http\Response;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth; 
use App\User; 
use Validator;
use Exception;
use GuzzleHttp\Client;
use App\Models\User as UModel;
use Laravel\Passport\Client as OClient; 


class AuthController extends BaseController
{

    public $successStatus = 200;
    /**
     * Check if user is authenticated
     *
     * @return object
     */
    public function check()
    {
    
        if (!Auth::check()) {
            return response()->json(
                [
                'status_code' => 401,
                'message' => 'Unauthenticated',
                'authenticated' => false
                ], 401
            );
        }

        return response()->json(
            [
            'status_code' => 200,
            'message' => 'Authenticated',
            'authenticated' => true
            ], 200
        );
    }

    /**
     * Issue ENV variables
     *
     * @return object
     */
    public function getEnv(Request $request)
    {
        $expose = ['API_HELIX_ENDPOINT'];
        $allEnv = [];

        foreach ($_ENV as $key => $value) {
            if(in_array($key, $expose)){
                $allEnv[$key] = $value;
            }
        }

        return $allEnv;
    }

     /** 
     * login api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function getToken() { 
        if(!Auth::attempt(['email' => request('email'), 'password' => request('pass')])){

            return response()->json([
                'message' => 'Unauthorized'
            ], 401);
        }        
        $oClient = OClient::where('password_client', 1)->first();
        return $this->getTokenAndRefreshToken($oClient, request('email'), request('pass'));
        
    }

    public function register(Request $request) { 
        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'email' => 'required|email|unique:users', 
            'pass' => 'required', 
            'c_pass' => 'required|same:pass', 
        ]);
        
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }
        $input = $request->all(); 
        $input['password'] = bcrypt($input['pass']); 
        $user = User::create($input);
        $oClient = OClient::where('password_client', 1)->first();
        return $this->getTokenAndRefreshToken($oClient, $user->email,$input['pass']);
      
    }

    public function getTokenAndRefreshToken(OClient $oClient, $email, $password) {      
        $oClient = OClient::where('password_client', 1)->first();
        $http = new Client;
        $response = $http->request('POST', config('app.url').'/oauth/token', [
            'form_params' => [
                'grant_type' => 'password',
                'client_id' => $oClient->id,
                'client_secret' => $oClient->secret,
                'username' => $email,
                'password' => $password,
                'scope' => '*',
            ],
        ]);

        $result = json_decode((string) $response->getBody(), true);
        return response()->json($result, $this->successStatus);
    }

    public function details() { 
        $user = Auth::user(); 
        return response()->json($user, $this->successStatus); 
    } 

    public function logout(Request $request) {
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }

    public function unauthorized() {         
        return response()->json("unauthorized", 401); 
    } 
}
