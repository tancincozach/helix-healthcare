<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class GeoCodeValidator.
 *
 * @package namespace App\Validators;
 */
class GeoCodeValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => ['a`1' => 'required'],
        ValidatorInterface::RULE_UPDATE => ['name' => 'required'],
    ];
}
