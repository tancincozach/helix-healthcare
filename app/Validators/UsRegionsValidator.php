<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class StatesValidator.
 *
 * @package namespace App\Validators;
 */
class UsRegionsValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => ['name' => 'required','state_id'=>'required','population'=>'required'],
        ValidatorInterface::RULE_UPDATE => ['name' => 'required'],
    ];
}
