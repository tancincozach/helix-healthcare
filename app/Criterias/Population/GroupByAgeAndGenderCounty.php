<?php

namespace App\Criterias\Population;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use DB;

/**
 * Class GroupByAgeAndGenderCounty.
 *
 * @package namespace App\Criteria\Population;
 */
class GroupByAgeAndGenderCounty implements CriteriaInterface
{
    public function __construct()
    {
 
    }

    public function apply($model, RepositoryInterface $repository)
    {
        set_time_limit(0);
        $model = $model->select(DB::raw("  counties.`name` AS county,
                                    counties.id AS county_id,
                                    parent.`id`,
                                    parent.`age_category`,
                                    parent.`population` AS total_population,
                                    parent.m_population,
                                    parent.f_population,
                                    (SELECT 
                                    SUM(population_by_age_counties.population) 
                                  FROM
                                    population_by_age_counties WHERE parent.county_id = population_by_age_counties.county_id) AS  over_all_population,
                                    (SELECT 
                                    SUM(population_by_age_counties.m_population) 
                                  FROM
                                    population_by_age_counties WHERE parent.county_id = population_by_age_counties.county_id) AS  over_all_m_population,
                                    (SELECT 
                                    SUM(population_by_age_counties.f_population) 
                                  FROM
                                    population_by_age_counties WHERE parent.county_id = population_by_age_counties.county_id) AS  over_all_f_population"))

                // $model = $model->select(DB::raw("DISTINCT 
                //                         (counties.name),
                //                         parent.id,                                        
                //                         counties.id AS county_id,
                //                         counties.name AS county,
                //                         parent.age_category,
                //                         parent.age_category_id,
                //                         SUM(parent.population) AS total_population"))
        ->leftJoin('counties', 'counties.id', '=', 'parent.county_id')
        ->leftJoin('categories_age', 'categories_age.id', '=', 'parent.age_category_id')
        ->withTrashed()      
        ->whereRaw('parent.deleted_at IS NULL')
        ->orderBy('parent.age_category_id','ASC');
        return $model;
    }
}
