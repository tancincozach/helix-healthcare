<?php

namespace App\Criterias\Population;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use DB;
/**
 * Class ByAgeWithCityId.
 *
 * @package namespace App\Criteria\Population;
 */
class ByAgeWithCityId implements CriteriaInterface
{
    private $cityID;
    public function __construct($id)
    {
        $this->cityID = $id;
    }

    public function apply($model, RepositoryInterface $repository)
    {

        return $model->where('parent.city_id', '=', trim($this->cityID));
    }
}
