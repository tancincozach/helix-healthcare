<?php

namespace App\Criterias\Population;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class WithCity.
 *
 * @package namespace App\Criteria\Population;
 */
class WithCity implements CriteriaInterface
{
    public function __construct()
    {
 
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->leftJoin('cities', 'cities.id', '=', 'population_cities.city_id');
        return $model;
    }
}
