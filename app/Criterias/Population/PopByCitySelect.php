<?php

namespace App\Criterias\Population;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use DB;

/**
 *
 * @package namespace App\Criteria\Population;
 */
class PopByCitySelect implements CriteriaInterface
{
    public function __construct()
    {

    }

    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->select(DB::raw('population_cities.*,                        
                        cities.state_id,
                        cities.name as city,
                        cities.lat,
                        cities.lng,
                        cities.ne_lat,
                        cities.ne_lng,
                        cities.sw_lat,
                        cities.sw_lng,
                        cities.current_acs_population,                                              
                        cities.acs_year as cities_acs_yr,
                        states.name as state'))
                       ->leftJoin('cities','cities.id', '=', 'population_cities.city_id')
                       ->leftJoin('states', 'states.id', '=', 'cities.state_id');
                       

        return $model;
    }
}
