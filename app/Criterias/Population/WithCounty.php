<?php

namespace App\Criterias\Population;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 *
 * @package namespace App\Criteria\Population;
 */
class WithCounty implements CriteriaInterface
{
    public function __construct()
    {

    }

    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->leftJoin('counties', 'counties.id', '=', 'population_counties.county_id');
        return $model;
    }
}
