<?php

namespace App\Criterias\Population;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use DB;

class SelectState implements CriteriaInterface
{
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->select(DB::raw('population_states.*,states.name as state'));
    }
}
