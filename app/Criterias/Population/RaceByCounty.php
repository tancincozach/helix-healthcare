<?php

namespace App\Criterias\Population;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use DB;
/**
 * Class RaceByCounty.
 *
 * @package namespace App\Criteria\Population;
 */
class RaceByCounty implements CriteriaInterface
{
    private $countyID;
    public function __construct($id)
    {
      $this->countyID = $id;
    }

    public function apply($model, RepositoryInterface $repository)
    {
      return $model->where('county_id', '=', trim($this->countyID));                              
    
    }
}
