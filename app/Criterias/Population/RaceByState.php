<?php

namespace App\Criterias\Population;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use DB;
/**
 * Class RaceByState.
 *
 * @package namespace App\Criteria\Population;
 */
class RaceByState implements CriteriaInterface
{
    private $stateID;
    public function __construct($id)
    {
      $this->stateID = $id;
    }

    public function apply($model, RepositoryInterface $repository)
    {
      return $model->where('state_id', '=', trim($this->stateID));                              
    
    }
}
