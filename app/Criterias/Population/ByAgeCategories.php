<?php

namespace App\Criterias\Population;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use DB;

/**
 * Class ByAgeCategories.
 *
 * @package namespace App\Criteria\Population;
 */
class ByAgeCategories implements CriteriaInterface
{
    public function __construct()
    {
 
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->select(DB::raw('SELECT DISTINCT(age_category) as  age_categories'));
        return $model;
    }
}
