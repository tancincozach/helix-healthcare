<?php

namespace App\Criterias\Population;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use DB;

/**
 * Class GroupByAgeAndGenderCity.
 *
 * @package namespace App\Criteria\Population;
 */
class GroupByAgeAndGenderCity implements CriteriaInterface
{
    public function __construct()
    {
 
    }

    public function apply($model, RepositoryInterface $repository)
    {
        set_time_limit(0);
        $model = $model->select(DB::raw("cities.`name` AS city,
                          cities.id AS city_id,
                          parent.`id`,
                          parent.`age_category`,
                          parent.`population` AS total_population,
                          parent.m_population,
                          parent.f_population,
                          (SELECT 
                          SUM(population_by_age_cities.population) 
                        FROM
                          population_by_age_cities WHERE parent.city_id = population_by_age_cities.city_id) AS  over_all_population,
                          (SELECT 
                          SUM(population_by_age_cities.m_population) 
                        FROM
                          population_by_age_cities WHERE parent.city_id = population_by_age_cities.city_id) AS  over_all_m_population,
                          (SELECT 
                          SUM(population_by_age_cities.f_population) 
                        FROM
        population_by_age_cities WHERE parent.city_id = population_by_age_cities.city_id) AS  over_all_f_population"))
        ->leftJoin('cities', 'cities.id', '=', 'parent.city_id')
        ->leftJoin('categories_age', 'categories_age.id', '=', 'parent.age_category_id')        
        ->withTrashed()      
        ->whereRaw('parent.deleted_at IS NULL')
        ->orderBy('parent.age_category_id','ASC');

        return $model;
    }
}
