<?php

namespace App\Criterias\Population;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use DB;

/**
 *
 * @package namespace App\Criteria\Population;
 */
class PopByCountySelect implements CriteriaInterface
{
    public function __construct()
    {

    }

    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->select(DB::raw('population_counties.*,
                        counties.census_county_id,
                        counties.state_id,
                        counties.name as county,
                        counties.lat,
                        counties.lng,
                        counties.ne_lat,
                        counties.ne_lng,
                        counties.sw_lat,
                        counties.sw_lng,
                        counties.current_acs_population,                                              
                        counties.acs_year as counties_acs_yr,
                        states.name as state'))
                       ->leftJoin('counties','counties.id', '=', 'population_counties.county_id')
                       ->leftJoin('states', 'states.id', '=', 'counties.state_id');
                       

        return $model;
    }
}
