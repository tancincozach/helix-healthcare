<?php

namespace App\Criterias\Population;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use DB;
/**
 * Class GroupByAgeAndGenderState.
 *
 * @package namespace App\Criteria\Population;
 */
class GroupByAgeAndGenderState implements CriteriaInterface
{
    public function __construct()
    {
 
    }

    public function apply($model, RepositoryInterface $repository)
    {

        $model = $model->select(DB::raw("   states.`name` AS state,
                                            states.id AS state_id,
                                            parent.`id`,
                                            parent.`age_category`,
                                            parent.`population` AS total_population,
                                            parent.m_population,
                                            parent.f_population,
                                            (SELECT 
                                            SUM(population_by_age_states.population) 
                                          FROM
                                            population_by_age_states WHERE parent.state_id = population_by_age_states.state_id) AS  over_all_population,
                                            (SELECT 
                                            SUM(population_by_age_states.m_population) 
                                          FROM
                                            population_by_age_states WHERE parent.state_id = population_by_age_states.state_id) AS  over_all_m_population,
                                            (SELECT 
                                            SUM(population_by_age_states.f_population) 
                                          FROM
                                            population_by_age_states WHERE parent.state_id = population_by_age_states.state_id) AS  over_all_f_population
                                            "))
                       ->leftJoin('states', 'states.id', '=', 'state_id')
                       ->leftJoin('categories_age', 'categories_age.id', '=', 'age_category_id')                       
                       ->withTrashed()      
                       ->whereRaw('parent.deleted_at IS NULL')
                       ->orderBy('parent.age_category_id','ASC');
                       
        return $model;
    
    }
}
