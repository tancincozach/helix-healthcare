<?php

namespace App\Criterias\Population;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use DB;
/**
 * Class ByAgeWithStateId.
 *
 * @package namespace App\Criteria\Population;
 */
class ByAgeWithStateId implements CriteriaInterface
{
    private $stateID;
    public function __construct($id)
    {
        $this->stateID = $id;
    }

    public function apply($model, RepositoryInterface $repository)
    {

        return $model->where('parent.state_id', '=', trim($this->stateID));
    }
}
