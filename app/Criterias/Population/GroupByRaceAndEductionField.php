<?php

namespace App\Criterias\Population;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use DB;

/**
 * Class GroupByAgeAndGenderCity.
 *
 * @package namespace App\Criteria\Population;
 */
class GroupByRaceAndEductionFIeld implements CriteriaInterface
{
    public function __construct()
    {
 
    }

    public function apply($model, RepositoryInterface $repository)
    {
        set_time_limit(0);
        $model = $model->select(DB::raw("population_edu_fld_race_states.*,states.`name` AS state,
        states.id AS state_id
         "))
->leftJoin('states', 'states.id', '=', 'state_id')
->leftJoin('categories_race', 'categories_race.id', '=', 'race_category_id')                       
->withTrashed()      
->whereRaw('population_edu_fld_race_states.deleted_at IS NULL');

        return $model;
    }
}
