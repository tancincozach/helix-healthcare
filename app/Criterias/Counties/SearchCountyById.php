<?php

namespace App\Criterias\Counties;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class UsernameOrEmailCriteria.
 *
 * @package namespace App\Criteria\User;
 */
class SearchCountyById implements CriteriaInterface
{
    private $countyID;    

    public function __construct($id)
    {
        $this->countyID = $id;

    }

    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where('counties.id', '=', trim($this->countyID));
    }
}
