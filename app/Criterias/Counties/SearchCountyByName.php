<?php

namespace App\Criterias\Counties;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class UsernameOrEmailCriteria.
 *
 * @package namespace App\Criteria\User;
 */
class SearchCountyByName implements CriteriaInterface
{
    private $countyName;    

    public function __construct($name)
    {
        $this->countyName = $name;

    }

    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where('counties.name', 'like', trim($this->countyName).'%');
    }
}
