<?php

namespace App\Criterias\Cities;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 *
 * @package namespace App\Criteria\Population;
 */
class WithState implements CriteriaInterface
{
    public function __construct()
    {

    }

    public function apply($model, RepositoryInterface $repository)
    {

        $model = $model->leftJoin('states', 'states.id', '=', 'cities.state_id');
        return $model;
    }
}
