<?php

namespace App\Criterias\Cities;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class UsernameOrEmailCriteria.
 *
 * @package namespace App\Criteria\User;
 */
class SearchCityByName implements CriteriaInterface
{
    private $cityName;    

    public function __construct($name)
    {
        $this->cityName = $name;

    }

    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where('cities.name', 'like', trim($this->cityName).'%');
    }
}
