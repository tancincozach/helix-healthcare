<?php

namespace App\Criterias\Cities;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class UsernameOrEmailCriteria.
 *
 * @package namespace App\Criteria\User;
 */
class SearchCityById implements CriteriaInterface
{
    private $cityID;    

    public function __construct($id)
    {
        $this->cityID = $id;

    }

    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where('cities.id', '=', trim($this->cityID));
    }
}
