<?php

namespace App\Criterias\User;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class UsernameOrEmail.
 *
 * @package namespace App\Criteria\User;
 */
class UsernameOrEmail implements CriteriaInterface
{
    private $username;
    private $email;

    public function __construct($username, $email)
    {
        $this->username = $username;
        $this->email = $email;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where('users.user_name', '=', trim($this->username))
                     ->orWhere('users.email', '=', trim($this->email));
    }
}
