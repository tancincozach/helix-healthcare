<?php

namespace App\Criterias\User;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class SearchByEmail.
 *
 * @package namespace App\Criteria\User;
 */
class SearchByEmail implements CriteriaInterface
{
    private $email;
    private $withTrash;

    public function __construct($email, $withTrash = false)
    {
        $this->email = $email;
        $this->withTrash = $withTrash;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $model = ($this->withTrash)
                ? $model->where('email', $this->email)->withTrashed()
                : $model->where('email', $this->email);

        return $model;
    }
}
