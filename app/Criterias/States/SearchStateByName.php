<?php

namespace App\Criterias\States;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class SearchStateByName.
 *
 * @package namespace App\Criteria\States;
 */
class SearchStateByName implements CriteriaInterface
{
    private $stateName;    

    public function __construct($name)
    {
        $this->stateName = $name;

    }

    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where('states.name', 'like', trim($this->stateName).'%');
    }
}
