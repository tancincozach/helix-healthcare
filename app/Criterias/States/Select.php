<?php

namespace App\Criterias\States;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;
use DB;

class Select implements CriteriaInterface
{
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->select(DB::raw('counties.*,states.name as state'));
    }
}
