<?php

namespace App\Criterias\States;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class SearchStateById.
 *
 * @package namespace App\Criteria\States;
 */
class SearchStateById implements CriteriaInterface
{
    private $stateID;    

    public function __construct($id)
    {
        $this->stateID = $id;

    }

    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where('states.id', '=', trim($this->stateID));
    }
}
