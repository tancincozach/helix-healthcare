<?php

namespace App\Traits;
use App\Models\AgeCategories;
use App\Models\RaceCategories;
use App\Models\FamilyCategories;
use App\Models\EducationCategories;
use App\Models\LanguageSpokenCategories;
use App\Models\HouseHoldSizeCategories;
use DB;

trait CensusVariableTrait{

    public function getAgeAndSexVariables($acsYear){
        $censusVariables=[];
        $itr=0;$ctr=0;
        $sexByAgeVariable='';
        $results = AgeCategories::select(DB::raw('id,name'))->where('census_group_variable','SEX BY AGE')
        ->orderBy('id','ASC')
        ->get();
        if(!empty($results)){
            $ageCategories = $results->toArray();
            for($i=0;$i<=49;$i++){
                if($i==26){
                    $ctr=0;
                }else{                    
                    if($i>=3){
                        $format = ($i<=9) ? 'B01001_00%uE':'B01001_0%uE';
                        if($i<=25){
                            $censusVariables['mVar'][$itr]=array(
                                'ageCategory'=>$ageCategories[$ctr]['name'],
                                'ageCatID'=>$ageCategories[$ctr]['id'],
                                'censusVariable'=>sprintf($format, $i,$sexByAgeVariable),
                                'gender'=>'M',  
                                'acsYear'=>$acsYear                                                  
                            );
                        }else{
                            $censusVariables['fVar'][$itr]=array(
                                'ageCategory'=>$ageCategories[$ctr]['name'],
                                'ageCatID'=>$ageCategories[$ctr]['id'],
                                'censusVariable'=>sprintf($format, $i,$sexByAgeVariable),
                                'gender'=>'F',  
                                'acsYear'=>$acsYear                                                  
                            );
                        }
                        $ctr++;
                        $itr++;
                    }                    
                }  
            }
        }
        return $censusVariables;
    }

    public function getAgeAndSexPovertyStatusVariables($acsYear){
        $censusVariables=[];
        $itr=0;$ctr=0;
        $sexByAgeVariable='';
        $results = AgeCategories::select(DB::raw('id,name'))->where('census_group_variable','POVERTY STATUS IN THE PAST 12 MONTHS BY SEX BY AGE')
        ->orderBy('id','ASC')
        ->get();
        if(!empty($results)){
            $ageCategories = $results->toArray();
            $censusVariables['Total']['overall'] = array('censusVariable'=>'B17001_001E','acs_year'=>$acsYear);
            $censusVariables['Total']['male'] = array('censusVariable'=>'B17001_003E','acs_year'=>$acsYear);
            $censusVariables['Total']['female'] = array('censusVariable'=>'B17001_017E','acs_year'=>$acsYear);

            for($i=0;$i<=30;$i++){
                if($i==16){
                    $ctr=0;
                }else{                    
                    if($i>=3){
                        $format = ($i<=9) ? 'B01001_00%uE':'B01001_0%uE';
                        if($i<=16){
                            $censusVariables['mVar'][$itr]=array(
                                'ageCategory'=>$ageCategories[$ctr]['name'],
                                'ageCatID'=>$ageCategories[$ctr]['id'],
                                'censusVariable'=>sprintf($format, $i,$sexByAgeVariable),
                                'gender'=>'M',  
                                'acsYear'=>$acsYear                                                  
                            );
                            $ctr++;
                        }
                        else{                            
                            if($i > 17){
                                $censusVariables['fVar'][$itr]=array(
                                    'ageCategory'=>$ageCategories[$ctr]['name'],
                                    'ageCatID'=>$ageCategories[$ctr]['id'],
                                    'censusVariable'=>sprintf($format, $i,$sexByAgeVariable),
                                    'gender'=>'F',  
                                    'acsYear'=>$acsYear                                                  
                                );
                                $ctr++;
                            }
                                           
                        }                        
                        $itr++;
                    }                    
                }  
            }
        }
        return $censusVariables;
    }
    public function getRaceVariableByRace($acsYear) {     
        $raceResults = RaceCategories::
        where('census_group_variable','RACE')
        ->orwhere('census_group_variable','HISPANIC OR LATINO ORIGIN BY SPECIFIC ORIGIN')
        ->get();
        if(!empty($raceResults)){
            $censusVariables= collect($raceResults,$acsYear)->map(function($race) use ($acsYear){
                            return [
                                'raceCatID'=>$race->id,
                                'race'=>$race->name,
                                'censusVariable'=>$race->census_variable_prefix,
                                'acs_year'=>$acsYear
                        ];      
                });  
            $censusVariables = $censusVariables->toArray();
        }
        return $censusVariables;
    }
    public function getEducationalFieldAttainmentByRace($acsYear){
        $raceResults = RaceCategories::
        where('census_group_variable','FIELD OF BACHELORS DEGREE FOR FIRST MAJOR THE POPULATION 25 YEARS AND OVER')        
        ->get();

        $eduFieldResults = EducationCategories::
        where('census_group_variable','FIELD OF BACHELORS DEGREE FOR FIRST MAJOR THE POPULATION 25 YEARS AND OVER')        
        ->get();

        if(!empty($raceResults) && !empty($eduFieldResults)){
            //Bachelor's Degree
            //Science and Engineering Bachelor's Degree
            //Science Engineering Related Bachelor's Degree
            //Business Bachelor's Degree
            //Education Bachelor's Degree
            //Arts/Humanities/Other Bachelor's Degree
            $temp = [];
            $return = [];
            foreach($raceResults as $raceRow){             
                    $censVar =[];
                    $ctr=1;
                    foreach($eduFieldResults as $eduRow){
                        $censVar[] = sprintf(($ctr<=9) ? $raceRow->census_variable_prefix.'_00%uE':$raceRow->census_variable_prefix.'_0%uE', $ctr,'');
                        $ctr++;
                    } 
                    $return['censusVar'][] =['censusVariable'=>implode(',',$censVar)];                    
            }
            $return['raceResult'] = $raceResults->toArray();
            $return['eduFieldResults'] = $eduFieldResults->toArray();
        }        
        return $return;
    }
    public function getEducationAttainment($acsYear){

        $educationResults = EducationCategories::
        where('census_group_variable','POVERTY STATUS IN THE PAST 12 MONTHS OF INDIVIDUALS BY SEX BY EDUCATIONAL ATTAINMENT')        
        ->get();
        $censusVariables = array();
        if(!empty($educationResults)){
            $censusVariables= collect($educationResults,$acsYear)->map(function($education) use ($acsYear){
                return [
                    'edCatID'=>$education->id,
                    'education'=>$education->name,
                    'censusVariable'=>$education->census_variable_prefix,
                    'acs_year'=>$acsYear
                ];      
            });  
            $censusVariables = $censusVariables->toArray();
        }
        return $censusVariables;
    }
    public function getEmployment($acsYear){

        $employmentResults = EmploymentCategories::
        where('census_group_variable','POVERTY STATUS IN THE PAST 12 MONTHS OF INDIVIDUALS BY SEX BY EMPLOYMENT STATUS')        
        ->get();        
        $censusVariables = array();
        if(!empty($employmentResults)){
            $censusVariables= collect($employmentResults,$acsYear)->map(function($emp) use ($acsYear){
                return [
                    'empCatID'=>$emp->id,
                    'employment'=>$emp->name,
                    'censusVariable'=>$emp->census_variable_prefix,
                    'acs_year'=>$acsYear
                ];      
            });  
            $censusVariables = $censusVariables->toArray();
        }        
        return $censusVariables;
    }
    public function getLangSpoken($acsYear){

        $langSpokenResults = LanguageSpokenCategories::
        where('census_group_variable','POVERTY STATUS IN THE PAST 12 MONTHS BY AGE BY LANGUAGE SPOKEN AT HOME FOR THE POPULATION 5 YEARS AND OVER')        
        ->get();        
        $censusVariables = array();
        if(!empty($langSpokenResults)){
            $censusVariables= collect($langSpokenResults,$acsYear)->map(function($lang) use ($acsYear){
                return [
                    'langCatID'=>$lang->id,
                    'langCategory'=>$lang->name,
                    'censusVariable'=>$lang->census_variable_prefix,
                    'acs_year'=>$acsYear
                ];      
            });  
            $censusVariables = $censusVariables->toArray();
        }        
        return $censusVariables;
    }
    public function getHouseHoldSize($acsYear){

        $houseHoldSizetResults = HouseHoldSizeCategories::
        where('census_group_variable','HOUSEHOLD TYPE BY HOUSEHOLD SIZE')
        ->get();        
        $censusVariables = array();
        if(!empty($houseHoldSizetResults)){
            $censusVariables= collect($houseHoldSizetResults,$acsYear)->map(function($hhold) use ($acsYear){
                return [
                    'hHoldID'=>$hhold->id,
                    'hHoldCategory'=>$hhold->name,
                    'censusVariable'=>$hhold->census_variable_prefix,
                    'acs_year'=>$acsYear
                ];      
            });  
            $censusVariables = $censusVariables->toArray();
        }        
        return $censusVariables;
    }
    public function  getBelowPovertyLvlVariableByRace($acsYear) {     
        $raceResults = RaceCategories::
        where('census_group_variable','POVERTY STATUS IN THE PAST 12 MONTHS')
        ->get();
        $censusVariables = array();
        if(!empty($raceResults)){
            $censusVariables= collect($raceResults,$acsYear)->map(function($race) use ($acsYear){
                            return [
                                'raceCatID'=>$race->id,
                                'race'=>$race->name,
                                'censusVariable'=>$race->census_variable_prefix,
                                'censusVariableGenderTotal'=>$race->other_census_variable_prefix,
                                'acs_year'=>$acsYear
                        ];      
                });  
            $censusVariables = $censusVariables->toArray();
        }
        return $censusVariables;
    }
    public function  getBelowPovertyLvlVariableByFamily($acsYear) {     
        $famCatResults = FamilyCategories::
        where('census_group_variable','POVERTY STATUS IN THE PAST 12 MONTHS OF FAMILIES BY HOUSEHOLD TYPE BY NUMBER OF WORKERS IN FAMILY')
        ->orwhere('census_group_variable','POVERTY STATUS IN THE PAST 12 MONTHS OF FAMILIES BY FAMILY TYPE BY PRESENCE OF RELATED CHILDREN UNDER 18 YEARS BY AGE OF RELATED CHILDREN')
        ->get();                
        $censusVariables = array();
        if(!empty($famCatResults)){
            $censusVariables= collect($famCatResults,$acsYear)->map(function($family) use ($acsYear){
                            return [
                                'famCatID'=>$family->id,
                                'family_category'=>$family->name,
                                'censusVariable'=>$family->census_variable_prefix,                             
                                'acs_year'=>$acsYear
                        ];      
                });  
            $censusVariables = $censusVariables->toArray();
        }        
        return $censusVariables;
    }
    public function getRaceVariableBySexAndAge($acsYear) {     
        $raceResults = RaceCategories::all();
        $censusVariables = array();
        if(!empty($raceResults)){
            $censusVariables = collect($raceResults,$acsYear)->map(function($rowRace,$year) use ($acsYear){
                                $tempArr = array();
                                $ageResults = AgeCategories::select(DB::raw('id,name'))
                                                    ->where('census_group_variable','SEX BY AGE WITH RACE')
                                                ->orderBy('id','ASC')
                                                ->get();
                                $ageCategories = $ageResults->toArray();    
                                if(!empty($ageCategories)){                    
                                    $ctr=0;
                                    $itr=0;                                        
                                    for($i=0;$i<=31;$i++){
                                        if($i==17){
                                            $ctr=0;
                                        }else{               
                                            if($i>=3){
                                                $format = ($i<=9) ? $rowRace->census_variable_prefix.'_00%uE':$rowRace->census_variable_prefix.'_0%uE';
                                                $tempArr[str_replace(' ','_',$rowRace->name)][$itr]= array(
                                                                            'ageCategory'=>$ageCategories[$ctr]['name'],
                                                                            'ageCatID'=>$ageCategories[$ctr]['id'],
                                                                            'censusVariable'=>sprintf($format, $i,''),
                                                                            'race'=>$rowRace->name,
                                                                            'raceCatID'=>$rowRace->id,
                                                                            'gender'=>($i<=16) ?'M':'F',
                                                                            'acsYear'=>$acsYear
                                                                        );
                                                $ctr++;
                                                $itr++;
                                            }                                                 
                                        }  
                                    }                   
                                }      
                                return  $tempArr;       
                });  
        }
        return $censusVariables;
    }
    public function getPovertyStatusAgeAndSexVariable($acsYear){
        $results = AgeCategories::where('census_group_variable','POVERTY STATUS IN THE PAST 12 MONTHS')
                                    ->orderBy('id','ASC')
                                    ->get();
        $i=49;
        if(!empty($results)){
            foreach($results as $row){
                $censusVariables[$i] = array(
                                                'ageCategory'=>$row->name,
                                                'censusVariable'=>sprintf('S1701_C01_0%uE', $i,''),
                                                'ageCatID'=>$row->id,
                                                'ageCategory'=>$row->name
                                            );
                $i++;
            }
            return $censusVariables;
        }
    }

}

?>
