<?php

namespace App\Traits;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Spatie\Geocoder\Geocoder; // Load Spatie Geocoder library

trait GeolocationTrait{

 /**
	 * Method that will get the geolocation using Google Geocdoer
	 * information based on supplied Address 
	 * @return [array]
	 */
	public static function getLocation(){
		$ocation = array();
	    $Client   = new \GuzzleHttp\Client();
		$GeoCoder = new Geocoder($Client);
		$GeoCoder->setApiKey(config('geocoder.key'));
		$GeoCoder->setCountry('US');	
		$Location = $GeoCoder->getCoordinatesForAddress($Address);
		$Location['ne_lat'] = @$Location['viewport']->northeast->lat;
		$Location['ne_lng'] = @$Location['viewport']->northeast->lng;
		$Location['sw_lat'] = @$Location['viewport']->southwest->lat;
		$Location['sw_lng'] = @$Location['viewport']->southwest->lng;
		unset($Client);
		unset($GeoCoder);
		return $Location;
    }
    /**
	 * Method that assigns coordinates to be used for tables(Cities,States,Counties)
	 * @return [array]
	 */
    public static function getCoordinates($coord_row){
        $coordinates = array();
        if(empty($coord_row)){
            if(((empty($coord_row->lat) || empty($coord_row->lng) || empty($coord_row->ne_lat)  || empty($coord_row->sw_lat)) || ($coord_row->lat=='0.000000' || $coord_row->lng=='0.000000' || $coord_row->ne_lat=='0.000000' || $coord_row->sw_lat=='0.000000'))){
                if(isset($coord_row->name) && !empty($coord_row->name)){
                    $LocationData = self::getLocation($coord_row->name);
                    if(empty($coord_row->lat)){
                        $coordinates['lat'] = $LocationData['lat'];
                    }                    
                    if(empty($coord_row->lat)){
                        $coordinates['lng'] = $LocationData['lng'];
                    }                    
                    if(empty($coord_row->ne_lat)){
                        $coordinates['ne_lat'] = $LocationData['ne_lat'];
                    }                    
                    if(empty($coord_row->sw_lat)){
                        $coordinates['sw_lat'] = $LocationData['sw_lat'];
                    }
                }                
            }
        }
        return $coordinates;
    }

}

?>