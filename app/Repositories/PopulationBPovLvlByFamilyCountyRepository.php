<?php

namespace App\Repositories;

use App\Models\Counties\PopulationBPovFamilyCounty;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;
use App\Validators\PopulationBPovByFamilyCountyValidator;

/**
 * Interface PopulationBPovLvlByFamilyCountyRepository.
 *
 * @package namespace App\Repositories;
 */
class PopulationBPovLvlByFamilyCountyRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;
    
    protected $cacheExcept = [];

     /**
     * Sepcify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return PopulationBPovByFamilyCountyValidator::class;
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
       return PopulationBPovFamilyCounty::class;
    }
}
