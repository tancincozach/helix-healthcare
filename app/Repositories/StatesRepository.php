<?php

namespace App\Repositories;

use App\Models\States\State;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;
use App\Validators\StatesValidator;
use App\Traits\GeolocationTrait;
use DB;
/**
 * Interface StatesRepository.
 *
 * @package namespace App\Repositories;
 */
class StatesRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;
    use GeolocationTrait;

    protected $cacheExcept = [];

     /**
     * Sepcify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return StatesValidator::class;
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return State::class;
    }
}
