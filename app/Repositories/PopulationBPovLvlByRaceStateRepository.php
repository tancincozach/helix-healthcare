<?php

namespace App\Repositories;

use App\Models\States\PopulationBPovRaceState;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;
use App\Validators\PopulationByRaceStateValidator;

/**
 * Interface PopulationBPovLvlByRaceStateRepository.
 *
 * @package namespace App\Repositories;
 */
class PopulationBPovLvlByRaceStateRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;
    
    protected $cacheExcept = [];

     /**
     * Sepcify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return PopulationByRaceStateValidator::class;
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
       return PopulationBPovRaceState::class;
    }
}
