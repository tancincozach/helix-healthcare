<?php

namespace App\Repositories;

use App\Models\Cities\PopulationByHouseholdCity;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;
use App\Validators\PopulationByHHoldCityValidator;

/**
 * Interface StatesRepository.
 *
 * @package namespace App\Repositories;
 */
class PopulationByHHoldSizeCityRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;
    
    protected $cacheExcept = [];

     /**
     * Sepcify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return PopulationByHHoldCityValidator::class;
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
       return PopulationByHouseholdCity::class;
    }
}
