<?php

namespace App\Repositories;

use App\Models\States\PopulationEduByRaceState;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;
use App\Validators\PopulationByRaceStateValidator;

/**
 * Interface PopulationEduByRaceStateRepository.
 *
 * @package namespace App\Repositories;
 */
class PopulationEduByRaceStateRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;
    
    protected $cacheExcept = [];

     /**
     * Sepcify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return PopulationByRaceStateValidator::class;
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
       return PopulationEduByRaceState::class;
    }
}
