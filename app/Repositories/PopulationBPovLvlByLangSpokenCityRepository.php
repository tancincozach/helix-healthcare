<?php

namespace App\Repositories;

use App\Models\Cities\PopulationBPovLangSpokenCity;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;
use App\Validators\PopulationBPovLangSpokenCityValidator;

/**
 * Interface PopulationBPovLvlByLangSpokenCityRepository.
 *
 * @package namespace App\Repositories;
 */
class PopulationBPovLvlByLangSpokenCityRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;
    
    protected $cacheExcept = [];

     /**
     * Sepcify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return PopulationBPovLangSpokenCityValidator::class;
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
       return PopulationBPovLangSpokenCity::class;
    }
}
