<?php

namespace App\Repositories;

use App\Models\Counties\PopulationBPovRaceCounty;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;
use App\Validators\PopulationByRaceCountyValidator;

/**
 * Interface PopulationBPovLvlByRaceCountyRepository.
 *
 * @package namespace App\Repositories;
 */
class PopulationBPovLvlByRaceCountyRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;
    
    protected $cacheExcept = [];

     /**
     * Sepcify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return PopulationByRaceCountyValidator::class;
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
       return PopulationBPovRaceCounty::class;
    }
}
