<?php

namespace App\Repositories;

use App\Models\AgeCategories;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;
use App\Validators\AgeCategoriesValidator;

/**
 * Interface StatesRepository.
 *
 * @package namespace App\Repositories;
 */
class AgeCategoriesRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;
    
    protected $cacheExcept = [];

     /**
     * Sepcify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return AgeCategoriesValidator::class;
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return AgeCategories::class;
    }
}
