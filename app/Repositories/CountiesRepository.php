<?php

namespace App\Repositories;

use App\Models\Counties\County;
use App\Models\States\State;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;
use App\Validators\CountiesValidator;
use App\Traits\GeolocationTrait;
use DB;

/**
 * Interface CountiesRepository.
 *
 * @package namespace App\Repositories;
 */
class CountiesRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;
    use GeolocationTrait;
    
    protected $cacheExcept = [];

     /**
     * Sepcify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return CountiesValidator::class;
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return County::class;
    }    
}
