<?php

namespace App\Repositories;

use App\Models\States\PopulationBPovAgeState;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;
use App\Validators\PopulationByAgeStateValidator;

/**
 * Interface PopulationBPovLvlByAgeStateRepository.
 *
 * @package namespace App\Repositories;
 */
class PopulationBPovLvlByAgeStateRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;
    
    protected $cacheExcept = [];

     /**
     * Sepcify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return PopulationByAgeStateValidator::class;
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
       return PopulationBPovAgeState::class;
    }
}
