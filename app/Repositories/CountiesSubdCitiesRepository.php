<?php

namespace App\Repositories;

use GuzzleHttp\Client;
use App\Models\CountiesSubdivisionCities;
use App\Models\CountiesSubdivision;
use App\Models\States\State;
use App\Models\Counties\County;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;
use App\Validators\CountiesSubdCitiesValidator;
use DB;

/**
 * Interface StatesRepository.
 *
 * @package namespace App\Repositories;
 */
class CountiesSubdCitiesRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;
    
    protected $cacheExcept = [];

     /**
     * Sepcify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return CountiesSubdCitiesValidator::class;
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return CountiesSubdivisionCities::class;
    }

    public function collectData($url){
        $http = new Client;
        $response = $http->request('GET',$url);
        $result = json_decode((string) $response->getBody(), true);
        return $result;
    }

    public function importCountiesSubdivisionCities($params=array()){
        CountiesSubdivision::select(DB::raw('counties_subdivision.*,counties.name as county,states.name as state,states.census_state_id,counties.census_county_id'))
                            ->join('states','states.id','=','counties_subdivision.state_id')
                            ->join('counties','counties.id','=','counties_subdivision.county_id')
                                ->chunk(100, function ($countiesSubdChunks) use($params){
            collect($countiesSubdChunks)->map(function($countiesSubd) use ($params){
                    $countySubdCitiesResponse = $this->collectData(
                                                                    'https://api.census.gov/data/'.$params['acsYear'].
                                                                    '/pep/population?get=GEONAME,POP&for=place:*&in=state:'.$countiesSubd->census_state_id.'%20county:'.$countiesSubd->census_county_id.'%20county%20subdivision:'.$countiesSubd->census_cs_id.'&key='.$params['apiKey']
                    );
                    collect($countySubdCitiesResponse)->map(function($CountySubCitiesData,$key) use ($countiesSubd,$params){ 
                        if($key > 0){              
                            $countySubdCities = array( 'name'=> ucwords($CountySubCitiesData[0]),
                            'cs_id'=>$countiesSubd->id,
                            'state_id'=>$countiesSubd->state_id,
                            'county_id'=>@$countiesSubd->county_id,
                            'current_population'=>$CountySubCitiesData[1],
                            'census_csc_id'=>$CountySubCitiesData[5]);
                            $countySubdCities = !empty($locationData) ? array_merge($countySubdCities,$locationData):$countySubdCities;
                            $this->model->firstOrCreate($countySubdCities);
                        }
                    });    
            });
        });
    }
    public function importCountiesSubdCities($countySubdCitiesResponse){
        collect($countySubdCitiesResponse)->map(function($CountySubCitiesData,$key){ 
            if($key > 0){                                                                             
                $this->model->firstOrCreate(array(
                    'name'=>ucwords($CountySubCitiesData[0]),
                    'current_population'=>$CountySubCitiesData[1],
                    'state_id'=>$countiesSubd->state_id,
                    'county_id'=>@$countiesSubd->county_id,
                    'cs_id'=>$countiesSubd->id,
                    'census_csc_id'=>$CountySubCitiesData[5]
                ));
            }
        });
    }
}
