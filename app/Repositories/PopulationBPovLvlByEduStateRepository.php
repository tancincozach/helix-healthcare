<?php

namespace App\Repositories;

use App\Models\States\PopulationBPovEduState;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;
use App\Validators\PopulationBPovByEduStateValidator;

/**
 * Interface PopulationBPovLvlByEduStateRepository.
 *
 * @package namespace App\Repositories;
 */
class PopulationBPovLvlByEduStateRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;
    
    protected $cacheExcept = [];

     /**
     * Sepcify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return PopulationBPovByEduStateValidator::class;
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
       return PopulationBPovEduState::class;
    }
}
