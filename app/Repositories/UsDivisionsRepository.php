<?php

namespace App\Repositories;

use App\Models\UsDivisions;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;
use App\Validators\UsDivisionsValidator;

/**
 * Interface StatesRepository.
 *
 * @package namespace App\Repositories;
 */
class UsDivisionsRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;
    
    protected $cacheExcept = [];

     /**
     * Sepcify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return UsDivisionsValidator::class;
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UsDivisions::class;
    }

    public function importDivisions( $divisions){
        collect($divisions)->map(function($result,$key){ 
            if($key > 0){                
                $this->model->firstOrCreate(array(
                                            'name'=>$result[0],
                                            'current_population'=>$result[1],
                                            'census_division_id'=>$result[2]
                ));
            }
        });
    }
}
