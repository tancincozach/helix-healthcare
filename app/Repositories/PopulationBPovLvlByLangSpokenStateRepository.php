<?php

namespace App\Repositories;

use App\Models\States\PopulationBPovLangSpokenState;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;
use App\Validators\PopulationBPovLangSpokenStateValidator;

/**
 * Interface PopulationBPovLvlByLangSpokenStateRepository.
 *
 * @package namespace App\Repositories;
 */
class PopulationBPovLvlByLangSpokenStateRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;
    
    protected $cacheExcept = [];

     /**
     * Sepcify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return PopulationBPovLangSpokenStateValidator::class;
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
       return PopulationBPovLangSpokenState::class;
    }
}
