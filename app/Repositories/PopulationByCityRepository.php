<?php

namespace App\Repositories;

use App\Models\Cities\PopulationCity;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;
use App\Validators\PopulationByCityValidator;

/**
 * Interface StatesRepository.
 *
 * @package namespace App\Repositories;
 */
class PopulationByCityRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;
    
    protected $cacheExcept = [];

     /**
     * Sepcify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return PopulationByCityValidator::class;
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
       return PopulationCity::class;
    }
}
