<?php

namespace App\Repositories;

use App\Models\CensusTracts;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;
use App\Validators\CensusTractsValidator;

/**
 * Interface CensusTractsRepository.
 *
 * @package namespace App\Repositories;
 */
class CensusTractsRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;
    
    protected $cacheExcept = [];

     /**
     * Sepcify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return CensusTractsValidator::class;
    }
 
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return CensusTracts::class;
    }
}
