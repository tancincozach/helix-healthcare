<?php

namespace App\Repositories;

use App\Models\Cities\PopulationBPovEmploymentCity;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;
use App\Validators\PopulationBPovByEmpCityValidator;

/**
 * Interface PopulationBPovLvlByEmpCityRepository.
 *
 * @package namespace App\Repositories;
 */
class PopulationBPovLvlByEmpCityRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;
    
    protected $cacheExcept = [];

     /**
     * Sepcify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return PopulationBPovByEmpCityValidator::class;
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
       return PopulationBPovEmploymentCity::class;
    }
}
