<?php

namespace App\Repositories;

use App\Models\Cities\City;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;
use App\Validators\CitiesValidator;

/**
 * Interface StatesRepository.
 *
 * @package namespace App\Repositories;
 */
class CitiesRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;
    
    protected $cacheExcept = [];

     /**
     * Sepcify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return CitiesValidator::class;
    }
 
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return City::class;
    }
}
