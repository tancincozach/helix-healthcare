<?php

namespace App\Repositories;

use App\Models\Counties\PopulationEduByRaceCounty;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;
use App\Validators\PopulationByRaceCountyValidator;

/**
 * Interface PopulationEduByRaceCountyRepository.
 *
 * @package namespace App\Repositories;
 */
class PopulationEduByRaceCountyRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;
    
    protected $cacheExcept = [];

     /**
     * Sepcify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return PopulationByRaceCountyValidator::class;
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
       return PopulationEduByRaceCounty::class;
    }
}
