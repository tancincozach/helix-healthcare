<?php

namespace App\Repositories;

use App\Models\Counties\PopulationCounty;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;
use App\Validators\PopulationByCountyValidator;
/**
 * Interface StatesRepository.
 *
 * @package namespace App\Repositories;
 */
class PopulationByCountyRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;
    
    protected $cacheExcept = [];

     /**
     * Sepcify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return PopulationByCountyValidator::class;
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PopulationCounty::class;
    }
}
