<?php

namespace App\Repositories;

use GuzzleHttp\Client;
use App\Models\CountiesSubdivision;
use App\Models\States\State;
use App\Models\Counties\County;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;
use App\Validators\CountiesSubdValidator;
use App\Traits\GeolocationTrait;
use DB;

/**
 * Interface StatesRepository.
 *
 * @package namespace App\Repositories;
 */
class CountiesSubdRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;
    use GeolocationTrait;
    
    protected $cacheExcept = [];

     /**
     * Sepcify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return CountiesSubdValidator::class;
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return CountiesSubdivision::class;
    }

    public function collectData($url){
        $http = new Client;
        $response = $http->request('GET',$url);
        $result = json_decode((string) $response->getBody(), true);
        return $result;
    }

    public function importCountiesSubdivision($params=array()){
        collect(State::all())->map(function($stateRow) use ($params){
            $countyDivResponse = $this->collectData('https://api.census.gov/data/'.$params['acsYear'].'/pep/population?get=GEONAME,POP&for=county%20subdivision:*&in=state:'.$stateRow->census_state_id.'&key='.$params['apiKey']);                
                collect($countyDivResponse)->map(function($CountySubData,$key) use ($stateRow,$params){ 
                    if($key > 0){                                         
                        $countyRow = County::firstWhere(['census_county_id'=>$CountySubData[3],'state_id'=>$stateRow->id]);
                        $locationData = GeolocationTrait::getCoordinates($stateRow);
                        $countySubd = array( 'name'=> ucwords($CountySubData[0]),
                        'census_cs_id'=>$CountySubData[4],
                        'state_id'=>$stateRow->id,
                        'county_id'=>@$countyRow->id,
                        'current_population'=>$CountySubData[1]);
                        $countySubd = !empty($locationData) ? array_merge($countySubd,$locationData):$countySubd;
                        $this->model->firstOrCreate($countySubd);                                       
                    }
                });       
        });        
    }

    public function importCountiesSubd($countiesSubdResponse=array()){
        collect($countiesSubdResponse)->map(function($CountySubData,$key){ 
            if($key > 0){                                         
                $countyRow = County::firstWhere(['census_county_id'=>$CountySubData[3],'state_id'=>$stateRow->id]);
                    $this->model->firstOrCreate(array(
                        'name'=>ucwords($CountySubData[0]),
                        'current_population'=>$CountySubData[1],
                        'state_id'=>$stateRow->id,
                        'county_id'=>@$countyRow->id,
                        'census_cs_id'=>$CountySubData[4]
                    ));                                       
            }
        });  
    }
}
