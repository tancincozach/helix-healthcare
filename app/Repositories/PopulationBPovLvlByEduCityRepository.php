<?php

namespace App\Repositories;

use App\Models\Cities\PopulationBPovEduCity;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;
use App\Validators\PopulationBPovByEduCityValidator;

/**
 * Interface PopulationBPovLvlByEduCityRepository.
 *
 * @package namespace App\Repositories;
 */
class PopulationBPovLvlByEduCityRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;
    
    protected $cacheExcept = [];

     /**
     * Sepcify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return PopulationBPovByEduCityValidator::class;
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
       return PopulationBPovEduCity::class;
    }
}
