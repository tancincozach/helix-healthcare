<?php

namespace App\Repositories;

use App\Models\Counties\PopulationBPovEmploymentCounty;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;
use App\Validators\PopulationBPovByEmpCountyValidator;

/**
 * Interface PopulationBPovLvlByEmpCountyRepository.
 *
 * @package namespace App\Repositories;
 */
class PopulationBPovLvlByEmpCountyRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;
    
    protected $cacheExcept = [];

     /**
     * Sepcify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return PopulationBPovByEmpCountyValidator::class;
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
       return PopulationBPovEmploymentCounty::class;
    }
}
