<?php

namespace App\Repositories;

use App\Models\Counties\PopulationByHouseholdCounty;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;
use App\Validators\PopulationByHHoldCountyValidator;

/**
 * Interface StatesRepository.
 *
 * @package namespace App\Repositories;
 */
class PopulationByHHoldSizeCountyRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;
    
    protected $cacheExcept = [];

     /**
     * Sepcify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return PopulationByHHoldCountyValidator::class;
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
       return PopulationByHouseholdCounty::class;
    }
}
