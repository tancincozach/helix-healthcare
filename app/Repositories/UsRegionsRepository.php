<?php

namespace App\Repositories;

use App\Models\UsRegions;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;
use App\Validators\UsRegionsValidator;

/**
 * Interface StatesRepository.
 *
 * @package namespace App\Repositories;
 */
class UsRegionsRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;
    
    protected $cacheExcept = [];

     /**
     * Sepcify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return UsRegionsValidator::class;
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UsRegions::class;
    }

    public function importRegions( $regions){
        collect($regions)->map(function($result,$key){ 
            if($key > 0){
                $this->model->firstOrCreate(array(
                                            'name'=>$result[0],
                                            'current_population'=>$result[1],
                                            'census_region_id'=>$result[2]
                ));
            }
        });
    }
}
