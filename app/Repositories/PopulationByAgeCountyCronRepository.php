<?php

namespace App\Repositories;

use App\Models\Counties\PopulationByAgeCountyCron;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;
use App\Validators\PopulationByAgeStateValidator;

/**
 * Interface StatesRepository.
 *
 * @package namespace App\Repositories;
 */
class PopulationByAgeCountyCronRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;
    
    protected $cacheExcept = [];

     /**
     * Sepcify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return PopulationByAgeStateValidator::class;
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
       return PopulationByAgeCountyCron::class;
    }
}
