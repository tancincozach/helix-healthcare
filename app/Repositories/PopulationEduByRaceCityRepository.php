<?php

namespace App\Repositories;

use App\Models\Cities\PopulationEduByRaceCity;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;
use App\Validators\PopulationByRaceCityValidator;

/**
 * Interface PopulationEduByRaceCityRepository.
 *
 * @package namespace App\Repositories;
 */
class PopulationEduByRaceCityRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;
    
    protected $cacheExcept = [];

     /**
     * Sepcify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return PopulationByRaceCityValidator::class;
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
       return PopulationEduByRaceCity::class;
    }
}
