<?php

namespace App\Repositories;

use App\Models\Cities\PopulationByAgeCityCron;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;
use App\Validators\PopulationByAgeCityValidator;
/**
 * Interface StatesRepository.
 *
 * @package namespace App\Repositories;
 */
class PopulationByAgeCityCronRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;
    
    protected $cacheExcept = [];

     /**
     * Sepcify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return PopulationByAgeCityValidator::class;
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
       return PopulationByAgeCityCron::class;
    }
}
