<?php

namespace App\Repositories;

use App\Models\States\PopulationBPovEmploymentState;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;
use App\Validators\PopulationBPovByEmpStateValidator;

/**
 * Interface PopulationBPovLvlByEmpStateRepository.
 *
 * @package namespace App\Repositories;
 */
class PopulationBPovLvlByEmpStateRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;
    
    protected $cacheExcept = [];

     /**
     * Sepcify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return PopulationBPovByEmpStateValidator::class;
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
       return PopulationBPovEmploymentState::class;
    }
}
