<?php

namespace App\Repositories;

use App\Models\States\PopulationBPovFamilyState;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;
use App\Validators\PopulationBPovByFamilyStateValidator;

/**
 * Interface PopulationBPovLvlByFamilyStateRepository.
 *
 * @package namespace App\Repositories;
 */
class PopulationBPovLvlByFamilyStateRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;
    
    protected $cacheExcept = [];

     /**
     * Sepcify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return PopulationBPovByFamilyStateValidator::class;
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
       return PopulationBPovFamilyState::class;
    }
}
