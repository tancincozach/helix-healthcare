<?php

namespace App\Repositories;

use App\Models\Cities\PopulationBPovFamilyCity;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;
use App\Validators\PopulationBPovByFamilyCityValidator;

/**
 * Interface PopulationBPovLvlByFamilyCityRepository.
 *
 * @package namespace App\Repositories;
 */
class PopulationBPovLvlByFamilyCityRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;
    
    protected $cacheExcept = [];

     /**
     * Sepcify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return PopulationBPovByFamilyCityValidator::class;
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
       return PopulationBPovFamilyCity::class;
    }
}
