<?php
namespace App\Services\ThirdParty;

use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Dingo\Api\Http\Request;
use App\Models\CronJobsCensusYear;
use App\Jobs\CensusBureauACSBPovByEduDataToDb;
use App\Repositories\StatesRepository;
use App\Repositories\CountiesRepository;
use App\Repositories\CitiesRepository;
use App\Repositories\CountiesSubdRepository;
use App\Traits\CensusVariableTrait;
use Illuminate\Support\Arr;
use DB;
class CensusBureauACSBelowPovertyByEducation extends APIService{

    use CensusVariableTrait;

    private $baseURL = 'https://api.census.gov';

    public $apiKey  = '';

    public $year = '';

    public function __construct(){            
        $this->baseUri .= $this->baseURL;
        $this->apiKey   = env('API_KEY_CENSUS','7bd712da9c51c10973018b94ab682ca1429775bf');
        $censusYear     = CronJobsCensusYear::firstWhere(['status'=>'1']);
        $this->year     = $censusYear->census_year;
        $this->baseUri.='/data/'. $this->year.'/acs/acs5';
        $this->educationVariable = $this->getEducationAttainment($this->year);
        parent::__construct();
    }

    private function storeResult($data_result,$category)
    {    
        try {
            switch($category){
                case 'cities':                  
                    CensusBureauACSBPovByEduDataToDb::dispatchNow($data_result,['year'=>$this->year,'category'=>$category],true);
                                                        // ->onConnection(env('QUEUE_CONNECTION','sync'));  
                break;             
                default:                    
                CensusBureauACSBPovByEduDataToDb::dispatchNow($data_result,['year'=>$this->year,'category'=>$category],true);
                                                            //->onConnection(env('QUEUE_CONNECTION','sync'));
                break;
            }            
        } catch (Exception $e) {
            return Log::info("Failed to fetch data for Catgory:".$category.".");
        }
    }
    private function storeResultWithRowdata($data_result,$category,$rowdata){
        try {
            if(!empty($category))
            switch($category){
                case 'counties_subd':
                case 'counties_subd_cities':
                    CensusBureauACSBPovByEduDataToDb::dispatchNow($data_result,['year'=>$this->year,'category'=>$category,'dataRow'=>$rowdata],true);         
                    // CensusBureauACSBPovByEduDataToDb::dispatchNow($data_result,['year'=>$this->year,'category'=>$category,'dataRow'=>$rowdata],true)
                    //                         ->onConnection('sync');                                            
                break;
                CensusBureauACSBPovByEduDataToDb::dispatchNow($data_result,['year'=>$this->year,'category'=>$category],true);  
                    // CensusBureauACSBPovByEduDataToDb::dispatchNow($data_result,['year'=>$this->year,'category'=>$category,'dataRow'=>$rowdata],true)
                    //                         ->onConnection('sync');                
            }            
        } catch (Exception $e) {
            return Log::info("Failed to fetch data for Catgory:".$category.".");
        }
    }
    
    public function segregateDataByEducation($params,$category){         
        $returnArr = [];
        if(!empty($params['responseData'])){            
            $response = array_slice($params['responseData'],0, $params['refVar']);            
            $totalPopulation = intval($response[0]) + intval($response[1]); // total population 
                 $this->storeResult(array_merge([                    
                    'ed_category_id'=>$this->educationVariable[$params['eduKey']]['edCatID'],
                    'educ_attainment'=>$this->educationVariable[$params['eduKey']]['education'],
                    'acs_year'=>$this->year,
                    'population_male'=>$response[0],
                    'population_female'=>$response[1],
                    'population'=>$totalPopulation],
                    $params['refPlace']),$category);    
        }
        return  $returnArr;
    }
    public function requestByState(){
      $rVariables =array_unique( Arr::pluck($this->educationVariable, 'censusVariable'));// get all census Race Variables       
      if(!empty($rVariables)){
        $sRepo = new StatesRepository(app());
        foreach($rVariables as $eduKey=>$censusGroup){            
            $refVar = strpos($censusGroup,',') ? explode(',',$censusGroup):[];
            $this->request('GET', '?get='.$censusGroup.',NAME&for=state:*&key='.$this->apiKey);
            $response  = json_decode($this->getResponse());            
            if(!empty($response) && !empty($refVar)){
                $criteria['eduKey'] =$eduKey;
                foreach($response as $key=>$data){
                    if($key > 0){                             
                        $rowState = $sRepo->model()::where('name','like',$data[count($refVar)])->first();
                        if(!empty($rowState)){
                            $criteria['responseData'] = $data;
                            $criteria['refVar'] =count($refVar);                                
                            $criteria['refPlace'] =['state_id'=>$rowState->id];                                
                            $this->segregateDataByEducation($criteria,'state');
                        }    
                    }
                }  
            }
        }        
      }
    }
    public function requestByCounty(){
        $rVariables =array_unique( Arr::pluck($this->educationVariable, 'censusVariable'));// get all census Race Variables        
        if(count($rVariables)){
            $cRepo = new CountiesRepository(app());
            foreach($rVariables as $eduKey=>$censusGroup){
                $refVar = strpos($censusGroup,',') ? explode(',',$censusGroup):[];
                $this->request('GET', '?get='.$censusGroup.',NAME&for=county:*&key='.$this->apiKey);
                $response  = json_decode($this->getResponse());
                if(!empty($response) && !empty($refVar)){
                    foreach($response as $key=>$data){
                        if($key > 0){
                            $rowCounty = $cRepo->model()::where('name','like',$data[count($refVar)])->first();
                            if(!empty($rowCounty)){
                                $criteria['responseData'] = $data;                                
                                $criteria['refVar'] =count($refVar);
                                $criteria['eduKey'] =$eduKey;
                                $criteria['refPlace'] =['county_id'=>$rowCounty->id];
                                $this->segregateDataByEducation($criteria,'counties');
                            }    
                        }
                    }  
                }   
            }        
        }      
    }
    public function requestByCity(){
        $rVariables =array_unique( Arr::pluck($this->educationVariable, 'censusVariable'));// get all census Race Variables
        if(count($rVariables)){
        $ctRepo = new CitiesRepository(app());
            foreach($rVariables as $eduKey=>$censusGroup){
                $refVar = strpos($censusGroup,',') ? explode(',',$censusGroup):[];
                $this->request('GET', '?get='.$censusGroup.',NAME&for=place:*&key='.$this->apiKey);
                $response  = json_decode($this->getResponse());               
                if(!empty($response) && !empty($refVar)){
                    foreach($response as $key=>$data){
                        if($key > 0){                                        
                            $rowCity = $ctRepo->model()::where('name','like',$data[count($refVar)])->first();                            
                            if(!empty($rowCity)){
                                $criteria['responseData'] = $data;                                
                                $criteria['refVar'] =count($refVar); 
                                $criteria['eduKey'] =$eduKey;
                                $criteria['refPlace'] =['city_id'=>$rowCity->id];
                                $this->segregateDataByEducation($criteria,'cities');
                            }    
                        }
                    }  
                }        
            }          
        }      
    }
    public function requestByCountySubdivision(){
        $sRepo = new StatesRepository(app());
        $param = ['apiKey'=>$this->apiKey,'raceVar'=>$this->educationVariable,'rVar'=>Arr::pluck($this->educationVariable, 'censusVariable')];
        collect($sRepo->model()::all())->map(function($stateRow) use ($param){
            $ctRepo = new CitiesRepository(app());
            if(count($param['rVar'])>0){
                foreach($param['rVar'] as $eduKey=>$censusGroup){
                    $refVar = strpos($censusGroup,',') ? explode(',',$censusGroup):[];
                    $this->request('GET', '?get='.$censusGroup.',NAME&for=county%20subdivision:*&in=state:'.$stateRow->census_state_id.'&key='.$param['apiKey']); //request By Race  Variable 
                    $response  = json_decode($this->getResponse());             
                    if(!empty($response) && !empty($refVar)){
                        foreach($response as $key=>$data){
                            if($key > 0){                                   
                                $rowCity = $ctRepo->model()::where('name','like',$data[count($refVar)])->first();
                                if(!empty($rowCity)){
                                    $criteria['responseData'] = $data;
                                    $criteria['refVar'] =count($refVar);
                                    $criteria['eduKey'] =$eduKey;              
                                    $criteria['refPlace'] =['city_id'=>$rowCity->id];
                                    $this->segregateDataByEducation($criteria,'counties_subd');
                                }    
                            }
                        }
                    }   
                }                               
            }          
        });         
    }
    public function requestByCountySubdivisionCities(){
        $cSubRepo = new CountiesSubdRepository(app());
        $param = ['apiKey'=>$this->apiKey,'raceVar'=>$this->educationVariable,'rVar'=>Arr::pluck($this->educationVariable, 'censusVariable')];
        $cSubRepo->model()::select(DB::raw('counties_subdivision.id as cs_id,
                                            counties_subdivision.county_id,
                                            counties_subdivision.city_id,
                                            counties_subdivision.census_cs_id,
                                            cities.state_id,
                                            cities.state_id,
                                            cities.*,counties.name as county,
                                            states.name as state,states.census_state_id,counties.census_county_id'))
                            ->join('cities','cities.id','=','counties_subdivision.city_id')
                            ->join('states','states.id','=','cities.state_id')
                            ->join('counties','counties.id','=','counties_subdivision.county_id')
                                ->chunk(1000, function ($countiesSubdChunks) use($param){
                                    collect($countiesSubdChunks)->map(function($countiesSubd) use ($param){   
                                        $ctRepo = new CitiesRepository(app());
                                        $state = new StatesRepository(app());
                                        if(count($param['rVar'])>0){
                                            foreach($param['rVar'] as $eduKey=>$censusGroup){
                                                $refVar = strpos($censusGroup,',') ? explode(',',$censusGroup):[];
                                                $this->request('GET','?get='.$censusGroup.',NAME&for=county%20subdivision:'.$countiesSubd->census_cs_id.'&in=state:'.$countiesSubd->census_state_id.'%20county:'.$countiesSubd->census_county_id.'&key='.$param['apiKey']); //request By Race  Variable 
                                                $response  = json_decode($this->getResponse());
                                                if(!empty($response) && !empty($refVar)){
                                                    foreach($response as $key=>$data){
                                                        if($key > 0 ){                                        
                                                            $rowCity = $ctRepo->model()::where('name','like',$data[count($param['rVar'])])->first();
                                                            if(!empty($rowCity)){
                                                                $criteria['responseData'] = $data;                                                                
                                                                $criteria['refVar'] =count($param['rVar']);
                                                                $criteria['eduKey'] =$eduKey;                                         
                                                                $criteria['refPlace'] =['city_id'=>$rowCity->id];
                                                                $this->segregateDataByEducation($criteria,'counties_subd_cities');
                                                            }    
                                                        }
                                                    }
                                                }
                                            }                                            
                                        }
                                    }); 
                            });
    }
}
?>
