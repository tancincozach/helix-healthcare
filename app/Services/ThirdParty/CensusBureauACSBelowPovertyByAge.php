<?php
namespace App\Services\ThirdParty;

use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Dingo\Api\Http\Request;
use App\Models\CronJobsCensusYear;
use App\Jobs\CensusBureauACSBPovAgeDataToDb;
use App\Repositories\StatesRepository;
use App\Repositories\CountiesRepository;
use App\Repositories\CitiesRepository;
use App\Repositories\CountiesSubdRepository;
use App\Traits\CensusVariableTrait;
use Illuminate\Support\Arr;
use DB;
class CensusBureauACSBelowPovertyByAge extends APIService{
    use CensusVariableTrait;

    private $baseURL = 'https://api.census.gov';

    private $apiKey;

    private $year;

    private $sexVariable;

    public function __construct(){            
        $this->baseUri .= $this->baseURL;
        $this->apiKey   = env('API_KEY_CENSUS','7bd712da9c51c10973018b94ab682ca1429775bf');
        $censusYear     = CronJobsCensusYear::firstWhere(['status'=>'1']);
        $this->year     = $censusYear->census_year;
        $this->baseUri.='/data/'. $this->year.'/acs/acs5';
        $this->sexVariable = $this->getAgeAndSexPovertyStatusVariables($this->year);
        parent::__construct();
    }

    private function storeResult($data_result,$category)
    {
        try {
            switch($category){
                case 'cities':                     
                    CensusBureauACSBPovAgeDataToDb::dispatchNow($data_result,['year'=>$this->year,'category'=>$category],true);
                                                 // ->onConnection('sync');
                break;             
                default:                    
                     CensusBureauACSBPovAgeDataToDb::dispatchNow($data_result,['year'=>$this->year,'category'=>$category],true);
                                                  //->onConnection('sync');                    
                break;
            }            
        } catch (Exception $e) {
            return Log::info("Failed to fetch data for Catgory:".$category.".");
        }
    }
    private function storeResultWithRowdata($data_result,$category,$rowdata){
        try {
            if(!empty($category))
            switch($category){
                case 'counties_subd':
                case 'counties_subd_cities':
                    CensusBureauACSBPovAgeDataToDb::dispatchNow($data_result,['year'=>$this->year,'category'=>$category,'dataRow'=>$rowdata],true);
                    // CensusBureauGeodataToDb::dispatchNow($data_result,['year'=>$this->year,'category'=>$category,'dataRow'=>$rowdata],true)
                    //                         ->onConnection('sync');                                            
                break;
            }            
        } catch (Exception $e) {
            return Log::info("Failed to fetch data for Catgory:".$category.".");
        }
    }

    public function segregateDataBySexAndGender($params,$category){        
        $returnArr = [];
        if(!empty($params['responseData'])){            
            $popSexVar = array_slice($params['responseData'],0, $params['refSexVar']); 
            $tempArr = [];
            if(!empty($popSexVar)){
                foreach($popSexVar as $key=>$popByAge){
                    $this->storeResult( array_merge([
                       'age_category'=>$params['generateSexVar'][$key]['ageCategory'],
                       'age_category_id'=>$params['generateSexVar'][$key]['ageCatID'],
                       'gender'=>$params['generateSexVar'][$key]['gender'],
                       'acs_year'=>$this->year,
                       'population'=>$params['responseData'][$key]],
                       $params['refPlace']),$category);
               }    
            }            
        }
        return  $returnArr;            
    }
    public function requestByState(){    
        $totalVar = Arr::pluck($this->sexVariable['Total'],'censusVariable');// get all total variables for male,female and over all 
        $mVariables = Arr::pluck($this->sexVariable['mVar'], 'censusVariable');// get all census Male Variables 
        $fVariables =Arr::pluck($this->sexVariable['fVar'], 'censusVariable') ;// get all census Female Variables 
        $sRepo = new StatesRepository(app());
        if(count($mVariables) > 0 && count($fVariables) > 0){
            $this->request('GET', '?get='.implode(',',$mVariables).',NAME&for=state:*&key='.$this->apiKey);//request Age By Sex Variable for Male
            $response  = json_decode($this->getResponse());
            if(!empty($response)){
                foreach($response as $key=>$data){              
                    if($key > 0){                                        
                        $rowState = $sRepo->model()::firstWhere(['census_state_id'=>count($mVariables)-1]);                    
                        if(!empty($rowState)){
                            $criteria['responseData'] = $data;
                            $criteria['generateSexVar'] =array_values($this->sexVariable['mVar']);// re index array keys
                            $criteria['refSexVar'] =count($mVariables);
                            $criteria['refPlace'] =['state_id'=>$rowState->id];
                            $this->segregateDataBySexAndGender($criteria,'state');
                        }    
                    }
                }   
            }    

            $this->request('GET', '?get='.implode(',',$fVariables).',NAME&for=state:*&key='.$this->apiKey); //request Age By Sex Variable for Female
            $response  = json_decode($this->getResponse()); 
            if(!empty($response)){
                foreach($response as $key=>$data){                
                    if($key > 0 ){                
                        $rowState = $sRepo->model()::firstWhere(['census_state_id'=>count($mVariables)-1]);
                        if(!empty($rowState)){
                            $criteria['responseData'] = $data;
                            $criteria['generateSexVar'] =array_values($this->sexVariable['fVar']);
                            $criteria['refSexVar'] =count($mVariables);
                            $criteria['refPlace'] =['state_id'=>$rowState->id];
                            $this->segregateDataBySexAndGender($criteria,'state');
                        }                    
                    }
                }
            }            
        }
    }
    public function requestByCounty(){    

        $mVariables = Arr::pluck($this->sexVariable['mVar'], 'censusVariable');// get all census Male Variables 
        $fVariables = Arr::pluck($this->sexVariable['fVar'], 'censusVariable');// get all census Female Variables 

        if(count($mVariables) > 0 && count($fVariables) > 0){
            $this->request('GET', '?get='.implode(',',$mVariables).',NAME&for=county:*&key='.$this->apiKey);//request Age By Sex Variable for Male
            $cRepo = new CountiesRepository(app());
            $response  = json_decode($this->getResponse());   
            $countyNotFound =[];
            $county=1;
            if(!empty($response)){
                foreach($response as $key=>$data){
                    if($key > 0){                   
                        $rowCounty = $cRepo->model()::where('name','like',$data[count($mVariables)])->first();
                        if(!empty($rowCounty)){
                            $criteria['responseData'] = $data;
                            $criteria['generateSexVar'] =array_values($this->sexVariable['mVar']);// re index array keys
                            $criteria['refSexVar'] =count($mVariables);
                            $criteria['refPlace'] =['county_id'=>$rowCounty->id];
                            $this->segregateDataBySexAndGender($criteria,'counties');  
                        }else{
                            $countyNotFound[]=$data[23];
                        }            
                    }
                }
            }     

            $this->request('GET', '?get='.implode(',',$fVariables).',NAME&for=county:*&key='.$this->apiKey); //request Age By Sex Variable for Female
            $response  = json_decode($this->getResponse());
            if(!empty($response)){
                foreach($response as $key=>$data){                
                    if($key > 0){                 
                        $rowCounty = $cRepo->model()::where('name','like',$data[count($fVariables)])->first();
                        if(!empty($rowCounty)){
                            $criteria['responseData'] = $data;
                            $criteria['generateSexVar'] =array_values($this->sexVariable['fVar']);
                            $criteria['refSexVar'] =count($mVariables);
                            $criteria['refPlace'] =['county_id'=>$rowCounty->id];
                            $this->segregateDataBySexAndGender($criteria,'counties');
                        }                    
                    }
                }
            }            
        }
    }
    public function requestByCity(){    
        $mVariables = Arr::pluck($this->sexVariable['mVar'], 'censusVariable');// get all census Male Variables 
        $fVariables = Arr::pluck($this->sexVariable['fVar'], 'censusVariable');// get all census Female Variables 
        $ctRepo = new CitiesRepository(app());

        if(count($mVariables) > 0 && count($fVariables) > 0){
            $this->request('GET', '?get='.implode(',',$mVariables).',NAME&for=place:*&key='.$this->apiKey);//request Age By Sex Variable for Male
            $response  = json_decode($this->getResponse());
            if(!empty($response)){
                foreach($response as $key=>$data){
                    if($key > 0 ){                                        
                        $censusCityID = $data[count($data)-1];
                        $rowCity = $ctRepo->model()::where('name','like',$data[count($mVariables)])->first();                    
                        if(!empty($rowCity)){
                            $criteria['responseData'] = $data;
                            $criteria['generateSexVar'] =array_values($this->sexVariable['mVar']);// re index array keys
                            $criteria['refSexVar'] =count($mVariables);
                            $criteria['refPlace'] =['city_id'=>$rowCity->id];
                            $this->segregateDataBySexAndGender($criteria,'cities');
                        }    
                    }
                }
            }
            
            $this->request('GET', '?get='.implode(',',$fVariables).',NAME&for=place:*&key='.$this->apiKey); //request Age By Sex Variable for Female
            $response  = json_decode($this->getResponse());
            if(!empty($response)){
                foreach($response as $key=>$data){                
                    if($key > 0 ){                 
                        $rowCity = $ctRepo->model()::where('name','like',$data[count($fVariables)])->first();
                        if(!empty($rowCity)){
                            $criteria['responseData'] = $data;
                            $criteria['generateSexVar'] =array_values($this->sexVariable['fVar']);
                            $criteria['refSexVar'] =count($mVariables);
                            $criteria['refPlace'] =['city_id'=>$rowCity->id];
                            $this->segregateDataBySexAndGender($criteria,'cities');
                        }                    
                    }
                }
            }
        }
    }
    public function requestByCountySubdivision(){
        $mVariables = Arr::pluck($this->sexVariable['mVar'], 'censusVariable');// get all census Male Variables 
        $fVariables = Arr::pluck($this->sexVariable['fVar'], 'censusVariable');// get all census Female Variables         
        $state = new StatesRepository(app());
        $param = ['apiKey'=>$this->apiKey];
        collect($state->model()::all())->map(function($stateRow) use ($param,$mVariables,$fVariables){
            $ctRepo = new CitiesRepository(app());
            if(count($mVariables) > 0 && count($fVariables) > 0){
                $this->request('GET', '?get='.implode(',',$mVariables).',NAME&for=county%20subdivision:*&in=state:'.$stateRow->census_state_id.'&key='.$param['apiKey']); //request Age By Sex Variable for Male
                $response  = json_decode($this->getResponse());
                if(!empty($response)){
                    foreach($response as $key=>$data){
                        if($key > 0){
                            $rowCity = $ctRepo->model()::where('name','like',$data[count($mVariables)])->first();
                            if(!empty($rowCity)){
                                $criteria['responseData'] = $data;
                                $criteria['generateSexVar'] =array_values($this->sexVariable['mVar']);// re index array keys
                                $criteria['refSexVar'] =count($mVariables);
                                $criteria['refPlace'] =['city_id'=>$rowCity->id];
                                $this->segregateDataBySexAndGender($criteria,'counties_subd');
                            }    
                        }
                    }
                }
                $this->request('GET', '?get='.implode(',',$fVariables).',NAME&for=county%20subdivision:*&in=state:'.$stateRow->census_state_id.'&key='.$param['apiKey']);  //request Age By Sex Variable for Female
                $response  = json_decode($this->getResponse());
                if(!empty($response)){
                    foreach($response as $key=>$data){                
                        if($key > 0){                 
                            $rowCity = $ctRepo->model()::where('name','like',$data[count($fVariables)])->first();
                            if(!empty($rowCity)){
                                $criteria['responseData'] = $data;
                                $criteria['generateSexVar'] =array_values($this->sexVariable['fVar']);
                                $criteria['refSexVar'] =count($mVariables);
                                $criteria['refPlace'] =['city_id'=>$rowCity->id];
                                $this->segregateDataBySexAndGender($criteria,'counties_subd');
                            }                    
                        }
                    }
                }                
            }            
        }); 
    }
    public function requestByCountySubdivisionCities(){
        $mVariables = Arr::pluck($this->sexVariable['mVar'], 'censusVariable');// get all census Male Variables 
        $fVariables = Arr::pluck($this->sexVariable['fVar'], 'censusVariable');// get all census Female Variables     
        $cSubRepo = new CountiesSubdRepository(app());
        $param = ['apiKey'=>$this->apiKey,'sexVar'=>$this->sexVariable,'mVar'=>$mVariables,'fVar'=>$fVariables];
        $cSubRepo->model()::select(DB::raw('
                            counties_subdivision.id as cs_id,
                            counties_subdivision.county_id,
                            counties_subdivision.city_id,
                            counties_subdivision.census_cs_id,
                            cities.state_id,
                            cities.state_id,
                            cities.*,counties.name as county,
                            states.name as state,states.census_state_id,counties.census_county_id'))
                            ->join('cities','cities.id','=','counties_subdivision.city_id')
                            ->join('states','states.id','=','cities.state_id')
                            ->join('counties','counties.id','=','counties_subdivision.county_id')
                                ->chunk(1000, function ($countiesSubdChunks) use($param){
                                    collect($countiesSubdChunks)->map(function($countiesSubd) use ($param){   
                                        $ctRepo = new CitiesRepository(app());
                                        $state = new StatesRepository(app());
                                        if(count($param['mVar'])>0){
                                            $this->request('GET', '?get='.implode(',',$param['mVar']).',NAME&for=county%20subdivision:'.$countiesSubd->census_cs_id.'&in=state:'.$countiesSubd->census_state_id.'%20county:'.$countiesSubd->census_county_id.'&key='.$param['apiKey']);
                                            $response  = json_decode($this->getResponse());
                                            if(!empty($response)){
                                                foreach($response as $key=>$data){
                                                    if($key > 0 ){                                                                                          
                                                        $rowCity = $ctRepo->model()::where('name','like',$data[count($param['mVar'])])->first();
                                                        if(!empty($rowCity)){
                                                            $criteria['responseData'] = $data;
                                                            $criteria['generateSexVar'] =array_values($param['sexVar']['mVar']);// re index array keys
                                                            $criteria['refSexVar'] =count($param['mVar']);
                                                            $criteria['refPlace'] =['city_id'=>$rowCity->id];
                                                            $this->segregateDataBySexAndGender($criteria,'counties_subd_cities');
                                                        }    
                                                    }
                                                }
                                            }
                                        }
                                        if(count($param['fVar'])){
                                            $this->request('GET', '?get='.implode(',',$param['fVar']).',NAME&for=county%20subdivision:'.$countiesSubd->census_cs_id.'&in=state:'.$countiesSubd->census_state_id.'%20county:'.$countiesSubd->census_county_id.'&key='.$param['apiKey']);
                                            $response  = json_decode($this->getResponse());
                                            if(!empty($response)){
                                                foreach($response as $key=>$data){                
                                                    if($key > 0){                 
                                                        $rowCity = $ctRepo->model()::where('name','like',$data[count($param['fVar'])])->first();
                                                        if(!empty($rowCity)){
                                                            $criteria['responseData'] = $data;
                                                            $criteria['generateSexVar'] =array_values($param['sexVar']['fVar']);
                                                            $criteria['refSexVar'] =count($param['fVar']);
                                                            $criteria['refPlace'] =['city_id'=>$rowCity->id];
                                                            $this->segregateDataBySexAndGender($criteria,'counties_subd_cities');
                                                        }                    
                                                    }
                                                }
                                            } 
                                        }   
                                    });
                            });
    }
}
?>
