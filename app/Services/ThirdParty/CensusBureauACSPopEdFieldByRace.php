<?php
namespace App\Services\ThirdParty;

use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Dingo\Api\Http\Request;
use App\Models\CronJobsCensusYear;
use App\Jobs\CensusBureauACSPopEdFieldByRaceDataToDb;
use App\Repositories\StatesRepository;
use App\Repositories\CountiesRepository;
use App\Repositories\CitiesRepository;
use App\Repositories\CountiesSubdRepository;
use App\Traits\CensusVariableTrait;
use Illuminate\Support\Arr;
use DB;
class CensusBureauACSPopEdFieldByRace extends APIService{

    use CensusVariableTrait;

    private $baseURL = 'https://api.census.gov';

    public $apiKey  = '';

    public $year = '';

    public function __construct(){            
        $this->baseUri .= $this->baseURL;
        $this->apiKey   = env('API_KEY_CENSUS','7bd712da9c51c10973018b94ab682ca1429775bf');
        $censusYear     = CronJobsCensusYear::firstWhere(['status'=>'1']);
        $this->year     = $censusYear->census_year;
        $this->baseUri.='/data/'. $this->year.'/acs/acs5';
        $this->raceVariable = $this->getEducationalFieldAttainmentByRace($this->year);        
        parent::__construct();
    }

    private function storeResult($data_result,$category)
    {
        try {
            switch($category){
                case 'cities':                  
                    CensusBureauACSPopEdFieldByRaceDataToDb::dispatchNow($data_result,['year'=>$this->year,'category'=>$category],true);
                                                        // ->onConnection(env('QUEUE_CONNECTION','sync'));  
                break;             
                default:                    
                CensusBureauACSPopEdFieldByRaceDataToDb::dispatchNow($data_result,['year'=>$this->year,'category'=>$category],true);
                                                            //->onConnection(env('QUEUE_CONNECTION','sync'));
                break;
            }            
        } catch (Exception $e) {
            return Log::info("Failed to fetch data for Catgory:".$category.".");
        }
    }
    private function storeResultWithRowdata($data_result,$category,$rowdata){
        try {
            if(!empty($category))
            switch($category){
                case 'counties_subd':
                case 'counties_subd_cities':
                    CensusBureauACSPopEdFieldByRaceDataToDb::dispatchNow($data_result,['year'=>$this->year,'category'=>$category,'dataRow'=>$rowdata],true);         
                    // CensusBureauACSPopEdFieldByRaceDataToDb::dispatchNow($data_result,['year'=>$this->year,'category'=>$category,'dataRow'=>$rowdata],true)
                    //                         ->onConnection('sync');                                            
                break;
                CensusBureauACSPopEdFieldByRaceDataToDb::dispatchNow($data_result,['year'=>$this->year,'category'=>$category],true);  
                    // CensusBureauACSPopEdFieldByRaceDataToDb::dispatchNow($data_result,['year'=>$this->year,'category'=>$category,'dataRow'=>$rowdata],true)
                    //                         ->onConnection('sync');                
            }            
        } catch (Exception $e) {
            return Log::info("Failed to fetch data for Catgory:".$category.".");
        }
    }
    
    public function segregateDataByRace($params,$category){         
        if(!empty($params['responseData']) && !empty($params['refRaceVar'])){
            $respData = array_slice($params['responseData'],0, $params['refRaceVar']);                         
            if(!empty($this->raceVariable['raceResult']) && !empty($this->raceVariable['eduFieldResults'])){  
                if(!empty($this->raceVariable['raceResult'][$params['raceKey']]['id']) && !empty($this->raceVariable['raceResult'][$params['raceKey']]['name']) && is_array($params['refPlace'])){
                    $temp = array_merge([
                                            'race_category_id'=>$this->raceVariable['raceResult'][$params['raceKey']]['id'],
                                            'race'=>$this->raceVariable['raceResult'][$params['raceKey']]['name'],
                                            'acs_year'=>$this->year],
                                        $params['refPlace']);
            
                    foreach($respData as $key=>$data){
                        $this->storeResult(array_merge($temp,[                                    
                                        'edu_fld_id'=>$this->raceVariable['eduFieldResults'][$key]['id'],
                                        'field_name'=>$this->raceVariable['eduFieldResults'][$key]['name'],                                    
                                        'population'=>$params['responseData'][$key]]),$category);
                    }                          
                }                
            }
        }
    }
    public function requestByState(){
      $rVariables =array_unique( Arr::pluck($this->raceVariable['censusVar'], 'censusVariable'));// get all census Race Variables       
      if(!empty($rVariables)){
        $sRepo = new StatesRepository(app());
        foreach($rVariables as $raceKey=>$censusGroup){            
            $refRaceVar = strpos($censusGroup,',') ? explode(',',$censusGroup):[];
            $this->request('GET', '?get='.$censusGroup.',NAME&for=state:*&key='.$this->apiKey);
            $response  = json_decode($this->getResponse());            
            if(!empty($response) && !empty($refRaceVar)){
                    foreach($response as $key=>$data){
                        if($key > 0){                             
                            $rowState = $sRepo->model()::where('name','like',$data[count($refRaceVar)])->first();
                            if(!empty($rowState)){
                                $criteria['responseData'] = $data;
                                $criteria['raceData'] =$this->raceVariable['raceResult'];// re index array keys
                                $criteria['refRaceVar'] =count($refRaceVar);
                                $criteria['raceKey'] = $raceKey;
                                $criteria['refPlace'] =['state_id'=>$rowState->id];                                
                                $this->segregateDataByRace($criteria,'state');
                            }    
                        }
                    }  
            }
        }            
      }
    }
    public function requestByCounty(){
        $rVariables =array_unique( Arr::pluck($this->raceVariable['censusVar'], 'censusVariable'));// get all census Race Variables        
        if(count($rVariables)){
            $cRepo = new CountiesRepository(app());
            foreach($rVariables as $raceKey=>$censusGroup){
                $refRaceVar = strpos($censusGroup,',') ? explode(',',$censusGroup):[];
                $this->request('GET', '?get='.$censusGroup.',NAME&for=county:*&key='.$this->apiKey);
                $response  = json_decode($this->getResponse());
                if(!empty($response) && !empty($refRaceVar)){
                    foreach($response as $key=>$data){
                        if($key > 0){
                            $rowCounty = $cRepo->model()::where('name','like',$data[count($refRaceVar)])->first();
                            if(!empty($rowCounty)){
                                $criteria['responseData'] = $data;
                                $criteria['raceData'] =$this->raceVariable['raceResult'];// re index array keys
                                $criteria['refRaceVar'] =count($refRaceVar);
                                $criteria['raceKey'] = $raceKey;
                                $criteria['refPlace'] =['county_id'=>$rowCounty->id];
                                $this->segregateDataByRace($criteria,'counties');
                            }    
                        }
                    }  
                }   
            }        
        }      
    }
    public function requestByCity(){
        $rVariables =array_unique( Arr::pluck($this->raceVariable['censusVar'], 'censusVariable'));// get all census Race Variables
        if(count($rVariables)){
        $ctRepo = new CitiesRepository(app());
            foreach($rVariables as $raceKey=>$censusGroup){
                $refRaceVar = strpos($censusGroup,',') ? explode(',',$censusGroup):[];
                $this->request('GET', '?get='.$censusGroup.',NAME&for=place:*&key='.$this->apiKey);
                $response  = json_decode($this->getResponse());               
                if(!empty($response) && !empty($refRaceVar)){
                    foreach($response as $key=>$data){
                        if($key > 0){                                        
                            $rowCity = $ctRepo->model()::where('name','like',$data[count($refRaceVar)])->first();                            
                            if(!empty($rowCity)){
                                $criteria['responseData'] = $data;
                                $criteria['raceData'] =$this->raceVariable['raceResult'];// re index array keys
                                $criteria['refRaceVar'] =count($refRaceVar);
                                $criteria['raceKey'] = $raceKey;
                                $criteria['refPlace'] =['city_id'=>$rowCity->id];
                                $this->segregateDataByRace($criteria,'cities');
                            }    
                        }
                    }  
                }        
            }          
        }      
    }
    public function requestByCountySubdivision(){
        $sRepo = new StatesRepository(app());
        $param = ['apiKey'=>$this->apiKey,'raceVar'=>$this->raceVariable,'rVar'=>Arr::pluck($this->raceVariable, 'censusVariable')];
        collect($sRepo->model()::all())->map(function($stateRow) use ($param){
            $ctRepo = new CitiesRepository(app());
            if(count($param['rVar'])>0){
                foreach($param['rVar'] as $raceKey=>$censusGroup){
                    $refRaceVar = strpos($censusGroup,',') ? explode(',',$censusGroup):[];
                    $this->request('GET', '?get='.$censusGroup.',NAME&for=county%20subdivision:*&in=state:'.$stateRow->census_state_id.'&key='.$param['apiKey']); //request By Race  Variable 
                    $response  = json_decode($this->getResponse());
                    if(!empty($response) && !empty($refRaceVar)){
                        foreach($response as $key=>$data){
                            if($key > 0){                                        
                                $rowCity = $ctRepo->model()::where('name','like',$data[$refRaceVar])->first();
                                if(!empty($rowCity)){
                                    $criteria['responseData'] = $data;
                                    $criteria['raceData'] =$this->raceVariable['raceResult'];// re index array keys
                                    $criteria['refRaceVar'] =count($refRaceVar);
                                    $criteria['raceKey'] = $raceKey;
                                    $criteria['refPlace'] =['city_id'=>$rowCity->id];
                                    $this->segregateDataByRace($criteria,'counties_subd');
                                }    
                            }
                        }
                    }   
                }                               
            }          
        });         
    }
    public function requestByCountySubdivisionCities(){
        $cSubRepo = new CountiesSubdRepository(app());
        $param = ['apiKey'=>$this->apiKey,'raceVar'=>$this->raceVariable,'rVar'=>Arr::pluck($this->raceVariable, 'censusVariable')];
        $cSubRepo->model()::select(DB::raw('counties_subdivision.id as cs_id,
                                            counties_subdivision.county_id,
                                            counties_subdivision.city_id,
                                            counties_subdivision.census_cs_id,
                                            cities.state_id,
                                            cities.state_id,
                                            cities.*,counties.name as county,
                                            states.name as state,states.census_state_id,counties.census_county_id'))
                            ->join('cities','cities.id','=','counties_subdivision.city_id')
                            ->join('states','states.id','=','cities.state_id')
                            ->join('counties','counties.id','=','counties_subdivision.county_id')
                                ->chunk(1000, function ($countiesSubdChunks) use($param){
                                    collect($countiesSubdChunks)->map(function($countiesSubd) use ($param){   
                                        $ctRepo = new CitiesRepository(app());
                                        $state = new StatesRepository(app());
                                        if(count($param['rVar'])>0){
                                            foreach($param['rVar'] as $raceKey=>$censusGroup){
                                                $refRaceVar = strpos($censusGroup,',') ? explode(',',$censusGroup):[];
                                                $this->request('GET','?get='.$censusGroup.',NAME&for=county%20subdivision:'.$countiesSubd->census_cs_id.'&in=state:'.$countiesSubd->census_state_id.'%20county:'.$countiesSubd->census_county_id.'&key='.$param['apiKey']); //request By Race  Variable 
                                                $response  = json_decode($this->getResponse());
                                                if(!empty($response) && !empty($refRaceVar)){
                                                    foreach($response as $key=>$data){
                                                        if($key > 0 ){                                        
                                                            $rowCity = $ctRepo->model()::where('name','like',$data[count($param['rVar'])])->first();
                                                            if(!empty($rowCity)){
                                                                $criteria['responseData'] = $data;
                                                                $criteria['generateRaceVar'] =array_values($refRaceVar);// re index array keys
                                                                $criteria['refRaceVar'] =count($param['rVar']);
                                                                $criteria['raceKey'] = $raceKey;
                                                                $criteria['refPlace'] =['city_id'=>$rowCity->id];
                                                                $this->segregateDataByRace($criteria,'counties_subd_cities');
                                                            }    
                                                        }
                                                    }
                                                }
                                            }                                            
                                        }
                                    }); 
                            });
    }
}
?>