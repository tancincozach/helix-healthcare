<?php
namespace App\Services\ThirdParty;

use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Dingo\Api\Http\Request;
use App\Models\CronJobsCensusYear;
use App\Jobs\CensusBureauACSPopEdFieldByRaceDataToDb;
use App\Repositories\StatesRepository;
use App\Repositories\CountiesRepository;
use App\Repositories\CitiesRepository;
use App\Repositories\CountiesSubdRepository;
use App\Repositories\PopulationByAgeStateCronRepository;
use App\Repositories\PopulationByAgeCityCronRepository;
use App\Repositories\PopulationByAgeCountyCronRepository;
use App\Repositories\PopulationByRaceStateRepository;
use App\Repositories\PopulationByRaceCityRepository;
use App\Repositories\PopulationByRaceCountyRepository;
use App\Repositories\PopulationEduFieldByRaceStateRepository;
use App\Repositories\PopulationEduFieldByRaceCityRepository;
use App\Repositories\PopulationEduFieldByRaceCountyRepository;
use App\Repositories\PopulationBPovLvlByAgeStateRepository;
use App\Repositories\PopulationBPovLvlByAgeCountyRepository;
use App\Repositories\PopulationBPovLvlByAgeCityRepository;
use App\Repositories\PopulationBPovLvlByEduStateRepository;
use App\Repositories\PopulationBPovLvlByEduCityRepository;
use App\Repositories\PopulationBPovLvlByEduCountyRepository;
use App\Repositories\PopulationBPovLvlByEmpStateRepository;
use App\Repositories\PopulationBPovLvlByEmpCityRepository;
use App\Repositories\PopulationBPovLvlByEmpCountyRepository;
use App\Repositories\PopulationBPovLvlByFamilyStateRepository;
use App\Repositories\PopulationBPovLvlByFamilyCountyRepository;
use App\Repositories\PopulationBPovLvlByFamilyCityRepository;
use App\Repositories\CensusTractsRepository;
use App\Repositories\CensusTractBlocksRepository;
use App\Traits\CensusVariableTrait;
use Illuminate\Support\Arr;
use DB;
class Geocoding extends APIService{

    use CensusVariableTrait;

    private $baseURL = 'https://address.melissadata.net/';

    private $baseURLCensus = 'https://geocoding.geo.census.gov/';

    public $apiKey  = '';

    public $year = '';

    public function __construct(){            
        $this->baseUri .= $this->baseURL;
        $this->baseUri.='v3/WEB/GlobalAddress/doGlobalAddress?';        
        $this->apiKey   = env('API_KEY_MELISSA_GEOCODE','QVX0VoCGxorQclDAWQ2NnQ**nSAcwXpxhQ0PC2lXxuDAZ-**');        
        parent::__construct();
    }

    // public function trackAddress(Request $request){
    //     $query = [];
    //     $query['id']=$this->apiKey;
    //     $query['a1']=$request->address;
    //     $query['ctry']='US';
    //     $query['format']='json';
    //     $query['opt']='OutputScript:LATN,DeliveryLines:ON';        
    //     $this->request('GET',$this->baseUri.http_build_query($query));
    //     $response  = json_decode($this->getResponse());
    //     return $response;
    // }

    /**
     * A method that will get the current vintage and benchmark from census gov
     *  @return array 
     */
    public function getVintageAndBenchmarks(){
        try{
            $this->baseUri = $this->baseURLCensus;
            $this->baseUri.='geocoder/benchmarks?format=json'; 
            $this->request('GET',$this->baseUri);
            $benchMarkResponse  = json_decode($this->getResponse());  
            if(empty($benchMarkResponse)){
               throw new \Exception('Empty Benchmark Response',412);  // sets HTTP STATUS CODE 412 Preconditioned failed due to empty Benchmark Response
            }
            if(!empty($benchMarkResponse)){
                $benchmark = collect($benchMarkResponse->benchmarks)->filter(function($response,$i){
                                         return $response->benchmarkName=='Public_AR_Current';
                });        
                if(!empty($benchmark)){
                    $this->baseUri = $this->baseURLCensus;
                    $this->baseUri.='geocoder/vintages?benchmark='.$benchmark[0]->id.'&format=json'; 
                    $this->request('GET',$this->baseUri);
                    $vintageResponse  = json_decode($this->getResponse());
                    $vintage = collect($vintageResponse->benchmarks)->filter(function($response,$i){
                        return $response->benchmarkName=='Public_AR_Current';
                    });
                }  
                if(empty($vintage)){                    
                   throw new \Exception('Empty Vintage Response',412);  // sets HTTP STATUS CODE 412 Preconditioned failed due to empty Vintage Response
                } 
                return ['vintage'=>$vintage[0]->id,'benchmark'=>$benchmark[0]->id];
            }

        } catch (\Exception $e) {  
            $status_code = ($e->getCode()=='0' ? 404:$e->getCode());
            return ['error'=>$e->getMessage(),'status_code'=>$status_code];
        }       
    }
    /**
     * @param $address
     * @param $vintage
     * @param $benchmark
     * 
     * @return array
     * 
     *  A method that will track ,verify address  and will return geographic(State,County and City) and demographic data.     
     */
    public function trackAddress($address,$benchmark,$vintage){      
        try{
            $response = [];                      
            if(empty($address))   throw new \Exception('Empty parameter:address, method:trackAddress');
            if(empty($benchmark)) throw new \Exception('Empty parameter:benchmark, method:trackAddress');
            if(empty($vintage))   throw new \Exception('Empty parameter:vintage, method:trackAddress');            
            $this->baseUri = $this->baseURLCensus;
            $this->baseUri.='geocoder/geographies/onelineaddress?'; 
            $query = [];
            $query['address']=$address;
            $query['vintage']=$vintage;
            $query['benchmark']=$benchmark;        
            $query['format']='json';            
            $this->request('GET',$this->baseUri.http_build_query($query));
            $response  = json_decode($this->getResponse());
            $response = $this->manageResponse($response);          
            return $response;          
        }catch (ModelNotFoundException $exception) {
            return ['error'=>$e->getMessage(),'status_code'=>$e->getCode()];
        } catch (\Exception $e) {            
            $status_code = ($e->getCode()=='0' ? 404:$e->getCode());
            return ['error'=>$e->getMessage(),'status_code'=>$status_code];
        }        
    }
    /**
     * @param $response
     * @return array
     * 
     *  A method that will segregate results and manage response to be return as a result
     *  that has geographic and demographics data based on the address
     */    
    private function manageResponse($response){
        try{
            if(empty($response)) throw new \Exception('Empty parameter:response, method:manageResponse');
            $resultResponse = [];
            $responseResult =[];
            unset($response->result->input);
            $resultResponse['censusGeocodingRawResponse'] = $response;            
            $responseResult = @$response->result;
            $responseResult->data =[];
            // checking of state response from census bureau gov geocoding service and reference the result to our db
            if(isset($responseResult->addressMatches[0]) && !empty($responseResult->addressMatches[0])){ // Check if the address matches on geocoding response from census bureau
                $addComponents = $responseResult->addressMatches[0]->addressComponents;// gather address components
                $geoData = $responseResult->addressMatches[0]->geographies; // gather geographical data States/Counties/Census Blocks and Census Tracts
                if(isset($geoData->States) && isset($geoData->States[0]->NAME) && !empty($geoData->States[0])){    //check if State exists
                    $stateResult = $this->getState($geoData->States[0]->NAME); // referenced census bureau result to our db to get state info
                    if(!empty($stateResult)){
                        $stateDemoGraphicResult = $this->getDemographicsByGeography('state',$stateResult);
                        if(!empty($stateDemoGraphicResult)){
                           array_push($responseResult->data,$stateDemoGraphicResult);                           
                        }
                    }                    
                }
                // checking of city response from census bureau gov geocoding service and reference the result to our db
                if(!empty($stateResult) && isset( $addComponents->city) && !empty( $addComponents->city)){ //check if State and City exists
                    $cityResult = $this->getCity( $addComponents->city,$stateResult); // referenced census bureau result to our db to get city info
                    if(!empty($cityResult)){    //check if City exists on our db                        
                        $cityDemoGraphicResult = $this->getDemographicsByGeography('city',$cityResult);
                        if(!empty($cityDemoGraphicResult)){
                            array_push($responseResult->data,$cityDemoGraphicResult);
                        }
                    }                    
                }
                // checking of county response from census bureau gov geocoding service and reference the result to our db
                if(isset($geoData->Counties) && isset($geoData->Counties[0]->NAME) && !empty($geoData->Counties[0])){  //check if County exists
                    $countyResult  = $this->getCounty(@$geoData->Counties[0]->NAME);
                    if(!empty($countyResult)){
                        $countyDemoGraphicResult = $this->getDemographicsByGeography('county',$countyResult);                        
                        $censusTract = $this->getCensusTractAndBlockPerCounty($countyResult['id']); // get census tract
                        if(!empty($countyDemoGraphicResult)){
                            array_push($responseResult->data,$countyDemoGraphicResult);
                        }
                        if(!empty($censusTract)){
                            array_push($responseResult->data,$censusTract);
                        }         
                    }                    
                }                             
            }
            return $resultResponse;
        } catch (\Exception $e) { 
            $status_code = ($e->getCode()=='0' ? 404:$e->getCode());
            return ['error'=>$e->getMessage(),'status_code'=>$status_code];
        }       
    }    
    /**
     *
     *  @param $geography
     *  @param $geoResult
     *  @return array
     * 
     *   A method that will gather demograhics data based on geography(State,City,County)
     */    
    private function getDemographicsByGeography($geography,$geoResult){
        try{
            if(empty($geography)) throw new \Exception('Empty parameter:geography, method:getDemographicsByGeography');
            if(empty($geoResult)) throw new \Exception('Empty parameter:geoResult, method:getDemographicsByGeography');            
            $resultResponse = [];
            $resultResponse[$geography] = $geoResult;
            $popByAge  = $this->getPopByAge($geography,$geoResult); // get population by age 
            $popByRace = $this->getPopByRace($geography,$geoResult); // get  below poverty level population by race
            $popByEdFieldByRace = $this->getPopEdFieldAttainmentByRace($geography,$geoResult); // get  below poverty level population Education Attainment Field By Race
            $belowPovLvlByAge = $this->getBelowPovAge($geography,$geoResult,'below'); // get  below poverty level population by age
            $belowPovLvlByEdu = $this->getBelowPovEducation($geography,$geoResult,'below'); // get below poverty level population by education
            $belowPovLvlByEmp = $this->getBelowPovEmployment($geography,$geoResult,'below'); // get below poverty level population by employment
            $belowPovLvlByFamily = $this->getBelowPovFamily($geography,$geoResult,'below');  // get below poverty level population by family
            if(!empty($popByAge) && count($popByAge) > 0 ){ // check if population by age result is not empty
                $resultResponse[$geography]['Population_By_Age'] =$popByAge;
            }                    
            if(!empty($popByRace) && count($popByRace) > 0 ){// check if population by race result is not empty
                $resultResponse[$geography]['Population_By_Race'] = $popByRace;
            }
            if(!empty($popByEdFieldByRace) && count($popByEdFieldByRace) > 0 ){ // check if Education Attainment Field By Race result is not empty
                $resultResponse[$geography]['Population_By_Education_Field_Race'] = $popByEdFieldByRace;
            }
            if(!empty($belowPovLvlByAge) && count($belowPovLvlByAge) > 0 ){ //check if below poverty level by age result is not empty
                $resultResponse[$geography]['Poverty_Lvl']['below']['age'] = $belowPovLvlByAge;
            }
            if(!empty($belowPovLvlByEdu) && count($belowPovLvlByEdu) > 0 ){ //check if below poverty level by education  result is not empty
                $resultResponse[$geography]['Poverty_Lvl']['below']['education'] = $belowPovLvlByEdu;
            }
            if(!empty($belowPovLvlByEmp) && count($belowPovLvlByEmp) > 0 ){ //check if below poverty level by employment  result is not empty
                $resultResponse[$geography]['Poverty_Lvl']['below']['employment'] = $belowPovLvlByEmp;
            }
            if(!empty($belowPovLvlByFamily) && count($belowPovLvlByFamily) > 0 ){ //check if below poverty level by family  result is not empty
                $resultResponse[$geography]['Poverty_Lvl']['below']['family'] = $belowPovLvlByFamily ;
            }
            return $resultResponse;
        } catch (\Exception $e) { 
        }
    }    
    /**
     * @param $stateName
     * @return array 
     * 
     *  A method that will get State information
     */
    private function getState($stateName){      
        try{            
            if(empty($stateName)) throw new \Exception('Empty parameter:stateCode, method:getState'); 
            $repo = new StatesRepository(app());
            $resultState =  $repo->model()::firstWhere('name','like',$stateName);            
             return (!empty($resultState)) ? $resultState->toArray():[];
        } catch (\Exception $e) { 
        }
    }
   /**
     * @param $cityName
     * @return array 
     * 
     *  A method that will get City information
     */
    private function getCity($cityName,$stateRow){
        try{
            if(empty($stateRow)) throw new \Exception('Empty parameter:stateRow, method:getCity');
            if(empty($cityName)) throw new \Exception('Empty parameter:cityName, method:getCity');
            $repo = new CitiesRepository(app());
            $city =  ucwords(strtolower( $cityName));            
            $resultCity =  $repo->model()::
            where('name','like','%'.$city.'%')
            ->orWhere('name', 'like','%'.str_replace('City','City City',$city).'%')
            ->where('state_id',$stateRow['id'])            
            ->first();
            return (!empty($resultCity)) ? $resultCity->toArray():[];   
        } catch (\Exception $e) { 
            $status_code = ($e->getCode()=='0' ? 404:$e->getCode());
            return ['error'=>$e->getMessage(),'status_code'=>$status_code];
        }        
    }
   /**
     * @param $county
     * @return array 
     * 
     *  A method that will get County information
     */
    private function getCounty($county){
        try{
            if(empty($county)) throw new \Exception('Empty parameter:state, method:getCounty');
            $repo = new CountiesRepository(app());
            $result =  $repo->model():: where('name','like','%'.$county.'%')->first();
            return (!empty($result)) ? $result->toArray():[];
         } catch (\Exception $e) { 

         }
    }
    /**
     * @param  $county_id
     * @return array
     * A method that will fetch census tract records by county
     */
    private function getCensusTractAndBlockPerCounty($county_id){
        try{            
            if(empty($county_id)) throw new \Exception('Empty parameter:county_id, method:getCensusTractPerCounty');
            $censusTractArr = [];
            $repo = new CensusTractsRepository(app());
            $censusTract = $repo->model()::where('county_id',$county_id)->get();    
            $censusTract = (!empty($result)) ? $result->toArray():[];
            if(!empty($censusTract)){
                $censusTractArr['census_tract'] = $censusTract;                
                $censusBlock = $this->getCensusBlockPerTract($censusTract);                
                if(!empty($censusBlock)){                
                   $censusTractArr['census_tract']['census_block'] = $censusBlock;                    
                }                                
            }            
            return $censusTractArr;
        }catch (ModelNotFoundException $exception) {

        }catch (\Exception $e) { 

        }
    }
    /**
     * @param  $tract_id
     * @return array
     * A method that will fetch census block records by census tract
     */
    private function getCensusBlockPerTract($tractRow){
        try{
            if(empty($tractRow)) throw new \Exception('Empty parameter:tractRow, method:getCensusBlockPerTract');
            $repo = new CensusTractBlocksRepository(app());
            return $repo->model()::where(['tract_id'=>$tractRow['id'],'acs_year'=>$tractRow['acs_year']])->get();
        }catch (ModelNotFoundException $exception) {
            
        } catch (\Exception $e) { 

        }
    }
    /**
     * @param $geography
     * @param $row
     * @return array
     * 
     *  A method that will get Population by Age for (State,County and City) information
     */
    private function getPopByAge( $geography ,$row){
        try{
            $result =[];
            if(empty($geography)) throw new \Exception('Empty parameter:geography, method:getPopByAge');
            if(empty($row)) throw new \Exception('Empty parameter:row, method:getPopByAge');  
            switch($geography){
                case 'state':
                    $repo = new PopulationByAgeStateCronRepository(app());
                    $result =  $repo->model()::
                                     where('state_id',$row['id'])                                    
                                    ->get();
                break;
                case 'city':
                    $repo = new PopulationByAgeCityCronRepository(app());
                    $result =  $repo->model():: where('city_id',$row['id'])                                    
                                    ->get();
                break;
                case 'county':                    
                    $repo = new PopulationByAgeCountyCronRepository(app());    
                    $result =  $repo->model():: where('county_id',$row['id'])                                   
                                    ->get();
                break;
            }
            return (!empty($result)) ? $result:$result;
       } catch (\Exception $e) {

       }                
    }
    /**
     * @param $geography
     * @param $row
     * @return array
     * 
     *  A method that will get Population by Race for (State,County and City) information
     */
    private function getPopByRace( $geography ,$row){
        try{
            $result =[];
            if(empty($geography)) throw new \Exception('Empty parameter:geography, method:getPopByRace');
            if(empty($row)) throw new \Exception('Empty parameter:row, method:getPopByRace');  
                switch($geography){
                    case 'state':
                        $repo = new PopulationByRaceStateRepository(app());
                        $result =  $repo->model():: where('state_id',$row['id'])
                                                    ->get();
                    case 'city':
                        $repo = new PopulationByRaceCityRepository(app());
                        $result =  $repo->model():: where('city_id',$row['id'])
                                                    ->get();  
                    break;
                    case 'county':
                        $repo = new PopulationByRaceCountyRepository(app());    
                        $result =  $repo->model():: where('county_id',$row['id'])
                                                    ->get();  
                    break;
                }
            return (!empty($result)) ? $result->toArray():$result;
        } catch (\Exception $e) {

       }          
    }    
    /**
     * @param $geography
     * @param $row
     * @param $category
     * @return array
     * 
     * A method that will get Below Poverty Level Population by Age for (State,County and City) information
     */
    private function getBelowPovAge($geography,$row,$category){
        try{            
            $result =[];
            $tempArr = [];    
            if(empty($geography)) throw new \Exception('Empty parameter:geography, method:getBelowPovAge');
            if(empty($row)) throw new \Exception('Empty parameter:row, method:getBelowPovAge');
            switch($geography){
                case 'state':                    
                    $repo = new PopulationBPovLvlByAgeStateRepository(app());
                    $result =  $repo->model():: where('state_id',$row['id'])
                                                ->get();
                break;
                case 'city':
                    $repo = new PopulationBPovLvlByAgeCityRepository(app());
                    $result =  $repo->model():: where('city_id',$row['id'])
                                                ->get();  
                break;
                case 'county':
                    $repo = new PopulationBPovLvlByAgeCountyRepository(app());    
                    $result =  $repo->model():: where('county_id',$row['id'])
                                                ->get();  
                break;
            }
            return (!empty($result)) ? $result->toArray():$result;
        } catch (\Exception $e) {

       }           
    }    
    /**
     * @param $geography
     * @param $row
     * @param $category
     * @return array
     * 
     * A method that will get Below Poverty Level Population by Education for (State,County and City) information
     */
    private function getBelowPovEducation($geography,$row,$category){
        try{  
            $result =[];
            $tempArr = [];   
            if(empty($geography)) throw new \Exception('Empty parameter:geography, method:getBelowPovEducation');
            if(empty($row)) throw new \Exception('Empty parameter:row, method:getBelowPovEducation');
                switch($geography){
                    case 'state':
                        $repo = new PopulationBPovLvlByEduStateRepository(app());
                        $result =  $repo->model():: where('state_id',$row['id'])
                                                    ->get();
                    break;
                    case 'city':
                        $repo = new PopulationBPovLvlByEduCityRepository(app());
                        $result =  $repo->model():: where('city_id',$row['id'])
                                                    ->get();  
                    break;
                    case 'county':
                        $repo = new PopulationBPovLvlByEduCountyRepository(app());    
                        $result =  $repo->model():: where('county_id',$row['id'])
                                                    ->get();  
                    break;
                }
                return (!empty($result)) ? $result->toArray():$result;
        } catch (\Exception $e) {

        }          
    }    
    /**
     * @param $geography
     * @param $row
     * @param $category
     * @return array
     * 
     * A method that will get Below Poverty Level Population by Empoloyment for (State,County and City) information
     */    
    private function getBelowPovEmployment($geography,$row,$category){
        try{  
            $result =[];
            if(empty($geography)) throw new \Exception('Empty parameter:geography, method:getBelowPovEmployment');
            if(empty($row)) throw new \Exception('Empty parameter:row, method:getBelowPovEmployment');
            switch($geography){
                case 'state':
                    $repo = new PopulationBPovLvlByEmpStateRepository(app());
                    $result =  $repo->model():: where('state_id',$row['id'])
                                                ->get();
                break;                                                
                case 'city':
                    $repo = new PopulationBPovLvlByEmpCityRepository(app());
                    $result =  $repo->model():: where('city_id',$row['id'])
                                                ->get();  
                break;
                case 'county':
                    $repo = new PopulationBPovLvlByEmpCountyRepository(app());    
                    $result =  $repo->model():: where('county_id',$row['id'])
                                                ->get();  
                break;
            }
            return (!empty($result)) ? $result->toArray():$result;
        } catch (\Exception $e) {

        }
    }    
    /**
     * @param $geography
     * @param $row
     * @param $category
     * @return array
     * 
     * A method that will get Below Poverty Level Population by Family for (State,County and City) information
     */
    private function getBelowPovFamily($geography,$row,$category){
        try{  
            $result =[];
            $tempArr = [];   
            if(empty($geography)) throw new \Exception('Empty parameter:geography, method:getBelowPovFamily');
            if(empty($row)) throw new \Exception('Empty parameter:row, method:getBelowPovFamily');
            switch($geography){
                case 'state':
                    $repo = new PopulationBPovLvlByFamilyStateRepository(app());
                    $result =  $repo->model():: where('state_id',$row['id'])
                                                ->get();
                break;                                                
                case 'city':
                    $repo = new PopulationBPovLvlByFamilyCityRepository(app());
                    $result =  $repo->model():: where('city_id',$row['id'])
                                                ->get();  
                break;
                case 'county':
                    $repo = new PopulationBPovLvlByFamilyCountyRepository(app());    
                    $result =  $repo->model():: where('county_id',$row['id'])
                                                ->get();  
                break;
            }
            return (!empty($result)) ? $result->toArray():$result;
        } catch (\Exception $e) {

        }       
    }    
    /**
     * @param $geography
     * @param $row
     * @param $category
     * @return array
     * 
     * A method that will get Population Education Attainment Field By Race for (State,County and City) information
     */
    private function getPopEdFieldAttainmentByRace($geography,$row){
        try{  
            $result =[];
            $tempArr = [];    
            if(empty($geography)) throw new \Exception('Empty parameter:geography, method:getPopEdFieldAttainmentByRace');
            if(empty($row)) throw new \Exception('Empty parameter:row, method:getPopEdFieldAttainmentByRace');    
                switch($geography){
                    case 'state':
                        $repo = new PopulationEduFieldByRaceStateRepository(app());
                        $result =  $repo->model():: where('state_id',$row['id'])
                                                    ->get();
                    break;                                                    
                    case 'city':
                        $repo = new PopulationEduFieldByRaceCityRepository(app());
                        $result =  $repo->model():: where('city_id',$row['id'])
                                                    ->get();  
                    break;
                    case 'county':
                        $repo = new PopulationEduFieldByRaceCountyRepository(app());    
                        $result =  $repo->model():: where('county_id',$row['id'])
                                                    ->get();  
                    break;
                }
                return (!empty($result)) ? $result->toArray():$result;
        } catch (\Exception $e) {

        }         
    }
}
?>