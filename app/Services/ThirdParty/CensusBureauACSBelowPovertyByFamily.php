<?php
namespace App\Services\ThirdParty;

use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Dingo\Api\Http\Request;
use App\Models\CronJobsCensusYear;
use App\Jobs\CensusBureauACSBPovFamilyDataToDb;
use App\Repositories\StatesRepository;
use App\Repositories\CountiesRepository;
use App\Repositories\CitiesRepository;
use App\Repositories\CountiesSubdRepository;
use App\Traits\CensusVariableTrait;
use Illuminate\Support\Arr;
use DB;
class CensusBureauACSBelowPovertyByFamily extends APIService{

    use CensusVariableTrait;

    private $baseURL = 'https://api.census.gov';

    public $apiKey  = '';

    public $year = '';

    public function __construct(){            
        $this->baseUri .= $this->baseURL;
        $this->apiKey   = env('API_KEY_CENSUS','7bd712da9c51c10973018b94ab682ca1429775bf');
        $censusYear     = CronJobsCensusYear::firstWhere(['status'=>'1']);
        $this->year     = $censusYear->census_year;
        $this->baseUri.='/data/'. $this->year.'/acs/acs5';
        $this->familyVariable = $this->getBelowPovertyLvlVariableByFamily($this->year);        
        parent::__construct();
    }

    private function storeResult($data_result,$category)
    {        
        try {
            switch($category){
                case 'cities':                     
                    CensusBureauACSBPovFamilyDataToDb::dispatchNow($data_result,['year'=>$this->year,'category'=>$category],true);
                                                        // ->onConnection(env('QUEUE_CONNECTION','sync'));  
                break;             
                default:                    
                      CensusBureauACSBPovFamilyDataToDb::dispatchNow($data_result,['year'=>$this->year,'category'=>$category],true);
                                                            //->onConnection(env('QUEUE_CONNECTION','sync'));
                break;
            }            
        } catch (Exception $e) {
            return Log::info("Failed to fetch data for Catgory:".$category.".");
        }
    }
    private function storeResultWithRowdata($data_result,$category,$rowdata){
        try {
            if(!empty($category))
            switch($category){
                case 'counties_subd':
                case 'counties_subd_cities':
                    CensusBureauACSBPovFamilyDataToDb::dispatchNow($data_result,['year'=>$this->year,'category'=>$category,'dataRow'=>$rowdata],true);         
                    // CensusBureauACSBPovRaceDataToDb::dispatchNow($data_result,['year'=>$this->year,'category'=>$category,'dataRow'=>$rowdata],true)
                    //                         ->onConnection('sync');                                            
                break;
                    CensusBureauACSBPovFamilyDataToDb::dispatchNow($data_result,['year'=>$this->year,'category'=>$category],true);  
                    // CensusBureauACSBPovRaceDataToDb::dispatchNow($data_result,['year'=>$this->year,'category'=>$category,'dataRow'=>$rowdata],true)
                    //                         ->onConnection('sync');                
            }            
        } catch (Exception $e) {
            return Log::info("Failed to fetch data for Catgory:".$category.".");
        }
    }
    public function segregateDataByFamily($params,$category){ 
        if(!empty($params['responseData'])){         
            $popVar = array_slice($params['responseData'],0, $params['refVar']); 
            $tempArr = [];
            foreach($popVar as $key=>$popByAge){
                 $this->storeResult(array_merge([                    
                    'family_category_id'=>$params['generateVar'][$key]['famCatID'],
                    'family_category'=>$params['generateVar'][$key]['family_category'],
                    'acs_year'=>$this->year,
                    'population'=>$params['responseData'][$key]],
                    $params['refPlace']),$category);
            }
    
        }
    }
    public function requestByState(){      
        $famVariables = Arr::pluck($this->familyVariable, 'censusVariable');// get all census Race Variables
        if(count($famVariables)){
            $sRepo = new StatesRepository(app());
            $this->request('GET', '?get='.implode(',',$famVariables).',NAME&for=state:*&key='.$this->apiKey);            
            $response  = json_decode($this->getResponse());            
            if(!empty($response)){
                foreach($response as $key=>$data){
                    if($key > 0){                                        
                        $rowState = $sRepo->model()::firstWhere(['census_state_id'=>$data[count($famVariables)+1]]);
                        if(!empty($rowState)){                            
                            $criteria['responseData'] = $data;
                            $criteria['generateVar'] =array_values($this->familyVariable);// re index array keys
                            $criteria['refVar'] =count($famVariables);
                            $criteria['refPlace'] =['state_id'=>$rowState->id];
                            $this->segregateDataByFamily($criteria,'state');
                        }    
                    }
                }  
            }        
        }      
    }
    public function requestByCounty(){
        $famVariables = Arr::pluck($this->familyVariable, 'censusVariable');// get all census Race Variables 
        if(count($famVariables)){
        $cRepo = new CountiesRepository(app());
        $this->request('GET', '?get='.implode(',',$famVariables).',NAME&for=county:*&key='.$this->apiKey);
        $response  = json_decode($this->getResponse());          
        if(!empty($response)){
            foreach($response as $key=>$data){
                if($key > 0){
                    $rowCounty = $cRepo->model()::where('name','like',$data[count($famVariables)])->first();
                    if(!empty($rowCounty)){
                        $criteria['responseData'] = $data;
                        $criteria['generateVar'] =array_values($this->familyVariable);// re index array keys
                        $criteria['refVar'] =count($famVariables);
                        $criteria['refPlace'] =['county_id'=>$rowCounty->id];
                        $this->segregateDataByFamily($criteria,'counties');
                    }    
                }
            }  
        }        
        }      
    }
    public function requestByCity(){
        $famVariables = Arr::pluck($this->familyVariable, 'censusVariable');// get all census Race Variables         
        if(count($famVariables)){
        $ctRepo = new CitiesRepository(app());
        $this->request('GET', '?get='.implode(',',$famVariables).',NAME&for=place:*&key='.$this->apiKey);
        $response  = json_decode($this->getResponse());          
        if(!empty($response)){
            foreach($response as $key=>$data){
                if($key > 0){                                        
                    $rowCity = $ctRepo->model()::where('name','like',$data[count($famVariables)])->first();
                    if(!empty($rowCity)){
                        $criteria['responseData'] = $data;
                        $criteria['generateVar'] =array_values($this->familyVariable);// re index array keys
                        $criteria['refVar'] =count($famVariables);
                        $criteria['refPlace'] =['city_id'=>$rowCity->id];
                        $this->segregateDataByFamily($criteria,'cities');
                    }    
                }
            }  
        }        
        }      
    }
    public function requestByCountySubdivision(){
        $sRepo = new StatesRepository(app());
        $param = ['apiKey'=>$this->apiKey,'raceVar'=>$this->familyVariable,'famVar'=>Arr::pluck($this->familyVariable, 'censusVariable')];
        $gendefamVariables = Arr::pluck($this->familyVariable, 'censusVariableGenderTotal');// get all census Female Variables  
        collect($sRepo->model()::all())->map(function($stateRow) use ($param){
            $ctRepo = new CitiesRepository(app());
            if(count($param['famVar'])>0){
                $this->request('GET', '?get='.implode(',',$param['famVar']).',NAME&for=county%20subdivision:*&in=state:'.$stateRow->census_state_id.'&key='.$param['apiKey']); //request By Race  Variable 
                $response  = json_decode($this->getResponse());
                if(!empty($response)){
                    foreach($response as $key=>$data){
                        if($key > 0){                                        
                            $rowCity = $ctRepo->model()::where('name','like',$data[count($param['famVar'])])->first();
                            if(!empty($rowCity)){
                                $criteria['responseData'] = $data;
                                $criteria['generateVar'] =array_values($this->familyVariable);// re index array keys
                                $criteria['refVar'] =count($param['famVar']);
                                $criteria['refPlace'] =['city_id'=>$rowCity->id];
                                $this->segregateDataByFamily($criteria,'counties_subd');
                            }    
                        }
                    }
                }                
            }          
        });         
    }
    public function requestByCountySubdivisionCities(){
        $cSubRepo = new CountiesSubdRepository(app());
        $param = ['apiKey'=>$this->apiKey,'raceVar'=>$this->familyVariable,'famVar'=>Arr::pluck($this->familyVariable, 'censusVariable')];
        $cSubRepo->model()::select(DB::raw('
                            counties_subdivision.id as cs_id,
                            counties_subdivision.county_id,
                            counties_subdivision.city_id,
                            counties_subdivision.census_cs_id,
                            cities.state_id,
                            cities.state_id,
                            cities.*,counties.name as county,
                            states.name as state,states.census_state_id,counties.census_county_id'))
                            ->join('cities','cities.id','=','counties_subdivision.city_id')
                            ->join('states','states.id','=','cities.state_id')
                            ->join('counties','counties.id','=','counties_subdivision.county_id')
                                ->chunk(1000, function ($countiesSubdChunks) use($param){
                                    collect($countiesSubdChunks)->map(function($countiesSubd) use ($param){   
                                        $ctRepo = new CitiesRepository(app());
                                        $state = new StatesRepository(app());
                                        if(count($param['famVar'])>0){
                                            $this->request('GET', '?get='.implode(',',$param['famVar']).',NAME&for=county%20subdivision:'.$countiesSubd->census_cs_id.'&in=state:'.$countiesSubd->census_state_id.'%20county:'.$countiesSubd->census_county_id.'&key='.$param['apiKey']); //request By Race  Variable 
                                            $response  = json_decode($this->getResponse());
                                            if(!empty($response)){
                                                foreach($response as $key=>$data){
                                                    if($key > 0 ){                                        
                                                        $rowCity = $ctRepo->model()::where('name','like',$data[count($param['famVar'])])->first();
                                                        if(!empty($rowCity)){
                                                            $criteria['responseData'] = $data;
                                                            $criteria['generateVar'] =array_values($param['raceVar']);// re index array keys
                                                            $criteria['refVar'] =count($param['famVar']);
                                                            $criteria['refPlace'] =['city_id'=>$rowCity->id];
                                                            $this->segregateDataByFamily($criteria,'counties_subd_cities');
                                                        }    
                                                    }
                                                }
                                            }
                                        }
                                    });
                            });
    }
}
?>