<?php
namespace App\Services\ThirdParty;

use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Dingo\Api\Http\Request;
use App\Models\CronJobsCensusYear;
use App\Jobs\CensusBureauACSPopRaceDataToDb;
use App\Repositories\StatesRepository;
use App\Repositories\CountiesRepository;
use App\Repositories\CitiesRepository;
use App\Repositories\CountiesSubdRepository;
use App\Traits\CensusVariableTrait;
use Illuminate\Support\Arr;
use DB;
class CensusBureauACSPopByRace extends APIService{

    use CensusVariableTrait;

    private $baseURL = 'https://api.census.gov';

    public $apiKey  = '';

    public $year = '';

    public function __construct(){            
        $this->baseUri .= $this->baseURL;
        $this->apiKey   = env('API_KEY_CENSUS','7bd712da9c51c10973018b94ab682ca1429775bf');
        $censusYear     = CronJobsCensusYear::firstWhere(['status'=>'1']);
        $this->year     = $censusYear->census_year;
        $this->baseUri.='/data/'. $this->year.'/acs/acs5';
        $this->raceVariable = $this->getRaceVariableByRace($this->year);        
        parent::__construct();
    }

    private function storeResult($data_result,$category)
    {
        try {
            switch($category){
                case 'cities':                     
                    CensusBureauACSPopRaceDataToDb::dispatchNow($data_result,['year'=>$this->year,'category'=>$category],true);
                                                        // ->onConnection(env('QUEUE_CONNECTION','sync'));  
                break;             
                default:                    
                CensusBureauACSPopRaceDataToDb::dispatchNow($data_result,['year'=>$this->year,'category'=>$category],true);
                                                            //->onConnection(env('QUEUE_CONNECTION','sync'));
                break;
            }            
        } catch (Exception $e) {
            return Log::info("Failed to fetch data for Catgory:".$category.".");
        }
    }
    private function storeResultWithRowdata($data_result,$category,$rowdata){
        try {
            if(!empty($category))
            switch($category){
                case 'counties_subd':
                case 'counties_subd_cities':
                    CensusBureauACSPopRaceDataToDb::dispatchNow($data_result,['year'=>$this->year,'category'=>$category,'dataRow'=>$rowdata],true);         
                    // CensusBureauACSPopRaceDataToDb::dispatchNow($data_result,['year'=>$this->year,'category'=>$category,'dataRow'=>$rowdata],true)
                    //                         ->onConnection('sync');                                            
                break;
                CensusBureauACSPopRaceDataToDb::dispatchNow($data_result,['year'=>$this->year,'category'=>$category],true);  
                    // CensusBureauACSPopRaceDataToDb::dispatchNow($data_result,['year'=>$this->year,'category'=>$category,'dataRow'=>$rowdata],true)
                    //                         ->onConnection('sync');                
            }            
        } catch (Exception $e) {
            return Log::info("Failed to fetch data for Catgory:".$category.".");
        }
    }
    
    public function segregateDataByRace($params,$category){ 
        $returnArr = [];
        if(!empty($params['responseData'])){            
            $popSexVar = array_slice($params['responseData'],0, $params['refRaceVar']); 
            $tempArr = [];
            foreach($popSexVar as $key=>$popByAge){
                 $this->storeResult( array_merge([                    
                    'race_category_id'=>$params['generateRaceVar'][$key]['raceCatID'],
                    'race'=>$params['generateRaceVar'][$key]['race'],
                    'acs_year'=>$this->year,
                    'population'=>$params['responseData'][$key]],
                    $params['refPlace']),$category);
            }
    
        }
        return  $returnArr;            
    }
    public function requestByState(){
      $rVariables = Arr::pluck($this->raceVariable, 'censusVariable');// get all census Race Variables      
      $genderVariables = Arr::pluck($this->raceVariable, 'censusVariableGenderTotal');// get all census Female Variables  
      if(count($rVariables)){
        $sRepo = new StatesRepository(app());
        $this->request('GET', '?get='.implode(',',$rVariables).',NAME&for=state:*&key='.$this->apiKey);
        $response  = json_decode($this->getResponse());
        if(!empty($response)){
            foreach($response as $key=>$data){
                if($key > 0){                                        
                    $rowState = $sRepo->model()::firstWhere(['census_state_id'=>$data[count($rVariables)+1]]);
                    if(!empty($rowState)){
                        $criteria['responseData'] = $data;
                        $criteria['generateRaceVar'] =array_values($this->raceVariable);// re index array keys
                        $criteria['refRaceVar'] =count($rVariables);
                        $criteria['refPlace'] =['state_id'=>$rowState->id];
                        $this->segregateDataByRace($criteria,'state');
                    }    
                }
            }  
        }        
      }      
    }
    public function requestByCounty(){
        $rVariables = Arr::pluck($this->raceVariable, 'censusVariable');// get all census Race Variables 
        if(count($rVariables)){
        $cRepo = new CountiesRepository(app());
          $this->request('GET', '?get='.implode(',',$rVariables).',NAME&for=county:*&key='.$this->apiKey);
          $response  = json_decode($this->getResponse());          
          if(!empty($response)){
              foreach($response as $key=>$data){
                  if($key > 0){
                      $rowCounty = $cRepo->model()::where('name','like',$data[count($rVariables)])->first();
                      if(!empty($rowCounty)){
                          $criteria['responseData'] = $data;
                          $criteria['generateRaceVar'] =array_values($this->raceVariable);// re index array keys
                          $criteria['refRaceVar'] =count($rVariables);
                          $criteria['refPlace'] =['county_id'=>$rowCounty->id];
                          $this->segregateDataByRace($criteria,'counties');
                      }    
                  }
              }  
          }        
        }      
    }
    public function requestByCity(){
        $rVariables = Arr::pluck($this->raceVariable, 'censusVariable');// get all census Race Variables 
        $genderVariables = Arr::pluck($this->raceVariable, 'censusVariableGenderTotal');// get all census Female Variables  
        if(count($rVariables)){
        $ctRepo = new CitiesRepository(app());
          $this->request('GET', '?get='.implode(',',$rVariables).',NAME&for=place:*&key='.$this->apiKey);
          $response  = json_decode($this->getResponse());          
          if(!empty($response)){
              foreach($response as $key=>$data){
                  if($key > 0){                                        
                      $rowCity = $ctRepo->model()::where('name','like',$data[count($rVariables)])->first();
                      if(!empty($rowCity)){
                          $criteria['responseData'] = $data;
                          $criteria['generateRaceVar'] =array_values($this->raceVariable);// re index array keys
                          $criteria['refRaceVar'] =count($rVariables);
                          $criteria['refPlace'] =['city_id'=>$rowCity->id];
                          $this->segregateDataByRace($criteria,'cities');
                      }    
                  }
              }  
          }        
        }      
    }
    public function requestByCountySubdivision(){
        $sRepo = new StatesRepository(app());
        $param = ['apiKey'=>$this->apiKey,'raceVar'=>$this->raceVariable,'rVar'=>Arr::pluck($this->raceVariable, 'censusVariable')];
        $genderVariables = Arr::pluck($this->raceVariable, 'censusVariableGenderTotal');// get all census Female Variables  
        collect($sRepo->model()::all())->map(function($stateRow) use ($param){
            $ctRepo = new CitiesRepository(app());
            if(count($param['rVar'])>0){
                $this->request('GET', '?get='.implode(',',$param['rVar']).',NAME&for=county%20subdivision:*&in=state:'.$stateRow->census_state_id.'&key='.$param['apiKey']); //request By Race  Variable 
                $response  = json_decode($this->getResponse());
                if(!empty($response)){
                    foreach($response as $key=>$data){
                        if($key > 0){                                        
                            $rowCity = $ctRepo->model()::where('name','like',$data[count($param['rVar'])])->first();
                            if(!empty($rowCity)){
                                $criteria['responseData'] = $data;
                                $criteria['generateRaceVar'] =array_values($this->raceVariable);// re index array keys
                                $criteria['refRaceVar'] =count($param['rVar']);
                                $criteria['refPlace'] =['city_id'=>$rowCity->id];
                                $this->segregateDataByRace($criteria,'counties_subd');
                            }    
                        }
                    }
                }                
            }          
        });         
    }
    public function requestByCountySubdivisionCities(){
        $cSubRepo = new CountiesSubdRepository(app());
        $param = ['apiKey'=>$this->apiKey,'raceVar'=>$this->raceVariable,'rVar'=>Arr::pluck($this->raceVariable, 'censusVariable')];
        $cSubRepo->model()::select(DB::raw('
                            counties_subdivision.id as cs_id,
                            counties_subdivision.county_id,
                            counties_subdivision.city_id,
                            counties_subdivision.census_cs_id,
                            cities.state_id,
                            cities.state_id,
                            cities.*,counties.name as county,
                            states.name as state,states.census_state_id,counties.census_county_id'))
                            ->join('cities','cities.id','=','counties_subdivision.city_id')
                            ->join('states','states.id','=','cities.state_id')
                            ->join('counties','counties.id','=','counties_subdivision.county_id')
                                ->chunk(1000, function ($countiesSubdChunks) use($param){
                                    collect($countiesSubdChunks)->map(function($countiesSubd) use ($param){   
                                        $ctRepo = new CitiesRepository(app());
                                        $state = new StatesRepository(app());
                                        if(count($param['rVar'])>0){
                                            $this->request('GET', '?get='.implode(',',$param['rVar']).',NAME&for=county%20subdivision:'.$countiesSubd->census_cs_id.'&in=state:'.$countiesSubd->census_state_id.'%20county:'.$countiesSubd->census_county_id.'&key='.$param['apiKey']); //request By Race  Variable 
                                            $response  = json_decode($this->getResponse());
                                            if(!empty($response)){
                                                foreach($response as $key=>$data){
                                                    if($key > 0 ){                                        
                                                        $rowCity = $ctRepo->model()::where('name','like',$data[count($param['rVar'])])->first();
                                                        if(!empty($rowCity)){
                                                            $criteria['responseData'] = $data;
                                                            $criteria['generateRaceVar'] =array_values($param['raceVar']);// re index array keys
                                                            $criteria['refRaceVar'] =count($param['rVar']);
                                                            $criteria['refPlace'] =['city_id'=>$rowCity->id];
                                                            $this->segregateDataByRace($criteria,'counties_subd_cities');
                                                        }    
                                                    }
                                                }
                                            }
                                        }
                                    });
                            });
    }
}
?>
