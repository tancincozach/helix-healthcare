<?php
namespace App\Services\ThirdParty;

use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Dingo\Api\Http\Request;
use App\Models\CronJobsCensusYear;
use App\Jobs\CensusBureauACSPopAgeDataToDb;
use App\Repositories\StatesRepository;
use App\Repositories\CountiesRepository;
use App\Repositories\CitiesRepository;
use App\Repositories\CountiesSubdRepository;
use App\Traits\CensusVariableTrait;
use Illuminate\Support\Arr;
use DB;
class CensusBureauACSPopByAge extends APIService{
    use CensusVariableTrait;

    private $baseURL = 'https://api.census.gov';

    private $apiKey;

    private $year;

    private $sexVariable;

    public function __construct(){            
        $this->baseUri .= $this->baseURL;
        $this->apiKey   = env('API_KEY_CENSUS','7bd712da9c51c10973018b94ab682ca1429775bf');
        $censusYear     = CronJobsCensusYear::firstWhere(['status'=>'1']);
        $this->year     = $censusYear->census_year;
        $this->baseUri.='/data/'. $this->year.'/acs/acs5';
        $this->sexVariable = $this->getAgeAndSexVariables($this->year);
        parent::__construct();
    }

    private function storeResult($data_result,$category)
    {
        try {
            switch($category){
                case 'cities':                     
                    CensusBureauACSPopAgedataToDb::dispatchNow($data_result,['year'=>$this->year,'category'=>$category],true);                                                 
                break;             
                default:                    
                    CensusBureauACSPopAgedataToDb::dispatchNow($data_result,['year'=>$this->year,'category'=>$category],true);                                                  
                break;
            }            
        } catch (Exception $e) {
            return Log::info("Failed to fetch data for Catgory:".$category.".");
        }
    }
    private function storeResultWithRowdata($data_result,$category,$rowdata){
        try {
            if(!empty($category))
            switch($category){
                case 'counties_subd':
                case 'counties_subd_cities':
                    CensusBureauACSPopAgedataToDb::dispatchNow($data_result,['year'=>$this->year,'category'=>$category,'dataRow'=>$rowdata],true);                    
                break;
            }            
        } catch (Exception $e) {
            return Log::info("Failed to fetch data for Catgory:".$category.".");
        }
    }
    public function segregateByAgeAndGender($params,$category){
        $censusAgeByGender = [];
        if(!empty($params['mResponse']) && !empty($params['fResponse']) && !empty($params['refPlace'])){
            $totalPopulation = 0;
            $params['mResponse'] = array_slice($params['mResponse'],0,count($this->sexVariable['mVar']));
            $params['fResponse'] = array_slice($params['fResponse'],0,count($this->sexVariable['fVar']));
            foreach($params['mResponse'] as $key=>$dataValue){            
                if(!empty($params['mResponse'][$key]) && !empty($params['fResponse'][$key])){                    
                    $totalPopulation =+ intval($params['mResponse'][$key]) + intval($params['fResponse'][$key]);
                    $censusAgeByGender[] =   $this->storeResult( array_merge([
                                                        'age_category'=>@$this->sexVariable['mVar'][$key]['ageCategory'],
                                                        'age_category_id'=>@$this->sexVariable['mVar'][$key]['ageCatID'],
                                                        'acs_year'=>$this->year,
                                                        'population'=>$totalPopulation,
                                                        'm_population'=>intval($params['mResponse'][$key]),
                                                        'f_population'=>intval($params['fResponse'][$key])
                                                    ]
                    ,$params['refPlace']),$category);
                }                
            }
        }
        return $censusAgeByGender;
    }
    public function requestByState(){    
        $mVariables = Arr::pluck($this->sexVariable['mVar'], 'censusVariable');// get all census Male Variables 
        $fVariables = Arr::pluck($this->sexVariable['fVar'], 'censusVariable');// get all census Female Variables         
        $sRepo = new StatesRepository(app());
        if(count($mVariables) > 0  && count($fVariables) > 0){
            $this->request('GET', '?get='.implode(',',$mVariables).',NAME&for=state:*&key='.$this->apiKey);//request Age By Sex Variable for Male            
            $mResponse =  json_decode($this->getResponse());              
            $this->request('GET', '?get='.implode(',',$fVariables).',NAME&for=state:*&key='.$this->apiKey); //request Age By Sex Variable for Female                        
            $fResponse =  json_decode($this->getResponse());
            $criteria['mSexVar']       = array_values($this->sexVariable['mVar']);
            $tempArr = [];
            if(!empty($mResponse)){
                foreach($mResponse as $key =>$data){
                    if($key > 0){
                        $rowState = $sRepo->model()::firstWhere(['census_state_id'=>$data[24]]);                    
                        if(!empty($rowState)){                        
                            $criteria['mResponse'] = @$mResponse[$key];
                            $criteria['fResponse'] = @$fResponse[$key];
                            $criteria['refPlace']   = ['state_id'=>$rowState->id];
                            $this->segregateByAgeAndGender($criteria,'state');
                        }         
                    }                
                }   
            }            
        }
    }
    public function requestByCounty(){    
        $mVariables = Arr::pluck($this->sexVariable['mVar'], 'censusVariable');// get all census Male Variables 
        $fVariables = Arr::pluck($this->sexVariable['fVar'], 'censusVariable');// get all census Female Variables 
        if(count($mVariables) > 0  && count($fVariables) > 0){
            $cRepo = new CountiesRepository(app());
            $this->request('GET', '?get='.implode(',',$mVariables).',NAME&for=county:*&key='.$this->apiKey);//request Age By Sex Variable for Male            
            $mResponse =  json_decode($this->getResponse());              
            $this->request('GET', '?get='.implode(',',$fVariables).',NAME&for=county:*&key='.$this->apiKey); //request Age By Sex Variable for Female                        
            $fResponse =  json_decode($this->getResponse());
            $criteria['mSexVar']       = array_values($this->sexVariable['mVar']);
            $tempArr = [];
            if(!empty($mResponse)){
                foreach($mResponse as $key =>$data){
                    if($key > 0){
                        $rowCounty = $cRepo->model()::where('name','like',$data[23])->first();                    
                        if(!empty($rowCounty)){                        
                            $criteria['mResponse'] = @$mResponse[$key];
                            $criteria['fResponse'] = @$fResponse[$key];
                            $criteria['refPlace']   = ['county_id'=>$rowCounty->id];
                            $this->segregateByAgeAndGender($criteria,'counties');
                        }         
                    }                
                }
            }            
        }
    }
    public function requestByCity(){    
        $mVariables = Arr::pluck($this->sexVariable['mVar'], 'censusVariable');// get all census Male Variables 
        $fVariables = Arr::pluck($this->sexVariable['fVar'], 'censusVariable');// get all census Female Variables 
        if(count($mVariables) > 0  && count($fVariables) > 0){
            $ctRepo = new CitiesRepository(app());
            $this->request('GET', '?get='.implode(',',$mVariables).',NAME&for=place:*&key='.$this->apiKey);//request Age By Sex Variable for Male            
            $mResponse =  json_decode($this->getResponse());                          
            $this->request('GET', '?get='.implode(',',$fVariables).',NAME&for=place:*&key='.$this->apiKey); //request Age By Sex Variable for Female                        
            $fResponse =  json_decode($this->getResponse());
            $criteria['mSexVar']       = array_values($this->sexVariable['mVar']);
            $tempArr = [];
            if(!empty($mResponse)){
                foreach($mResponse as $key =>$data){
                    if($key > 0){
                        $rowCity = $ctRepo->model()::where('name','like',$data[23])->first();                     
                        if(!empty($rowCity)){                        
                            $criteria['mResponse'] = @$mResponse[$key];
                            $criteria['fResponse'] = @$fResponse[$key];
                            $criteria['refPlace']   = ['city_id'=>$rowCity->id];
                            $this->segregateByAgeAndGender($criteria,'cities');
                        }         
                    }                
                }
            }            
        }              
    }
    public function requestByCountySubdivision(){
        $mVariables = Arr::pluck($this->sexVariable['mVar'], 'censusVariable');// get all census Male Variables 
        $fVariables = Arr::pluck($this->sexVariable['fVar'], 'censusVariable');// get all census Female Variables         
        $state = new StatesRepository(app());
        $param = ['apiKey'=>$this->apiKey];
        collect($state->model()::all())->map(function($stateRow) use ($param,$mVariables,$fVariables){
            if(count($mVariables) > 0  && count($fVariables) > 0){
                $ctRepo = new CitiesRepository(app());
                $this->request('GET', '?get='.implode(',',$mVariables).',NAME&for=county%20subdivision:*&in=state:'.$stateRow->census_state_id.'&key='.$param['apiKey']); //request Age By Sex Variable for Male
                $mResponse =  json_decode($this->getResponse());              
                $this->request('GET', '?get='.implode(',',$fVariables).',NAME&for=county%20subdivision:*&in=state:'.$stateRow->census_state_id.'&key='.$param['apiKey']); //request Age By Sex Variable for Male
                $fResponse =  json_decode($this->getResponse());
                $criteria['mSexVar']       = array_values($this->sexVariable['mVar']);
                $tempArr = [];
                if(!empty($mResponse)){
                    foreach($mResponse as $key =>$data){
                        if($key > 0){
                            $rowCity = $ctRepo->model()::where('name','like',$data[23])->first();                    
                            if(!empty($rowCity)){                        
                                $criteria['mResponse'] = @$mResponse[$key];
                                $criteria['fResponse'] = @$fResponse[$key];
                                $criteria['refPlace']   = ['city_id'=>$rowCity->id];
                                $this->segregateByAgeAndGender($criteria,'cities');
                            }         
                        }                
                    }
                }                
            }                                       
        }); 
    }
    public function requestByCountySubdivisionCities(){
        $mVariables = Arr::pluck($this->sexVariable['mVar'], 'censusVariable');// get all census Male Variables 
        $fVariables = Arr::pluck($this->sexVariable['fVar'], 'censusVariable');// get all census Female Variables     
        $cSubRepo = new CountiesSubdRepository(app());
        $param = ['apiKey'=>$this->apiKey,'sexVar'=>$this->sexVariable,'mVar'=>$mVariables,'fVar'=>$fVariables];
        $cSubRepo->model()::select(DB::raw('
                            counties_subdivision.id as cs_id,
                            counties_subdivision.county_id,
                            counties_subdivision.city_id,
                            counties_subdivision.census_cs_id,
                            cities.state_id,
                            cities.state_id,
                            cities.*,counties.name as county,
                            states.name as state,states.census_state_id,counties.census_county_id'))
                            ->join('cities','cities.id','=','counties_subdivision.city_id')
                            ->join('states','states.id','=','cities.state_id')
                            ->join('counties','counties.id','=','counties_subdivision.county_id')
                                ->chunk(1000, function ($countiesSubdChunks) use($param){
                                    collect($countiesSubdChunks)->map(function($countiesSubd) use ($param){ 
                                        $mVariables = $param['mVar'];
                                        $fVariables = $param['fVar'];
                                        if(count($mVariables) > 0  && count($fVariables) > 0){
                                            $ctRepo = new CitiesRepository(app());
                                            $this->request('GET', '?get='.implode(',',$mVariables).',NAME&for=county%20subdivision:'.$countiesSubd->census_cs_id.'&in=state:'.$countiesSubd->census_state_id.'%20county:'.$countiesSubd->census_county_id.'&key='.$param['apiKey']);
                                            $mResponse =  json_decode($this->getResponse());              
                                            $this->request('GET', '?get='.implode(',',$fVariables).',NAME&for=county%20subdivision:'.$countiesSubd->census_cs_id.'&in=state:'.$countiesSubd->census_state_id.'%20county:'.$countiesSubd->census_county_id.'&key='.$param['apiKey']);
                                            $fResponse =  json_decode($this->getResponse());
                                            $criteria['mSexVar']       = array_values($mVariables);
                                            $tempArr = [];
                                            if(!empty($mResponse)){
                                                foreach($mResponse as $key =>$data){
                                                    if($key > 0){
                                                        $rowCity = $ctRepo->model()::where('name','like',$data[23])->first();                    
                                                        if(!empty($rowCity)){                        
                                                            $criteria['mResponse'] = @$mResponse[$key];
                                                            $criteria['fResponse'] = @$fResponse[$key];
                                                            $criteria['refPlace']   = ['city_id'=>$rowCity->id];
                                                            $this->segregateByAgeAndGender($criteria,'cities');
                                                        }         
                                                    }                
                                                }
                                            }                                            
                                        }                                                                                           
                                    });
                            });
    }
}
?>
