<?php
namespace App\Services\ThirdParty;

use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Dingo\Api\Http\Request;
use App\Traits\CensusVariableTrait;
use CountryState;// Load US based States to get State code to be used for method:processState
use App\Models\CountiesSubdivision;
use App\Models\CronJobsCensusYear;
use App\Traits\GeolocationTrait;
use App\Jobs\CensusBureauGeoDataToDb;
use  App\Repositories\CountiesSubdRepository;
use  App\Repositories\CountiesRepository;
use App\Repositories\StatesRepository;
use App\Repositories\CensusTractsRepository;
use App\Repositories\CensusTractBlocksRepository;
use DB;
class CensusBureauGeo extends APIService{

    use GeolocationTrait;

    private $baseURL = 'https://api.census.gov';

    public $apiKey  = '';

    public $year = '';

    public function __construct(){            
        $this->baseUri .= $this->baseURL;
        $this->apiKey   = env('API_KEY_CENSUS','7bd712da9c51c10973018b94ab682ca1429775bf');
        $censusYear     = CronJobsCensusYear::firstWhere(['status'=>'1']);
        $this->year     = $censusYear->census_year;
        $this->baseUri.='/data/'. $this->year.'/pep/population';
        parent::__construct();
    }
    private function storeResult($data_result,$category)
    {
        try {
            switch($category){
                case 'cities':                    
                    CensusBureauGeoDataToDb::dispatchNow($data_result,['year'=>$this->year,'category'=>$category],true);                                      
                break;             
                default:                    
                    CensusBureauGeoDataToDb::dispatchNow($data_result,['year'=>$this->year,'category'=>$category],true);                                     
                break;
            }            
        } catch (Exception $e) {
            return Log::info("Failed to fetch data for Catgory:".$category.".");
        }
    }
    private function storeResultWithRowdata($data_result,$category,$rowdata){
        try {
            if(!empty($category))
            switch($category){
                case 'counties_subd':
                case 'counties_subd_cities':
                case 'census_tracts':
                case 'census_blocks':
                    CensusBureauGeoDataToDb::dispatchNow($data_result,['year'=>$this->year,'category'=>$category,'dataRow'=>$rowdata],true);
                                            //->onConnection('sync');
                break;
            }            
        } catch (Exception $e) {
            return Log::info("Failed to fetch data for Catgory:".$category.".");
        }
    }
    public function requestAndSaveState(){      
      $this->request('GET', '?get=GEONAME,POP&for=state:*&key='.$this->apiKey);
      $response  = json_decode($this->getResponse());
      collect($response)->map(function($data,$key){
            if($key > 0 ) $this->storeResult($data,'state');
      });
    }
    public function requestAndSaveCounty(){      
        $this->request('GET', '?get=GEONAME,POP&for=county:*&key='.$this->apiKey);
        $response  = json_decode($this->getResponse());
        collect($response)->map(function($data,$key){
            if($key > 0 ) $this->storeResult($data,'counties');
        });
    }
    public function requestAndSaveDivision(){      
        $this->request('GET', '?get=GEONAME,POP&for=division:*&key='.$this->apiKey);
        $response  = json_decode($this->getResponse());
        collect($response)->map(function($data,$key){
            if($key > 0 ) $this->storeResult($data,'divisions');
        });
    }
    public function requestAndSaveRegion(){      
        $this->request('GET', '?get=GEONAME,POP&for=region:*&key='.$this->apiKey);
        $response  = json_decode($this->getResponse());
        collect($response)->map(function($data,$key){
            if($key > 0 ) $this->storeResult($data,'regions');
        });
    }

    public function requestAndSaveCity(){      
        $this->request('GET', '?get=GEONAME,POP&for=place:*&key='.$this->apiKey);
        $response  = json_decode($this->getResponse());
        collect($response)->map(function($data,$key){
            if($key > 0 ) $this->storeResult($data,'cities');
        });
    } 

    public function requestAndSaveCitiesWithCountySubdivision(){
        $state = new StatesRepository(app());
        $param = ['apiKey'=>$this->apiKey];
        collect($state->model()::all())->map(function($stateRow) use ($param){
            $this->request('GET', '?get=GEONAME,POP&for=county%20subdivision:*&in=state:'.$stateRow->census_state_id.'&key='.$param['apiKey']); 
            $countyDivResponse  = json_decode($this->getResponse());
            collect($countyDivResponse)->map(function($data,$key) use ($stateRow,$param){    
                if($key > 0 ) $this->storeResultWithRowdata($data,'counties_subd',$stateRow);
            });       
        });         
    }
    public function requestAndSaveCitiesLowestBlock(){
        $cSubRepo = new CountiesSubdRepository(app());
        $param = ['apiKey'=>$this->apiKey];
        $cSubRepo->model()::select(DB::raw('
                            counties_subdivision.id as cs_id,
                            counties_subdivision.county_id,
                            counties_subdivision.city_id,
                            counties_subdivision.census_cs_id,
                            cities.state_id,
                            cities.state_id,
                            cities.*,counties.name as county,
                            states.name as state,states.census_state_id,counties.census_county_id'))
                            ->join('cities','cities.id','=','counties_subdivision.city_id')
                            ->join('states','states.id','=','cities.state_id')
                            ->join('counties','counties.id','=','counties_subdivision.county_id')
                                ->chunk(1000, function ($countiesSubdChunks) use($param){
                                        collect($countiesSubdChunks)->map(function($countiesSubd) use ($param){                    
                                                $this->request('GET', '?get=GEONAME,POP&for=place:*&in=state:'.$countiesSubd->census_state_id.'%20county:'.$countiesSubd->census_county_id.'%20county%20subdivision:'.$countiesSubd->census_cs_id.'&key='.$param['apiKey']);
                                                $response  = json_decode($this->getResponse());                    
                                                collect($response)->map(function($data,$key) use ($countiesSubd,$param){                         
                                                    if($key > 0 ) $this->storeResultWithRowdata($data,'counties_subd_cities',$countiesSubd);
                                                });    
                                        });
        });
    }
    public function requestAndSaveCensusTracts(){        
        $this->baseUri = '';
        $this->baseUri .= $this->baseURL;        
        $this->baseUri.='/data/'. $this->year.'/acs/acs5';        
        $reqUrI = $this->baseUri;
        $state = new StatesRepository(app());
        $param = ['apiKey'=>$this->apiKey];
        collect($state->model()::all())->map(function($stateRow) use ($param,$reqUrI){
            $this->request('GET',   $reqUrI.'?get=NAME,B00001_001E,GEO_ID&for=tract:*&in=state:'.$stateRow->census_state_id.'&key='.$param['apiKey']);
            $tractsResponse  = json_decode($this->getResponse());
            collect($tractsResponse)->map(function($data,$key) use ($stateRow,$reqUrI,$param){
                $cRepo = new CountiesRepository(app());         
                if($key > 0 ){                                        
                    $countiesRow = $cRepo->model()::firstWhere(['census_county_id'=>$data[3],'state_id'=>$stateRow->id]);                    
                    $this->storeResultWithRowdata($data,'census_tracts',$countiesRow);
                }                    
            });       
        });
    }
    public function requestAndSaveCensusBlocks(){        
        $this->baseUri = '';
        $this->baseUri .= $this->baseURL;        
        $this->baseUri.='/data/'. $this->year.'/acs/acs5';        
        $reqUrI = $this->baseUri;
        $counties = new CountiesRepository(app());
        $param = ['apiKey'=>$this->apiKey];

        $counties->model()::select(DB::raw('counties.id as county_id,counties.census_county_id,states.census_state_id'))
                            ->join('states','states.id','=','counties.state_id')                            
                            ->chunk(1000, function ($countyRowChunks) use($param,$reqUrI){
                                collect($countyRowChunks)->map(function($countyRow) use ($param,$reqUrI){
                                    $this->request('GET',$reqUrI.'?get=NAME,B00001_001E,GEO_ID&for=block%20group:*&in=state:'.$countyRow->census_state_id.'&in=county:'.$countyRow->census_county_id.'&in=tract:*&key='.$param['apiKey']);
                                    $blocksResponse  = json_decode($this->getResponse());                                    
                                    collect($blocksResponse)->map(function($data,$key) use ($countyRow,$reqUrI,$param){
                                        if($key > 0 ){  
                                            $tractRepo = new CensusTractsRepository(app()); 
                                            $tractRow = $tractRepo->model()::firstWhere(['tract_no'=>$data[5]]); 
                                            $this->storeResultWithRowdata($data,'census_blocks',$tractRow);
                                        }
                                    });
                                });
                            });
    }
}
?>
