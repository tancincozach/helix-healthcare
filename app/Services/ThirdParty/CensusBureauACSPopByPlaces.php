<?php
namespace App\Services\ThirdParty;

use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Dingo\Api\Http\Request;
use App\Models\CronJobsCensusYear;
use App\Jobs\CensusBureauACSPopPlacesDataToDb;
use App\Repositories\StatesRepository;
use App\Repositories\CountiesSubdRepository;
use DB;
class CensusBureauACSPopByPlaces extends APIService{


    private $baseURL = 'https://api.census.gov';

    public $apiKey  = '';

    public $year = '';

    public function __construct(){            
        $this->baseUri .= $this->baseURL;
        $this->apiKey   = env('API_KEY_CENSUS','7bd712da9c51c10973018b94ab682ca1429775bf');
        $censusYear     = CronJobsCensusYear::firstWhere(['status'=>'1']);
        $this->year     = $censusYear->census_year;
        $this->baseUri.='/data/'. $this->year.'/acs/acs5';
        parent::__construct();
    }

    private function storeResult($data_result,$category)
    {
        try {
            switch($category){
                case 'cities':                     
                    CensusBureauACSPopPlacesDataToDb::dispatchNow($data_result,['year'=>$this->year,'category'=>$category],true);                                                    
                break;             
                default:                    
                    CensusBureauACSPopPlacesDataToDb::dispatchNow($data_result,['year'=>$this->year,'category'=>$category],true);                                                    
                break;
            }            
        } catch (Exception $e) {
            return Log::info("Failed to fetch data for Catgory:".$category.".");
        }
    }
    private function storeResultWithRowdata($data_result,$category,$rowdata){
        try {
            if(!empty($category))
            switch($category){
                case 'counties_subd':
                case 'counties_subd_cities':
                    CensusBureauACSPopPlacesDataToDb::dispatchNow($data_result,['year'=>$this->year,'category'=>$category,'dataRow'=>$rowdata],true);                                                    
                break;
            }            
        } catch (Exception $e) {
            return Log::info("Failed to fetch data for Catgory:".$category.".");
        }
    }
    public function requestByState(){      
      $this->request('GET', '?get=B01001_001E,NAME&for=state:*&key='.$this->apiKey);
      $response  = json_decode($this->getResponse());
      collect($response)->map(function($data,$key){
            if($key > 0 ) $this->storeResult($data,'states');
      });
    }
    public function requestByCounty(){      
        $this->request('GET', '?get=B01001_001E,NAME&for=county:*&key='.$this->apiKey);
        $response  = json_decode($this->getResponse());
        collect($response)->map(function($data,$key){
            if($key > 0 ) $this->storeResult($data,'counties');
        });
    }
    public function requestByCity(){      
        $this->request('GET', '?get=B01001_001E,NAME&for=place:*&key='.$this->apiKey);
        $response  = json_decode($this->getResponse());
        collect($response)->map(function($data,$key){
            if($key > 0 ) $this->storeResult($data,'cities');
        });
    } 
    public function requestByCountySubdivision(){
        $state = new StatesRepository(app());
        $param = ['apiKey'=>$this->apiKey];
        collect($state->model()::all())->map(function($stateRow) use ($param){
            $this->request('GET', '?get=B01001_001E,NAME&for=county%20subdivision:*&in=state:'.$stateRow->census_state_id.'&key='.$param['apiKey']); 
            $countyDivResponse  = json_decode($this->getResponse());
            collect($countyDivResponse)->map(function($data,$key) use ($stateRow,$param){    
                if($key > 0 ) $this->storeResultWithRowdata($data,'counties_subd',$stateRow);
            });       
        });         
    }
    public function requestByCountySubdivisionCities(){
        $cSubRepo = new CountiesSubdRepository(app());
        $param = ['apiKey'=>$this->apiKey];
        $cSubRepo->model()::select(DB::raw('
                            counties_subdivision.id as cs_id,
                            counties_subdivision.county_id,
                            counties_subdivision.city_id,
                            counties_subdivision.census_cs_id,
                            cities.state_id,
                            cities.state_id,
                            cities.*,counties.name as county,
                            states.name as state,states.census_state_id,counties.census_county_id'))
                            ->join('cities','cities.id','=','counties_subdivision.city_id')
                            ->join('states','states.id','=','cities.state_id')
                            ->join('counties','counties.id','=','counties_subdivision.county_id')
                                ->chunk(1000, function ($countiesSubdChunks) use($param){
            collect($countiesSubdChunks)->map(function($countiesSubd) use ($param){                    
                    $this->request('GET', '?get=B01001_001E,NAME&for=county%20subdivision:'.$countiesSubd->census_cs_id.'&in=state:'.$countiesSubd->census_state_id.'%20county:'.$countiesSubd->census_county_id.'&key='.$param['apiKey']);
                    $response  = json_decode($this->getResponse());
                    collect($response)->map(function($data,$key) use ($countiesSubd,$param){                         
                        if($key > 0 ) $this->storeResultWithRowdata($data,'counties_subd_cities',$countiesSubd);
                    });    
            });
        });
    }
}
?>