<?php
namespace App\Services\ThirdParty;

use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Dingo\Api\Http\Request;
use App\Models\CronJobsCensusYear;
use App\Jobs\CensusBureauACSPopHHoldSizeDataToDb;
use App\Repositories\StatesRepository;
use App\Repositories\CountiesRepository;
use App\Repositories\CitiesRepository;
use App\Repositories\CountiesSubdRepository;
use App\Traits\CensusVariableTrait;
use Illuminate\Support\Arr;
use DB;

class CensusBureauACSPopByHHoldSize extends APIService{

    use CensusVariableTrait;

    private $baseURL = 'https://api.census.gov';

    public $apiKey  = '';

    public $year = '';

    public function __construct(){
        $this->baseUri .= $this->baseURL;
        $this->apiKey   = env('API_KEY_CENSUS','7bd712da9c51c10973018b94ab682ca1429775bf');
        $censusYear     = CronJobsCensusYear::firstWhere(['status'=>'1']);
        $this->year     = $censusYear->census_year;
        $this->baseUri.='/data/'. $this->year.'/acs/acs5';
        $this->hHoldSizeVariable = $this->getHouseHoldSize($this->year);        
        parent::__construct();
    }

    private function storeResult($data_result,$category)    
    {
        try {
            switch($category){
                case 'cities':                  
                    CensusBureauACSPopHHoldSizeDataToDb::dispatchNow($data_result,['year'=>$this->year,'category'=>$category],true);                                                        
                break;             
                default:                    
                CensusBureauACSPopHHoldSizeDataToDb::dispatchNow($data_result,['year'=>$this->year,'category'=>$category],true);
                break;
            }            
        } catch (Exception $e) {
            return Log::info("Failed to fetch data for Catgory:".$category.".");
        }
    }
    private function storeResultWithRowdata($data_result,$category,$rowdata){
        try {
            if(!empty($category))
            switch($category){
                case 'counties_subd':
                case 'counties_subd_cities':
                    CensusBureauACSPopHHoldSizeDataToDb::dispatchNow($data_result,['year'=>$this->year,'category'=>$category,'dataRow'=>$rowdata],true);                    
                break;
                CensusBureauACSPopHHoldSizeDataToDb::dispatchNow($data_result,['year'=>$this->year,'category'=>$category],true);                
            }            
        } catch (Exception $e) {
            return Log::info("Failed to fetch data for Catgory:".$category.".");
        }
    }
    
    public function segregateDataByHHoldSize($params,$category){        
        $returnArr = [];        
        if(!empty($params['responseData'])){            
            $response = array_slice($params['responseData'],0, $params['refVar']);
            $tempArr = [];
            if(!empty($response)){
                foreach($response as $key=>$responseVal){
                    $this->storeResult( array_merge([                       
                       'hhold_category_id'=>$this->hHoldSizeVariable[$params['hKey']]['hHoldID'],
                       'household_size'=>$this->hHoldSizeVariable[$params['hKey']]['hHoldCategory'],                  
                       'acs_year'=>$this->year,
                       'population'=>@$response[0]],// total population
                       $params['refPlace']),$category);
               }    
            }            
        }
        return  $returnArr;            
    }
    public function requestByState(){        
      $variables =array_unique( Arr::pluck($this->hHoldSizeVariable, 'censusVariable'));// get all census HouseHold Size Variables      
      if(!empty($variables)){
        $sRepo = new StatesRepository(app());
        foreach($variables as $hKey=>$censusGroup){         
            $refVar = [0=>$censusGroup];            
            $this->request('GET', '?get='.$censusGroup.',NAME&for=state:*&key='.$this->apiKey);
            $response  = json_decode($this->getResponse());            
            if(!empty($response) && !empty($refVar)){
                    foreach($response as $key=>$data){
                        if($key > 0){
                            $rowState = $sRepo->model()::where('name','like',$data[count($refVar)])->first();
                            if(!empty($rowState)){
                                $criteria['responseData'] = $data;
                                $criteria['refVar'] =count($refVar);
                                $criteria['hKey'] =$hKey;
                                $criteria['refPlace'] =['state_id'=>$rowState->id];
                                $this->segregateDataByHHoldSize($criteria,'state');
                            }    
                        }
                    }  
            }
        }        
      }
    }
    public function requestByCounty(){
        $variables =array_unique( Arr::pluck($this->hHoldSizeVariable, 'censusVariable'));// get all census HouseHold Size Variables
        if(count($variables)){
            $cRepo = new CountiesRepository(app());
            foreach($variables as $hKey=>$censusGroup){
                $refVar = [0=>$censusGroup];
                $this->request('GET', '?get='.$censusGroup.',NAME&for=county:*&key='.$this->apiKey);
                $response  = json_decode($this->getResponse());
                if(!empty($response) && !empty($refVar)){
                    foreach($response as $key=>$data){
                        if($key > 0){
                            $rowCounty = $cRepo->model()::where('name','like',$data[count($refVar)])->first();
                            if(!empty($rowCounty)){
                                $criteria['responseData'] = $data;
                                $criteria['refVar'] =count($refVar);
                                $criteria['hKey'] =$hKey;
                                $criteria['refPlace'] =['county_id'=>$rowCounty->id];
                                $this->segregateDataByHHoldSize($criteria,'counties');
                            }    
                        }
                    }  
                }   
            }        
        }      
    }
    public function requestByCity(){
        $variables =array_unique( Arr::pluck($this->hHoldSizeVariable, 'censusVariable'));// get all census HouseHold Size Variables
        if(count($variables)){
        $ctRepo = new CitiesRepository(app());
            foreach($variables as $hKey=>$censusGroup){
                $refVar = [0=>$censusGroup];
                $this->request('GET', '?get='.$censusGroup.',NAME&for=place:*&key='.$this->apiKey);
                $response  = json_decode($this->getResponse());
                if(!empty($response) && !empty($refVar)){
                    foreach($response as $key=>$data){
                        if($key > 0){
                            $rowCity = $ctRepo->model()::where('name','like',$data[count($refVar)])->first();
                            if(!empty($rowCity)){
                                $criteria['responseData'] = $data;
                                $criteria['refVar'] =count($refVar);
                                $criteria['hKey'] =$hKey;
                                $criteria['refPlace'] =['city_id'=>$rowCity->id];
                                $this->segregateDataByHHoldSize($criteria,'cities');
                            }    
                        }
                    }  
                }        
            }          
        }      
    }
    public function requestByCountySubdivision(){
        $sRepo = new StatesRepository(app());
        $param = ['apiKey'=>$this->apiKey,'hVar'=>Arr::pluck($this->hHoldSizeVariable, 'censusVariable')];
        collect($sRepo->model()::all())->map(function($stateRow) use ($param){
            $ctRepo = new CitiesRepository(app());
            if(count($param['hVar'])>0){
                foreach($param['hVar'] as $hKey=>$censusGroup){
                    $refVar = [0=>$censusGroup];
                    $this->request('GET', '?get='.$censusGroup.',NAME&for=county%20subdivision:*&in=state:'.$stateRow->census_state_id.'&key='.$param['apiKey']); //request By HouseHold Size  Variable 
                    $response  = json_decode($this->getResponse());
                    if(!empty($response) && !empty($refVar)){
                        foreach($response as $key=>$data){
                            if($key > 0){
                                $rowCity = $ctRepo->model()::where('name','like',$data[count($refVar)])->first();
                                if(!empty($rowCity)){
                                    $criteria['responseData'] = $data;
                                    $criteria['refVar'] =count($refVar);
                                    $criteria['hKey'] =$hKey;
                                    $criteria['refPlace'] =['city_id'=>$rowCity->id];
                                    $this->segregateDataByHHoldSize($criteria,'counties_subd');
                                }    
                            }
                        }
                    }   
                }
            }          
        });         
    }
    public function requestByCountySubdivisionCities(){
        $cSubRepo = new CountiesSubdRepository(app());
        $param = ['apiKey'=>$this->apiKey,'hVar'=>Arr::pluck($this->hHoldSizeVariable, 'censusVariable')];
        $cSubRepo->model()::select(DB::raw('counties_subdivision.id as cs_id,
                                            counties_subdivision.county_id,
                                            counties_subdivision.city_id,
                                            counties_subdivision.census_cs_id,
                                            cities.state_id,
                                            cities.state_id,
                                            cities.*,counties.name as county,
                                            states.name as state,states.census_state_id,counties.census_county_id'))
                            ->join('cities','cities.id','=','counties_subdivision.city_id')
                            ->join('states','states.id','=','cities.state_id')
                            ->join('counties','counties.id','=','counties_subdivision.county_id')
                                ->chunk(1000, function ($countiesSubdChunks) use($param){
                                    collect($countiesSubdChunks)->map(function($countiesSubd) use ($param){   
                                        $ctRepo = new CitiesRepository(app());
                                        $state = new StatesRepository(app());
                                        if(count($param['hVar'])>0){
                                            foreach($param['hVar'] as $hKey=>$censusGroup){
                                                $refVar = [0=>$censusGroup];
                                                $this->request('GET','?get='.$censusGroup.',NAME&for=county%20subdivision:'.$countiesSubd->census_cs_id.'&in=state:'.$countiesSubd->census_state_id.'%20county:'.$countiesSubd->census_county_id.'&key='.$param['apiKey']); //request By HouseHold Size  Variable
                                                $response  = json_decode($this->getResponse());
                                                if(!empty($response) && !empty($refVar)){
                                                    foreach($response as $key=>$data){
                                                        if($key > 0 ){
                                                            $rowCity = $ctRepo->model()::where('name','like',$data[count($param['hVar'])])->first();
                                                            if(!empty($rowCity)){
                                                                $criteria['responseData'] = $data;
                                                                $criteria['refVar'] =count($param['hVar']);
                                                                $criteria['hKey'] =$hKey;
                                                                $criteria['refPlace'] =['city_id'=>$rowCity->id];
                                                                $this->segregateDataByHHoldSize($criteria,'counties_subd_cities');
                                                            }    
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }); 
                            });
    }
}
?>