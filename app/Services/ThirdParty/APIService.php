<?php


namespace App\Services\ThirdParty;


use App\Models\ApiRequest;
use Carbon\Carbon;
use GuzzleHttp\Client;

/**
 * Class APIService
 *
 * Contains all necessary methods to centralize initialization of third-party API
 *
 * @package App\Services\ThirdParty
 */
abstract class APIService
{
    /**
     * API name holder
     * @var string
     */
    protected $apiName = 'healthcare';

    /**
     * API base uri
     * @var string
     */
    protected $baseUri = '';

    /**
     * API request container
     * @var string
     */
    protected $request = '';

    /**
     * API response container
     * @var string
     */
    protected $response = '';

    /**
     * The default client object
     * @var APIService|null
     */
    protected $client = null;

    /**
     * The guzzle client object instance
     * @var Client|null
     */
    private $guzzleClient = null;

    /**
     * Default options
     * @var array
     */
    protected $defaultOptions = [];

    /**
     * Log filename
     * @var string
     */
    private $logFileName = __CLASS__ . '.log';

    /**
     * Switch whether to log all request and response to a file
     * @var bool
     */
    protected $logToFile = false;

    /**
     * Switch whether to log all request and response to the database
     * @var bool
     */
    protected $logToDB = false;

    /**
     * Accepts $config value as an array or a string.
     * If string is supplied as parameter, it is assumed as value for <i>base_uri</i>, otherwise, accepts options
     * same as GuzzleHttp\Client()
     *
     * @param mixed $config
     *
     * @throws ThirdPartyAPIException
     * @see http://docs.guzzlephp.org/en/stable/request-options.html
     */
    function __construct($config = [])
    {
        if (!empty($this->baseUri) && false !== filter_var($this->baseUri, FILTER_VALIDATE_URL, FILTER_VALIDATE_IP)) {
            $config['base_uri'] = trim($this->baseUri);
        }
        // check if required base_uri or url as parameter in this construction is provided
        switch (true) {
            case is_array($config) && count($config) :
                if (empty($config['base_uri'])) {
                    throw new ThirdPartyAPIException('No valid base_uri is found');
                }
                break;

            case is_string($config) :
                // convert provided string as base_uri value
                $config['base_uri'] = trim($config['base_uri']);
                break;

            default :
                throw new ThirdPartyAPIException('No valid base_uri is found');
                break;
        }        
        // final check if base_uri if a valid length
        if (strlen($config['base_uri']) < 4) {
            throw new ThirdPartyAPIException('No valid base_uri is found');
        }

        // merge default options to current config array values
        $config = array_merge($config, $this->defaultOptions) ?? [];

        // init guzzle client object
        $this->guzzleClient = new Client($config);
        // override $client to be this class instance
        $this->client = $this;

        // set log filename
        true === $this->logToFile and $this->setLogFileName();

        return $this;
    }

    /**
     * Sets $logFileName value
     *
     * @param $name
     *
     * @return mixed
     */
    private function setLogFileName()
    {
        $this->logFileName = strtolower(class_basename(get_class($this))) . '.log';
    }

    /**
     * Sets request header
     *
     * @param array $headers
     *
     * @return $this
     */
    protected function setHeaders(array $headers)
    {
        $this->defaultOptions['headers'] = array_merge($this->defaultOptions['headers'] ?? [], $headers);
        return $this;
    }

    /**
     * Retrieves API response
     *
     * @return string
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * Retrieves the API raw request object
     *
     * @return string
     */
    public function getRawRequest()
    {
        return $this->request;
    }

    /**
     * Request and response logger
     */
    public function logRequest()
    {
        $logTimestamp = '[' . Carbon::now()->format('Y-m-d H:i:sO') . ' ' . \Request::getClientIp() . ']';
        $request = '';

        // @todo: need to get the request method via the main client object 
        $request .= ($baseUri = $this->guzzleClient->getConfig('base_uri'))->getPath() ."\n\n";

        foreach ($this->request->getHeaders() as $name => $value) {
            $request .= "$name: $value[0]\n";
        }
        $request .= "\n". $baseUri->getQuery();

        // save request and response to log file
        true === $this->logToFile and \Storage::disk('apilogs')->append($this->logFileName, $logTimestamp . "\n\nREQUEST:\n". $request . "\n\nRESPONSE:\n" . $this->response . "\n" . str_repeat('-', 80));

        true === $this->logToDB and app(ApiRequest::class)->logRequest(['request' => $request, 'response' => $this->response]);
    }

    /**
     * @param        $method
     * @param string $uri
     * @param array  $options
     *
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function request($method, $uri = '', array $options = [])
    {
        
        $response = $this->guzzleClient->request($method, $uri, $options);

        // store response into a global property
        // @todo: do something about this response
        $this->response =(string) $response->getBody();        

        // store request into a global property
        $this->request = $response;

        // call our logger buddy
        $this->logRequest();

        // @todo: please modify App\Services\ThirdParty\TSheets so this can be modified to return the actual API response and not this dumb raw object!
        return $response;
    }

    public function __call($name, $arguments)
    {
        return call_user_func_array([$this->guzzleClient, $name], $arguments);
    }

}

/**
 * Custom API exception error class
 * @package App\Services\ThirdParty
 */
class ThirdPartyAPIException extends \Exception {}
