<?php

namespace App\Models\Counties;

use App\Models\BaseModel;
use App\Models\Counties\County;
use App\Models\AgeCategories;
use DB;


class PopulationByAgeRaceCounty extends BaseModel
{
    protected $table = 'population_by_age_race_counties';
    
    public function counties(){
        return $this->belongsTo(County::class,'county_id');
    }
    
    public static function insertOrUpdate($censusResponse,$params){
        if(!empty($censusResponse)){
            foreach($censusResponse as $key=>$responseValue){
                if($key>0){                    
                    if(isset($responseValue[0])){
                        $rowCounty = County::select(DB::raw('id'))->firstWhere(['name'=>$responseValue[1]]);
                        if(!empty($rowCounty)){
                            $rowItem =  PopulationByAgeRaceCounty::firstOrNew(array(
                                'population'=>$responseValue[0],
                                'age_category'=>$params['ageCategory'],
                                'race_category_id'=>$params['raceCatID'],
                                'race'=>$params['race'],
                                'age_category_id'=>$params['ageCatID'],
                                'gender'=>$params['gender'],
                                'county_id'=>$rowCounty->id,
                                'acs_year'=>$params['acsYear']
                                ));              
                            $rowItem->save();                          
                        }                        
                    }
                }
            }
        }
    }
}

