<?php

namespace App\Models\Counties;
use App\Models\BaseModel;
use App\Models\Counties\County;

class PopulationBPovRaceCounty extends BaseModel
{
    protected $table="population_bpov_lvl_race_counties";

    public function counties(){
        return $this->belongsTo(County::class,'county_id');
    }
}
