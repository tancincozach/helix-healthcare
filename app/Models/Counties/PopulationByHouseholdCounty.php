<?php

namespace App\Models\Counties;

use App\Models\BaseModel;
use App\Models\Counties\County;
use App\Models\AgeCategories;
use DB;


class PopulationByHouseholdCounty extends BaseModel
{
    protected $table = 'population_by_house_size_counties';
    
    public function counties(){
        return $this->belongsTo(County::class,'county_id');
    }
    
}

