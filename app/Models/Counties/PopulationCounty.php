<?php

namespace App\Models\Counties;

use App\Models\BaseModel;
use App\Models\Counties\County;
use App\Models\States\State;
use DB;

class PopulationCounty extends BaseModel
{
    protected $table = 'population_counties';
    //protected $fillable = ['county_id','population','acs_year']; 
    public $timestamps = true;

    // public function states(){
    //     return $this->belongsTo(State::class,'id');
    // }
    // public function counties(){
    //     return $this->belongsTo(County::class,'county_id');
    // }
}
