<?php

namespace App\Models\Counties;

use App\Models\BaseModel;
use App\Models\Counties\County;
use App\Models\AgeCategories;
use DB;


class PopulationByRaceCounty extends BaseModel
{
    protected $table = 'population_by_race_counties';
    
    public function counties(){
        return $this->belongsTo(County::class,'county_id');
    }
    
}

