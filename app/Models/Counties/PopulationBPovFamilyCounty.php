<?php

namespace App\Models\Counties;

use App\Models\BaseModel;
use App\Models\Counties\County;
use DB;

class PopulationBPovFamilyCounty extends BaseModel
{
    protected $table = 'population_bpov_lvl_family_counties';

    public function counties(){
       return  $this->belongsTo(County::class,'county_id');
    }
    
}
