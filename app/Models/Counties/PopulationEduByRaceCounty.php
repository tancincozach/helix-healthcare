<?php

namespace App\Models\Counties;

use App\Models\BaseModel;
use App\Models\Counties\County;
use App\Models\EducationCategories;
use App\Models\RaceCategories;
use DB;

class PopulationEduByRaceCounty extends BaseModel
{
    protected $table = 'population_edu_fld_race_counties';

    public function counties(){
        return  $this->belongsTo(County::class,'county_id');
    } 
    public function race(){
        return  $this->belongsTo(RaceCategories::class,'race_category_id');
    }
    public function educationAttainment(){
        return  $this->belongsTo(EducationCategories::class,'edu_fld_id');
    }
   
}

