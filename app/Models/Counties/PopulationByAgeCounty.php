<?php

namespace App\Models\Counties;

use App\Models\BaseModel;
use App\Models\Counties\County;
use App\Models\AgeCategories;
use DB;

class PopulationByAgeCounty extends BaseModel
{
    protected $table = 'population_by_age_counties as parent';
    
    public function counties(){
        return $this->belongsTo(County::class,'county_id');
    }

    public static function insertOrUpdate($censusResponse,$params){      
        if(!empty($censusResponse)){
            foreach($censusResponse as $key=>$responseValue){                
                if($key > 0){                    
                    if(isset($responseValue[0])){
                        $rowCounty = County::select(DB::raw('id'))->firstWhere(['name'=>$responseValue[1]]);
                        $rowItem = PopulationByAgeCounty::firstOrCreate(array(
                                                'population'=>$responseValue[0],
                                                'age_category'=>$params['ageCategory'],
                                                'age_category_id'=>$params['ageCatID'],
                                                'gender'=>$params['gender'],
                                                'county_id'=>$rowCounty->id,
                                                'acs_year'=>$params['acsYear']  
                                                ));            
                        $rowItem->save();                            
                    }
                }
            }
        }
    }
}
