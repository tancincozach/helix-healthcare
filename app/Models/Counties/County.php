<?php

namespace App\Models\Counties;

use App\Models\BaseModel;
use App\Traits\GeolocationTrait;
use App\Models\States\State;
use DB;

class County extends BaseModel
{
    use GeolocationTrait;

    protected $table = 'counties';
    public $timestamps = true; 
    
    public function formatforLocation(){
        return[
            'id'=>$this->id,
            'name'=>$this->name,
            'lat'=>$this->lat,
            'lng'=>$this->lng,
            'ne_lat'=>$this->ne_lat,
            'ne_lng'=>$this->ne_lng,
            'sw_lat'=>$this->sw_lat,
            'sw_lng'=>$this->sw_lng
        ];
    }
    public function states(){
        return $this->belongsTo(State::class,'state_id');
    }
}

