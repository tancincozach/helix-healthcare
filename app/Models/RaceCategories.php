<?php

namespace App\Models;

use App\Models\BaseModel;

class RaceCategories extends BaseModel
{
    //
    protected $table = 'categories_race';
    public $timestamps = true;
}
