<?php

namespace App\Models;

use App\Models\BaseModel;

class HouseHoldSizeCategories extends BaseModel
{
    protected $table='categories_household_size';
}
