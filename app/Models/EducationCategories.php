<?php

namespace App\Models;

use App\Models\BaseModel;

class EducationCategories extends BaseModel
{
    protected $table='categories_educational_attainment';
}
