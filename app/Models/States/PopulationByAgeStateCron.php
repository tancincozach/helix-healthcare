<?php

namespace App\Models\States;

use App\Models\BaseModel;
use App\Models\States\State;
use App\Models\AgeCategories;
use DB;

class PopulationByAgeStateCron extends BaseModel
{
    protected $table = 'population_by_age_states';

    public function states(){
        return $this->belongsTo(State::class,'state_id');
    }
    
}

