<?php

namespace App\Models\States;

use App\Models\BaseModel;
use App\Models\States\State;
use App\Models\EducationCategories;
use App\Models\RaceCategories;
use DB;

class PopulationEduByRaceState extends BaseModel
{
    protected $table = 'population_edu_fld_race_states';

    public function states(){
        return  $this->belongsTo(State::class,'state_id');
    } 
    public function race(){
        return  $this->belongsTo(RaceCategories::class,'race_category_id');
    }
    public function educationAttainment(){
        return  $this->belongsTo(EducationCategories::class,'edu_fld_id');
    }
   
}

