<?php

namespace App\Models\States;
use App\Models\BaseModel;
use App\Traits\GeolocationTrait;
use DB;

class State extends BaseModel
{
    use GeolocationTrait;
    
    protected $table = 'states';

    //protected $fillable = array('current_acs_population');

    public $timestamps = true;

    public function formatforLocation(){
        return[
            'id'=>$this->id,
            'name'=>$this->name,
            'lat'=>$this->lat,
            'lng'=>$this->lng,
            'ne_lat'=>$this->ne_lat,
            'ne_lng'=>$this->ne_lng,
            'sw_lat'=>$this->sw_lat,
            'sw_lng'=>$this->sw_lng
        ];
    }
}
