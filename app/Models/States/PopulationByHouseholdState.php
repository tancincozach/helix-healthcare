<?php

namespace App\Models\States;

use App\Models\BaseModel;
use App\Models\States\State;
use DB;

class PopulationByHouseholdState extends BaseModel
{
    protected $table = 'population_by_house_size_states';

    public function states(){
        return $this->belongsTo(State::class,'state_id');
    }
    
}

