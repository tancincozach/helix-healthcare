<?php

namespace App\Models\States;

use App\Models\BaseModel;
use App\Models\States\State;
use App\Models\AgeCategories;
use DB;

class PopulationByAgeState extends BaseModel
{
    protected $table = 'population_by_age_states as parent';

    public function states(){
        return $this->belongsTo(State::class,'state_id');
    }
    public function totalPopulation($id){
        return DB::table('population_by_age_states')->select(DB::raw('SUM(population) as total_population'))->where('state_id',$id)->first();
    }
    
    public static function insertOrUpdate($censusResponse,$params){
        if(!empty($censusResponse)){
            foreach($censusResponse as $key=>$responseValue){
                if($key>0){                    
                    if(isset($responseValue[0])){
                        $rowState = State::select(DB::raw('id'))->firstWhere(['census_state_id'=>$responseValue[2]]);
                        $rowItem =  PopulationByAgeState::firstOrNew(array(
                                                                    'population'=>$responseValue[0],
                                                                    'age_category'=>$params['ageCategory'],
                                                                    'age_category_id'=>$params['ageCatID'],
                                                                    'gender'=>$params['gender'],
                                                                    'state_id'=>$rowState->id,
                                                                    'acs_year'=>$params['acsYear']
                            ));              
                        $rowItem->save();                      
                    }
                }
            }
        }
    }
}

