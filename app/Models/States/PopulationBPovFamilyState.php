<?php

namespace App\Models\States;

use App\Models\BaseModel;
use App\Models\States\State;
use DB;

class PopulationBPovFamilyState extends BaseModel
{
    protected $table = 'population_bpov_lvl_family_states';

    public function states(){
       return  $this->belongsTo(State::class,'state_id');
    }
    
}
