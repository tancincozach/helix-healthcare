<?php

namespace App\Models\States;

use App\Models\BaseModel;
use App\Models\States\State;
use DB;

class PopulationByAgeRaceState extends BaseModel
{
    protected $table = 'population_by_age_race_states as parent';

    public function states(){
        return $this->belongsTo(State::class,'state_id');
    }
    
    public static function insertOrUpdate($censusResponse,$params){
        if(!empty($censusResponse)){
            foreach($censusResponse as $key=>$responseValue){
                if($key>0){                    
                    if(isset($responseValue[0])){
                        $rowState = State::select(DB::raw('id'))->firstWhere(['census_state_id'=>$responseValue[2]]);
                        if(!empty($rowState)){
                            $rowItem =  PopulationByAgeRaceState::firstOrNew(array('population'=>$responseValue[0],
                                                    'age_category'=>$params['ageCategory'],
                                                    'race_category_id'=>$params['raceCatID'],
                                                    'race'=>$params['race'],
                                                    'age_category_id'=>$params['ageCatID'],
                                                    'gender'=>$params['gender'],
                                                    'state_id'=>$rowState->id,
                                                    'acs_year'=>$params['acsYear']
                            ));              
                            $rowItem->save();                      
                        }                        
                    }
                }
            }
        }
    }
}

