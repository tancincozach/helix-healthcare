<?php

namespace App\Models\States;

use App\Models\BaseModel;
use App\Models\States\State;
use DB;

class PopulationState extends BaseModel
{
    protected $table = 'population_states';
    protected $fillable = ['state_id','population','acs_year']; 
    
    public function states(){
        return $this->belongsTo(State::class,'state_id');
    }
}
