<?php

namespace App\Models;

use App\Models\BaseModel;

class LanguageSpokenCategories extends BaseModel
{
    protected $table='categories_language_spoken';
}
