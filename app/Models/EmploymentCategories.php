<?php

namespace App\Models;

use App\Models\BaseModel;

class EmploymentCategories extends BaseModel
{
    protected $table='categories_employment';
}
