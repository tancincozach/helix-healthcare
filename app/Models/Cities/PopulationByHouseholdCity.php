<?php

namespace App\Models\Cities;

use App\Models\BaseModel;
use App\Models\Cities\City;
use App\Models\AgeCategories;
use DB;

class PopulationByHouseholdCity extends BaseModel
{
    protected $table = 'population_by_house_size_cities';

    public function cities(){
        return  $this->belongsTo(City::class,'city_id');
     } 
   
}

