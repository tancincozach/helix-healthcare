<?php

namespace App\Models\Cities;

use App\Models\BaseModel;
use App\Models\Cities\City;
use App\Models\AgeCategories;
use DB;

class PopulationBPovFamilyCity extends BaseModel
{
    protected $table = 'population_bpov_lvl_family_cities';

    public function cities(){
       return  $this->belongsTo(City::class,'city_id');
    }
    
}
