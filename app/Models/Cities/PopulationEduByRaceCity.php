<?php

namespace App\Models\Cities;

use App\Models\BaseModel;
use App\Models\Cities\City;
use App\Models\EducationCategories;
use App\Models\RaceCategories;
use DB;

class PopulationEduByRaceCity extends BaseModel
{
    protected $table = 'population_edu_fld_race_cities';

    public function cities(){
        return  $this->belongsTo(City::class,'city_id');
    } 
    public function race(){
        return  $this->belongsTo(RaceCategories::class,'race_category_id');
    }
    public function educationAttainment(){
        return  $this->belongsTo(EducationCategories::class,'edu_fld_id');
    }
}

