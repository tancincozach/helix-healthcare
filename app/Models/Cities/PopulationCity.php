<?php

namespace App\Models\Cities;

use App\Models\BaseModel;
use App\Models\Cities\City;
use DB;

class PopulationCity extends BaseModel
{
    protected $table = 'population_cities';
    public $timestamps = true;
   // protected $fillable = ['city_id','population','acs_year']; 

    
    public function cities(){
        return  $this->belongsTo(City::class,'city_id');
    }
}