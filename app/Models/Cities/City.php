<?php
namespace App\Models\Cities;

use App\Models\BaseModel;
use App\Traits\GeolocationTrait;
use App\Models\States\State;
use DB;

class City extends BaseModel
{
    use GeolocationTrait;

    protected $table = 'cities';
    public $timestamps = true;

    public function states(){
        return $this->belongsTo(State::class,'state_id');
    }
}
