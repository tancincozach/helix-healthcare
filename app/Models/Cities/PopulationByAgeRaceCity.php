<?php

namespace App\Models\Cities;

use App\Models\BaseModel;
use App\Models\Cities\City;
use App\Models\AgeCategories;
use DB;

class PopulationByAgeRaceCity extends BaseModel
{
    protected $table = 'population_by_age_race_cities';

    public function cities(){
        return  $this->belongsTo(City::class,'city_id');
     } 
    
    public static function insertOrUpdate($censusResponse,$params){
        if(!empty($censusResponse)){
            foreach($censusResponse as $key=>$responseValue){
                if($key>0){                    
                    if(isset($responseValue[0])){
                        $rowCity = City::select(DB::raw('id'))
                                         ->firstWhere(['name'=>$censusResponse[1]]);
                        if(!empty($rowCity)){
                            $rowItem =  PopulationByAgeRaceCity::firstOrNew(array(
                                'population'=>$censusResponse[0],
                                'age_category'=>$params['ageCategory'],
                                'race_category_id'=>$params['raceCatID'],
                                'race'=>$params['race'],
                                'age_category_id'=>$params['ageCatID'],
                                'gender'=>$params['gender'],
                                'city_id'=>$rowCity->id,
                                'acs_year'=>$params['acsYear']
                            ));              
                            $rowItem->save();                      
                        }                        
                    }
                }
            }
        }
    }
}

