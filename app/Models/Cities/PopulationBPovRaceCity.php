<?php

namespace App\Models\Cities;
use App\Models\BaseModel;
use App\Models\Cities\City;

class PopulationBPovRaceCity extends BaseModel
{
    protected $table="population_bpov_lvl_race_cities";

    public function cities(){
        return $this->belongsTo(City::class,'city_id');
    }
}
