<?php

namespace App\Models\Cities;

use App\Models\BaseModel;
use App\Models\Cities\City;
use App\Models\AgeCategories;
use DB;

class PopulationByAgeCity extends BaseModel
{
    protected $table = 'population_by_age_cities as parent';

    public function cities(){
       return  $this->belongsTo(City::class,'city_id');
    }
    
    public static function insertOrUpdate($censusResponse,$params){
        if(!empty($censusResponse)){         
            foreach($censusResponse as $key=>$censusResponse){
                if($key>0){                    
                    if(isset($censusResponse[0])){
                        $rowCity = City::select(DB::raw('id'))
                                         ->firstWhere(['name'=>$censusResponse[1]]);
                        $rowItem =  PopulationByAgeCity::firstOrNew(
                                                                array(
                                                                    'population'=>$censusResponse[0],
                                                                    'age_category'=>$params['ageCategory'],
                                                                    'age_category_id'=>$params['ageCatID'],
                                                                    'gender'=>$params['gender'],
                                                                    'city_id'=>$rowCity->id,
                                                                    'acs_year'=>$params['acsYear']
                        ));              
                        $rowItem->save();
                    }
                }
            }
        }
    }
}
