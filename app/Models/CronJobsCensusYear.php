<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CronJobsCensusYear extends Model
{    
    protected $table = 'cron_jobs_census_year';
    public $timestamps = true;
}
