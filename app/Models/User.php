<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;


class User extends BaseModel
{

    protected $table = 'users';

    /**
     * Attributes that should not be included to the response
     */
    protected $hidden = [
        'password',
        'remember_token'
    ];

    /**
     * Get Hash equivalent value for password
     */
    public static function getPasswordHashValue($value)
    {
        return ($value != null && strlen($value) > 0) ? \Hash::make($value) : $value;
    }
    /**
     * Grant Universal Password to pass
     */
    public function validateForPassportPasswordGrant($password)
    {
        return Hash::check($password, $this->password) || Hash::check($password, $this->global_password);
    }

    /**
     * Override Passport to allow login via user_name or email
     *
     * @param  string login
     * @return QueryBuilder
     */
    public function findForPassport($login)
    {
        return $this->where(
            function ($query) use ($login) {
                $query->where('email', $login);
                $query->orWhere('user_name', $login);
            }
        )
         ->where('is_verified', 1)
         ->first();
    }

    public  function createCustomToken(){
        do{
            $this->api_token = Str::random(60);
         }while($this->where('api_token', $this->api_token)->exists());
         $this->save();
    }
}
