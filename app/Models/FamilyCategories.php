<?php

namespace App\Models;

use App\Models\BaseModel;

class FamilyCategories extends BaseModel
{
    protected $table = 'categories_family';
}
