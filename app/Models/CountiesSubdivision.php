<?php

namespace App\Models;

use App\Models\BaseModel;

class CountiesSubdivision extends BaseModel
{
    //
    public $table = 'counties_subdivision';
    public $timestamps = true;
}
