<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApiRequest extends Model
{
    protected $fillable = [
        'api_user',
        'request',
        'response',
        'ip_address',
    ];

    protected static function boot()
    {
        parent::boot();
        static::created(function (ApiRequest $apiRequest) {
            try {
                $apiRequest->api_user_id = request()->session()->has('api_user_id') ? request()->session()->get('api_user_id') : 0;
            } catch (\Throwable $e) {
                $apiRequest->api_user_id = 0;
            }

            empty($apiRequest->request) and $apiRequest->request = LogHelper::getRawRequest();
            $apiRequest->ip_address = request()->getClientIp();
            $apiRequest->save();
        });
    }

    public function setIpAddressAttribute($value)
    {
        $this->attributes['ip_address'] = request()->getClientIp();
    }

    /**
     * "Request" field value manipulation
     *
     * @param $value
     */
    public function setRequestAttribute($value)
    {
        $this->attributes['request'] = $value ?? LogHelper::getRawRequest();
    }

    /**
     * Explicitly sets API user id
     *
     * @param $value
     */
    public function setApiUserId($value)
    {
        try {
            $this->attributes['api_user_id'] = $value ?? (request()->session()->has('api_user_id') ? request()->session()->get('api_user_id') : 0);
        } catch (\Throwable $e) {
            $this->attributes['api_user_id'] = 0;
        }
    }

    /**
     * Simple API logger method
     *
     * @param array $data
     *
     * @return mixed
     */
    public function logRequest(array $data)
    {
        return $this->create([
            'request'  => $data['request'] ?? null,
            'response' => $data['response'] ?? '',
        ]);
    }
}