<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Repositories\PopulationEduFieldByRaceStateRepository;
use App\Repositories\PopulationEduFieldByRaceCityRepository;
use App\Repositories\PopulationEduFieldByRaceCountyRepository;
use DB;

class CensusBureauACSPopEdFieldByRaceDataToDb implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($responseData=array(),$otherDataArr,$fromApi=false)
    {
        $this->scrapedDataArray = $responseData;
        $this->scrapedDataPopCategory = $otherDataArr['category'];
        $this->dataRow   = @$otherDataArr['dataRow'];
        $this->censusAcsYear= $otherDataArr['year'];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(PopulationEduFieldByRaceStateRepository $pbrsRepo,                            
                           PopulationEduFieldByRaceCountyRepository $pbrcRepo,
                           PopulationEduFieldByRaceCityRepository $pbrctRepo
                          ){
            if($this->scrapedDataPopCategory=='state'){                
                $pbrsRepo->model()::firstOrCreate($this->scrapedDataArray);
            }
            if($this->scrapedDataPopCategory=='counties'){
                $pbrcRepo->model()::firstOrCreate($this->scrapedDataArray);
            }
            if($this->scrapedDataPopCategory=='cities' || $this->scrapedDataPopCategory=='counties_subd' || $this->scrapedDataPopCategory=='counties_subd_cities'){
                $pbrctRepo->model()::firstOrCreate($this->scrapedDataArray);
            }
    }
}
