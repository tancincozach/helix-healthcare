<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Repositories\StatesRepository;
use App\Repositories\CountiesRepository;
use App\Repositories\CitiesRepository;
use App\Repositories\PopulationByStateRepository;
use App\Repositories\PopulationByCityRepository;
use App\Repositories\PopulationByCountyRepository;
use DB;

class CensusBureauACSPopPlacesDataToDb implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($responseData=array(),$otherDataArr,$fromApi=false)
    {
        $this->scrapedDataArray = $responseData;
        $this->scrapedDataPopCategory = $otherDataArr['category'];
        $this->dataRow   = @$otherDataArr['dataRow'];
        $this->censusAcsYear= $otherDataArr['year'];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(StatesRepository $sRepo,
                            CountiesRepository $cRepo,
                            CitiesRepository $ctRepo,
                            PopulationByStateRepository $psRepo,
                            PopulationByCountyRepository $pcRepo,
                            PopulationByCityRepository $pctRepo
                          )
    {
            if($this->scrapedDataPopCategory=='states'){
                if(!empty($this->scrapedDataArray)){
                    $rowState = $sRepo->model()::select(DB::raw('states.id'))
                    ->firstWhere(['states.census_state_id'=>$this->scrapedDataArray[2]
                            ]);
                    if(!empty($rowState)){
                        $existRow = $psRepo->model()::select(DB::raw('id'))
                                            ->firstWhere([
                                                            'state_id'=>$rowState->id,
                                                            'acs_year'=>$this->censusAcsYear
                                        ]);
                        if(!empty($existRow)){
                            $psRepo->model()::where(['id'=>$existRow->id])
                                                ->update(['population'=>$this->scrapedDataArray[0]
                            ]);
                        }else{                    
                            $psRepo->model()::create([
                                'state_id' => $rowState->id,
                                'population'=>$this->scrapedDataArray[0],
                                'acs_year' =>$this->censusAcsYear
                            ]);
                        }
                    }  
                }       
            }
            if($this->scrapedDataPopCategory=='counties'){
                if(!empty($this->scrapedDataArray)){
                    $rowCounty = $cRepo->model()::select(DB::raw('id'))
                    ->firstWhere([ 'name'=>$this->scrapedDataArray[1]]);                         
                    if(!empty($rowCounty)){
                        $existRow = $pcRepo->model()::select(DB::raw('id'))
                                                    ->firstWhere([
                                                                'county_id'=>$rowCounty->id,
                                                                'acs_year' =>$this->censusAcsYear
                                ]);
                        if(!empty($existRow)){
                            $pcRepo->model()::where(['id'=>$existRow->id])
                                        ->update(['population'=>$this->scrapedDataArray[0]]);
                        }else{                    
                            $pcRepo->model()::create([
                                'county_id' => $rowCounty->id,
                                'population'=>$this->scrapedDataArray[0],
                                'acs_year' =>$this->censusAcsYear
                            ]);
                        }                
                    }  
                }
            }
            if($this->scrapedDataPopCategory=='cities' || $this->scrapedDataPopCategory=='counties_subd' || $this->scrapedDataPopCategory=='counties_subd_cities'){
                $rowCity = $ctRepo->model()::select(DB::raw('id'))
                                ->firstWhere(['cities.name'=>ucwords($this->scrapedDataArray[1])]);
                    if(!empty($rowCity)){
                        $existRow = $pctRepo->model()::select(DB::raw('id'))
                                                ->firstWhere([
                                                            'city_id'=>$rowCity->id,
                                                            'acs_year' =>$this->censusAcsYear
                                ]);
                        if(!empty($existRow)){
                            $pctRepo->model()::where(['id'=>$existRow->id])
                                        ->update(['population'=>$this->scrapedDataArray[0]
                            ]);
                        }else{                      
                            $pctRepo->model()::create([
                                'city_id' => $rowCity->id,
                                'population'=>$this->scrapedDataArray[0],
                                'acs_year' =>$this->censusAcsYear
                            ]);
                        }                
                    }         
            }
    }
}
