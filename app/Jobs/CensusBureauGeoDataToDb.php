<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Services\ThirdParty\CensusBureauGeo;
use App\Repositories\StatesRepository;
use App\Repositories\CountiesRepository;
use App\Repositories\UsRegionsRepository;
use App\Repositories\UsDivisionsRepository;
use App\Repositories\CountiesSubdRepository;
use App\Repositories\CitiesRepository;
use App\Repositories\CountiesSubdCitiesRepository;
use App\Repositories\CensusTractsRepository;
use App\Repositories\CensusTractBlocksRepository;
use App\Traits\GeolocationTrait;
use DB;

class CensusBureauGeoDataToDb implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels,GeolocationTrait;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $scrapedDataArray,$scrapedDataGeoCategory, $censusYear,$dataRow;

    public function __construct($responseData=array(),$otherDataArr,$fromApi=false)
    {   
        $this->scrapedDataArray = $responseData;
        $this->scrapedDataGeoCategory = $otherDataArr['category'];
        $this->dataRow   = @$otherDataArr['dataRow'];
        $this->censusYear= $otherDataArr['year'];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(
        CensusBureauGeo $censusBur,
        UsRegionsRepository $rRepo,
        UsDivisionsRepository $dRepo,
        StatesRepository $sRepo,
        CountiesRepository $cRepo,
        CitiesRepository $ctRepo,
        CountiesSubdRepository $csRepo,
        CountiesSubdCitiesRepository $cscRepo,
        CensusTractsRepository $ctractsRepo,
        CensusTractBlocksRepository $ctblocksRepo)
    {
        // Inserting of US States Based on Census Bureau PEP        
        if(!empty($this->scrapedDataArray)){
            if($this->scrapedDataGeoCategory=='state'){                
                $placeName = ucwords($this->scrapedDataArray[0]);
                $locRow = $sRepo->model()::firstWhere(['name'=>$placeName]);
                $locationData = GeolocationTrait::getCoordinates($locRow);
                $tempArr = [
                                'name'=> $placeName,
                                'census_state_id'=>$this->scrapedDataArray[2],
                                'current_acs_population'=>$this->scrapedDataArray[1],
                                'acs_year'=>$this->censusYear
                            ];
                $tempArr = !empty($locationData) ? array_merge($stateArr,$locationData):$tempArr;
                $sRepo->firstOrCreate($tempArr);
            }
            // Inserting of US Counties Based on Census Bureau PEP   
            if($this->scrapedDataGeoCategory=='counties'){
                $placeName = ucwords($this->scrapedDataArray[0]);
                $locRow    = $cRepo->model()::firstWhere(['name'=>$placeName]);
                $stateRow  = $sRepo->model()::firstWhere(['census_state_id'=>$this->scrapedDataArray[2]]);
                $locationData = GeolocationTrait::getCoordinates($locRow);
                $tempArr = [
                                'name'=> $placeName,
                                'census_county_id'=>$this->scrapedDataArray[3],
                                'state_id'=>$stateRow->id,
                                'current_acs_population'=>$this->scrapedDataArray[1],
                                'acs_year'=>$this->censusYear
                            ];
                $tempArr = !empty($locationData) ? array_merge($tempArr,$locationData):$tempArr;
                $cRepo->firstOrCreate($tempArr);
            }
            // Inserting of US Regions Based on Census Bureau PEP   
            if($this->scrapedDataGeoCategory=='regions'){
                $placeName = ucwords($this->scrapedDataArray[0]);
                $tempArr = ['name'=> $placeName,
                            'census_region_id'=>$this->scrapedDataArray[2],
                            'current_population'=>$this->scrapedDataArray[1]];
                $rRepo->firstOrCreate($tempArr);
            }
            // Inserting of US Divisions Based on Census Bureau PEP   
            if($this->scrapedDataGeoCategory=='divisions'){
                $placeName = ucwords($this->scrapedDataArray[0]);
                $tempArr = ['name'=>$placeName,
                            'census_division_id'=>$this->scrapedDataArray[2],
                            'current_population'=>$this->scrapedDataArray[1]];
                $dRepo->firstOrCreate($tempArr);                
            }
            //Inserting of US Cities Based on Census Bureau PEP   
            if($this->scrapedDataGeoCategory=='cities'){
                $placeName = ucwords($this->scrapedDataArray[0]);
                $citiesRow = $ctRepo->model()::firstWhere(['name'=>$placeName]);
                if(!empty($citiesRow)){
                    $ctRepo->where('id', $citiesRow->id)
                    ->update(['census_place_id'=>$this->scrapedDataArray[3]]);    
                }
                $placeName = ucwords($this->scrapedDataArray[0]);
                $locationData = GeolocationTrait::getCoordinates($citiesRow);
                $stateRow = $sRepo->model()::select('states.id')
                ->where('census_state_id',$this->scrapedDataArray[2])->first();
                $tempArr = ['name'=> ucwords($placeName),
                            'state_id'=>$stateRow->id, 
                            'census_place_id'=>$this->scrapedDataArray[3],                   
                            'current_acs_population'=>$this->scrapedDataArray[0],
                            'acs_year'=>$this->censusYear];
                $tempArr = !empty($locationData) ? array_merge($tempArr,$locationData):$tempArr;
                $ctRepo->firstOrCreate($tempArr);
            }
            // Inserting of US Cities with Counties Based on Census Bureau PEP   
            if($this->scrapedDataGeoCategory=='counties_subd'){
                $placeName = ucwords($this->scrapedDataArray[0]);
                $stateRow  = $this->dataRow;
                if(isset($stateRow)){
                    $countyRow = $cRepo->model()::firstWhere(['census_county_id'=>$this->scrapedDataArray[3],'state_id'=>$stateRow->id]);
                    $cityRow   = $ctRepo->model()::firstWhere(['name'=>$placeName]);
                    if(!empty($cityRow)){                    
                        $countySubdCities = ['city_id'=>$cityRow->id,
                                            'county_id'=>@$countyRow->id,
                                            'census_cs_id'=>$this->scrapedDataArray[4]];
                        $csRepo->firstOrCreate($countySubdCities);
                        $ctRepo->model()::where('id',$cityRow->id)
                        ->update(['census_place_id'=>$this->scrapedDataArray[4]]);
                    }else{
                        $locationData = GeolocationTrait::getCoordinates($cityRow);
                        $cities = ['name'=>$placeName,
                                  'state_id'=>$stateRow->id,
                                  'acs_year'=>$this->censusYear,
                                  'current_acs_population'=>$this->scrapedDataArray[1]];
                        $cities  = !empty($locationData) ? array_merge($cities,$locationData):$cities; 
                        $cityRow = $ctRepo->firstOrCreate($cities);
                        $countySubdCities = ['city_id'=>$cityRow->id,
                                            'county_id'=>@$countyRow->id,                                        
                                            'census_cs_id'=>$this->scrapedDataArray[4]];
                        $csRepo->firstOrCreate($countySubdCities);
                    }
                }               
            }
            // Inserting of US Cities down to the lowest block which has a county  
            if($this->scrapedDataGeoCategory=='counties_subd_cities'){
                $placeName = ucwords($this->scrapedDataArray[0]);
                $countiesSubd  = $this->dataRow;
                if(isset($countiesSubd)){                
                    $cityRow   = $ctRepo->model()::firstWhere(['name'=>$placeName]);
                    if(!empty($cityRow)){                    
                        $countySubdCities = [
                                            'cs_id'=>$countiesSubd->cs_id,
                                            'city_id'=>$cityRow->id,                                            
                                            'census_csc_id'=>$this->scrapedDataArray[5]];
                        $cscRepo->firstOrCreate($countySubdCities);
                        $ctRepo->model()::where('id',$cityRow->id)
                                          ->update(['census_place_id'=>$this->scrapedDataArray[5]]);
                    }else{
                        $locationData = GeolocationTrait::getCoordinates($cityRow);
                        $cities = ['name'=>$placeName,
                                'state_id'=>$countiesSubd->state_id,
                                'census_place_id'=>$this->scrapedDataArray[5],
                                'acs_year'=>$this->censusYear,
                                'current_acs_population'=>$this->scrapedDataArray[1]];
                        $cities  = !empty($locationData) ? array_merge($cities,$locationData):$cities; 
                        $cityRow = $ctRepo->firstOrCreate($cities);
                        $countySubdCities = ['cs_id'=>$countiesSubd->cs_id,
                                            'city_id'=>$cityRow->id,                                            
                                            'census_csc_id'=>$this->scrapedDataArray[5]];
                        $cscRepo->firstOrCreate($countySubdCities);
                    }
                }  
            }
            // Inserting of US Census Tracts
            if($this->scrapedDataGeoCategory=='census_tracts'){
                $placeName = ucwords($this->scrapedDataArray[0]);
                $counties  = $this->dataRow;                
                if(isset($counties)){                                                                    
                    $census_tracts = ['name'=>$placeName,
                                    'geo_id'=>$this->scrapedDataArray[2],
                                    'county_id'=>$counties->id,
                                    'tract_no'=>$this->scrapedDataArray[5],
                                    'acs_year'=>$this->censusYear,
                                    'current_acs_population'=>$this->scrapedDataArray[1]];
                    $ctractsRepo->firstOrCreate($census_tracts);                
                }  
            }
            // Inserting of US Census Blocks
            if($this->scrapedDataGeoCategory=='census_blocks'){
                $placeName = ucwords($this->scrapedDataArray[0]);
                $censusTracts  = $this->dataRow;     
                if(isset($censusTracts)){                                                                    
                    $census_tracts_blocks = ['name'=>$placeName,
                                            'geo_id'=>$this->scrapedDataArray[2],
                                            'county_id'=>$censusTracts->county_id,
                                            'tract_id'=>$censusTracts->id,
                                            'block_group_no'=>$this->scrapedDataArray[6],
                                            'acs_year'=>$this->censusYear,
                                            'current_acs_population'=>$this->scrapedDataArray[1]];
                    
                    $ctblocksRepo->firstOrCreate($census_tracts_blocks);                
                }                
            }
        }        
        
    }
}
