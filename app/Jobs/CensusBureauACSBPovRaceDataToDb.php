<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Repositories\PopulationBPovLvlByRaceStateRepository;
use App\Repositories\PopulationBPovLvlByRaceCountyRepository;
use App\Repositories\PopulationBPovLvlByRaceCityRepository;
use DB;

class CensusBureauACSBPovRaceDataToDb implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($responseData=array(),$otherDataArr,$fromApi=false)
    {
        $this->scrapedDataArray = $responseData;
        $this->scrapedDataPopCategory = $otherDataArr['category'];
        $this->dataRow   = @$otherDataArr['dataRow'];
        $this->censusAcsYear= $otherDataArr['year'];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(PopulationBPovLvlByRaceStateRepository $sPovLvlRepo,
                            PopulationBPovLvlByRaceCountyRepository $cPovLvlRepo,
                            PopulationBPovLvlByRaceCityRepository $ctPovLvlRepo
                          ){                        
            if($this->scrapedDataPopCategory=='state'){
                $sPovLvlRepo->model()::firstOrCreate($this->scrapedDataArray);
            }
            if($this->scrapedDataPopCategory=='counties'){
                $cPovLvlRepo->model()::firstOrCreate($this->scrapedDataArray);
            }
            if($this->scrapedDataPopCategory=='cities' || $this->scrapedDataPopCategory=='counties_subd' || $this->scrapedDataPopCategory=='counties_subd_cities'){
                $ctPovLvlRepo->model()::firstOrCreate($this->scrapedDataArray);
            }
    }
}
