<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Repositories\PopulationBPovLvlByLangSpokenStateRepository;
use App\Repositories\PopulationBPovLvlByLangSpokenCityRepository;
use App\Repositories\PopulationBPovLvlByLangSpokenCountyRepository;
use DB;

class CensusBureauACSBPovByLangSpokenToDb implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($responseData=array(),$otherDataArr,$fromApi=false)
    {
        $this->scrapedDataArray = $responseData;
        $this->scrapedDataPopCategory = $otherDataArr['category'];
        $this->dataRow   = @$otherDataArr['dataRow'];
        $this->censusAcsYear= $otherDataArr['year'];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(PopulationBPovLvlByLangSpokenStateRepository $pbrsRepo,
                           PopulationBPovLvlByLangSpokenCountyRepository $pbrcRepo,
                           PopulationBPovLvlByLangSpokenCityRepository $pbrctRepo
                          ){
            if($this->scrapedDataPopCategory=='state'){                
                $pbrsRepo->model()::firstOrCreate($this->scrapedDataArray);
            }
            if($this->scrapedDataPopCategory=='counties'){
                $pbrcRepo->model()::firstOrCreate($this->scrapedDataArray);
            }
            if($this->scrapedDataPopCategory=='cities' || $this->scrapedDataPopCategory=='counties_subd' || $this->scrapedDataPopCategory=='counties_subd_cities'){
                $pbrctRepo->model()::firstOrCreate($this->scrapedDataArray);
            }
    }
}
