<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Repositories\PopulationByHHoldSizeStateRepository;
use App\Repositories\PopulationByHHoldSizeCityRepository;
use App\Repositories\PopulationByHHoldSizeCountyRepository;
use DB;

class CensusBureauACSPopHHoldSizeDataToDb implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($responseData=array(),$otherDataArr,$fromApi=false)
    {
        $this->scrapedDataArray = $responseData;
        $this->scrapedDataPopCategory = $otherDataArr['category'];
        $this->dataRow   = @$otherDataArr['dataRow'];
        $this->censusAcsYear= $otherDataArr['year'];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(PopulationByHHoldSizeStateRepository $sPopA,
                            PopulationByHHoldSizeCityRepository $ctPopA,
                            PopulationByHHoldSizeCountyRepository $cPopA
                          ){            
            if($this->scrapedDataPopCategory=='state'){
                $sPopA->model()::firstOrCreate($this->scrapedDataArray);
            }
            if($this->scrapedDataPopCategory=='counties'){
                $cPopA->model()::firstOrCreate($this->scrapedDataArray);
            }
            if($this->scrapedDataPopCategory=='cities' || $this->scrapedDataPopCategory=='counties_subd' || $this->scrapedDataPopCategory=='counties_subd_cities'){
                $ctPopA->model()::firstOrCreate($this->scrapedDataArray);
            }
    }
}
