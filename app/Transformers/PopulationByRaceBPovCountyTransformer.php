<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Counties\PopulationBPovRaceCounty;

class PopulationByRaceBPovCountyTransformer extends TransformerAbstract
{
    public function transform(PopulationBPovRaceCounty $model)
    {               
            return [
                'id'          => (int)$model->id,
                'county'       => $model->counties->name,
                'name'        => $model->race,
                'population'  => number_format($model->population),
                'acsYear'     => $model->acs_year,
                'created_at'  => $model->created_at,
                'updated_at'  => !is_null($model->updated_at) ? $model->updated_at->format('Y-m-d H:m:s') : null
            ];
       
        
    }
}
