<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\States\State;

class StatesTransformer extends TransformerAbstract
{
    public function transform(State $model)
    {
            return [
                'acsYr'=> $model->acs_year,
                'censusStateID'=> $model->census_state_id,
                'stateCode'=>$model->code,
                'createdAt' => $model->created_at,
                'currAcsPop' => $model->current_acs_population,
                'deletedAt'=> $model->deleted_at,
                'id' => $model->id,
                'lat'=> $model->lat,
                'lng'=> $model->lng,
                'name'=> $model->name,
                'ne_lat'=> $model->ne_lat,
                'ne_lng'=> $model->ne_lng,
                'sw_lat'=> $model->sw_lat,
                'sw_lng'=> $model->sw_lng,
                'updated_at'=> $model->updated_at
             ];
       
        
    }
}
