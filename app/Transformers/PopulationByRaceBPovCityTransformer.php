<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Cities\PopulationBPovRaceCity;

class PopulationByRaceBPovCityTransformer extends TransformerAbstract
{
    public function transform(PopulationBPovRaceCity $model)
    {               
            return [
                'id'          => (int)$model->id,
                'city'        => $model->cities->name,
                'name'        => $model->race,
                'population'  => number_format($model->population),
                'acsYear'     => $model->acs_year,
                'created_at'  => $model->created_at,
                'updated_at'  => !is_null($model->updated_at) ? $model->updated_at->format('Y-m-d H:m:s') : null
            ];
       
        
    }
}
