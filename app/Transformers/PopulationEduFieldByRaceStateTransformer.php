<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\States\PopulationEduByRaceState;

class PopulationEduFieldByRaceStateTransformer extends TransformerAbstract
{
    public function transform(PopulationEduByRaceState $model)
    {               
            return [
                'id'          => (int)$model->id,
                'state'       => $model->states->name,
                'race'        =>$model->race,
                'name'        => $model->field_name,
                'population'  => number_format($model->population),
                'acsYear'     => $model->acs_year,
                'created_at'  => $model->created_at,
                'updated_at'  => !is_null($model->updated_at) ? $model->updated_at->format('Y-m-d H:m:s') : null
            ];
       
        
    }
}
