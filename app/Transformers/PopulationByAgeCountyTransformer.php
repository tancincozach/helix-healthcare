<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Counties\PopulationByAgeCounty;

class PopulationByAgeCountyTransformer extends TransformerAbstract
{
    public function transform(PopulationByAgeCounty $model)
    {
        return [
            'id'          => (int)$model->id,
            'county'     => $model->county,
            'age_category'     => $model->age_category,
            'total_pop_m' =>number_format($model->m_population),
            'over_all_population'=>number_format($model->over_all_population),
            'over_all_total_m' =>number_format($model->over_all_m_population),
            'over_all_total_f' =>number_format($model->over_all_f_population),
            'total_pop_f' =>number_format($model->f_population),
            'county_id'     => $model->county_id,             
            'total_population'  => number_format($model->total_population),
            'created_at'  => $model->created_at,
            'updated_at'  => !is_null($model->updated_at) ? $model->updated_at->format('Y-m-d H:m:s') : null
        ];
        
    }
}
