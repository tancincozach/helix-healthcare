<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Counties\PopulationBPovEduCounty;

class PopulationByEduBPovCountyTransformer extends TransformerAbstract
{
    public function transform(PopulationBPovEduCounty $model)
    {
            return [
                'id'          => (int)$model->id,                
                'counties'     => $model->counties, 
                'name'     => $model->counties->name,       
                'population'  => $model->population,
                'acsYear'  => $model->acs_year,
                'created_at'  => $model->created_at,
                'updated_at'  => !is_null($model->updated_at) ? $model->updated_at->format('Y-m-d H:m:s') : null
            ];
       
        
    }
}
