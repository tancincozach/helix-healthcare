<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\States\PopulationState;

class PopulationByStateTransformer extends TransformerAbstract
{
    public function transform(PopulationState $model)
    {
            return [
                'id'          => (int)$model->id,                
                'states'     => $model->states, 
                'name'     => $model->states->name,       
                'population'  => $model->population,
                'acsYear'  => $model->acs_year,
                'created_at'  => $model->created_at,
                'updated_at'  => !is_null($model->updated_at) ? $model->updated_at->format('Y-m-d H:m:s') : null
            ];
       
        
    }
}
