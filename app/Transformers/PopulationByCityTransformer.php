<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Cities\PopulationCity;

class PopulationByCityTransformer extends TransformerAbstract
{
    public function transform(PopulationCity $model)
    {
            $city = array ();
            $city['state_id']= $model->state_id;
            $city['name']= $model->city;
            $city['lat']= $model->lat;
            $city['lng']= $model->lng;
            $city['ne_lat']= $model->ne_lat;
            $city['ne_lng']= $model->ne_lng;
            $city['sw_lat']= $model->sw_lat;
            $city['sw_lng']= $model->sw_lng;
            $city['current_acs_population']= $model->current_acs_population;
            $city['acs_year']= $model->cities_acs_yr;
            return [
                'id'          => (int)$model->id,
                'city_info'   => (object)$city,
                'city'        => $model->city,
                'state'       => $model->state,
                'name'        => $model->name,
                'city_id'     => $model->city_id,             
                'population'  => $model->population,
                'acsYear'     => $model->acs_year,
                'created_at'  => $model->created_at,
                'updated_at'  => !is_null($model->updated_at) ? $model->updated_at->format('Y-m-d H:m:s') : null
            ];
       
        
    }
}
