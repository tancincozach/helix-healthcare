<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\User;

class UserTransformer extends TransformerAbstract
{
    public function transform(User $model)
    {
            return [
                'id'          => (int)$model->id,
                'email'       => $model->email,
                'user_name'   => $model->user_name,
                'is_verified' => $model->is_verified,
                'updated_at'  => !is_null($model->updated_at) ? $model->updated_at->format('Y-m-d H:m:s') : null
            ];
       
        
    }
}
