<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Cities\PopulationBPovFamilyCity;

class PopulationByFamilyBPovCityTransformer extends TransformerAbstract
{
    public function transform(PopulationBPovFamilyCity $model)
    {               
            return [
                'id'          => (int)$model->id,
                'city'       => $model->cities->name,
                'name'        => $model->family_category,
                'population'  => number_format($model->population),
                'acsYear'     => $model->acs_year,
                'created_at'  => $model->created_at,
                'updated_at'  => !is_null($model->updated_at) ? $model->updated_at->format('Y-m-d H:m:s') : null
            ];
       
        
    }
}
