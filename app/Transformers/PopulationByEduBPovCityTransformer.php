<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Cities\PopulationBPovEduCity;

class PopulationByEduBPovCityTransformer extends TransformerAbstract
{
    public function transform(PopulationBPovEduCity $model)
    {
            return [
                'id'          => (int)$model->id,                
                'cities'     => $model->cities, 
                'name'     => $model->cities->name,       
                'population'  => $model->population,
                'acsYear'  => $model->acs_year,
                'created_at'  => $model->created_at,
                'updated_at'  => !is_null($model->updated_at) ? $model->updated_at->format('Y-m-d H:m:s') : null
            ];
       
        
    }
}
