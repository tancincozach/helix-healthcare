<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\AgeCategories;

class AgeCategoriesTransformer extends TransformerAbstract
{
    public function transform(AgeCategories $model)
    {   
        
        return [       
            'category_name' => $model->name
        ];
    
    }
}
