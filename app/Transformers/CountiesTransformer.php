<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Counties\County;

class CountiesTransformer extends TransformerAbstract
{
    public function transform(County $model)
    {
            return [
                'acsYr'=> $model->acs_year,
                'censusCID'=> $model->census_county_id,                
                'createdAt' => $model->created_at,
                'state'=>$model->states->name,
                'currAcsPop' => $model->current_acs_population,
                'deletedAt'=> $model->deleted_at,
                'id' => $model->id,
                'lat'=> $model->lat,
                'lng'=> $model->lng,
                'name'=> $model->name,
                'ne_lat'=> $model->ne_lat,
                'ne_lng'=> $model->ne_lng,
                'sw_lat'=> $model->sw_lat,
                'sw_lng'=> $model->sw_lng,
                'updated_at'=> $model->updated_at
             ];
       
        
    }
}
