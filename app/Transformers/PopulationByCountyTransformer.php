<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Counties\PopulationCounty;

class PopulationByCountyTransformer extends TransformerAbstract
{
    public function transform(PopulationCounty $model)
    {               
            $county = array ();
            $county['census_county_id']= $model->census_county_id;
            $county['state_id']= $model->state_id;
            $county['name']= $model->county;
            $county['lat']= $model->lat;
            $county['lng']= $model->lng;
            $county['ne_lat']= $model->ne_lat;
            $county['ne_lng']= $model->ne_lng;
            $county['sw_lat']= $model->sw_lat;
            $county['sw_lng']= $model->sw_lng;
            $county['current_acs_population']= $model->current_acs_population;
            $county['acs_year']= $model->counties_acs_yr;

            return [
                'id'          => (int)$model->id,
                'state'       => $model->state,
                'name'        => $model->county,
                'county'        => $model->county,
                'counties'    => (object)$county,
                'population'  => $model->population,
                'acsYear'     => $model->acs_year,
                'created_at'  => $model->created_at,
                'updated_at'  => !is_null($model->updated_at) ? $model->updated_at->format('Y-m-d H:m:s') : null
            ];
       
        
    }
}
