<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Cities\PopulationByAgeCity;

class PopulationByAgeCityTransformer extends TransformerAbstract
{
    public function transform(PopulationByAgeCity $model)
    {
        return [
                'id'          => (int)$model->id,
                'city'     => $model->city,
                'age_category'     => $model->age_category,
                'total_pop_m' =>number_format($model->m_population),
                'over_all_population'=>number_format($model->over_all_population),
                'over_all_total_m' =>number_format($model->over_all_m_population),
                'over_all_total_f' =>number_format($model->over_all_f_population),
                'total_pop_f' =>number_format($model->f_population),
                'city_id'     => $model->city_id,             
                'total_population'  => number_format($model->total_population),
                'created_at'  => $model->created_at,
                'updated_at'  => !is_null($model->updated_at) ? $model->updated_at->format('Y-m-d H:m:s') : null
        ];
   
    }
}
