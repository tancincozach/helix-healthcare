<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Services\ThirdParty\CensusBureauACSPopEdFieldByRace;

class FetchCensusACSEdFieldByRaceData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'censuspop-ed-field-race:get';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetching of Census ACS Pobulation with Bachelors Degree with Respective Field By Race (State,State and Cities) Data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $censusACSPopEdFieldRace = new CensusBureauACSPopEdFieldByRace();
        $censusACSPopEdFieldRace->requestByState();
        $censusACSPopEdFieldRace->requestByCounty();
        $censusACSPopEdFieldRace->requestByCity();
        $censusACSPopEdFieldRace->requestByCountySubdivision();
        $censusACSPopEdFieldRace->requestByCountySubdivisionCities();
    }
}
