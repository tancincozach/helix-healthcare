<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Services\ThirdParty\CensusBureauACSPopByHHoldSize;

class FetchCensusACSPopByHouseHoldSizeData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'censuspop-household-size:get';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetching of Census ACS  Population By HouseHold Size(State,State and Cities) Data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $censusACSPopHHold = new CensusBureauACSPopByHHoldSize();
        $censusACSPopHHold->requestByState();
        $censusACSPopHHold->requestByCounty();
        $censusACSPopHHold->requestByCity();        
        $censusACSPopHHold->requestByCountySubdivision();
        $censusACSPopHHold->requestByCountySubdivisionCities();

    }
}
