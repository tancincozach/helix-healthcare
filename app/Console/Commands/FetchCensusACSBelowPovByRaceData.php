<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Services\ThirdParty\CensusBureauACSBelowPovertyByRace;

class FetchCensusACSBelowPovByRaceData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'censusbpov-race:get';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetching of Census ACS Pobulation Below Poverty Level By Race(State,State and Cities) Data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $censusACSPopBelowPovRace = new CensusBureauACSBelowPovertyByRace();
        $censusACSPopBelowPovRace->requestByState();        
        $censusACSPopBelowPovRace->requestByCounty();
        $censusACSPopBelowPovRace->requestByCity();
        $censusACSPopBelowPovRace->requestByCountySubdivision();
        $censusACSPopBelowPovRace->requestByCountySubdivisionCities();

    }
}
