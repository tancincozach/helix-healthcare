<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Services\ThirdParty\CensusBureauACSPopByRace;

class FetchCensusACSRace extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'censuspop-race:get';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetching of Census American Survey By Race';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $censusRace = new CensusBureauACSPopByRace();
        $censusRace->requestByState();
        $censusRace->requestByCity();
        $censusRace->requestByCounty();
        $censusRace->requestByCountySubdivision();
        $censusRace->requestByCountySubdivisionCities();
    }
}
