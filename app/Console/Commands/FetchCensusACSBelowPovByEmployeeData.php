<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Services\ThirdParty\CensusBureauACSBelowPovertyByEmployment;

class FetchCensusACSBelowPovByEmployeeData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'censusbpov-employment:get';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetching of Census ACS Below Poverty Pobulation By Employment(State,State and Cities) Data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $censusACSPopEmp = new CensusBureauACSBelowPovertyByEmployment();
        $censusACSPopEmp->requestByState();
        $censusACSPopEmp->requestByCounty();
        $censusACSPopEmp->requestByCity();
        $censusACSPopEmp->requestByCountySubdivision();
        $censusACSPopEmp->requestByCountySubdivisionCities();
    }
}
