<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Services\ThirdParty\CensusBureauACSBelowPovertyByEducation;

class FetchCensusACSBelowPovByEduData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'censusbpov-edu:get';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetching of Census ACS Below Poverty Pobulation By Education(State,State and Cities) Data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $censusACSPopEdu = new CensusBureauACSBelowPovertyByEducation();
        $censusACSPopEdu->requestByState();
        $censusACSPopEdu->requestByCity();
        $censusACSPopEdu->requestByCounty();
        $censusACSPopEdu->requestByCountySubdivision();
        $censusACSPopEdu->requestByCountySubdivisionCities();
    }
}
