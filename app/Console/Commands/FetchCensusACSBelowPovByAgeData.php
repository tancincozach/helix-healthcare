<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Services\ThirdParty\CensusBureauACSBelowPovertyByAge;

class FetchCensusACSBelowPovByAgeData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'censusbpov-age:get';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetching of Census ACS Pobulation Below Poverty Level By Sex And Age(State,State and Cities) Data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $censusACSPopBelowPovByAge = new CensusBureauACSBelowPovertyByAge();
        $censusACSPopBelowPovByAge->requestByState();
        $censusACSPopBelowPovByAge->requestByCity();
        $censusACSPopBelowPovByAge->requestByCounty();
        $censusACSPopBelowPovByAge->requestByCountySubdivision();
        $censusACSPopBelowPovByAge->requestByCountySubdivisionCities();

    }
}
