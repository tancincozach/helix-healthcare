<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Services\ThirdParty\CensusBureauACSPopByPlaces;

class FetchCensusACSPopByPlacesData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'censuspop-places:get';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetching of Census ACS Unweighted(Total) Population By Places(State,State and Cities) Data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $censusACSPopByPlaces = new CensusBureauACSPopByPlaces();
        // $censusACSPopByPlaces->requestByState();        
        // $censusACSPopByPlaces->requestByCounty();
        $censusACSPopByPlaces->requestByCity();
        $censusACSPopByPlaces->requestByCountySubdivision();
        $censusACSPopByPlaces->requestByCountySubdivisionCities();
    }
}
