<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Services\ThirdParty\CensusBureauGeo;

class FetchCensusGeoData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'censusgeo:get';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetching of Census Geo Data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $censusGeo = new CensusBureauGeo();
        // $censusGeo->requestAndSaveState();
        // $censusGeo->requestAndSaveCounty();
        // $censusGeo->requestAndSaveRegion();
        // $censusGeo->requestAndSaveDivision();    
        $censusGeo->requestAndSaveCensusTracts();
        $censusGeo->requestAndSaveCensusBlocks();        
        // $censusGeo->requestAndSaveCity();
        // $censusGeo->requestAndSaveCitiesWithCountySubdivision();
        // $censusGeo->requestAndSaveCitiesLowestBlock();
    }
}
