<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {        
        $schedule->command('censusgeo:get')
                        ->daily()
                        ->withoutOverlapping()
                        ->runInBackground();
        $schedule->command('censuspop-places:get')
                        ->daily()
                        ->withoutOverlapping()
                        ->runInBackground();
        $schedule->command('censuspop-age:get')
                        ->daily()
                        ->withoutOverlapping()
                        ->runInBackground();
        $schedule->command('censuspop-race:get')
                        ->daily()
                        ->withoutOverlapping()
                        ->runInBackground();
        $schedule->command('censuspop-ed-field-race')
                        ->daily()
                        ->withoutOverlapping()
                        ->runInBackground();
        $schedule->command('censusbpov-age:get')
                        ->daily()
                        ->withoutOverlapping()
                        ->runInBackground();
        $schedule->command('censusbpov-race:get')
                        ->daily()
                        ->withoutOverlapping()
                        ->runInBackground();
        $schedule->command('censusbpov-family:get')
                        ->daily()
                        ->withoutOverlapping()
                        ->runInBackground();
        $schedule->command('censusbpov-edu:get')
                        ->daily()
                        ->withoutOverlapping()
                        ->runInBackground();
        $schedule->command('censusbpov-employment:get')
                        ->daily()
                        ->withoutOverlapping()
                        ->runInBackground();
        $schedule->command('censusbpov-lang-spoken:get')
                        ->daily()
                        ->withoutOverlapping()
                        ->runInBackground();
        $schedule->command('censuspop-household-size:get')
                        ->daily()
                        ->withoutOverlapping()
                        ->runInBackground();

        // $schedule->command('censusgeo:get')->cron('0 */6 * * *');
        // $schedule->command('censuspop-places:get')->cron('0 */6 * * *');
        // $schedule->command('censuspop-age:get')->cron('0 */6 * * *');
        // $schedule->command('censuspop-race:get')->cron('0 */6 * * *');
        // $schedule->command('censusbpov-age:get')->cron('0 */6 * * *');
        // $schedule->command('censusbpov-race:get')->cron('0 */6 * * *');
        // $schedule->command('censuspop-ed-field-race')->cron('0 */6 * * *');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
