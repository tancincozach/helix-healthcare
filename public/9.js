(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[9],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/base/ExploreAutoSuggest.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/base/ExploreAutoSuggest.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vue_autosuggest__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue-autosuggest */ "./node_modules/vue-autosuggest/dist/vue-autosuggest.esm.js");
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'AutoSuggest',
  components: {
    VueAutosuggest: vue_autosuggest__WEBPACK_IMPORTED_MODULE_1__["VueAutosuggest"]
  },
  data: function data() {
    return {
      searchCategory: '',
      dataResponse: {
        'sexAge': [],
        'bpov': [],
        'race': [],
        'efield': []
      },
      popByAgeData: [],
      popBpovData: [],
      popEduFieldData: [],
      popRaceData: [],
      population: {
        'over_all_population': 0,
        'over_all_pop_m': 0,
        'over_all_pop_f': 0
      },
      totalPopulation: 0,
      searchSuggestLimit: 11,
      searchByOptions: 'state',
      searchOptions: [],
      searchInput: '',
      autoSuggest: {
        selected: '',
        filteredOptions: [{
          data: []
        }],
        options: [{
          data: []
        }],
        inputProps: {
          id: 'autosuggest__input',
          onInputChange: this.autoSuggestOnInputChange,
          placeholder: 'Search',
          onclick: 'this.select();'
        },
        limit: 8
      },
      query: "",
      selected: ""
    };
  },
  computed: _objectSpread({
    filteredOptions: function filteredOptions() {
      var _this = this;

      return [{
        data: this.suggestions[0].data.filter(function (option) {
          return option.name.toLowerCase().indexOf(_this.query.toLowerCase()) > -1;
        })
      }];
    }
  }, Object(vuex__WEBPACK_IMPORTED_MODULE_2__["mapGetters"])({
    meta: 'explore/meta',
    exploreData: 'explore/data',
    state_info: 'pop_age_states/data',
    county_info: 'pop_age_counties/data',
    city_info: 'pop_age_cities/data',
    race_state_info: 'race_by_state/data',
    race_county_info: 'race_by_county/data',
    race_city_info: 'race_by_city/data',
    race_bpov_state: 'race_bpov_state/data',
    race_bpov_county: 'race_bpov_county/data',
    race_bpov_city: 'race_bpov_city/data',
    race_edu_field_state: 'race_edu_field_state/data',
    race_edu_field_county: 'race_edu_field_county/data',
    race_edu_field_city: 'race_edu_field_city/data'
  })),
  methods: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_2__["mapActions"])({
    getExploreData: 'explore/getExploreResult',
    getPopByAgeStates: 'pop_age_states/getPopByAgeStates',
    getPopByAgeCounties: 'pop_age_counties/getPopByAgeCounties',
    getPopByAgeCities: 'pop_age_cities/getPopByAgeCities',
    getPopRaceStates: 'race_by_state/getPopRaceState',
    getPopRaceCounties: 'race_by_county/getPopRaceCounty',
    getPopRaceCities: 'race_by_city/getPopRaceCity',
    getPopBpovRaceStates: 'race_bpov_state/getPopBpovRaceStates',
    getPopBpovRaceCounties: 'race_bpov_county/getPopBpovRaceCounties',
    getPopBpovRaceCities: 'race_bpov_city/getPopBpovRaceCities',
    getPopEduFieldRaceStates: 'race_edu_field_state/getPopEduFieldRaceStates',
    getPopEduFieldRaceCounties: 'race_edu_field_county/getPopEduFieldRaceCounties',
    getPopEduFieldRaceCities: 'race_edu_field_city/getPopEduFieldRaceCities'
  }), _defineProperty({
    changeSearchBy: function changeSearchBy() {},

    /** Auto-suggest function **/
    autoSuggestOnSelect: function autoSuggestOnSelect(option) {
      this.autoSuggest.selected = option ? option.item : '';

      if (this.autoSuggest.selected) {
        this.selectLocation();
        this.searchInput = this.autoSuggest.selected;
        this.search();
      }
    },

    /** Auto-suggest function **/
    autoSuggestOnInputChange: function autoSuggestOnInputChange(text) {
      if (text === '' || text === undefined) {
        this.autoSuggest.filteredOptions = [{
          data: []
        }];
        this.searchInput = '';
        return;
      }

      this.searchInput = text;
      this.searchAsync(text);

      if (this.autoSuggest.options[0].data) {
        var filteredData = this.autoSuggest.options[0].data.filter(function (item) {
          return item.name.toLowerCase().indexOf(text.toLowerCase()) > -1;
        }).slice(0, this.autoSuggest.limit);
        this.autoSuggest.filteredOptions = [{
          data: filteredData
        }];
      } else {}
    },
    exportFile: function exportFile() {
      if (!_.isEmpty(this.dataResponse.sexAge) || !_.isEmpty(this.dataResponse.bpov) || !_.isEmpty(this.dataResponse.efield)) {
        var a = document.createElement("a");
        var file = new Blob([JSON.stringify(this.dataResponse, null, 4)], {
          type: 'text/plain'
        });
        a.href = URL.createObjectURL(file);
        a.download = this.searchByOptions + '.json';
        a.click();
      }
    },
    selectLocation: function selectLocation() {
      var _this2 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _this2.dataResponse.sexAge = [];
                _this2.dataResponse.bpov = [];
                _this2.dataResponse.race = [];
                _this2.dataResponse.efield = [];
                _this2.population.over_all_population = 0;
                _this2.population.over_all_pop_f = 0;
                _this2.population.over_all_pop_m = 0;
                _context.t0 = _this2.searchByOptions;
                _context.next = _context.t0 === 'state' ? 10 : _context.t0 === 'county' ? 23 : _context.t0 === 'city' ? 36 : 49;
                break;

              case 10:
                _context.next = 12;
                return _this2.getPopByAgeStates({
                  query: {
                    'id': _this2.autoSuggest.selected.id
                  }
                });

              case 12:
                _this2.dataResponse.sexAge = _this2.state_info;
                _context.next = 15;
                return _this2.getPopRaceStates({
                  query: {
                    'id': _this2.autoSuggest.selected.id
                  }
                });

              case 15:
                _this2.dataResponse.race = _this2.race_state_info;
                _context.next = 18;
                return _this2.getPopBpovRaceStates({
                  query: {
                    'id': _this2.autoSuggest.selected.id
                  }
                });

              case 18:
                _this2.dataResponse.bpov = _this2.race_bpov_state;
                _context.next = 21;
                return _this2.getPopEduFieldRaceStates({
                  query: {
                    'id': _this2.autoSuggest.selected.id
                  }
                });

              case 21:
                _this2.dataResponse.efield = _this2.race_edu_field_state;
                return _context.abrupt("break", 49);

              case 23:
                _context.next = 25;
                return _this2.getPopByAgeCounties({
                  query: {
                    'id': _this2.autoSuggest.selected.id
                  }
                });

              case 25:
                _this2.dataResponse.sexAge = _this2.county_info;
                _context.next = 28;
                return _this2.getPopRaceCounties({
                  query: {
                    'id': _this2.autoSuggest.selected.id
                  }
                });

              case 28:
                _this2.dataResponse.race = _this2.race_county_info;
                _context.next = 31;
                return _this2.getPopBpovRaceCounties({
                  query: {
                    'id': _this2.autoSuggest.selected.id
                  }
                });

              case 31:
                _this2.dataResponse.bpov = _this2.race_bpov_county;
                _context.next = 34;
                return _this2.getPopEduFieldRaceCounties({
                  query: {
                    'id': _this2.autoSuggest.selected.id
                  }
                });

              case 34:
                _this2.dataResponse.efield = _this2.race_edu_field_county;
                return _context.abrupt("break", 49);

              case 36:
                _context.next = 38;
                return _this2.getPopByAgeCities({
                  query: {
                    'id': _this2.autoSuggest.selected.id
                  }
                });

              case 38:
                _this2.dataResponse.sexAge = _this2.city_info;
                _context.next = 41;
                return _this2.getPopRaceCities({
                  query: {
                    'id': _this2.autoSuggest.selected.id
                  }
                });

              case 41:
                _this2.dataResponse.race = _this2.race_city_info;
                _context.next = 44;
                return _this2.getPopBpovRaceCities({
                  query: {
                    'id': _this2.autoSuggest.selected.id
                  }
                });

              case 44:
                _this2.dataResponse.bpov = _this2.race_bpov_city;
                _context.next = 47;
                return _this2.getPopEduFieldRaceCities({
                  query: {
                    'id': _this2.autoSuggest.selected.id
                  }
                });

              case 47:
                _this2.dataResponse.efield = _this2.race_edu_field_city;
                return _context.abrupt("break", 49);

              case 49:
                if (!_.isEmpty(_this2.dataResponse.sexAge[0])) {
                  _this2.population.over_all_population = _this2.dataResponse.sexAge[0].over_all_population;
                  _this2.population.over_all_pop_f = _this2.dataResponse.sexAge[0].over_all_total_f;
                  _this2.population.over_all_pop_m = _this2.dataResponse.sexAge[0].over_all_total_m;
                }

              case 50:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }))();
    },
    searchAsync: function searchAsync(search_value) {
      var _this3 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        var selectOptions;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.next = 2;
                return _this3.getExploreData({
                  query: {
                    'search': search_value,
                    'searchBy': _this3.searchByOptions
                  }
                });

              case 2:
                selectOptions = [];

                _.map(_this3.exploreData, function (result) {
                  selectOptions.unshift({
                    id: result.id,
                    name: result.name
                  });
                });

                _this3.searchOptions = selectOptions;

              case 5:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }))();
    },
    doFilter: function doFilter(value) {
      this.sort = false;
      this.$emit('displayFilter', value);
    },
    doSort: function doSort(value) {
      if (this.sort) this.$emit('sort', value);
      this.sort = true;
    },
    doSearch: function doSearch() {
      // set default event to emit
      var emit = 'search'; //emit

      this.$emit(emit, this.term);
      /**
       * Always set the current page to 1 when making queries.
       * This is to avoid rendering issue on the datatable and pagination.
       */

      this.$emit('page', 1);
    },
    search: function search(event) {
      this.autoSuggest.options = [{
        data: this.searchOptions
      }]; // this.autoSuggest.inputProps.placeholder = this.searchPlaceholder;

      this.autoSuggest.limit = this.searchSuggestLimit;

      if (event) {
        if (event.keyCode != 13 && event.keyCode != 8 && event.keyCode != 46) {
          return;
        }

        if ((event.keyCode == 8 || event.keyCode == 46) && this.searchInput) {
          return;
        }
      }

      this.term = this.searchInput;

      if (this.timeOut != null) {
        this.isTyping = false;
        clearTimeout(this.timeOut);
        this.timeOut = null;
      }

      if (!this.isTyping) {
        this.timeOut = setTimeout(this.doSearch, 300);
      }
    },

    /**
     * This is what the <input/> value is set to when you are selecting a suggestion.
     */
    getSuggestionValue: function getSuggestionValue(suggestion) {
      return suggestion.item.name;
    },
    clearState: function clearState() {
      document.getElementsByName("Search")[0].value = '';
      this.autoSuggest.selected = '';
      this.selectOptions = [];
      console.log('clear!');
      this.$refs.SearchExplore.searchInput = '';
      this.autoSuggest.options = [{
        data: []
      }];
    }
  }, "changeSearchBy", function changeSearchBy(event) {
    this.searchByOptions = event.target.value;
    this.clearState();
  }))
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/pages/Dashboard.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/pages/Dashboard.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _components_base_ExploreAutoSuggest__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @/components/base/ExploreAutoSuggest */ "./resources/js/components/base/ExploreAutoSuggest.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'Dashboard',
  components: {
    ExploreAutoSuggest: _components_base_ExploreAutoSuggest__WEBPACK_IMPORTED_MODULE_0__["default"]
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/base/ExploreAutoSuggest.vue?vue&type=style&index=0&lang=css&":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/base/ExploreAutoSuggest.vue?vue&type=style&index=0&lang=css& ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\ninput {\r\n  width: 100%;\r\n  padding: 0.5rem;\n}\ndiv.autosuggest_parent_container ul {\r\n  position: absolute;\r\n  width: 98%;\r\n  color: rgba(30, 39, 46,1.0);\r\n  list-style: none;\r\n  margin: 0;\r\n  padding: 0.5rem 0 .5rem 0;\r\n  background:white;\r\n  z-index: 1;\n}\ndiv.autosuggest-container li {\r\n  margin: 0 0 0 0;\r\n  border-radius: 5px;\r\n  padding: 0.75rem 0 0.75rem 0.75rem;\r\n  display: flex;\r\n  align-items: center;\n}\ndiv.autosuggest-container li:hover {\r\n  cursor: pointer;\n}\n.autosuggest-container {\r\n  display: flex;\r\n  justify-content: center;\r\n  width: 98%;\n}\n#autosuggest { width: 100%; display: block;}\n.autosuggest__results-item--highlighted {\r\n  background-color: rgba(51, 217, 178,0.2);\n}\r\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/pages/Dashboard.vue?vue&type=style&index=0&lang=css&":
/*!****************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/pages/Dashboard.vue?vue&type=style&index=0&lang=css& ***!
  \****************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.multi-level, .item div.item-details, .demo-nav input[type=\"checkbox\"] {\r\n    display: none;\n}\n#menu:checked ~ .multi-level, .item input:checked ~ div.item-details{\r\n    display: block;\n}\r\n/*Func*/\n.multi-level{\r\n    display:block;\n}\r\n/*Arrow*/\n.item-details{\r\n  padding-left:3%;\n}\n.multi-level .arrow {\r\n    width: 12px;\r\n    height: 12px;\r\n    vertical-align: middle;\r\n    float: left;\r\n    z-index: 0;\r\n    margin: 17px 1em 0 2em;\n}\n.item input +  .arrow {\r\n    transform: rotate(-90deg);\r\n    transition: 0.1s;\n}\n.item input:checked +  .arrow {\r\n    transform: rotate(0deg);\r\n    transition: 0.1s;\n}\r\n\r\n\r\n\r\n/*Styles*/\n.multi-level label:hover {\r\n    cursor: pointer;\n}\n.multi-level label {\r\n    width: 100%;\r\n    display: block;\r\n    z-index: 3;\r\n    position: relative;\r\n    font-weight: bold;\n}\n.demo-nav {\r\n    width: 100%;\r\n    background-color: white;\r\n    overflow-x: hidden;\r\n    border-bottom: 1px solid #CFD8DC;\n}\n#nav-icon {\r\n    font-size: 28px;\r\n    line-height: 50px;\r\n    padding-left: 1em;\r\n    color: white;\r\n    background-color: #F44336;\n}\n.demo-nav ul, .demo-nav li, label {\r\n    line-height: 50px;\r\n    margin: 0;\r\n    padding: 0 2em;\r\n    list-style: none;\r\n    text-decoration: none;\r\n    color: #90A4AE;\r\n    font-weight: 100;\r\n    width: 100%;\n}\n.item ul {\r\n    padding: 0 0.25em;\n}\n.demo-nav li a {\r\n    line-height: 50px;\r\n    margin: 0;\r\n    padding: 0 4em;\r\n    list-style: none;\r\n    text-decoration: none;\r\n    color: #90A4AE;\r\n    font-weight: 100;\n}\n.table-cls {\r\n  width: 100vw;\r\n  height: 100vh;\r\n  display: flex;\r\n  flex-direction: column;\n}\n.table-row {\r\n  width: 100%;  \r\n  min-height:4vw;\r\n  display: flex;\r\n  border-bottom: 1px solid #ccc;\n}\n.table-header {\r\n  font-weight: bold;\n}\n.table-row div:first-child {\r\n  width: 10% !important;\n}\n.table-row-column {\r\n  width: 30%;\r\n  height: 100%;\r\n  display: flex;\r\n  align-items: center;\r\n  justify-content: center;\n}\r\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/base/ExploreAutoSuggest.vue?vue&type=style&index=0&lang=css&":
/*!*********************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/base/ExploreAutoSuggest.vue?vue&type=style&index=0&lang=css& ***!
  \*********************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./ExploreAutoSuggest.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/base/ExploreAutoSuggest.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/pages/Dashboard.vue?vue&type=style&index=0&lang=css&":
/*!********************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/pages/Dashboard.vue?vue&type=style&index=0&lang=css& ***!
  \********************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./Dashboard.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/pages/Dashboard.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/base/ExploreAutoSuggest.vue?vue&type=template&id=02945de5&":
/*!**************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/base/ExploreAutoSuggest.vue?vue&type=template&id=02945de5& ***!
  \**************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c(
        "CCard",
        [
          _c("CCardBody", [
            _c("div", { staticClass: "container" }),
            _vm._v(" "),
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col-md-8" }, [
                _c("div", { staticClass: "demo" }, [
                  _c(
                    "div",
                    { staticClass: "autosuggest-container" },
                    [
                      _c("vue-autosuggest", {
                        ref: "SearchExplore",
                        class: "autosuggest_parent_container normal-mode",
                        attrs: {
                          name: "Search",
                          suggestions: _vm.autoSuggest.filteredOptions,
                          "input-props": _vm.autoSuggest.inputProps,
                          limit: _vm.autoSuggest.limit
                        },
                        on: {
                          selected: _vm.autoSuggestOnSelect,
                          input: _vm.autoSuggestOnInputChange,
                          click: _vm.autoSuggestOnSelect,
                          keyup: function($event) {
                            $event.preventDefault()
                            return _vm.search($event)
                          }
                        },
                        scopedSlots: _vm._u([
                          {
                            key: "default",
                            fn: function(ref) {
                              var suggestion = ref.suggestion
                              return [
                                _c(
                                  "span",
                                  { staticClass: "my-suggestion-item" },
                                  [_vm._v(_vm._s(suggestion.item.name))]
                                )
                              ]
                            }
                          }
                        ]),
                        model: {
                          value: _vm.query,
                          callback: function($$v) {
                            _vm.query = $$v
                          },
                          expression: "query"
                        }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _vm.selected
                    ? _c(
                        "div",
                        {
                          staticStyle: { "padding-top": "10px", width: "100%" }
                        },
                        [
                          _vm._v("\r\n                  You have selected "),
                          _c("code", [
                            _vm._v(
                              _vm._s(_vm.selected.name) +
                                ", the " +
                                _vm._s(_vm.selected.race)
                            )
                          ])
                        ]
                      )
                    : _vm._e()
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "col-md-2" }, [
                _c(
                  "select",
                  {
                    staticClass: "form-control",
                    attrs: { name: "SearchBy" },
                    on: { change: _vm.changeSearchBy }
                  },
                  [
                    _c("option", { attrs: { value: "state" } }, [
                      _vm._v("State")
                    ]),
                    _vm._v(" "),
                    _c("option", { attrs: { value: "county" } }, [
                      _vm._v("County")
                    ]),
                    _vm._v(" "),
                    _c("option", { attrs: { value: "city" } }, [
                      _vm._v("City/Place")
                    ])
                  ]
                )
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "col-md-2" }, [
                _c(
                  "button",
                  {
                    staticClass: "btn btn-primary pull-right",
                    on: {
                      click: function($event) {
                        if ($event.target !== $event.currentTarget) {
                          return null
                        }
                        $event.preventDefault()
                        return _vm.exportFile($event)
                      }
                    }
                  },
                  [_vm._v("Export")]
                )
              ])
            ])
          ])
        ],
        1
      ),
      _vm._v(" "),
      _vm.autoSuggest.selected !== ""
        ? _c(
            "CCard",
            [
              _c("CCardBody", [
                _c("div", { staticClass: "row" }, [
                  _c(
                    "div",
                    {
                      staticClass:
                        "col-md-12 font-weight-bold bg-info text-center p-2 text-white"
                    },
                    [_c("h3", [_vm._v(_vm._s(_vm.autoSuggest.selected.name))])]
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "row" }, [
                  _c("div", { staticClass: "col-md-12 demo-nav" }, [
                    _c("div", { staticClass: "multi-level" }, [
                      _vm.dataResponse.sexAge !== ""
                        ? _c("div", { staticClass: "item" }, [
                            _c("input", {
                              attrs: { type: "checkbox", id: "A" }
                            }),
                            _vm._v(" "),
                            _c("img", {
                              staticClass: "arrow",
                              attrs: { src: "img/Arrow.png" }
                            }),
                            _c("label", { attrs: { for: "A" } }, [
                              _vm._v("Sex And Age")
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "row item-details" }, [
                              _c("div", { staticClass: "col-md-12" }, [
                                _c(
                                  "table",
                                  {
                                    staticClass:
                                      "table table-striped table-hover"
                                  },
                                  [
                                    _c("tr", [
                                      _c("td", { attrs: { width: "30%" } }, [
                                        _vm._v("Total Population")
                                      ]),
                                      _vm._v(" "),
                                      _c("td", [
                                        _vm._v(
                                          _vm._s(
                                            _vm.population.over_all_population
                                          )
                                        )
                                      ])
                                    ]),
                                    _vm._v(" "),
                                    _c("tr", [
                                      _c("td", [
                                        _vm._v("Total Male Population")
                                      ]),
                                      _vm._v(" "),
                                      _c("td", [
                                        _vm._v(
                                          _vm._s(_vm.population.over_all_pop_m)
                                        )
                                      ])
                                    ]),
                                    _vm._v(" "),
                                    _c("tr", [
                                      _c("td", [
                                        _vm._v("Total Female Population")
                                      ]),
                                      _vm._v(" "),
                                      _c("td", [
                                        _vm._v(
                                          _vm._s(_vm.population.over_all_pop_f)
                                        )
                                      ])
                                    ])
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "table",
                                  {
                                    staticClass:
                                      "table table-striped table-hover"
                                  },
                                  [
                                    _c("thead", [
                                      _c("tr", [
                                        _c("th", { attrs: { width: "30%" } }, [
                                          _vm._v(" ")
                                        ]),
                                        _vm._v(" "),
                                        _c("th", [_vm._v("Population")]),
                                        _vm._v(" "),
                                        _c("th", [_vm._v("Male")]),
                                        _vm._v(" "),
                                        _c("th", [_vm._v("Female")])
                                      ])
                                    ]),
                                    _vm._v(" "),
                                    _vm._l(_vm.dataResponse.sexAge, function(
                                      age
                                    ) {
                                      return _c("tr", { key: age.id }, [
                                        _c("td", [
                                          _vm._v(_vm._s(age.age_category))
                                        ]),
                                        _vm._v(" "),
                                        _c("td", [
                                          _vm._v(_vm._s(age.total_population))
                                        ]),
                                        _vm._v(" "),
                                        _c("td", [
                                          _vm._v(_vm._s(age.total_pop_m))
                                        ]),
                                        _vm._v(" "),
                                        _c("td", [
                                          _vm._v(_vm._s(age.total_pop_f))
                                        ])
                                      ])
                                    })
                                  ],
                                  2
                                )
                              ])
                            ])
                          ])
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.dataResponse.race !== ""
                        ? _c("div", { staticClass: "item" }, [
                            _c("input", {
                              attrs: { type: "checkbox", id: "B" }
                            }),
                            _vm._v(" "),
                            _c("img", {
                              staticClass: "arrow",
                              attrs: { src: "img/Arrow.png" }
                            }),
                            _c("label", { attrs: { for: "B" } }, [
                              _vm._v("Race")
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "row item-details" }, [
                              _c("div", { staticClass: "col-md-12" }, [
                                _c(
                                  "table",
                                  {
                                    staticClass:
                                      "table table-striped table-hover"
                                  },
                                  _vm._l(_vm.dataResponse.race, function(race) {
                                    return _c("tr", { key: race.id }, [
                                      _c("td", { attrs: { width: "30%" } }, [
                                        _vm._v(_vm._s(race.name))
                                      ]),
                                      _vm._v(" "),
                                      _c("td", [
                                        _vm._v(_vm._s(race.population))
                                      ])
                                    ])
                                  }),
                                  0
                                )
                              ])
                            ])
                          ])
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.dataResponse.bpov !== ""
                        ? _c("div", { staticClass: "item" }, [
                            _c("input", {
                              attrs: { type: "checkbox", id: "C" }
                            }),
                            _vm._v(" "),
                            _c("img", {
                              staticClass: "arrow",
                              attrs: { src: "img/Arrow.png" }
                            }),
                            _c("label", { attrs: { for: "C" } }, [
                              _vm._v("Race Below Poverty Level")
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "row item-details" }, [
                              _c("div", { staticClass: "col-md-12" }, [
                                _c(
                                  "table",
                                  {
                                    staticClass:
                                      "table table-striped table-hover"
                                  },
                                  _vm._l(_vm.dataResponse.bpov, function(
                                    bpov_race
                                  ) {
                                    return _c("tr", { key: bpov_race.id }, [
                                      _c("td", { attrs: { width: "30%" } }, [
                                        _vm._v(_vm._s(bpov_race.name))
                                      ]),
                                      _vm._v(" "),
                                      _c("td", [
                                        _vm._v(_vm._s(bpov_race.population))
                                      ])
                                    ])
                                  }),
                                  0
                                )
                              ])
                            ])
                          ])
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.dataResponse.efield !== ""
                        ? _c("div", { staticClass: "item" }, [
                            _c("input", {
                              attrs: { type: "checkbox", id: "D" }
                            }),
                            _vm._v(" "),
                            _c("img", {
                              staticClass: "arrow",
                              attrs: { src: "img/Arrow.png" }
                            }),
                            _c("label", { attrs: { for: "D" } }, [
                              _vm._v("Race Educational Attainment By Field")
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "row item-details" }, [
                              _c("div", { staticClass: "col-md-12" }, [
                                _c(
                                  "table",
                                  {
                                    staticClass:
                                      "table table-striped table-hover"
                                  },
                                  _vm._l(_vm.dataResponse.efield, function(
                                    field
                                  ) {
                                    return _c("tr", { key: field.id }, [
                                      _c("td", { attrs: { width: "30%" } }, [
                                        _vm._v(_vm._s(field.race))
                                      ]),
                                      _vm._v(" "),
                                      _c("td", { attrs: { width: "30%" } }, [
                                        _vm._v(_vm._s(field.name))
                                      ]),
                                      _vm._v(" "),
                                      _c("td", [
                                        _vm._v(_vm._s(field.population))
                                      ])
                                    ])
                                  }),
                                  0
                                )
                              ])
                            ])
                          ])
                        : _vm._e()
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-md-6 font-weight-bold" })
                ])
              ])
            ],
            1
          )
        : _vm._e()
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/pages/Dashboard.vue?vue&type=template&id=04d4e0ba&":
/*!*************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/pages/Dashboard.vue?vue&type=template&id=04d4e0ba& ***!
  \*************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [_c("ExploreAutoSuggest")], 1)
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/base/ExploreAutoSuggest.vue":
/*!*************************************************************!*\
  !*** ./resources/js/components/base/ExploreAutoSuggest.vue ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ExploreAutoSuggest_vue_vue_type_template_id_02945de5___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ExploreAutoSuggest.vue?vue&type=template&id=02945de5& */ "./resources/js/components/base/ExploreAutoSuggest.vue?vue&type=template&id=02945de5&");
/* harmony import */ var _ExploreAutoSuggest_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ExploreAutoSuggest.vue?vue&type=script&lang=js& */ "./resources/js/components/base/ExploreAutoSuggest.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _ExploreAutoSuggest_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ExploreAutoSuggest.vue?vue&type=style&index=0&lang=css& */ "./resources/js/components/base/ExploreAutoSuggest.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _ExploreAutoSuggest_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ExploreAutoSuggest_vue_vue_type_template_id_02945de5___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ExploreAutoSuggest_vue_vue_type_template_id_02945de5___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/base/ExploreAutoSuggest.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/base/ExploreAutoSuggest.vue?vue&type=script&lang=js&":
/*!**************************************************************************************!*\
  !*** ./resources/js/components/base/ExploreAutoSuggest.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ExploreAutoSuggest_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./ExploreAutoSuggest.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/base/ExploreAutoSuggest.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ExploreAutoSuggest_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/base/ExploreAutoSuggest.vue?vue&type=style&index=0&lang=css&":
/*!**********************************************************************************************!*\
  !*** ./resources/js/components/base/ExploreAutoSuggest.vue?vue&type=style&index=0&lang=css& ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_ExploreAutoSuggest_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./ExploreAutoSuggest.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/base/ExploreAutoSuggest.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_ExploreAutoSuggest_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_ExploreAutoSuggest_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_ExploreAutoSuggest_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_ExploreAutoSuggest_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_ExploreAutoSuggest_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/base/ExploreAutoSuggest.vue?vue&type=template&id=02945de5&":
/*!********************************************************************************************!*\
  !*** ./resources/js/components/base/ExploreAutoSuggest.vue?vue&type=template&id=02945de5& ***!
  \********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ExploreAutoSuggest_vue_vue_type_template_id_02945de5___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./ExploreAutoSuggest.vue?vue&type=template&id=02945de5& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/base/ExploreAutoSuggest.vue?vue&type=template&id=02945de5&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ExploreAutoSuggest_vue_vue_type_template_id_02945de5___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ExploreAutoSuggest_vue_vue_type_template_id_02945de5___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/views/pages/Dashboard.vue":
/*!************************************************!*\
  !*** ./resources/js/views/pages/Dashboard.vue ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Dashboard_vue_vue_type_template_id_04d4e0ba___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Dashboard.vue?vue&type=template&id=04d4e0ba& */ "./resources/js/views/pages/Dashboard.vue?vue&type=template&id=04d4e0ba&");
/* harmony import */ var _Dashboard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Dashboard.vue?vue&type=script&lang=js& */ "./resources/js/views/pages/Dashboard.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Dashboard_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Dashboard.vue?vue&type=style&index=0&lang=css& */ "./resources/js/views/pages/Dashboard.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Dashboard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Dashboard_vue_vue_type_template_id_04d4e0ba___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Dashboard_vue_vue_type_template_id_04d4e0ba___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/pages/Dashboard.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/pages/Dashboard.vue?vue&type=script&lang=js&":
/*!*************************************************************************!*\
  !*** ./resources/js/views/pages/Dashboard.vue?vue&type=script&lang=js& ***!
  \*************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Dashboard.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/pages/Dashboard.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/pages/Dashboard.vue?vue&type=style&index=0&lang=css&":
/*!*********************************************************************************!*\
  !*** ./resources/js/views/pages/Dashboard.vue?vue&type=style&index=0&lang=css& ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./Dashboard.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/pages/Dashboard.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/views/pages/Dashboard.vue?vue&type=template&id=04d4e0ba&":
/*!*******************************************************************************!*\
  !*** ./resources/js/views/pages/Dashboard.vue?vue&type=template&id=04d4e0ba& ***!
  \*******************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_template_id_04d4e0ba___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Dashboard.vue?vue&type=template&id=04d4e0ba& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/pages/Dashboard.vue?vue&type=template&id=04d4e0ba&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_template_id_04d4e0ba___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_template_id_04d4e0ba___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);