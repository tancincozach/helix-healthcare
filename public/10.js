(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[10],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/base/Autosuggest.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/base/Autosuggest.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_autosuggest__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-autosuggest */ "./node_modules/vue-autosuggest/dist/vue-autosuggest.esm.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'AutoSuggest',
  props: {
    searchOptions: {
      type: Array,
      required: true
    },
    searchPlaceholder: {
      type: 'String',
      required: false
    },
    searchSuggestLimit: {
      type: Number,
      required: false
    }
  },
  components: {
    VueAutosuggest: vue_autosuggest__WEBPACK_IMPORTED_MODULE_0__["VueAutosuggest"]
  },
  data: function data() {
    return {
      searchInput: '',
      autoSuggest: {
        selected: '',
        filteredOptions: [{
          data: []
        }],
        options: [{
          data: []
        }],
        inputProps: {
          id: 'autosuggest__input',
          onInputChange: this.autoSuggestOnInputChange,
          placeholder: 'Enter keyword...',
          onclick: 'this.select();'
        },
        limit: 8
      },
      query: "",
      selected: ""
    };
  },
  computed: {
    filteredOptions: function filteredOptions() {
      var _this = this;

      return [{
        data: this.suggestions[0].data.filter(function (option) {
          return option.name.toLowerCase().indexOf(_this.query.toLowerCase()) > -1;
        })
      }];
    }
  },
  methods: {
    autoSuggestChange: function autoSuggestChange() {},

    /** Auto-suggest function **/
    autoSuggestOnSelect: function autoSuggestOnSelect(option) {
      this.autoSuggest.selected = option ? option.item : '';

      if (this.autoSuggest.selected) {
        this.searchInput = this.autoSuggest.selected;
        this.search();
      }
    },

    /** Auto-suggest function **/
    autoSuggestOnInputChange: function autoSuggestOnInputChange(text) {
      if (text === '' || text === undefined) {
        this.autoSuggest.filteredOptions = [{
          data: []
        }];
        this.searchInput = '';
        this.search();
        return;
      }

      this.searchInput = text;

      if (this.autoSuggest.options[0].data) {
        var filteredData = this.autoSuggest.options[0].data.filter(function (item) {
          return item.name.toLowerCase().indexOf(text.toLowerCase()) > -1;
        }).slice(0, this.autoSuggest.limit);
        this.autoSuggest.filteredOptions = [{
          data: filteredData
        }];
      } else {}
    },
    doFilter: function doFilter(value) {
      this.sort = false;
      this.$emit('displayFilter', value);
    },
    doSort: function doSort(value) {
      if (this.sort) this.$emit('sort', value);
      this.sort = true;
    },
    doSearch: function doSearch() {
      // set default event to emit
      var emit = 'search'; //emit

      this.$emit(emit, this.term);
      /**
       * Always set the current page to 1 when making queries.
       * This is to avoid rendering issue on the datatable and pagination.
       */

      this.$emit('page', 1);
    },
    search: function search(event) {
      this.autoSuggest.options = [{
        data: this.searchOptions
      }]; // this.autoSuggest.inputProps.placeholder = this.searchPlaceholder;

      this.autoSuggest.limit = this.searchSuggestLimit;

      if (event) {
        if (event.keyCode != 13 && event.keyCode != 8 && event.keyCode != 46) {
          return;
        }

        if ((event.keyCode == 8 || event.keyCode == 46) && this.searchInput) {
          return;
        }
      }

      this.term = this.searchInput;

      if (this.timeOut != null) {
        this.isTyping = false;
        clearTimeout(this.timeOut);
        this.timeOut = null;
      }

      if (!this.isTyping) {
        this.timeOut = setTimeout(this.doSearch, 300);
      }
    },
    searchAsync: function searchAsync(event) {
      this.autoSuggest.options = [{
        data: this.searchOptions
      }]; // this.autoSuggest.inputProps.placeholder = this.searchPlaceholder;

      this.autoSuggest.limit = this.searchSuggestLimit;

      if (event) {
        if (event.keyCode != 13 && event.keyCode != 8 && event.keyCode != 46) {
          return;
        }

        if ((event.keyCode == 8 || event.keyCode == 46) && this.searchInput) {
          return;
        }
      }

      this.term = this.searchInput;

      if (this.timeOut != null) {
        this.isTyping = false;
        clearTimeout(this.timeOut);
        this.timeOut = null;
      }

      if (!this.isTyping) {
        this.timeOut = setTimeout(this.doSearch, 300);
      }
    },
    clearAdvancedSearch: function clearAdvancedSearch() {},
    clickHandler: function clickHandler(item) {// event fired when clicking on the input
    },
    onSelected: function onSelected(item) {
      this.selected = item.item;
    },
    onInputChange: function onInputChange(text) {
      // event fired when the input changes
      console.log(text);
    },

    /**
     * This is what the <input/> value is set to when you are selecting a suggestion.
     */
    getSuggestionValue: function getSuggestionValue(suggestion) {
      return suggestion.item.name;
    },
    focusMe: function focusMe(e) {
      console.log(e); // FocusEvent
    },
    created: function created() {}
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/pages/States.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/pages/States.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _common_mixin_DataTableMixin__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/common/mixin/DataTableMixin */ "./resources/js/common/mixin/DataTableMixin.js");
/* harmony import */ var _components_base_Autosuggest__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/components/base/Autosuggest */ "./resources/js/components/base/Autosuggest.vue");
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");


function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'States',
  data: function data() {
    var sortOrder = {};
    var sortKey = 'name';
    var columns = [{
      key: 'id',
      label: 'ID'
    }, {
      key: 'censusStateID'
    }, {
      key: 'name',
      label: 'Name',
      sortKey: 'name',
      _classes: 'font-weight-bold'
    }, // { key: 'currAcsPop', label: 'Population',sortKey:'current_acs_population',_classes: 'font-weight-bold' },
    {
      key: 'stateCode',
      label: 'Code'
    }, {
      key: 'lat',
      label: 'lat'
    }, {
      key: 'lng',
      label: 'lng'
    }, {
      key: 'ne_lat',
      label: 'NE Lat'
    }, {
      key: 'ne_lng',
      label: 'NE Lng'
    }, {
      key: 'sw_lat',
      label: 'SW Lat'
    }, {
      key: 'sw_lng',
      label: 'SW Lng'
    }];
    columns.forEach(function (col) {
      sortOrder[col.sortKey] = 'asc';
    });
    return {
      // placeHolder:'Search Keyword',
      page: 1,
      selectOptions: [],
      loading: false,
      items: [],
      fields: columns,
      caption: 'States',
      hover: true,
      striped: true,
      bordered: true,
      dark: false,
      sortKey: sortKey,
      sortOrder: sortOrder,
      paginationVar: {},
      total_pages: 0,
      limit: 10,
      open: false,
      data: {
        columns: columns,
        rows: []
      },
      states: [],
      activePage: 1
    };
  },
  created: function created() {
    var _this2 = this;

    return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
      var params, selectOptions;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _this2.setSorting("".concat(_this2.sortKey, "|").concat(_this2.sortOrder[_this2.sortKey]));

              _this2.setPaginationLimit(15);

              _this2.loading = true;
              _context.next = 5;
              return _this2.getStates({
                query: _.merge(_this2.getParams())
              });

            case 5:
              _this2.loading = false;
              _this2.items = _this2.state_info;
              _this2.paginationVar = _this2.meta.pagination;

              _this2.setPagination(_this2.meta.pagination);

              params = _this2.getParams();

              _this2.addClickEventOnPagination();

              selectOptions = [];

              _.map(_this2.state_info, function (result) {
                selectOptions.unshift({
                  id: result.censusStateID,
                  name: result.name
                });
              });

              _this2.selectOptions = selectOptions;

            case 14:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }))();
  },
  computed: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_3__["mapGetters"])({
    meta: 'states/meta',
    categories: 'states/formatted',
    state_info: 'states/data'
  })),
  methods: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_3__["mapActions"])({
    getStates: 'states/getStates'
  }), {
    autoSuggestSearch: function autoSuggestSearch(value) {
      console.log(value);
    },
    addClickEventOnPagination: function addClickEventOnPagination() {
      var _this = this;

      var pages = document.getElementsByClassName("page-item");
      Array.from(pages).forEach(function (element) {
        _this.addClickEvent(element);
      });
    },
    addClickEvent: function addClickEvent(element) {
      var _this = this;

      element.addEventListener('click', function () {
        _this.changePage();
      });
    },
    changePage: function changePage() {
      var _this3 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        var params;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                params = _this3.getParams();
                params.page = document.getElementById('page_num').value;
                _this3.loading = true;
                _context2.next = 5;
                return _this3.getStates({
                  query: _.merge(params)
                });

              case 5:
                _this3.loading = false;
                _this3.items = _this3.state_info;
                _this3.paginationVar = _this3.meta.pagination;

                _this3.setPagination(_this3.meta.pagination);

              case 9:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }))();
    }
  }),
  components: {
    AutoSuggest: _components_base_Autosuggest__WEBPACK_IMPORTED_MODULE_2__["default"]
  },
  mixins: [_common_mixin_DataTableMixin__WEBPACK_IMPORTED_MODULE_1__["default"]]
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/base/Autosuggest.vue?vue&type=style&index=0&lang=css&":
/*!**********************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/base/Autosuggest.vue?vue&type=style&index=0&lang=css& ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\ninput {\r\n  width: 100%;\r\n  padding: 0.5rem;\n}\ndiv.autosuggest_parent_container ul {\r\n  position: absolute;\r\n  width: 98%;\r\n  color: rgba(30, 39, 46,1.0);\r\n  list-style: none;\r\n  margin: 0;\r\n  padding: 0.5rem 0 .5rem 0;\r\n  background:white;\r\n  z-index: 1;\n}\ndiv.autosuggest-container li {\r\n  margin: 0 0 0 0;\r\n  border-radius: 5px;\r\n  padding: 0.75rem 0 0.75rem 0.75rem;\r\n  display: flex;\r\n  align-items: center;\n}\ndiv.autosuggest-container li:hover {\r\n  cursor: pointer;\n}\n.autosuggest-container {\r\n  display: flex;\r\n  justify-content: center;\r\n  width: 98%;\n}\n#autosuggest { width: 100%; display: block;}\n.autosuggest__results-item--highlighted {\r\n  background-color: rgba(51, 217, 178,0.2);\n}\r\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/base/Autosuggest.vue?vue&type=style&index=0&lang=css&":
/*!**************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/base/Autosuggest.vue?vue&type=style&index=0&lang=css& ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./Autosuggest.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/base/Autosuggest.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/base/Autosuggest.vue?vue&type=template&id=72ac3362&":
/*!*******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/base/Autosuggest.vue?vue&type=template&id=72ac3362& ***!
  \*******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "demo" }, [
    _c(
      "div",
      { staticClass: "autosuggest-container" },
      [
        _c("vue-autosuggest", {
          class: "autosuggest_parent_container normal-mode",
          attrs: {
            suggestions: _vm.autoSuggest.filteredOptions,
            "input-props": _vm.autoSuggest.inputProps,
            limit: _vm.autoSuggest.limit
          },
          on: {
            selected: _vm.autoSuggestOnSelect,
            input: _vm.autoSuggestOnInputChange,
            click: _vm.autoSuggestOnSelect,
            change: _vm.autoSuggestChange,
            keyup: function($event) {
              $event.preventDefault()
              return _vm.search($event)
            }
          },
          scopedSlots: _vm._u([
            {
              key: "default",
              fn: function(ref) {
                var suggestion = ref.suggestion
                return [
                  _c("span", { staticClass: "my-suggestion-item" }, [
                    _vm._v(_vm._s(suggestion.item.name))
                  ])
                ]
              }
            }
          ]),
          model: {
            value: _vm.query,
            callback: function($$v) {
              _vm.query = $$v
            },
            expression: "query"
          }
        })
      ],
      1
    ),
    _vm._v(" "),
    _vm.selected
      ? _c("div", { staticStyle: { "padding-top": "10px", width: "100%" } }, [
          _vm._v("\n    You have selected "),
          _c("code", [
            _vm._v(
              _vm._s(_vm.selected.name) + ", the " + _vm._s(_vm.selected.race)
            )
          ])
        ])
      : _vm._e()
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/pages/States.vue?vue&type=template&id=28aa9ccc&":
/*!**********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/pages/States.vue?vue&type=template&id=28aa9ccc& ***!
  \**********************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "CCard",
    [
      _c(
        "CCardHeader",
        [
          _vm._t("header", [
            _c("CIcon", { attrs: { name: "cil-grid" } }),
            _vm._v(" " + _vm._s(_vm.caption) + "\n    ")
          ])
        ],
        2
      ),
      _vm._v(" "),
      _c(
        "CCardBody",
        [
          _c("CDataTable", {
            attrs: {
              "items-per-page-select": "",
              hover: "",
              striped: "",
              sorter: "",
              items: _vm.items,
              fields: _vm.fields,
              "items-per-page": 15,
              "active-page": _vm.activePage,
              pagination: { doubleArrows: true },
              loading: _vm.loading
            },
            scopedSlots: _vm._u([
              {
                key: "status",
                fn: function(data) {
                  return [
                    _c("td", [_vm._v(_vm._s(data.item.id))]),
                    _vm._v(" "),
                    _c("td", [_vm._v(_vm._s(data.item.censusStateID))]),
                    _vm._v(" "),
                    _c("td", [_vm._v(_vm._s(data.item.name))]),
                    _vm._v(" "),
                    _c("td", [_vm._v(_vm._s(data.item.code))]),
                    _vm._v(" "),
                    _c("td", [_vm._v(_vm._s(data.item.lat))]),
                    _vm._v(" "),
                    _c("td", [_vm._v(_vm._s(data.item.lng))]),
                    _vm._v(" "),
                    _c("td", [_vm._v(_vm._s(data.item.ne_lat))]),
                    _vm._v(" "),
                    _c("td", [_vm._v(_vm._s(data.item.ne_lng))]),
                    _vm._v(" "),
                    _c("td", [_vm._v(_vm._s(data.item.sw_lat))]),
                    _vm._v(" "),
                    _c("td", [_vm._v(_vm._s(data.item.sw_lng))]),
                    _vm._v(">\n      ")
                  ]
                }
              }
            ])
          }),
          _vm._v(" "),
          [
            _c(
              "div",
              [
                _c("input", {
                  attrs: { type: "hidden", id: "page_num" },
                  domProps: { value: _vm.page }
                }),
                _vm._v(" "),
                _c("CPagination", {
                  attrs: {
                    activePage: _vm.page,
                    pages: _vm.paginationVar.total_pages
                  },
                  on: {
                    "update:activePage": function($event) {
                      _vm.page = $event
                    },
                    "update:active-page": function($event) {
                      _vm.page = $event
                    }
                  }
                })
              ],
              1
            )
          ]
        ],
        2
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/common/mixin/DataTableMixin.js":
/*!*****************************************************!*\
  !*** ./resources/js/common/mixin/DataTableMixin.js ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      query: {
        filter: null,
        sorting: null,
        search: null,
        searchBy: null,
        pagination: {
          currentPage: 1,
          limit: 20 //limit is the same as perPage - variable:  this.query.pagination.limit

        }
      },
      config: {
        pagination: {
          lastPage: '',
          currentPage: 1,
          pages: 1,
          total: 0,
          perPage: 20,
          //perPage is the same as limit - variable:  this.config.pagination.perPage
          nextPageUrl: null,
          prevPageUrl: null,
          from: 0,
          to: 0
        }
      }
    };
  },
  computed: {
    paginationFrom: function paginationFrom() {
      return (this.config.pagination.currentPage - 1) * this.config.pagination.perPage + 1;
    },
    paginationPages: function paginationPages() {
      return Math.ceil(this.config.pagination.total / this.config.pagination.perPage);
    },
    paginationTo: function paginationTo() {
      var to = this.config.pagination.from * this.config.pagination.perPage;
      return to <= this.config.pagination.total ? to : this.config.pagination.total;
    }
  },
  methods: {
    alternateBgColor: function alternateBgColor(index) {
      return index % 2 == 0 ? 'even' : 'odd';
    },
    setPagination: function setPagination(data) {
      if (data) {
        this.config.pagination.lastPage = data.last_page;
        this.config.pagination.currentPage = data.current_page;
        this.config.pagination.total = data.total;
        this.config.pagination.perPage = data.per_page; //perPage is the same as limit

        this.config.pagination.nextPageUrl = data.links.next;
        this.config.pagination.prevPageUrl = data.links.previous;
        this.config.pagination.from = this.paginationFrom;
        this.config.pagination.to = this.paginationTo;
        this.config.pagination.pages = this.paginationPages;
        this.query.pagination.currentPage = data.current_page;
      }
    },
    setFilter: function setFilter(filters) {
      this.query.filter = filters;
    },
    setSorting: function setSorting(orderBy) {
      this.query.sorting = orderBy;
    },
    setSearch: function setSearch(searches) {
      this.query.search = searches;
    },
    setSearchBy: function setSearchBy(searchBy) {
      this.query.searchBy = searchBy;
    },
    setPaginationLimit: function setPaginationLimit(limit) {
      //limit is the same as perPage
      this.query.pagination.limit = limit;
      this.query.pagination.currentPage = Math.ceil(this.config.pagination.total / limit) < this.query.pagination.currentPage ? Math.ceil(this.config.pagination.total / limit) : this.query.pagination.currentPage;
    },
    getParams: function getParams() {
      var params = {
        take: this.query.pagination.limit,
        page: this.query.pagination.currentPage
      }; // see if we have sorting

      if (this.query.sorting) {
        params['orderBy'] = this.query.sorting;
      } // see if we have filters


      if (this.query.filter) {
        params['filters'] = this.query.filter;
      } // see if we have searches


      if (this.query.search) {
        params['search'] = this.query.search;
      } // see if we have search by options


      if (this.query.searchBy) {
        params['searchBy'] = this.query.searchBy;
      }

      return params;
    },
    gotoPage: function gotoPage(page) {
      if (page == 'prev') {
        // if previous page
        this.query.pagination.currentPage -= 1;
      } else if (page == 'next') {
        // if next page
        this.query.pagination.currentPage += 1;
      } else {
        // otherwise, go to page as specicifed
        this.query.pagination.currentPage = page;
      }
    },

    /**
     * Insert a div with width=100%, height with specified after the element that being represented by the selector
     * It's like, insert a new div that element (selector) with width=100% and height specified.
     * @param selector
     * @param height
     */
    insertSpacer: function insertSpacer(selector, height) {
      var _arguments = arguments;
      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var className, $, $newDiv;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                className = _arguments.length > 2 && _arguments[2] !== undefined ? _arguments[2] : 'spacer';
                $ = jQuery;
                $newDiv = $("<div>", {
                  "class": className
                });
                $newDiv.css({
                  width: '100%',
                  height: height
                });
                $(selector).after($newDiv);

              case 5:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }))();
    },
    freezeElement: function freezeElement(selector) {
      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        var $;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                $ = jQuery;
                $(selector).css({
                  position: 'fixed'
                });

              case 2:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }))();
    },
    insertStyle: function insertStyle(selector, css) {
      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                $(selector).css(css);

              case 1:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3);
      }))();
    }
  }
});

/***/ }),

/***/ "./resources/js/components/base/Autosuggest.vue":
/*!******************************************************!*\
  !*** ./resources/js/components/base/Autosuggest.vue ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Autosuggest_vue_vue_type_template_id_72ac3362___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Autosuggest.vue?vue&type=template&id=72ac3362& */ "./resources/js/components/base/Autosuggest.vue?vue&type=template&id=72ac3362&");
/* harmony import */ var _Autosuggest_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Autosuggest.vue?vue&type=script&lang=js& */ "./resources/js/components/base/Autosuggest.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Autosuggest_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Autosuggest.vue?vue&type=style&index=0&lang=css& */ "./resources/js/components/base/Autosuggest.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Autosuggest_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Autosuggest_vue_vue_type_template_id_72ac3362___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Autosuggest_vue_vue_type_template_id_72ac3362___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/base/Autosuggest.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/base/Autosuggest.vue?vue&type=script&lang=js&":
/*!*******************************************************************************!*\
  !*** ./resources/js/components/base/Autosuggest.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Autosuggest_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Autosuggest.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/base/Autosuggest.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Autosuggest_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/base/Autosuggest.vue?vue&type=style&index=0&lang=css&":
/*!***************************************************************************************!*\
  !*** ./resources/js/components/base/Autosuggest.vue?vue&type=style&index=0&lang=css& ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Autosuggest_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./Autosuggest.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/base/Autosuggest.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Autosuggest_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Autosuggest_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Autosuggest_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Autosuggest_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Autosuggest_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/base/Autosuggest.vue?vue&type=template&id=72ac3362&":
/*!*************************************************************************************!*\
  !*** ./resources/js/components/base/Autosuggest.vue?vue&type=template&id=72ac3362& ***!
  \*************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Autosuggest_vue_vue_type_template_id_72ac3362___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Autosuggest.vue?vue&type=template&id=72ac3362& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/base/Autosuggest.vue?vue&type=template&id=72ac3362&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Autosuggest_vue_vue_type_template_id_72ac3362___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Autosuggest_vue_vue_type_template_id_72ac3362___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/views/pages/States.vue":
/*!*********************************************!*\
  !*** ./resources/js/views/pages/States.vue ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _States_vue_vue_type_template_id_28aa9ccc___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./States.vue?vue&type=template&id=28aa9ccc& */ "./resources/js/views/pages/States.vue?vue&type=template&id=28aa9ccc&");
/* harmony import */ var _States_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./States.vue?vue&type=script&lang=js& */ "./resources/js/views/pages/States.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _States_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _States_vue_vue_type_template_id_28aa9ccc___WEBPACK_IMPORTED_MODULE_0__["render"],
  _States_vue_vue_type_template_id_28aa9ccc___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/pages/States.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/pages/States.vue?vue&type=script&lang=js&":
/*!**********************************************************************!*\
  !*** ./resources/js/views/pages/States.vue?vue&type=script&lang=js& ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_States_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./States.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/pages/States.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_States_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/pages/States.vue?vue&type=template&id=28aa9ccc&":
/*!****************************************************************************!*\
  !*** ./resources/js/views/pages/States.vue?vue&type=template&id=28aa9ccc& ***!
  \****************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_States_vue_vue_type_template_id_28aa9ccc___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./States.vue?vue&type=template&id=28aa9ccc& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/pages/States.vue?vue&type=template&id=28aa9ccc&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_States_vue_vue_type_template_id_28aa9ccc___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_States_vue_vue_type_template_id_28aa9ccc___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);