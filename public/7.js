(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[7],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/pages/PopulationByAge.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/pages/PopulationByAge.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _views_pages_PopulationByAgeState__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @/views/pages/PopulationByAgeState */ "./resources/js/views/pages/PopulationByAgeState.vue");
/* harmony import */ var _views_pages_PopulationByAgeCity__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/views/pages/PopulationByAgeCity */ "./resources/js/views/pages/PopulationByAgeCity.vue");
/* harmony import */ var _views_pages_PopulationByAgeCounty__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/views/pages/PopulationByAgeCounty */ "./resources/js/views/pages/PopulationByAgeCounty.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'PopulationByAge',
  components: {
    PopulationByAgeState: _views_pages_PopulationByAgeState__WEBPACK_IMPORTED_MODULE_0__["default"],
    PopulationByAgeCity: _views_pages_PopulationByAgeCity__WEBPACK_IMPORTED_MODULE_1__["default"],
    PopulationByAgeCounty: _views_pages_PopulationByAgeCounty__WEBPACK_IMPORTED_MODULE_2__["default"]
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/pages/PopulationByAgeCity.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/pages/PopulationByAgeCity.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _common_mixin_DataTableMixin__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/common/mixin/DataTableMixin */ "./resources/js/common/mixin/DataTableMixin.js");
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");


function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'PopAgeByCity',
  data: function data() {
    var sortOrder = {};
    var sortKey = 'city';
    var columns = [{
      key: 'city',
      label: 'City',
      _classes: 'font-weight-bold',
      sortKey: 'city'
    }, {
      key: 'age_category',
      label: 'Age Category',
      _classes: 'font-weight-bold'
    }, // { key: 'total_pop_m', label: 'Male', _classes: 'font-weight-bold'},
    // { key: 'total_pop_f', label: 'Female', _classes: 'font-weight-bold'},
    {
      key: 'total_population',
      label: 'Population',
      _classes: 'font-weight-bold'
    }];
    columns.forEach(function (col) {
      sortOrder[col.sortKey] = 'asc';
    });
    return {
      page: 1,
      pagesLinks: [],
      loading: false,
      items: [],
      fields: columns,
      caption: 'City',
      hover: true,
      striped: true,
      bordered: true,
      dark: false,
      sortKey: sortKey,
      sortOrder: sortOrder,
      paginationVar: {},
      total_pages: 0,
      open: false,
      data: {
        columns: columns,
        rows: []
      },
      states: [],
      activePage: 1
    };
  },
  created: function created() {
    var _this2 = this;

    return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
      var params;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _this2.setSorting("".concat(_this2.sortKey, "|").concat(_this2.sortOrder[_this2.sortKey]));

              _this2.setPaginationLimit(15);

              _this2.loading = true;
              _context.next = 5;
              return _this2.getPopByAgeCounties({
                query: _.merge(_this2.getParams())
              });

            case 5:
              _this2.loading = false;
              _this2.items = _this2.city_info;
              _this2.paginationVar = _this2.meta.pagination;

              _this2.setPagination(_this2.meta.pagination);

              params = _this2.getParams();

              _this2.addClickEventOnPagination();

            case 11:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }))();
  },
  computed: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_2__["mapGetters"])({
    meta: 'pop_age_cities/meta',
    categories: 'pop_age_cities/formatted',
    city_info: 'pop_age_cities/data'
  })),
  methods: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_2__["mapActions"])({
    getPopByAgeCities: 'pop_age_cities/getPopByAgeCities'
  }), {
    addClickEventOnPagination: function addClickEventOnPagination() {
      var _this = this;

      var pages = document.getElementsByClassName("page-item");
      Array.from(pages).forEach(function (element) {
        _this.addClickEvent(element);
      });
    },
    addClickEvent: function addClickEvent(element) {
      var _this = this;

      element.addEventListener('click', function () {
        _this.changePage();
      });
    },
    changePage: function changePage() {
      var _this3 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        var params;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                params = _this3.getParams();
                params.page = document.getElementById('page_num_county').value;
                _this3.loading = true;
                _context2.next = 5;
                return _this3.getPopByAgeCities({
                  query: _.merge(params)
                });

              case 5:
                _this3.loading = false;
                _this3.items = _this3.city_info;
                _this3.paginationVar = _this3.meta.pagination;

                _this3.setPagination(_this3.meta.pagination);

              case 9:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }))();
    }
  }),
  mixins: [_common_mixin_DataTableMixin__WEBPACK_IMPORTED_MODULE_1__["default"]]
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/pages/PopulationByAgeCounty.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/pages/PopulationByAgeCounty.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _common_mixin_DataTableMixin__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/common/mixin/DataTableMixin */ "./resources/js/common/mixin/DataTableMixin.js");
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");


function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'PopAgeByCounty',
  data: function data() {
    var sortOrder = {};
    var sortKey = 'county';
    var columns = [{
      key: 'county',
      label: 'County',
      _classes: 'font-weight-bold',
      sortKey: 'county'
    }, {
      key: 'age_category',
      label: 'Age Category',
      _classes: 'font-weight-bold'
    }, // { key: 'total_pop_m', label: 'Male', _classes: 'font-weight-bold'},
    // { key: 'total_pop_f', label: 'Female', _classes: 'font-weight-bold'},
    {
      key: 'total_population',
      label: 'Population',
      _classes: 'font-weight-bold'
    }];
    columns.forEach(function (col) {
      sortOrder[col.sortKey] = 'asc';
    });
    return {
      page: 1,
      pagesLinks: [],
      loading: false,
      items: [],
      fields: columns,
      caption: 'County',
      hover: true,
      striped: true,
      bordered: true,
      dark: false,
      sortKey: sortKey,
      sortOrder: sortOrder,
      paginationVar: {},
      total_pages: 0,
      open: false,
      data: {
        columns: columns,
        rows: []
      },
      states: [],
      activePage: 1
    };
  },
  created: function created() {
    var _this2 = this;

    return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
      var params;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _this2.setSorting("".concat(_this2.sortKey, "|").concat(_this2.sortOrder[_this2.sortKey]));

              _this2.setPaginationLimit(15);

              _this2.loading = true;
              _context.next = 5;
              return _this2.getPopByAgeCounties({
                query: _.merge(_this2.getParams())
              });

            case 5:
              _this2.loading = false;
              _this2.items = _this2.county_info;
              _this2.paginationVar = _this2.meta.pagination;

              _this2.setPagination(_this2.meta.pagination);

              params = _this2.getParams();

              _this2.addClickEventOnPagination();

            case 11:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }))();
  },
  computed: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_2__["mapGetters"])({
    meta: 'pop_age_counties/meta',
    county_info: 'pop_age_counties/data'
  })),
  methods: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_2__["mapActions"])({
    getPopByAgeCounties: 'pop_age_counties/getPopByAgeCounties'
  }), {
    addClickEventOnPagination: function addClickEventOnPagination() {
      var _this = this;

      var pages = document.getElementsByClassName("page-item");
      Array.from(pages).forEach(function (element) {
        _this.addClickEvent(element);
      });
    },
    addClickEvent: function addClickEvent(element) {
      var _this = this;

      element.addEventListener('click', function () {
        _this.changePage();
      });
    },
    changePage: function changePage() {
      var _this3 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        var params;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                params = _this3.getParams();
                params.page = document.getElementById('page_county_num').value;
                _this3.loading = true;
                _context2.next = 5;
                return _this3.getPopByAgeCounties({
                  query: _.merge(params)
                });

              case 5:
                _this3.loading = false;
                _this3.items = _this3.county_info;
                _this3.paginationVar = _this3.meta.pagination;

                _this3.setPagination(_this3.meta.pagination);

              case 9:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }))();
    }
  }),
  mixins: [_common_mixin_DataTableMixin__WEBPACK_IMPORTED_MODULE_1__["default"]]
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/pages/PopulationByAgeState.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/pages/PopulationByAgeState.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _common_mixin_DataTableMixin__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/common/mixin/DataTableMixin */ "./resources/js/common/mixin/DataTableMixin.js");
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");


function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'PopAgeByState',
  data: function data() {
    var sortOrder = {};
    var sortKey = 'state';
    var columns = [{
      key: 'state',
      label: 'State',
      _classes: 'font-weight-bold',
      sortKey: 'state'
    }, {
      key: 'age_category',
      label: 'Age Category',
      _classes: 'font-weight-bold'
    }, // { key: 'total_pop_m', label: 'Male', _classes: 'font-weight-bold'},
    // { key: 'total_pop_f', label: 'Female', _classes: 'font-weight-bold'},
    {
      key: 'total_population',
      label: 'Population',
      _classes: 'font-weight-bold'
    }];
    columns.forEach(function (col) {
      sortOrder[col.sortKey] = 'asc';
    });
    return {
      page: 1,
      pagesLinks: [],
      loading: false,
      items: [],
      fields: columns,
      caption: 'State',
      hover: true,
      striped: true,
      bordered: true,
      dark: false,
      sortKey: sortKey,
      sortOrder: sortOrder,
      paginationVar: {},
      total_pages: 0,
      open: false,
      data: {
        columns: columns,
        rows: []
      },
      states: [],
      activePage: 1
    };
  },
  created: function created() {
    var _this2 = this;

    return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
      var params;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _this2.setSorting("".concat(_this2.sortKey, "|").concat(_this2.sortOrder[_this2.sortKey]));

              _this2.setPaginationLimit(15);

              _this2.loading = true;
              _context.next = 5;
              return _this2.getPopByAgeStates({
                query: _.merge(_this2.getParams())
              });

            case 5:
              _this2.loading = false;
              _this2.items = _this2.state_info;
              _this2.paginationVar = _this2.meta.pagination;

              _this2.setPagination(_this2.meta.pagination);

              params = _this2.getParams();

              _this2.addClickEventOnPagination();

            case 11:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }))();
  },
  computed: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_2__["mapGetters"])({
    meta: 'pop_age_states/meta',
    categories: 'pop_age_states/formatted',
    state_info: 'pop_age_states/data'
  })),
  methods: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_2__["mapActions"])({
    getPopByAgeStates: 'pop_age_states/getPopByAgeStates'
  }), {
    addClickEventOnPagination: function addClickEventOnPagination() {
      var _this = this;

      var pages = document.getElementsByClassName("page-item");
      Array.from(pages).forEach(function (element) {
        _this.addClickEvent(element);
      });
    },
    addClickEvent: function addClickEvent(element) {
      var _this = this;

      element.addEventListener('click', function () {
        _this.changePage();
      });
    },
    changePage: function changePage() {
      var _this3 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        var params;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                params = _this3.getParams();
                params.page = document.getElementById('page_num').value;
                _this3.loading = true;
                _context2.next = 5;
                return _this3.getPopByAgeStates({
                  query: _.merge(params)
                });

              case 5:
                _this3.loading = false;
                _this3.items = _this3.state_info;
                _this3.paginationVar = _this3.meta.pagination;

                _this3.setPagination(_this3.meta.pagination);

              case 9:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }))();
    }
  }),
  mixins: [_common_mixin_DataTableMixin__WEBPACK_IMPORTED_MODULE_1__["default"]]
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/pages/PopulationByAge.vue?vue&type=template&id=95cf83fe&":
/*!*******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/pages/PopulationByAge.vue?vue&type=template&id=95cf83fe& ***!
  \*******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "CCard",
    [
      _c(
        "CCardBody",
        [
          _c(
            "CTabs",
            { attrs: { variant: "pills", "active-tab": 0 } },
            [
              _c(
                "CTab",
                { attrs: { title: "State" } },
                [_c("PopulationByAgeState")],
                1
              ),
              _vm._v(" "),
              _c(
                "CTab",
                { attrs: { title: "County" } },
                [_c("PopulationByAgeCounty")],
                1
              ),
              _vm._v(" "),
              _c(
                "CTab",
                { attrs: { title: "City" } },
                [_c("PopulationByAgeCity")],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/pages/PopulationByAgeCity.vue?vue&type=template&id=afca9ce8&":
/*!***********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/pages/PopulationByAgeCity.vue?vue&type=template&id=afca9ce8& ***!
  \***********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "CCard",
    [
      _c(
        "CCardBody",
        [
          _c("CDataTable", {
            attrs: {
              "items-per-page-select": "",
              hover: "",
              striped: "",
              sorter: "",
              items: _vm.items,
              fields: _vm.fields,
              "items-per-page": 15,
              "active-page": _vm.activePage,
              pagination: { doubleArrows: false },
              loading: _vm.loading
            },
            scopedSlots: _vm._u([
              {
                key: "status",
                fn: function(data) {
                  return [
                    _c("td", [_vm._v(_vm._s(data.item.city))]),
                    _vm._v(" "),
                    _c("td", [_vm._v(_vm._s(data.item.age_category))]),
                    _vm._v(" "),
                    _c("td", [_vm._v(_vm._s(data.item.total_population))])
                  ]
                }
              }
            ])
          }),
          _vm._v(" "),
          [
            _c(
              "div",
              [
                _c("input", {
                  attrs: { type: "hidden", id: "page_num_county" },
                  domProps: { value: _vm.page }
                }),
                _vm._v(" "),
                _c("CPagination", {
                  attrs: {
                    activePage: _vm.page,
                    pages: _vm.paginationVar.total_pages
                  },
                  on: {
                    "update:activePage": function($event) {
                      _vm.page = $event
                    },
                    "update:active-page": function($event) {
                      _vm.page = $event
                    }
                  }
                })
              ],
              1
            )
          ]
        ],
        2
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/pages/PopulationByAgeCounty.vue?vue&type=template&id=1f4943ab&":
/*!*************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/pages/PopulationByAgeCounty.vue?vue&type=template&id=1f4943ab& ***!
  \*************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "CCard",
    [
      _c(
        "CCardBody",
        [
          _c("CDataTable", {
            attrs: {
              "items-per-page-select": "",
              hover: "",
              striped: "",
              sorter: "",
              items: _vm.items,
              fields: _vm.fields,
              "items-per-page": 15,
              "active-page": _vm.activePage,
              pagination: { doubleArrows: false },
              loading: _vm.loading
            },
            scopedSlots: _vm._u([
              {
                key: "status",
                fn: function(data) {
                  return [
                    _c("td", [_vm._v(_vm._s(data.item.county))]),
                    _vm._v(" "),
                    _c("td", [_vm._v(_vm._s(data.item.age_category))]),
                    _vm._v(" "),
                    _c("td", [_vm._v(_vm._s(data.item.total_population))])
                  ]
                }
              }
            ])
          }),
          _vm._v(" "),
          [
            _c(
              "div",
              [
                _c("input", {
                  attrs: { type: "hidden", id: "page_county_num" },
                  domProps: { value: _vm.page }
                }),
                _vm._v(" "),
                _c("CPagination", {
                  attrs: {
                    activePage: _vm.page,
                    pages: _vm.paginationVar.total_pages
                  },
                  on: {
                    "update:activePage": function($event) {
                      _vm.page = $event
                    },
                    "update:active-page": function($event) {
                      _vm.page = $event
                    }
                  }
                })
              ],
              1
            )
          ]
        ],
        2
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/pages/PopulationByAgeState.vue?vue&type=template&id=237714c0&":
/*!************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/pages/PopulationByAgeState.vue?vue&type=template&id=237714c0& ***!
  \************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "CCard",
    [
      _c(
        "CCardBody",
        [
          _c("CDataTable", {
            attrs: {
              "items-per-page-select": "",
              hover: "",
              striped: "",
              sorter: "",
              items: _vm.items,
              fields: _vm.fields,
              "items-per-page": 15,
              "active-page": _vm.activePage,
              pagination: { doubleArrows: false },
              loading: _vm.loading
            },
            scopedSlots: _vm._u([
              {
                key: "status",
                fn: function(data) {
                  return [
                    _c("td", [_vm._v(_vm._s(data.item.state))]),
                    _vm._v(" "),
                    _c("td", [_vm._v(_vm._s(data.item.age_category))]),
                    _vm._v(" "),
                    _c("td", [_vm._v(_vm._s(data.item.total_population))])
                  ]
                }
              }
            ])
          }),
          _vm._v(" "),
          [
            _c(
              "div",
              [
                _c("input", {
                  attrs: { type: "hidden", id: "page_num" },
                  domProps: { value: _vm.page }
                }),
                _vm._v(" "),
                _c("CPagination", {
                  attrs: {
                    activePage: _vm.page,
                    pages: _vm.paginationVar.total_pages
                  },
                  on: {
                    "update:activePage": function($event) {
                      _vm.page = $event
                    },
                    "update:active-page": function($event) {
                      _vm.page = $event
                    }
                  }
                })
              ],
              1
            )
          ]
        ],
        2
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/common/mixin/DataTableMixin.js":
/*!*****************************************************!*\
  !*** ./resources/js/common/mixin/DataTableMixin.js ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      query: {
        filter: null,
        sorting: null,
        search: null,
        searchBy: null,
        pagination: {
          currentPage: 1,
          limit: 20 //limit is the same as perPage - variable:  this.query.pagination.limit

        }
      },
      config: {
        pagination: {
          lastPage: '',
          currentPage: 1,
          pages: 1,
          total: 0,
          perPage: 20,
          //perPage is the same as limit - variable:  this.config.pagination.perPage
          nextPageUrl: null,
          prevPageUrl: null,
          from: 0,
          to: 0
        }
      }
    };
  },
  computed: {
    paginationFrom: function paginationFrom() {
      return (this.config.pagination.currentPage - 1) * this.config.pagination.perPage + 1;
    },
    paginationPages: function paginationPages() {
      return Math.ceil(this.config.pagination.total / this.config.pagination.perPage);
    },
    paginationTo: function paginationTo() {
      var to = this.config.pagination.from * this.config.pagination.perPage;
      return to <= this.config.pagination.total ? to : this.config.pagination.total;
    }
  },
  methods: {
    alternateBgColor: function alternateBgColor(index) {
      return index % 2 == 0 ? 'even' : 'odd';
    },
    setPagination: function setPagination(data) {
      if (data) {
        this.config.pagination.lastPage = data.last_page;
        this.config.pagination.currentPage = data.current_page;
        this.config.pagination.total = data.total;
        this.config.pagination.perPage = data.per_page; //perPage is the same as limit

        this.config.pagination.nextPageUrl = data.links.next;
        this.config.pagination.prevPageUrl = data.links.previous;
        this.config.pagination.from = this.paginationFrom;
        this.config.pagination.to = this.paginationTo;
        this.config.pagination.pages = this.paginationPages;
        this.query.pagination.currentPage = data.current_page;
      }
    },
    setFilter: function setFilter(filters) {
      this.query.filter = filters;
    },
    setSorting: function setSorting(orderBy) {
      this.query.sorting = orderBy;
    },
    setSearch: function setSearch(searches) {
      this.query.search = searches;
    },
    setSearchBy: function setSearchBy(searchBy) {
      this.query.searchBy = searchBy;
    },
    setPaginationLimit: function setPaginationLimit(limit) {
      //limit is the same as perPage
      this.query.pagination.limit = limit;
      this.query.pagination.currentPage = Math.ceil(this.config.pagination.total / limit) < this.query.pagination.currentPage ? Math.ceil(this.config.pagination.total / limit) : this.query.pagination.currentPage;
    },
    getParams: function getParams() {
      var params = {
        take: this.query.pagination.limit,
        page: this.query.pagination.currentPage
      }; // see if we have sorting

      if (this.query.sorting) {
        params['orderBy'] = this.query.sorting;
      } // see if we have filters


      if (this.query.filter) {
        params['filters'] = this.query.filter;
      } // see if we have searches


      if (this.query.search) {
        params['search'] = this.query.search;
      } // see if we have search by options


      if (this.query.searchBy) {
        params['searchBy'] = this.query.searchBy;
      }

      return params;
    },
    gotoPage: function gotoPage(page) {
      if (page == 'prev') {
        // if previous page
        this.query.pagination.currentPage -= 1;
      } else if (page == 'next') {
        // if next page
        this.query.pagination.currentPage += 1;
      } else {
        // otherwise, go to page as specicifed
        this.query.pagination.currentPage = page;
      }
    },

    /**
     * Insert a div with width=100%, height with specified after the element that being represented by the selector
     * It's like, insert a new div that element (selector) with width=100% and height specified.
     * @param selector
     * @param height
     */
    insertSpacer: function insertSpacer(selector, height) {
      var _arguments = arguments;
      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var className, $, $newDiv;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                className = _arguments.length > 2 && _arguments[2] !== undefined ? _arguments[2] : 'spacer';
                $ = jQuery;
                $newDiv = $("<div>", {
                  "class": className
                });
                $newDiv.css({
                  width: '100%',
                  height: height
                });
                $(selector).after($newDiv);

              case 5:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }))();
    },
    freezeElement: function freezeElement(selector) {
      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        var $;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                $ = jQuery;
                $(selector).css({
                  position: 'fixed'
                });

              case 2:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }))();
    },
    insertStyle: function insertStyle(selector, css) {
      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                $(selector).css(css);

              case 1:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3);
      }))();
    }
  }
});

/***/ }),

/***/ "./resources/js/views/pages/PopulationByAge.vue":
/*!******************************************************!*\
  !*** ./resources/js/views/pages/PopulationByAge.vue ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _PopulationByAge_vue_vue_type_template_id_95cf83fe___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./PopulationByAge.vue?vue&type=template&id=95cf83fe& */ "./resources/js/views/pages/PopulationByAge.vue?vue&type=template&id=95cf83fe&");
/* harmony import */ var _PopulationByAge_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./PopulationByAge.vue?vue&type=script&lang=js& */ "./resources/js/views/pages/PopulationByAge.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _PopulationByAge_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _PopulationByAge_vue_vue_type_template_id_95cf83fe___WEBPACK_IMPORTED_MODULE_0__["render"],
  _PopulationByAge_vue_vue_type_template_id_95cf83fe___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/pages/PopulationByAge.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/pages/PopulationByAge.vue?vue&type=script&lang=js&":
/*!*******************************************************************************!*\
  !*** ./resources/js/views/pages/PopulationByAge.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PopulationByAge_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./PopulationByAge.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/pages/PopulationByAge.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PopulationByAge_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/pages/PopulationByAge.vue?vue&type=template&id=95cf83fe&":
/*!*************************************************************************************!*\
  !*** ./resources/js/views/pages/PopulationByAge.vue?vue&type=template&id=95cf83fe& ***!
  \*************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PopulationByAge_vue_vue_type_template_id_95cf83fe___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./PopulationByAge.vue?vue&type=template&id=95cf83fe& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/pages/PopulationByAge.vue?vue&type=template&id=95cf83fe&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PopulationByAge_vue_vue_type_template_id_95cf83fe___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PopulationByAge_vue_vue_type_template_id_95cf83fe___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/views/pages/PopulationByAgeCity.vue":
/*!**********************************************************!*\
  !*** ./resources/js/views/pages/PopulationByAgeCity.vue ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _PopulationByAgeCity_vue_vue_type_template_id_afca9ce8___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./PopulationByAgeCity.vue?vue&type=template&id=afca9ce8& */ "./resources/js/views/pages/PopulationByAgeCity.vue?vue&type=template&id=afca9ce8&");
/* harmony import */ var _PopulationByAgeCity_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./PopulationByAgeCity.vue?vue&type=script&lang=js& */ "./resources/js/views/pages/PopulationByAgeCity.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _PopulationByAgeCity_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _PopulationByAgeCity_vue_vue_type_template_id_afca9ce8___WEBPACK_IMPORTED_MODULE_0__["render"],
  _PopulationByAgeCity_vue_vue_type_template_id_afca9ce8___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/pages/PopulationByAgeCity.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/pages/PopulationByAgeCity.vue?vue&type=script&lang=js&":
/*!***********************************************************************************!*\
  !*** ./resources/js/views/pages/PopulationByAgeCity.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PopulationByAgeCity_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./PopulationByAgeCity.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/pages/PopulationByAgeCity.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PopulationByAgeCity_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/pages/PopulationByAgeCity.vue?vue&type=template&id=afca9ce8&":
/*!*****************************************************************************************!*\
  !*** ./resources/js/views/pages/PopulationByAgeCity.vue?vue&type=template&id=afca9ce8& ***!
  \*****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PopulationByAgeCity_vue_vue_type_template_id_afca9ce8___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./PopulationByAgeCity.vue?vue&type=template&id=afca9ce8& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/pages/PopulationByAgeCity.vue?vue&type=template&id=afca9ce8&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PopulationByAgeCity_vue_vue_type_template_id_afca9ce8___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PopulationByAgeCity_vue_vue_type_template_id_afca9ce8___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/views/pages/PopulationByAgeCounty.vue":
/*!************************************************************!*\
  !*** ./resources/js/views/pages/PopulationByAgeCounty.vue ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _PopulationByAgeCounty_vue_vue_type_template_id_1f4943ab___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./PopulationByAgeCounty.vue?vue&type=template&id=1f4943ab& */ "./resources/js/views/pages/PopulationByAgeCounty.vue?vue&type=template&id=1f4943ab&");
/* harmony import */ var _PopulationByAgeCounty_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./PopulationByAgeCounty.vue?vue&type=script&lang=js& */ "./resources/js/views/pages/PopulationByAgeCounty.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _PopulationByAgeCounty_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _PopulationByAgeCounty_vue_vue_type_template_id_1f4943ab___WEBPACK_IMPORTED_MODULE_0__["render"],
  _PopulationByAgeCounty_vue_vue_type_template_id_1f4943ab___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/pages/PopulationByAgeCounty.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/pages/PopulationByAgeCounty.vue?vue&type=script&lang=js&":
/*!*************************************************************************************!*\
  !*** ./resources/js/views/pages/PopulationByAgeCounty.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PopulationByAgeCounty_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./PopulationByAgeCounty.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/pages/PopulationByAgeCounty.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PopulationByAgeCounty_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/pages/PopulationByAgeCounty.vue?vue&type=template&id=1f4943ab&":
/*!*******************************************************************************************!*\
  !*** ./resources/js/views/pages/PopulationByAgeCounty.vue?vue&type=template&id=1f4943ab& ***!
  \*******************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PopulationByAgeCounty_vue_vue_type_template_id_1f4943ab___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./PopulationByAgeCounty.vue?vue&type=template&id=1f4943ab& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/pages/PopulationByAgeCounty.vue?vue&type=template&id=1f4943ab&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PopulationByAgeCounty_vue_vue_type_template_id_1f4943ab___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PopulationByAgeCounty_vue_vue_type_template_id_1f4943ab___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/views/pages/PopulationByAgeState.vue":
/*!***********************************************************!*\
  !*** ./resources/js/views/pages/PopulationByAgeState.vue ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _PopulationByAgeState_vue_vue_type_template_id_237714c0___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./PopulationByAgeState.vue?vue&type=template&id=237714c0& */ "./resources/js/views/pages/PopulationByAgeState.vue?vue&type=template&id=237714c0&");
/* harmony import */ var _PopulationByAgeState_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./PopulationByAgeState.vue?vue&type=script&lang=js& */ "./resources/js/views/pages/PopulationByAgeState.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _PopulationByAgeState_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _PopulationByAgeState_vue_vue_type_template_id_237714c0___WEBPACK_IMPORTED_MODULE_0__["render"],
  _PopulationByAgeState_vue_vue_type_template_id_237714c0___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/pages/PopulationByAgeState.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/pages/PopulationByAgeState.vue?vue&type=script&lang=js&":
/*!************************************************************************************!*\
  !*** ./resources/js/views/pages/PopulationByAgeState.vue?vue&type=script&lang=js& ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PopulationByAgeState_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./PopulationByAgeState.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/pages/PopulationByAgeState.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PopulationByAgeState_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/pages/PopulationByAgeState.vue?vue&type=template&id=237714c0&":
/*!******************************************************************************************!*\
  !*** ./resources/js/views/pages/PopulationByAgeState.vue?vue&type=template&id=237714c0& ***!
  \******************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PopulationByAgeState_vue_vue_type_template_id_237714c0___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./PopulationByAgeState.vue?vue&type=template&id=237714c0& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/pages/PopulationByAgeState.vue?vue&type=template&id=237714c0&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PopulationByAgeState_vue_vue_type_template_id_237714c0___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PopulationByAgeState_vue_vue_type_template_id_237714c0___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);