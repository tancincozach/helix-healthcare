(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[19],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/pages/PopulationByCity.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/pages/PopulationByCity.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _common_mixin_DataTableMixin__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/common/mixin/DataTableMixin */ "./resources/js/common/mixin/DataTableMixin.js");
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");


function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'PopByCity',
  data: function data() {
    var sortOrder = {},
        sortKey = 'city',
        columns = [{
      key: 'city',
      label: 'Name',
      _classes: 'font-weight-bold',
      sortKey: 'city'
    }, {
      key: 'state',
      label: 'State'
    }, {
      key: 'population',
      label: 'Population',
      classes: 'font-weight-bold'
    }, {
      key: 'acsYear',
      label: 'American Survey Year'
    }];
    columns.forEach(function (col) {
      sortOrder[col.sortKey] = 'asc';
    });
    return {
      page: 1,
      pagesLinks: [],
      loading: false,
      items: [],
      fields: columns,
      caption: 'Population By City',
      hover: true,
      striped: true,
      bordered: true,
      dark: false,
      sortKey: sortKey,
      sortOrder: sortOrder,
      paginationVar: {},
      total_pages: 0,
      open: false,
      data: {
        columns: columns,
        rows: []
      },
      states: [],
      activePage: 1
    };
  },
  created: function created() {
    var _this2 = this;

    return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
      var params;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _this2.setSorting("".concat(_this2.sortKey, "|").concat(_this2.sortOrder[_this2.sortKey]));

              _this2.setPaginationLimit(25);

              _this2.loading = true;
              _context.next = 5;
              return _this2.getPopByCities({
                query: _.merge(_this2.getParams())
              });

            case 5:
              _this2.loading = false;
              _this2.items = _this2.city_info;
              _this2.paginationVar = _this2.meta.pagination;

              _this2.setPagination(_this2.meta.pagination);

              params = _this2.getParams();

              _this2.addClickEventOnPagination();

              _this2.addEventOnDataTableInputElement;

            case 12:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }))();
  },
  mounted: function mounted() {},
  computed: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_2__["mapGetters"])({
    meta: 'pop_by_cities/meta',
    city_info: 'pop_by_cities/data'
  })),
  methods: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_2__["mapActions"])({
    getPopByCities: 'pop_by_cities/getPopCities'
  }), {
    addClickEventOnPagination: function addClickEventOnPagination() {
      var _this = this;

      var pages = document.getElementsByClassName("page-item");
      Array.from(pages).forEach(function (element) {
        _this.addClickEvent(element);
      });
    },
    addEventOnDataTableInputElement: function addEventOnDataTableInputElement() {
      var _this = this,
          inputElements = document.getElementsByClassName("form-control"),
          currParams = this.getParams();

      _.map(inputElements, function (element) {
        switch (element.type) {
          case 'text':
            element.removeEventListener('change');
            element.addEventListener('change', function () {
              currParams.search = element.value;

              _this.changeSearchAndLimit(params);
            });
            break;

          case 'select-one':
            element.addEventListener('change', function () {
              currParams.take = element.value;

              _this.changeSearchAndLimit(currParams);
            });
            break;
        }
      });
    },
    changePage: function changePage() {
      var _this3 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        var params;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                params = _this3.getParams();
                params.page = document.getElementById('page_num').value;
                _this3.loading = true;
                _context2.next = 5;
                return _this3.getPopByCities({
                  query: _.merge(params)
                });

              case 5:
                _this3.loading = false;
                _this3.items = _this3.city_info;
                _this3.paginationVar = _this3.meta.pagination;

                _this3.setPagination(_this3.meta.pagination);

              case 9:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }))();
    },
    changeSearchAndLimit: function changeSearchAndLimit() {
      var _arguments = arguments,
          _this4 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3() {
        var params, currParams;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                params = _arguments.length > 0 && _arguments[0] !== undefined ? _arguments[0] : {};
                currParams = _this4.getParams();

                if (!_.isEmptyObject(params.search)) {
                  currParams.search = params.search;
                }

                if (!_.isEmptyObject(params.take)) {
                  currParams.take = params.take;
                }

                _this4.loading = true;
                _context3.next = 7;
                return _this4.getPopByCities({
                  query: _.merge(currParams)
                });

              case 7:
                _this4.loading = false;
                _this4.items = _this4.city_info;
                _this4.paginationVar = _this4.meta.pagination;

                _this4.setPagination(_this4.meta.pagination);

              case 11:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3);
      }))();
    },
    addClickEvent: function addClickEvent(element) {
      var _this = this;

      element.addEventListener('click', function () {
        _this.changePage();
      });
    }
  }),
  mixins: [_common_mixin_DataTableMixin__WEBPACK_IMPORTED_MODULE_1__["default"]]
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/pages/PopulationByCity.vue?vue&type=template&id=451731d9&":
/*!********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/pages/PopulationByCity.vue?vue&type=template&id=451731d9& ***!
  \********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "CCard",
    [
      _c(
        "CCardHeader",
        [
          _vm._t("header", [
            _c("CIcon", { attrs: { name: "cil-grid" } }),
            _vm._v(" " + _vm._s(_vm.caption) + "\n    ")
          ])
        ],
        2
      ),
      _vm._v(" "),
      _c(
        "CCardBody",
        [
          _c("CDataTable", {
            attrs: {
              pagination: false,
              "table-filter": "",
              "items-per-page-select": "",
              hover: "",
              striped: "",
              sorter: "",
              items: _vm.items,
              fields: _vm.fields,
              "items-per-page": 25,
              "active-page": _vm.activePage,
              loading: _vm.loading
            },
            scopedSlots: _vm._u([
              {
                key: "status",
                fn: function(data) {
                  return [
                    _c("td", [_vm._v(_vm._s(data.item.city))]),
                    _vm._v(" "),
                    _c("td", [_vm._v(_vm._s(data.item.state))]),
                    _vm._v(" "),
                    _c("td", [_vm._v(_vm._s(data.item.population))]),
                    _vm._v(" "),
                    _c("td", [_vm._v(_vm._s(data.item.acsYear))])
                  ]
                }
              }
            ])
          }),
          _vm._v(" "),
          [
            _c(
              "div",
              [
                _c("input", {
                  attrs: { type: "hidden", id: "page_num" },
                  domProps: { value: _vm.page }
                }),
                _vm._v(" "),
                _c("CPagination", {
                  attrs: {
                    activePage: _vm.page,
                    pages: _vm.paginationVar.total_pages
                  },
                  on: {
                    "update:activePage": function($event) {
                      _vm.page = $event
                    },
                    "update:active-page": function($event) {
                      _vm.page = $event
                    }
                  }
                })
              ],
              1
            )
          ]
        ],
        2
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/common/mixin/DataTableMixin.js":
/*!*****************************************************!*\
  !*** ./resources/js/common/mixin/DataTableMixin.js ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      query: {
        filter: null,
        sorting: null,
        search: null,
        searchBy: null,
        pagination: {
          currentPage: 1,
          limit: 20 //limit is the same as perPage - variable:  this.query.pagination.limit

        }
      },
      config: {
        pagination: {
          lastPage: '',
          currentPage: 1,
          pages: 1,
          total: 0,
          perPage: 20,
          //perPage is the same as limit - variable:  this.config.pagination.perPage
          nextPageUrl: null,
          prevPageUrl: null,
          from: 0,
          to: 0
        }
      }
    };
  },
  computed: {
    paginationFrom: function paginationFrom() {
      return (this.config.pagination.currentPage - 1) * this.config.pagination.perPage + 1;
    },
    paginationPages: function paginationPages() {
      return Math.ceil(this.config.pagination.total / this.config.pagination.perPage);
    },
    paginationTo: function paginationTo() {
      var to = this.config.pagination.from * this.config.pagination.perPage;
      return to <= this.config.pagination.total ? to : this.config.pagination.total;
    }
  },
  methods: {
    alternateBgColor: function alternateBgColor(index) {
      return index % 2 == 0 ? 'even' : 'odd';
    },
    setPagination: function setPagination(data) {
      if (data) {
        this.config.pagination.lastPage = data.last_page;
        this.config.pagination.currentPage = data.current_page;
        this.config.pagination.total = data.total;
        this.config.pagination.perPage = data.per_page; //perPage is the same as limit

        this.config.pagination.nextPageUrl = data.links.next;
        this.config.pagination.prevPageUrl = data.links.previous;
        this.config.pagination.from = this.paginationFrom;
        this.config.pagination.to = this.paginationTo;
        this.config.pagination.pages = this.paginationPages;
        this.query.pagination.currentPage = data.current_page;
      }
    },
    setFilter: function setFilter(filters) {
      this.query.filter = filters;
    },
    setSorting: function setSorting(orderBy) {
      this.query.sorting = orderBy;
    },
    setSearch: function setSearch(searches) {
      this.query.search = searches;
    },
    setSearchBy: function setSearchBy(searchBy) {
      this.query.searchBy = searchBy;
    },
    setPaginationLimit: function setPaginationLimit(limit) {
      //limit is the same as perPage
      this.query.pagination.limit = limit;
      this.query.pagination.currentPage = Math.ceil(this.config.pagination.total / limit) < this.query.pagination.currentPage ? Math.ceil(this.config.pagination.total / limit) : this.query.pagination.currentPage;
    },
    getParams: function getParams() {
      var params = {
        take: this.query.pagination.limit,
        page: this.query.pagination.currentPage
      }; // see if we have sorting

      if (this.query.sorting) {
        params['orderBy'] = this.query.sorting;
      } // see if we have filters


      if (this.query.filter) {
        params['filters'] = this.query.filter;
      } // see if we have searches


      if (this.query.search) {
        params['search'] = this.query.search;
      } // see if we have search by options


      if (this.query.searchBy) {
        params['searchBy'] = this.query.searchBy;
      }

      return params;
    },
    gotoPage: function gotoPage(page) {
      if (page == 'prev') {
        // if previous page
        this.query.pagination.currentPage -= 1;
      } else if (page == 'next') {
        // if next page
        this.query.pagination.currentPage += 1;
      } else {
        // otherwise, go to page as specicifed
        this.query.pagination.currentPage = page;
      }
    },

    /**
     * Insert a div with width=100%, height with specified after the element that being represented by the selector
     * It's like, insert a new div that element (selector) with width=100% and height specified.
     * @param selector
     * @param height
     */
    insertSpacer: function insertSpacer(selector, height) {
      var _arguments = arguments;
      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var className, $, $newDiv;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                className = _arguments.length > 2 && _arguments[2] !== undefined ? _arguments[2] : 'spacer';
                $ = jQuery;
                $newDiv = $("<div>", {
                  "class": className
                });
                $newDiv.css({
                  width: '100%',
                  height: height
                });
                $(selector).after($newDiv);

              case 5:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }))();
    },
    freezeElement: function freezeElement(selector) {
      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        var $;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                $ = jQuery;
                $(selector).css({
                  position: 'fixed'
                });

              case 2:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }))();
    },
    insertStyle: function insertStyle(selector, css) {
      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                $(selector).css(css);

              case 1:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3);
      }))();
    }
  }
});

/***/ }),

/***/ "./resources/js/views/pages/PopulationByCity.vue":
/*!*******************************************************!*\
  !*** ./resources/js/views/pages/PopulationByCity.vue ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _PopulationByCity_vue_vue_type_template_id_451731d9___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./PopulationByCity.vue?vue&type=template&id=451731d9& */ "./resources/js/views/pages/PopulationByCity.vue?vue&type=template&id=451731d9&");
/* harmony import */ var _PopulationByCity_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./PopulationByCity.vue?vue&type=script&lang=js& */ "./resources/js/views/pages/PopulationByCity.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _PopulationByCity_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _PopulationByCity_vue_vue_type_template_id_451731d9___WEBPACK_IMPORTED_MODULE_0__["render"],
  _PopulationByCity_vue_vue_type_template_id_451731d9___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/pages/PopulationByCity.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/pages/PopulationByCity.vue?vue&type=script&lang=js&":
/*!********************************************************************************!*\
  !*** ./resources/js/views/pages/PopulationByCity.vue?vue&type=script&lang=js& ***!
  \********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PopulationByCity_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./PopulationByCity.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/pages/PopulationByCity.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PopulationByCity_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/pages/PopulationByCity.vue?vue&type=template&id=451731d9&":
/*!**************************************************************************************!*\
  !*** ./resources/js/views/pages/PopulationByCity.vue?vue&type=template&id=451731d9& ***!
  \**************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PopulationByCity_vue_vue_type_template_id_451731d9___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./PopulationByCity.vue?vue&type=template&id=451731d9& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/pages/PopulationByCity.vue?vue&type=template&id=451731d9&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PopulationByCity_vue_vue_type_template_id_451731d9___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PopulationByCity_vue_vue_type_template_id_451731d9___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);